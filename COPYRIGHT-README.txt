The EUPL licence applies to all software in this project containing the 
notice below. The text of the licence can be found in file COPYING.pdf 
or COPYING.txt in this directory or at http://ec.europa.eu/idabc/eupl5.

/* * Copyright 2011-2016 Universiteit Antwerpen** Licensed under the EUPL, Version 1.1 or  as soon they* will be approved by the European Commission - subsequent* versions of the EUPL (the "Licence");* You may not use this work except in compliance with the* Licence.* You may obtain a copy of the Licence at:** http://ec.europa.eu/idabc/eupl5** Unless required by applicable law or agreed to in* writing, software distributed under the Licence is* distributed on an "AS IS" basis,* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either* express or implied.* See the Licence for the specific language governing* permissions and limitations under the Licence.*/

The EUPL license does not apply to the third party software used in the 
project. The original license  applies for that software. Please consider 
all other copyright mentions that are present in thesoftware components 
used in this application.

The sources in src/main/resources/lib/gtest are the well-known Google Test
framework distributed by Google Inc. Their license, reproduced in 
LICENSE_GTEST.txt in this directory applies to these sources.

The sources in src/main/resources/lib/tclap constitute the TCLAP library.
TCLAP is a small, flexible library that provides a simple interface for 
defining and accessing command line arguments (see tclap.sourceforge.net).
It is licensed under the MIT License for worry free distribution, reproduced
in LICENSE_TCLAP.txt in this directory.

Some of the icons used in the graphical user interface are taken from the
Tango Icon Library, see http://tango.freedesktop.org/Tango_Icon_Library.
Their license, reproduced in LICENSE_TANGO.txt in this directory applies to
those materials.