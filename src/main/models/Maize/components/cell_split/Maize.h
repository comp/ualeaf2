#ifndef MAIZE_CELL_SPLIT_MAIZECS_H_INCLUDED
#define MAIZE_CELL_SPLIT_MAIZECS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Maize model.
 */

#include "sim/CoreData.h"

#include <array>
#include <tuple>

namespace SimPT_Sim { class Cell; }

namespace SimPT_Maize {
namespace CellSplit {

using namespace SimPT_Sim;

/**
 * CellSplit for Maize model.
 */
class Maize
{
public:
        /// Initializing constructor.
        Maize(const CoreData& cd);

        /// Initialize or re-initialize.
        void Initialize(const CoreData& cd);

        /// Execute.
        std::tuple<bool, bool, std::array<double, 3>> operator()(Cell* cell);

private:
        CoreData     m_cd;                    ///< Core data (mesh, params, sim_time,...).
        double       m_cell_base_area;        ///<
        double       m_div_area;              ///<
        double       m_division_ratio;        ///<
        bool         m_fixed_division_axis;   ///<
};

} // namespace
} // namespace

#endif // include guard
