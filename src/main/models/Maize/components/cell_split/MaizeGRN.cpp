/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for MaizeGRN model.
 */

#include "MaizeGRN.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "math/RandomEngine.h"

#include <boost/property_tree/ptree.hpp>
#include <trng/uniform01_dist.hpp>

namespace SimPT_Maize {
namespace CellSplit {

using namespace std;
using boost::property_tree::ptree;

MaizeGRN::MaizeGRN(const CoreData& cd)
{
        Initialize(cd);
}

void MaizeGRN::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters;

        const trng::uniform01_dist<double> dist;
        m_uniform_generator = m_cd.m_random_engine->GetGenerator(dist);

        m_CDK_threshold	      	   = p->get<double>("maizegrn.CDK_threshold");
        m_cell_base_area       	   = p->get<double>("cell_mechanics.base_area");
        m_cell_division_noise  	   = p->get<double>("maizegrn.cell_division_noise");
        m_cell_division_threshold  = p->get<double>("maizegrn.cell_division_threshold");
        m_division_ratio           = p->get<double>("cell_mechanics.division_ratio");
        m_fixed_division_axis      = p->get<bool>("cell_mechanics.fixed_division_axis", false);
        m_div_area                 = m_division_ratio * m_cell_base_area;
}

std::tuple<bool, bool, std::array<double, 3>> MaizeGRN::operator()(Cell* cell)
{
        double time_now = m_cd.m_time_data->m_sim_time;
        const double a_area = cell->GetArea();
        bool must_divide = false;

        //	    double CCnoise2 = 1 + (m_uniform_generator() - 0.5) / 5;
        double CCnoise2 = 1 + m_cell_division_noise * (m_uniform_generator() - 0.5);

        if (time_now <= 300) {
                if ((cell->GetCentroid()[1]) > (128. - 80) && cell->GetCellType() != 0
                        && a_area >= (m_cell_division_threshold/* CCnoise2*/)) {
                        must_divide = true;
                }
        } else {
                if (((cell->GetChemical(9) / cell->GetArea()) > m_CDK_threshold /*|| (c->Chemical(6) / c->Area()) > 0.5  */)
                        && cell->GetArea() >= (m_cell_division_threshold * CCnoise2)) {
                        must_divide = true;
                }
        }

        return std::make_tuple(must_divide, true, array<double, 3> { { 1.0, 0.0, 0.0 } });

}

} // namespace
} // namespace
