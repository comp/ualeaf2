/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for WortelLight model.
 */

#include "WortelLight.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "math/RandomEngine.h"

#include <boost/property_tree/ptree.hpp>
#include <trng/uniform01_dist.hpp>

namespace SimPT_Maize {
namespace CellSplit {

using namespace std;
using boost::property_tree::ptree;

WortelLight::WortelLight(const CoreData& cd)
{
        Initialize(cd);
}

void WortelLight::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        const trng::uniform01_dist<double> dist;
        m_uniform_generator = m_cd.m_random_engine->GetGenerator(dist);

        m_cell_base_area     	= p->get<double>("cell_mechanics.base_area");
        m_division_ratio       	= p->get<double>("cell_mechanics.division_ratio");
        m_fixed_division_axis 	= p->get<bool>("cell_mechanics.fixed_division_axis", false);
        m_cell_division_noise	= p->get<double>("wortellight.cell_division_noise");
        m_ga_threshold		= p->get<double>("wortel.ga_threshold");
        m_shy2_threshold	= p->get<double>("wortel.shy2_threshold");
}

std::tuple<bool, bool, std::array<double, 3>> WortelLight::operator()(Cell* cell)
{
        const double chem2 = cell->GetChemical(2);
        const double chem3 = cell->GetChemical(3);
        //const int celltype	= cell->GetCellType();
        double a_area = cell->GetArea();
        bool must_divide = false;

        double time_now = m_cd.m_time_data->m_sim_time;
        double time_start = 0; //DDV: How fetch from xml file?

        if (cell->GetBoundaryType() == BoundaryType::None) {
                double CCnoise = 1 + m_cell_division_noise * (m_uniform_generator() - 0.5);

                if (cell->GetIndex() > 15) {
                        if (((chem2 / a_area) < m_shy2_threshold) && ((chem3 / a_area) >= m_ga_threshold)) {
                                if ( /*( celltype > 2 && celltype < 8 ) &&*/(a_area >= (100. * CCnoise))
                                        && ((time_now - time_start) >= 10.)) {
                                        must_divide = true;
                                }
                                //				if ( celltype != 0 && ( celltype > 7 || celltype < 3 ) && ( a_area >= ( 400.* CCnoise) )
                                //					&& ( ( time_now - time_start ) >= 10.) ) {
                                //					must_divide = true;
                                //				}
                        }
                }
        }

        return std::make_tuple(must_divide, true, array<double, 3> { { 1.0, 0.0, 0.0 } });
}

} // namespace
} // namespace
