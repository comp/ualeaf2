/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Maize model.
 */


#include "Maize.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/CellAttributes.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellSplit {

using namespace std;
using boost::property_tree::ptree;

Maize::Maize(const CoreData& cd)
{
        Initialize(cd);
}

void Maize::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_cell_base_area = p->get<double>("cell_mechanics.base_area");
        m_division_ratio = p->get<double>("cell_mechanics.division_ratio");
        m_fixed_division_axis = p->get<bool>("cell_mechanics.fixed_division_axis", false);
        m_div_area = m_division_ratio * m_cell_base_area;
}

std::tuple<bool, bool, std::array<double, 3>> Maize::operator()(Cell* cell)
{
        const double a_area = cell->GetArea();
        bool must_divide = false;

        if (cell->GetBoundaryType() == BoundaryType::None) {

                /*		if ( ( chem1 / a_area ) >= 5. ) {
				 if ( a_area >= 400.) {
				 must_divide = true;
				 }
				 }

				 //
                 */
                //    	if (  (c->Chemical(1) / c->Area() ) >= /*0.15*/50. )
                //    		c->DivideOverAxis(Vector(1,0,0));
                //    	{
                //    		if (c->Area() > par.rel_cell_div_threshold * c->BaseArea())
                if ((cell->GetCentroid()[1]) > (128. - 80/*200.*/)
                        && cell->GetCellType() != 0 && a_area >= (200./* CCnoise2100*/)) {
                        must_divide = true;
                        //    	    	if ( c->HasNeighborOfTypeZero() )
                        //		{
                        //    	    		c->SetChemical(2,0.0);
                        //    	    		c->SetChemical(3,0.0);
                        //    	    		c->SetChemical(4,0.0);
                        //    	    		c->SetChemical(5,0.0);
                        //    	    		c->SetChemical(6,0.0);
                        //    	    		c->SetChemical(7,0.0);
                        //    	    		c->SetChemical(8,0.0);
                        //		}
                }
        }

        return std::make_tuple(must_divide, true, array<double, 3> { { 1.0, 0.0, 0.0 } });

}

} // namespace
} // namespace
