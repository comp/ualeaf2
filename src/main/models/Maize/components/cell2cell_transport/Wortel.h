#ifndef MAIZE_CELL_TO_CELL_TRANSPORT_WORTELRT_H_INCLUDED
#define MAIZE_CELL_TO_CELL_TRANSPORT_WORTELRT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

/**
 * @file
 * CellToCellTransport for Wortel model.
 */

#include "sim/CoreData.h"
#include "model/ComponentInterfaces.h"

namespace SimPT_Sim {
class Wall;
}

namespace SimPT_Maize {
namespace CellToCellTransport {

using namespace SimPT_Sim;

/**
 * CellToCellTransport for Wortel model.
 */
class Wortel
{
public:
        /// Initializing constructor.
        Wortel(const CoreData& cd);

        /// Initialize or re-initialize.
        void Initialize(const CoreData& cd);

        /// Execute.
        void operator()(Wall* w, double* dchem_c1, double* dchem_c2);

private:
        CoreData         m_cd;                    ///< Core data (mesh, params, sim_time,...).
        double           m_d;
        double           m_transport;

private:
        CellToCellTransportBoundaryComponent     m_boundary_condition;
        bool                                  m_has_boundary_condition;
};

} // namespace
} // namespace

#endif // end_of_include_guard
