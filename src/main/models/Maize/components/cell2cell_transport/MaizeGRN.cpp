/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

/**
 * @file
 * CellToCellTransport for MaizeGRN.
 */

#include "MaizeGRN.h"

#include "../ComponentFactory.h"
#include "bio/Cell.h"
#include "bio/Wall.h"
#include "model/ComponentTraits.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellToCellTransport {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace boost::property_tree;

MaizeGRN::MaizeGRN(const CoreData& cd)
{
        Initialize(cd);
}

void MaizeGRN::Initialize(const CoreData& cd)
{
        m_cd    = cd;
        const auto& p = m_cd.m_parameters;

        m_apoplast_thickness      = p->get<double>("maizegrn.apoplast_thickness");
        m_chemical_count          = p->get<unsigned int>("model.cell_chemical_count");
        m_transport               = p->get<double>("auxin_transport.transport");

        const ptree& arr_D = p->get_child("auxin_transport.D.value_array");
        unsigned int c = 0;
        for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
                if (c == m_chemical_count) {
                        break;
                }
                m_D.push_back(it->second.get_value<double>());
                c++;
        }
        // Defensive: if fewer than chemical_count parameters were specified, fill up.
        for (unsigned int i = c; i < m_chemical_count; i++) {
                m_D.push_back(0.0);
        }

        m_has_boundary_condition = false;
        if (p->get<std::string>("model." + SimPT_Sim::ComponentTraits<CellToCellTransportBoundaryTag>::Label(),"") != "") {
                m_has_boundary_condition = true;
                m_boundary_condition = ComponentFactory::CreateCellToCellTransportBoundary(cd);
                if (m_boundary_condition == nullptr) {
                        throw runtime_error("MaizeGRN::Initialize> nullptr for reaction_transport boundary codition.");
                }
        }
}

void MaizeGRN::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
        if (!(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon())) {
                //DDV2014: diffusive hormone - CK
                double phi0 = (w->GetLength() / m_apoplast_thickness) * m_D[0]
                        * ((w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()))
                                - (w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()))); //LEVELS!

                dchem_c1[0] += phi0;
                dchem_c2[0] -= phi0;

                //DDV2012:second diffusive hormone - chemical '1'
                double phi1 = (w->GetLength() / m_apoplast_thickness) * m_D[1]
                        * ((w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()))
                                - (w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()))); //LEVELS!

                dchem_c1[1] += phi1;
                dchem_c2[1] -= phi1;

                //	double phi2 = (w->Length() / apoplast_thickness) * 600 * ( ( w->C2()->Chemical(2) / (w->C2()->Area()) )
                //			- ( w->C1()->Chemical(2) / (w->C1()->Area()) ) ); //LEVELS!
                //
                //
                //	dchem_c1[2] += phi2 ; //LEVELS!
                //	dchem_c2[2] -= phi2 ; //LEVELS!
                //
                //DDV14: GA1
                double phi3 = (w->GetLength() / m_apoplast_thickness) * m_D[3]
                        * ((w->GetC2()->GetChemical(3) / (w->GetC2()->GetArea()))
                                - (w->GetC1()->GetChemical(3) / (w->GetC1()->GetArea()))); //LEVELS!

                dchem_c1[3] += phi3;
                dchem_c2[3] -= phi3;

                //KLU
                //
//		double phi2 = (w->GetLength() / apoplast_thickness) * 1000 * ( ( w->GetC2()->GetChemical(2) / (w->GetC2()->GetArea()) )
//				- ( w->GetC1()->GetChemical(2) / (w->GetC1()->GetArea()) ) ); //LEVELS!
//
//
//		dchem_c1[2] += phi2 ; //LEVELS!
//		dchem_c2[2] -= phi2 ; //LEVELS!

                //DDV14: GA8
                double phi4 = (w->GetLength() / m_apoplast_thickness) * m_D[4]
                        * ((w->GetC2()->GetChemical(4) / (w->GetC2()->GetArea()))
                                - (w->GetC1()->GetChemical(4) / (w->GetC1()->GetArea()))); //LEVELS!

                dchem_c1[4] += phi4;
                dchem_c2[4] -= phi4;

                //	double phi5 = (w->Length() / apoplast_thickness) * 600 * ( ( w->C2()->Chemical(5) / (w->C2()->Area()) )
                //			- ( w->C1()->Chemical(5) / (w->C1()->Area()) ) ); //LEVELS!
                //
                //
                //	dchem_c1[5] += phi5 ; //LEVELS!
                //	dchem_c2[5] -= phi5 ; //LEVELS!
                //
                //	double phi6 = (w->Length() / apoplast_thickness) * 60 * ( ( w->C2()->Chemical(6) / (w->C2()->Area()) )
                //			- ( w->C1()->Chemical(6) / (w->C1()->Area()) ) ); //LEVELS!
                //
                //
                //	dchem_c1[6] += phi6 ; //LEVELS!
                //	dchem_c2[6] -= phi6 ; //LEVELS!
                //
                //
                //	double phi7 = (w->Length() / apoplast_thickness) * 600 * ( ( w->C2()->Chemical(7) / (w->C2()->Area()) )
                //			- ( w->C1()->Chemical(7) / (w->C1()->Area()) ) ); //LEVELS!
                //
                //
                //	dchem_c1[7] += phi7 ; //LEVELS!
                //	dchem_c2[7] -= phi7 ; //LEVELS!

        } else {
                if (m_has_boundary_condition)
                        m_boundary_condition(w, dchem_c1, dchem_c2);
        }
}

} // namespace
} // namespace
