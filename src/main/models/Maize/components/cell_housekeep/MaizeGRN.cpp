/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for MaizeGRN model.
 */

#include "MaizeGRN.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellHousekeep  {

using namespace std;
using boost::property_tree::ptree;

MaizeGRN::MaizeGRN(const CoreData& cd)
{
        Initialize(cd);
}

void MaizeGRN::Initialize(const CoreData& cd)
{
        m_cd = cd;

        const auto& p      = m_cd.m_parameters->get_child("maizegrn");
        m_CDK_threshold    = p.get<double>("CDK_threshold");
        m_expansion_DZ     = p.get<double>("expansion_DZ");
        m_expansion_EZ     = p.get<double>("expansion_EZ");
        m_tEZ              = p.get<double>("tEZ");

}

void MaizeGRN::operator()(Cell* cell)
{
        double time_now = m_cd.m_time_data->m_sim_time;
        const double t_area = cell->GetTargetArea();
        const double a_area = cell->GetArea();

        if (time_now <= 300.) {
                if (((cell->GetCentroid()[1])) > (128. - 200.)) {
                        const double incr = 0.2 * a_area; //Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                }
                else if (((cell->GetCentroid()[1])) > (128. - 500/*600.*/))
                {
                        const double incr = 0.5 * a_area; //Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                }
        } else {
                if ((cell->GetChemical(9) / a_area) > m_CDK_threshold /*|| (c->Chemical(6) / c->Area()) > 0.5  */)
                {
                        const double incr = m_expansion_DZ * a_area; //Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                        cell->SetDivisionTime(time_now);
                }
                else if (((time_now - cell->GetDivisionTime()) >= 0 && (time_now - cell->GetDivisionTime()) <= m_tEZ/*160*/)) {
                        const double incr = m_expansion_EZ * a_area; //Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                }
        }
}

} // namespace
} // namespace
