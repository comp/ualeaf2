/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Maize model.
 */

#include "Maize.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellHousekeep {

using namespace std;
using boost::property_tree::ptree;

Maize::Maize(const CoreData& cd)
{
        Initialize(cd);
}

void Maize::Initialize(const CoreData& cd)
{
        m_cd = cd;
}

void Maize::operator()(Cell* cell)
{
        const double t_area = cell->GetTargetArea();
        const double a_area = cell->GetArea();

        if (cell->GetBoundaryType() == BoundaryType::None) {
                /*
				 if ( ( chem1 / a_area ) > 0.5 ) {
				 const double incr		= 2*(0.025/2) * a_area;//This is tuned for 30 minutes time step!
				 const double update_t_area	= t_area + incr;
				 cell->SetTargetArea(update_t_area);
				 }
				 }
                 */
                //
                //    		time_now = m_mesh->GetSimTime();
                //    		if ( c->CellType() != 0 && (( time_now - time_start ) >= 0.) && (c->NumberOfDivisions() > 2) )
                //    		{
                if (((cell->GetCentroid()[1])) > (128. - 200.)) {
                        const double incr = 0.2 * a_area;	//Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                }
                else if (((cell->GetCentroid()[1])) > (128. - 400/*600.*/)) {
                        const double incr = 0.5 * a_area;	//Tuned for 30 minutes time step!
                        const double update_t_area = t_area + incr;
                        cell->SetTargetArea(update_t_area);
                }
        }
}

} // namespace
} // namespace
