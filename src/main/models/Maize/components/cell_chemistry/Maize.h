#ifndef MAIZE_CELL_CHEMISTRY_MAIZE_H_INCLUDED
#define MAIZE_CELL_CHEMISTRY_MAIZE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Maize model.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim { class Cell; }

namespace SimPT_Maize {
namespace CellChemistry {

using namespace SimPT_Sim;

/**
 * Cell chemistry for Maize model.
 */
class Maize
{
public:
	/// Initializing constructor.
	Maize(const CoreData& cd);

	/// Initialize or re-initialization.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* cell, double* dchem);

private:
	//Calculate rate according to Hill cooperative kinetics
	double Hill(double Vm, double Km, double h, double S);

	CoreData    m_cd;              ///< Core data (mesh, params, sim_time,...).
	double      m_aux1prod;        ///<
	double      m_aux_breakdown;   ///<
};

} // namespace
} // namespace

#endif // include guard
