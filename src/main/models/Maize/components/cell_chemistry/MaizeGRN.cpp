/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for MaizeGRN model.
 */

#include "MaizeGRN.h"

#include "bio/Cell.h"

#include <boost/property_tree/detail/ptree_implementation.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
#include <cmath>

namespace SimPT_Maize {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;

MaizeGRN::MaizeGRN(const CoreData& cd)
{
	Initialize(cd);
}

double MaizeGRN::Michaelis(double M1, double J1, double K1, double S1)
{
	double rate = 0;
	return rate = M1 * S1 / ( J1 + K1 );

}

double MaizeGRN::Hill(double Vm, double Km, double h, double S)
{
	double rate = 0;
	return rate = Vm * pow ( S, h ) / ( pow ( S, h ) + pow ( Km, h ) ) ;
}

double MaizeGRN::Goldbeter(double A1, double A2, double A3, double A4)
{
	double rate = 0;
	double BB = A2 - A1 + A3 * A2 + A4 * A1 ;
	return rate = 2.0 * A4 * A1 / ( BB + pow ( ( pow ( BB, 2.0 ) - 4.0 * ( A2 - A1 ) * A4 * A1 ), .5 ) ) ;
}

void MaizeGRN::Initialize(const CoreData& cd)
{
	m_cd = cd;
	const auto& p = m_cd.m_parameters->get_child("auxin_transport");
	const auto& q = m_cd.m_parameters->get_child("maizegrn");

	m_aux1prod = p.get<double>("aux1prod");
	m_aux_breakdown = p.get<double>("aux_breakdown");
	m_ksCK = q.get<double>("ksCK");
	m_ksCKp = q.get<double>("ksCKp");
	m_kdCK = q.get<double>("kdCK");
	m_ksAUX = q.get<double>("ksAUX");
	m_ksAUXp = q.get<double>("ksAUXp");
	m_kdAUX = q.get<double>("kdAUX");
	m_kdAUXp = q.get<double>("kdAUXp");
	m_tKLUH = q.get<double>("tKLUH");
	m_ksKLUH = q.get<double>("ksKLUH");
	m_kdKLUH = q.get<double>("kdKLUH");
	m_kdKLUHp = q.get<double>("kdKLUHp");
	m_kdKLUHpp = q.get<double>("kdKLUHpp");
	m_kdGA1 = q.get<double>("kdGA1");
	m_kGA203OX = q.get<double>("kGA203OX");
	m_kGA2OX = q.get<double>("kGA2OX");
	m_KmGA1 = q.get<double>("KmGA1");
	m_kdGA8 = q.get<double>("kdGA8");
	m_ksDELLA = q.get<double>("ksDELLA");
	m_kdDELLA = q.get<double>("kdDELLA");
	m_kdDELLAp = q.get<double>("kdDELLAp");
	m_kfERF = q.get<double>("kfERF");
	m_kfERFp = q.get<double>("kfERFp");
	m_krERF = q.get<double>("krERF");
	m_KmfERF = q.get<double>("KmfERF");
	m_KmrERF = q.get<double>("KmrERF");
	m_ksGA2OX = q.get<double>("ksGA2OX");
	m_ksGA2OXp = q.get<double>("ksGA2OXp");
	m_ksGA2OXpp = q.get<double>("ksGA2OXpp");
	m_KiDELLA = q.get<double>("KiDELLA");
	m_kdGA2OX = q.get<double>("kdGA2OX");
	m_ksGA203OX = q.get<double>("ksGA203OX");
	m_kdGA203OX = q.get<double>("kdGA203OX");
	m_kdGA203OXp = q.get<double>("kdGA203OXp");
	m_ksACC = q.get<double>("ksACC");
	m_KiCK = q.get<double>("KiCK");
	m_kdACC = q.get<double>("kdACC");
	m_ksCDK = q.get<double>("ksCDK");
	m_ksCDKp = q.get<double>("ksCDKp");
	m_KiDELLAp = q.get<double>("KiDELLAp");
	m_KmCKAUX = q.get<double>("KmCKAUX");
	m_KmAUX = q.get<double>("KmAUX");
	m_kdCDK = q.get<double>("kdCDK");
	m_ckaux_threshold = q.get<double>("ckaux_threshold");
}

void MaizeGRN::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// Generation III model: KLUH-ethylene

	double time_now 	= m_cd.m_time_data->m_sim_time;

	const double a_area	= cell->GetArea();

	const double chem0      = cell->GetChemical(0);//CK
	const double chem1      = cell->GetChemical(1);//AUX
	const double chem2      = cell->GetChemical(2);//KLUH
	const double chem3      = cell->GetChemical(3);//GA1
	const double chem4      = cell->GetChemical(4);//GA8
	const double chem5      = cell->GetChemical(5);//DELLA
	const double chem6      = cell->GetChemical(6);//GA203OX
	const double chem7      = cell->GetChemical(7);//GA2OX
	const double chem8      = cell->GetChemical(8);//ACC
	const double chem9      = cell->GetChemical(9);//CDK


	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
	    	dchem[0] = m_ksCK - m_kdCK * chem0;//CK
	    	dchem[1] = m_ksAUX - ( m_kdAUX  +   m_kdAUXp * ( chem2 / a_area )) * chem1;//AUX - KLUH dep
	    	dchem[3] = - m_kdGA1 * chem3;//GA1

	    	if( time_now < m_tKLUH)
	    	{
	    		dchem[2] = m_ksKLUH - ( m_kdKLUH + m_kdKLUHpp * ( chem5 / a_area ) ) * chem2;//KLUH - DELLA dep. breakdown
	    	}
	    	else
	    	{
	    		dchem[2] = - ( m_kdKLUHp + m_kdKLUHpp * ( chem5 / a_area ) ) * chem2;//KLUH
	    	}
	}
	else
	{
		dchem[0] = m_ksCKp - m_kdCK * chem0;
		dchem[1] = m_ksAUXp - ( m_kdAUX  +  m_kdAUXp * ( chem2 / a_area )) * chem1;
		dchem[2] = - ( m_kdKLUH + m_kdKLUHpp * ( chem5 / a_area ) ) * chem2;//KLUH
	}

	if ( (cell->GetIndex() < 28 || cell->GetIndex() > 31 ) )
	{

		dchem[3] = m_kGA203OX * chem6 +
			+ 0.00000 * a_area * Hill( chem7 / a_area , 10 , 1, chem4 / a_area )//GA8->GA1
    		- m_kGA2OX * a_area * Hill( chem7 / a_area , m_KmGA1 , 1 , chem3 / a_area )//GA1->GA8
    		- m_kdGA1 * chem3;//GA1->0


		if( ( ( chem0 / chem1 ) > m_ckaux_threshold ) )//CK/AUX ratio determines GA1 production
		{
    		////GA20-OX+GA3-OX
			dchem[6] = m_ksGA203OX * a_area - m_kdGA203OX * chem6;//fast response to CK/AUX
		}

		else
		{
			dchem[6] = - m_kdGA203OXp * chem6;
		}

		////GA8
		dchem[4] = m_kGA2OX * a_area * Hill( chem7 / a_area , m_KmGA1 , 1 , chem3 / a_area )//GA1->GA8
				- 0.00000 * a_area * Hill( chem7 / a_area , 10 , 1, chem4 / a_area )//GA8->GA1
				- m_kdGA8 * chem4;//GA8->0

		////DELLA
		dchem[5] = m_ksDELLA * a_area//0->DELLA
				 -  chem5 * ( m_kdDELLA + m_kdDELLAp * chem3 / a_area );//DELLA->0

		////GA2-OX
		double ERF6P =  1 * Goldbeter(m_kfERF + m_kfERFp * ( chem8 / a_area ), m_krERF, m_KmfERF, m_KmrERF );//P-cascade switch G(Vf,Vr,Jf,Jr): area dependence?
		dchem[7] =  a_area * ( m_ksGA2OX + ERF6P * (m_ksGA2OXp + m_ksGA2OXpp / ( m_KiDELLA + (chem5 / a_area) ))) - m_kdGA2OX * chem7;

		////ACC OR 'X factor'
		dchem[8] = a_area * m_ksACC / (m_KiCK + ( chem0 / a_area ) ) - m_kdACC * chem8;//maybe change CK effect later to ethylene stimulation?

		////CDK

		if (time_now >= 30)//avoids instability w.r.t. auxin transport ode
		{
		double CAratio =  chem0 / chem1 ;
		dchem[9] =  a_area  * ( CAratio / ( m_KmCKAUX + CAratio ) )
					* ( (chem1 / a_area) / ( m_KmAUX + (chem1 / a_area)  ) )
					* ( m_ksCDK +  m_ksCDKp / ( m_KiDELLAp +  (chem5 / a_area) ) )
					- m_kdCDK * chem9;//change synthesis kinetics?

		}

	}
}

} // namespace
} // namespace
