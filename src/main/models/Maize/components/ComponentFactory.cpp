/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry component factory map.
 */

#include "ComponentFactory.h"

#include "cell_chemistry/factories.h"
#include "cell_color/factories.h"
#include "cell_daughters/factories.h"
#include "cell_housekeep/factories.h"
#include "cell_split/factories.h"
#include "cell2cell_transport/factories.h"
#include "cell2cell_transport_boundary/factories.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {

using namespace std;

template<typename T>
static typename ComponentTraits<T>::ComponentType Helper(const CoreData& cd, const typename ComponentTraits<T>::MapType& map)
{
         const auto  it = map.find(cd.m_parameters->get<std::string>(ComponentTraits<T>::Label()));
         return (it != map.end()) ? (it->second)(cd) : typename ComponentTraits<T>::ComponentType();
}

template<typename T>
static vector<string> ListHelper(const typename ComponentTraits<T>::MapType& map)
{
         vector<string> vec;
         for(const auto& e : map) {
                 vec.push_back(e.first);
         }
         return vec;
}

CellChemistryComponent ComponentFactory::CreateCellChemistry(const CoreData& cd) const
{
        return Helper<CellChemistryTag>(cd, CellChemistry::g_component_factories);
}

CellColorComponent ComponentFactory::CreateCellColor(const string& select, const ptree& pt) const
{
        const auto it = CellColor::g_component_factories.find(select);
        return (it != CellColor::g_component_factories.end()) ? (it->second)(pt) : CellColorComponent();
}

CellDaughtersComponent ComponentFactory::CreateCellDaughters(const CoreData& cd) const
{
        return Helper<CellDaughtersTag>(cd, CellDaughters::g_component_factories);
}

CellHousekeepComponent ComponentFactory::CreateCellHousekeep(const CoreData& cd) const
{
        return Helper<CellHousekeepTag>(cd, CellHousekeep::g_component_factories);
}

CellSplitComponent ComponentFactory::CreateCellSplit(const CoreData& cd) const
{
        return Helper<CellSplitTag>(cd, CellSplit::g_component_factories);
}

CellToCellTransportComponent ComponentFactory::CreateCellToCellTransport(const CoreData& cd) const
{
        return Helper<CellToCellTransportTag>(cd, CellToCellTransport::g_component_factories);
}

CellToCellTransportBoundaryComponent  ComponentFactory::CreateCellToCellTransportBoundary(const CoreData& cd)
{
        return Helper<CellToCellTransportBoundaryTag>(cd, CellToCellTransportBoundary::g_component_factories);
}

vector<string>  ComponentFactory::ListCellChemistry() const
{
        return ListHelper<CellChemistryTag>(CellChemistry::g_component_factories);
}

vector<string>  ComponentFactory::ListCellColor() const
{
        return ListHelper<CellColorTag>(CellColor::g_component_factories);
}

vector<string>  ComponentFactory::ListCellDaughters() const
{
        return ListHelper<CellDaughtersTag>(CellDaughters::g_component_factories);
}

vector<string>  ComponentFactory::ListCellHousekeep() const
{
        return ListHelper<CellHousekeepTag>(CellHousekeep::g_component_factories);
}

vector<string>  ComponentFactory::ListCellSplit() const
{
        return ListHelper<CellSplitTag>(CellSplit::g_component_factories);
}

vector<string>  ComponentFactory::ListCellToCellTransport() const
{
        return ListHelper<CellToCellTransportTag>(CellToCellTransport::g_component_factories);
}

} // namespace
