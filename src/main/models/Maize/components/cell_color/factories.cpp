/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellColor component factory map.
 */

#include "factories.h"

#include "Maize.h"
#include "MaizeGRN.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

using namespace std;

namespace SimPT_Maize {
namespace CellColor {

const typename ComponentTraits<CellColorTag>::MapType g_component_factories
{{
        std::make_pair("maize",           boost::value_factory<Maize>()),
        std::make_pair("maizegrn",        boost::value_factory<MaizeGRN>())
}};

} // namespace
} // namespace
