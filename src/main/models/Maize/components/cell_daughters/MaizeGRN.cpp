/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters for MaizeGRN model.
 */

#include "MaizeGRN.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellDaughters {

using namespace std;
using boost::property_tree::ptree;

MaizeGRN::MaizeGRN(const CoreData& cd)
{
        Initialize(cd);
}

void MaizeGRN::Initialize(const CoreData& cd)
{
        m_cd = cd;
}

void MaizeGRN::operator()(const CellAttributes& parent, Cell* daughter1, Cell* daughter2)
{
        // Fix the solute for the daughters
        const auto new_solute       = parent.GetSolute() / sqrt(2.0);
        daughter1->SetSolute(new_solute);
        daughter2->SetSolute(new_solute);

        // Fix the target area for the daughters
        const auto new_t_area       = parent.GetTargetArea() / 2.0;
        daughter1->SetTargetArea(new_t_area);
        daughter2->SetTargetArea(new_t_area);

        // Fix the target length for the daughters
        const auto new_t_length     = parent.GetTargetLength() / sqrt(2.0);
        daughter1->SetTargetLength(new_t_length);
        daughter2->SetTargetLength(new_t_length);

        // Chemicals.
        const double area1 = daughter1->GetArea();
        const double area2 = daughter2->GetArea();
        const double tot_area = area1 + area2;
        const double fraction1 = area1 / tot_area;
        const double fraction2 = area2 / tot_area;

        if (daughter1->HasNeighborOfTypeZero()) {

                daughter1->SetChemical(0, parent.GetChemical(0) * fraction1);
                daughter1->SetChemical(1, parent.GetChemical(1) * fraction1);
                daughter1->SetChemical(2, 0.);
                daughter1->SetChemical(3, 0.);
                daughter1->SetChemical(4, 0.);
                daughter1->SetChemical(5, 0.);
                daughter1->SetChemical(6, 0.);
                daughter1->SetChemical(7, 0.);
                daughter1->SetChemical(8, 0.);
                daughter1->SetChemical(9, parent.GetChemical(9) * fraction1);

                daughter2->SetChemical(0, parent.GetChemical(0) * fraction2);
                daughter2->SetChemical(1, parent.GetChemical(1) * fraction2);
                daughter2->SetChemical(2, parent.GetChemical(2) * fraction2);
                daughter2->SetChemical(3, parent.GetChemical(3) * fraction2);
                daughter2->SetChemical(4, parent.GetChemical(4) * fraction2);
                daughter2->SetChemical(5, parent.GetChemical(5) * fraction2);
                daughter2->SetChemical(6, parent.GetChemical(6) * fraction2);
                daughter2->SetChemical(7, parent.GetChemical(7) * fraction2);
                daughter2->SetChemical(8, parent.GetChemical(8) * fraction2);
                daughter2->SetChemical(9, parent.GetChemical(9) * fraction2);

        } else if (daughter2->HasNeighborOfTypeZero()) {

                daughter1->SetChemical(0, parent.GetChemical(0) * fraction1);
                daughter1->SetChemical(1, parent.GetChemical(1) * fraction1);
                daughter1->SetChemical(2, parent.GetChemical(2) * fraction1);
                daughter1->SetChemical(3, parent.GetChemical(3) * fraction1);
                daughter1->SetChemical(4, parent.GetChemical(4) * fraction1);
                daughter1->SetChemical(5, parent.GetChemical(5) * fraction1);
                daughter1->SetChemical(6, parent.GetChemical(6) * fraction1);
                daughter1->SetChemical(7, parent.GetChemical(7) * fraction1);
                daughter1->SetChemical(8, parent.GetChemical(8) * fraction1);
                daughter1->SetChemical(9, parent.GetChemical(9) * fraction1);

                daughter2->SetChemical(0, parent.GetChemical(0) * fraction2);
                daughter2->SetChemical(1, parent.GetChemical(1) * fraction2);
                daughter2->SetChemical(2, 0.);
                daughter2->SetChemical(3, 0.);
                daughter2->SetChemical(4, 0.);
                daughter2->SetChemical(5, 0.);
                daughter2->SetChemical(6, 0.);
                daughter2->SetChemical(7, 0.);
                daughter2->SetChemical(8, 0.);
                daughter2->SetChemical(9, parent.GetChemical(9) * fraction2);

        } else {

                daughter1->SetChemical(0, parent.GetChemical(0) * fraction1); //CK
                daughter2->SetChemical(0, parent.GetChemical(0) * fraction2);
                daughter1->SetChemical(1, parent.GetChemical(1) * fraction1); //AUX
                daughter2->SetChemical(1, parent.GetChemical(1) * fraction2);
                daughter1->SetChemical(2, parent.GetChemical(2) * fraction1); //KLUH
                daughter2->SetChemical(2, parent.GetChemical(2) * fraction2);
                daughter1->SetChemical(3, parent.GetChemical(3) * fraction1); //GA1
                daughter2->SetChemical(3, parent.GetChemical(3) * fraction2);
                daughter1->SetChemical(4, parent.GetChemical(4) * fraction1); //GA8
                daughter2->SetChemical(4, parent.GetChemical(4) * fraction2);
                daughter1->SetChemical(5, parent.GetChemical(5) * fraction1); //DELLA
                daughter2->SetChemical(5, parent.GetChemical(5) * fraction2);
                daughter1->SetChemical(6, parent.GetChemical(6) * fraction1); //GA203OX
                daughter2->SetChemical(6, parent.GetChemical(6) * fraction2);
                daughter1->SetChemical(7, parent.GetChemical(7) * fraction1); //GA2OX
                daughter2->SetChemical(7, parent.GetChemical(7) * fraction2);
                daughter1->SetChemical(8, parent.GetChemical(8) * fraction1); //ACC
                daughter2->SetChemical(8, parent.GetChemical(8) * fraction2);
                daughter1->SetChemical(9, parent.GetChemical(9) * fraction1); //CDK
                daughter2->SetChemical(9, parent.GetChemical(9) * fraction2);
        }

        // Transporters.
        daughter1->SetTransporters(1, 1., 1.);
        daughter2->SetTransporters(1, 1., 1.);
}

} // namespace
} // namespace

