/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for AuxinGrowth model.
 */

#include "AuxinGrowth.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "bio/ReduceCellWalls.h"
#include "bio/Wall.h"
#include "bio/WallAttributes.h"
#include "sim/CoreData.h"

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

AuxinGrowth::AuxinGrowth(const CoreData& cd)
{
	Initialize(cd);
}

void AuxinGrowth::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("auxin_transport");

        m_aux_breakdown			= p.get<double>("aux_breakdown");
        m_aux_cons			= p.get<double>("aux_cons");
        m_k1				= p.get<double>("k1");
        m_k2				= p.get<double>("k2");
        m_km				= p.get<double>("km");
        m_kr				= p.get<double>("kr");
        m_pin_breakdown			= p.get<double>("pin_breakdown");
        m_pin_production		= p.get<double>("pin_production");
        m_pin_production_in_epidermis	= p.get<double>("pin_production_in_epidermis");
        m_r  				= p.get<double>("r");
        m_sam_auxin			= p.get<double>("sam_auxin");
        m_sam_auxin_breakdown		= p.get<double>("sam_auxin_breakdown");
}

void AuxinGrowth::operator()(Cell* cell, double* dchem)
{
	using std::placeholders::_1;
	using std::placeholders::_2;
	using std::placeholders::_3;

	// Note: Pi and Pij measured in numbers of molecules, not concentrations

	double const sum_Pij = cell->GetSumTransporters(1);
	double const chem = cell->GetChemical(0);
	double dPidt = 0.;

	// exocytosis regulated:
	function<double(Cell*, Cell*, Wall*)> f
		= bind(&AuxinGrowth::Complex_PijAj, this, _1, _2, _3);
	dPidt = -m_k1 * ReduceCellWalls<double>(cell, f) + m_k2 * sum_Pij;

	// production of PIN depends on auxin concentration
	dPidt += (cell->HasBoundaryWall() ? m_pin_production_in_epidermis : m_pin_production) * chem
		- cell->GetChemical(1) * m_pin_breakdown;

	// no PIN production in SAM
	if (cell->GetBoundaryType() == BoundaryType::SAM) {
		dchem[1] = 0.;
		dchem[0] = -m_sam_auxin_breakdown * chem;
	} else {
		dchem[1] = dPidt;
		// source of auxin
		dchem[0] = m_aux_cons - m_aux_breakdown * chem;
	}
}

double AuxinGrowth::Complex_PijAj(Cell* here, Cell* nb, Wall* w)
{
	// gives the amount of complex "auxinreceptor-Pin1"  at the wall (at QSS)
	double const nb_aux =
		(nb->IsBoundaryPolygon() && w->IsAuxinSink()) ? m_sam_auxin : nb->GetChemical(0);
	double const receptor = nb_aux * m_r / (m_kr + nb_aux);
	double const chem = here->GetChemical(1);

	return chem * receptor / (m_km + chem);
}

} // namespace
} // namespace
