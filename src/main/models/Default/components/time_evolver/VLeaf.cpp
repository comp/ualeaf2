/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for time evolver of Merks et al.
 */

#include "VLeaf.h"

#include "Transport.h"

#include "algo/CellDivider.h"
#include "algo/CellHousekeeper.h"
#include "algo/NodeInserter.h"
#include "algo/NodeMover.h"

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace std;
using namespace SimPT_Sim::ClockMan;
using boost::property_tree::ptree;

VLeaf::VLeaf(const CoreData& cd)
{
	Initialize(cd);
}

void VLeaf::Initialize(const CoreData& cd)
{
        assert( cd.Check() && "CoreData not ok.");
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_mc_abs_tolerance     = p->get<double>("cell_mechanics.mc_abs_tolerance");
        m_mc_cell_step_size    = p->get<double>("cell_mechanics.mc_cell_stepsize");
        m_mc_step_size         = p->get<double>("cell_mechanics.mc_stepsize");
        m_mc_sweep_limit       = p->get<double>("cell_mechanics.mc_sweep_limit");
        m_mc_temperature       = p->get<double>("cell_mechanics.mc_temperature");
}

std::tuple<SimTimingTraits::CumulativeTimings, bool> VLeaf::operator()(double time_slice, SimPhase)
{
	// Only allow for node insertion, cell division and cell chemistry if the system has
	// reached equilibrium i.e. cell wall tension equilibrium is achieved faster than
	// biological processes, including division, cell wall yielding and cell expansion

	// -------------------------------------------------------------------------------
	// Initialize
	// -------------------------------------------------------------------------------
	NodeMover node_mover;
	node_mover.Initialize(m_cd);
	NodeInserter node_inserter;
	node_inserter.Initialize(m_cd);
	bool is_stationary = false;
	unsigned int sweep_count = 0U;
	CumulativeTimings  timings;
	bool go_on = false;

	do {
		++sweep_count;

		// -------------------------------------------------------------------------------
		// Metropolis
		// -------------------------------------------------------------------------------
		Stopclock chrono_met("growth", true);
		const auto info = node_mover.SweepOverNodes(m_mc_step_size, m_mc_temperature);
		//const double dh = info.down_dh + info.up_dh;
		const double dh = info.down_dh + info.up_dh;
		node_inserter.InsertNodes();
		timings.Record(chrono_met.GetName(), chrono_met.Get());

		go_on = !( (-dh) < m_mc_abs_tolerance );

	} while (go_on && sweep_count < m_mc_sweep_limit);

		// ---------------------------------------------------------------------------
		// Cell housekeeping
		// ---------------------------------------------------------------------------
		Stopclock chrono_hk("housekeeping", true);
		CellDivider divider(m_cd);
		divider.DivideCells();
		CellHousekeeper  cell_housekeeper;
		cell_housekeeper.Initialize(m_cd);
		cell_housekeeper.Exec();
		timings.Record(chrono_hk.GetName(), chrono_hk.Get());

		// ---------------------------------------------------------------------------
		// Reaction and transport dynamics
		// -------------------------------------------------------------------------------
		Stopclock chrono_ode("transport", true);
		Transport transporter(m_cd);
		auto tup = transporter(time_slice);
		timings.Record(chrono_ode.GetName(), chrono_ode.Get());


	return make_tuple(timings, is_stationary);
}

} // namespace
} // namespace
