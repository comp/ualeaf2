/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for AuxinPIN1 colorizer.
 */

#include "AuxinPIN1.h"

#include "bio/Cell.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellColor {

using boost::property_tree::ptree;
using SimPT_Sim::Cell;

AuxinPIN1::AuxinPIN1(const ptree& )
{
}

std::array<double, 3> AuxinPIN1::operator()(Cell* cell)
{
	const double c0 = (cell->GetChemicals().size() >= 2) ? cell->GetChemical(0): 0.0;
	const double c1 = (cell->GetChemicals().size() >= 2) ? cell->GetChemical(1): 0.0;
	return {{c1 / (1.0 + c1), c0 / (1.0 + c0), 0.0}};
}

} // namespace
} // namespace

