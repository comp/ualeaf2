/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellColor::Meinhardt implementation.
 */

#include "Meinhardt.h"

#include "bio/Cell.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <cmath>
#include <stdexcept>

namespace SimPT_Default {
namespace CellColor {

using namespace std;

Meinhardt::Meinhardt(const boost::property_tree::ptree&)
{
}

array<double, 3> Meinhardt::operator()(SimPT_Sim::Cell* cell)
{
        array<double, 3> ret_val {{1.0, 1.0, 1.0}};

	if (cell->GetChemicals().size() >= 4) {
	        const double x0 = cell->GetChemical(0);
	        const double x1 = cell->GetChemical(1);
	        const double x3 = cell->GetChemical(3);
	        if ((fpclassify(x0) == FP_NAN) || (x0 < 0.0)) {
	                throw runtime_error("Meinhardt::CellColor> Numerical instability !");
	        }
	        ret_val = {{x1/(1.0+x1), x0/(1.0+x0), x3/(1.0+x3)}};
	}

	return ret_val;
}

} // namespace
} // namespace


