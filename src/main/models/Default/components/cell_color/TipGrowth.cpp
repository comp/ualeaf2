/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimplyRed colorizer.
 */
#include "TipGrowth.h"
#include "bio/Cell.h"
#include "util/color/hsv.h"
#include "util/color/Hsv2Rgb.h"

#include <boost/property_tree/ptree.hpp>


namespace SimPT_Default {
namespace CellColor {

using namespace std;
using boost::property_tree::ptree;
using namespace SimPT_Sim::Color;

TipGrowth::TipGrowth(const ptree& )
{
}

std::array<double, 3> TipGrowth::operator()(SimPT_Sim::Cell* cell)
{
	const double c0 = (cell->GetChemicals().size() >= 1) ? cell->GetChemical(0): 0.0;
	double val =  1 - c0 / (1 + c0);
	return {{val,val,val}};
}

} // namespace
} // namespace

