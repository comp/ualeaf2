/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters::BasicPIN implementation file.
 */

#include "BasicPIN.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"

using namespace std;
using namespace boost::property_tree;

namespace SimPT_Default {
namespace CellDaughters {

BasicPIN::BasicPIN(const CoreData& cd)
{
	Initialize(cd);
}

void BasicPIN::Initialize(const CoreData& cd)
{
	m_cd = cd;
	auto& p = m_cd.m_parameters;

	ptree const& arr_initval = p->get_child("auxin_transport.initval.value_array");
	m_x_initval = (++arr_initval.begin())->second.get_value<double>();
}

void BasicPIN::operator()(const CellAttributes& parent_attributes, Cell* daughter1, Cell* daughter2)
{
        // Fix the solute for the daughters
        const auto new_solute       = parent_attributes.GetSolute() / sqrt(2.0);
        daughter1->SetSolute(new_solute);
        daughter2->SetSolute(new_solute);

        // Fix the target area for the daughters
        const auto new_t_area       = parent_attributes.GetTargetArea() / 2.0;
        daughter1->SetTargetArea(new_t_area);
        daughter2->SetTargetArea(new_t_area);

        // Fix the target length for the daughters
        const auto new_t_length     = parent_attributes.GetTargetLength() / sqrt(2.0);
        daughter1->SetTargetLength(new_t_length);
        daughter2->SetTargetLength(new_t_length);

        // Chemicals.
	daughter1->SetChemical(1, m_x_initval);
	daughter2->SetChemical(1, m_x_initval);
}

} // namespace
} // namespace
