/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters::SmithPhyllotaxis implementation.
 */

#include "SmithPhyllotaxis.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"

namespace SimPT_Default {
namespace CellDaughters {

using namespace std;
using namespace boost::property_tree;

SmithPhyllotaxis::SmithPhyllotaxis(const CoreData& cd)
{
        Initialize(cd);
}

void SmithPhyllotaxis::Initialize(const CoreData& cd)
{
        m_cd = cd;
}

void SmithPhyllotaxis::operator()(const CellAttributes& parent_attributes, Cell* daughter1, Cell* daughter2)
{
        // Fix the solute for the daughters
        const auto new_solute       = parent_attributes.GetSolute() / sqrt(2.0);
        daughter1->SetSolute(new_solute);
        daughter2->SetSolute(new_solute);

        // Fix the target area for the daughters
        const auto new_t_area       = parent_attributes.GetTargetArea() / 2.0;
        daughter1->SetTargetArea(new_t_area);
        daughter2->SetTargetArea(new_t_area);

        // Fix the target length for the daughters
        const auto new_t_length     = parent_attributes.GetTargetLength() / sqrt(2.0);
        daughter1->SetTargetLength(new_t_length);
        daughter2->SetTargetLength(new_t_length);

        // IAA (chem_0) is concentration based so daughters inherits parent's
        // concentration which is already the default behaviour in
        // CellDivider::DivideOverAxis().

        // PIN (chem_1) distributes over mother and daughter proportional to area
        // hence we scale the default distribution according to relative areas.
        const double area1 = daughter1->GetArea();
        const double area2 = daughter2->GetArea();
        const double area_tot = area1 + area1;
        const double chemical1 = parent_attributes.GetChemical(1);
        daughter1->SetChemical(1, chemical1 * (area1 / area_tot));
        daughter2->SetChemical(1, chemical1 * (area2 / area_tot));
}

} // namespace
} // namespace

