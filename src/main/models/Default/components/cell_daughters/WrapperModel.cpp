/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughter component for Wrapper model.
 */

#include "WrapperModel.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellDaughters {

using namespace std;
using boost::property_tree::ptree;

WrapperModel::WrapperModel(const CoreData& cd)
{
	Initialize(cd);
}

void WrapperModel::Initialize(const CoreData& cd)
{
	m_cd = cd;
}

void WrapperModel::operator()(const CellAttributes& parent_attributes, Cell* daughter1, Cell* daughter2)
{
        // Fix the solute for the daughters
        const auto new_solute       = parent_attributes.GetSolute() / sqrt(2.0);
        daughter1->SetSolute(new_solute);
        daughter2->SetSolute(new_solute);

        // Fix the target area for the daughters
        const auto new_t_area       = parent_attributes.GetTargetArea() / 2.0;
        daughter1->SetTargetArea(new_t_area);
        daughter2->SetTargetArea(new_t_area);

        // Fix the target length for the daughters
        const auto new_t_length     = parent_attributes.GetTargetLength() / sqrt(2.0);
        daughter1->SetTargetLength(new_t_length);
        daughter2->SetTargetLength(new_t_length);

        // Chemicals.
	double const area1 = daughter1->GetArea();
	double const area2 = daughter2->GetArea();
	double const tot_area = area1 + area2;

	daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
	daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
	daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
	daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));
}

} // namespace
} // namespace
