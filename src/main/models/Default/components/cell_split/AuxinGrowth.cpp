/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for AuxinGrowth model.
 */

#include "AuxinGrowth.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"

#include <cmath>

namespace SimPT_Default {
namespace CellSplit {

using namespace std;
using namespace boost::property_tree;

AuxinGrowth::AuxinGrowth(const CoreData& cd)
{
	Initialize(cd);
}

void AuxinGrowth::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_base_area             = p->get<double>("cell_mechanics.base_area");
        m_division_ratio        = p->get<double>("cell_mechanics.division_ratio");
        m_div_area              = m_division_ratio * m_base_area;
}

std::tuple<bool, bool, std::array<double, 3>> AuxinGrowth::operator()(Cell* cell)
{
	bool must_divide        = false;

	if (cell->GetBoundaryType() == BoundaryType::None) {
		if (cell->GetArea() > m_div_area) {
			cell->SetChemical(0, 0);
			must_divide = true;
		}
	}

	return std::make_tuple(must_divide, false, std::array<double, 3>());
}

} // namespace
} // namespace
