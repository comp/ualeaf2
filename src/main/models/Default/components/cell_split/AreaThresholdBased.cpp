/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for AreaThresholdBased model.
 */

#include "AreaThresholdBased.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellSplit {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace boost::property_tree;

AreaThresholdBased::AreaThresholdBased(const CoreData& cd)
{
	Initialize(cd);
}

void AreaThresholdBased::Initialize(const CoreData& cd)
{
	m_cd = cd;
	auto& p = m_cd.m_parameters;

	m_base_area = p->get<double>("cell_mechanics.base_area");
	m_division_ratio = p->get<double>("cell_mechanics.division_ratio");
	m_div_area = m_division_ratio * m_base_area;
}

tuple<bool, bool, array<double, 3>> AreaThresholdBased::operator()(Cell* cell)
{
	bool must_divide = false;

	// Only trigger division for cells with a regular boundary
	if (cell->GetBoundaryType() == BoundaryType::None && cell->GetArea() > m_div_area) {
		must_divide = true;
	}

	return make_tuple(must_divide, false, array<double, 3>());
}

} // namespace
} // namespace

