#ifndef DEFAULT_CELL_SPLIT_META_SPLIT_H_INCLUDED
#define DEFAULT_CELL_SPLIT_META_SPLIT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * MetaSplit demo model.
 */

#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>
#include <functional>
#include <array>
#include <tuple>

namespace SimPT_Sim {
class Cell;
}

namespace SimPT_Default {
namespace CellSplit {

using namespace SimPT_Sim;

/**
 * Meta cell splitter switches at (configurable) point in time.
 */
class MetaSplit
{
public:
	/// Initializing constructor.
	MetaSplit(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	std::tuple<bool, bool, std::array<double, 3>> operator()(Cell* cell);

private:
	CoreData   m_cd;                     ///< Core data (mesh, params, sim_time,...).
	double     m_switch_time;            ///< Time point for switching between two selections for CellSplitter.

private:
	std::function<std::tuple<bool, bool, std::array<double, 3>> (Cell*)>   m_splitter1;   ///< First option for CellSplitter.
	std::function<std::tuple<bool, bool, std::array<double, 3>> (Cell*)>   m_splitter2;   ///< Second option for CellSplitter.
};

} // namespace
} // namespace

#endif // end_of_include_guard
