/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * MetaSplit implementation.
 */

#include "MetaSplit.h"
#include "cell_split/factories.h"

#include <string>

namespace SimPT_Default {
namespace CellSplit {

using namespace std;
using namespace boost::property_tree;

MetaSplit::MetaSplit(const CoreData& cd)
{
        Initialize(cd);
}

void MetaSplit::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters;

        m_switch_time         = p->get<double>("model.MetaSplit.switch_time");
        const auto select_1   = p->get<string>("model.MetaSplit.splitter1");
        const auto select_2   = p->get<string>("model.MetaSplit.splitter2");
        m_splitter1 = (g_component_factories.at(select_1))(m_cd);
        m_splitter2 = (g_component_factories.at(select_2))(m_cd);
}

std::tuple<bool, bool, std::array<double, 3>> MetaSplit::operator()(Cell* cell)
{
        return (m_cd.m_time_data->m_sim_time < m_switch_time) ? m_splitter1(cell) : m_splitter2(cell);
}

} // namespace
} // namespace
