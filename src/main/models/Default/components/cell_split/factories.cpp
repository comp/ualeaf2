/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit component factory map.
 */

#include "factories.h"

#include "AreaThresholdBased.h"
#include "AuxinGrowth.h"
#include "Geometric.h"
#include "MetaSplit.h"
#include "NoOp.h"
#include "Wortel.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

namespace SimPT_Default {
namespace CellSplit {

// ------------------------------------------------------------------------------
// Add new classes here (but do not forget to include the header also).
// ------------------------------------------------------------------------------
const typename ComponentTraits<CellSplitTag>::MapType g_component_factories
{{
	std::make_pair("AreaThresholdBased",    boost::value_factory<AreaThresholdBased>()),
	std::make_pair("AuxinGrowth",           boost::value_factory<AuxinGrowth>()),
	std::make_pair("Geometric",             boost::value_factory<Geometric>()),
	std::make_pair("MetaSplit",             boost::value_factory<MetaSplit>()),
	std::make_pair("NoOp",                  boost::value_factory<NoOp>()),
	std::make_pair("Wortel",                boost::value_factory<Wortel>())
}};

} // namespace
} // namespace
