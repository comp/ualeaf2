/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Geometric model.
 */

#include "Geometric.h"

#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>
#include <cmath>

namespace SimPT_Default {
namespace CellHousekeep {

using namespace std;
using namespace boost::property_tree;

Geometric::Geometric(const CoreData& cd)
{
	Initialize(cd);
}

void Geometric::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_cell_base_area       = p->get<double>("cell_mechanics.base_area");
        m_cell_expansion_rate  = p->get<double>("cell_mechanics.cell_expansion_rate");
        m_division_ratio       = p->get<double>("cell_mechanics.division_ratio");
        m_elastic_modulus      = p->get<double>("cell_mechanics.elastic_modulus");
        m_fixed_division_axis  = p->get<bool>("cell_mechanics.fixed_division_axis");
        m_response_time        = p->get<double>("cell_mechanics.response_time");
        m_time_step            = p->get<double>("model.time_step");
        m_viscosity_const      = p->get<double>("cell_mechanics.viscosity_const");

        m_area_incr            = m_cell_expansion_rate * m_time_step;
        m_div_area             = m_division_ratio * m_cell_base_area;
}

void Geometric::operator()(Cell* cell)
{
	const string ham_select = m_cd.m_parameters->get<string>("model.mc_hamiltonian");

	const double t_area     = cell->GetTargetArea();
	const double t_length   = cell->GetTargetLength();

	const double update_t_area    = t_area + m_area_incr;
	const double update_t_length  = t_length * sqrt(update_t_area / t_area);

	cell->SetTargetArea(update_t_area);
	cell->SetTargetLength(update_t_length);

	if (ham_select == "ElasticWall") {
		// Update the rest length of wall
		// TODO: 1.50 (maximal extension) and 1.25 (irreversible extension) are hidden parameters
		for (list<Wall*>::iterator i = cell->GetWalls().begin(); i != cell->GetWalls().end(); ++i) {
			Wall* w = *i;
			if (w->GetLength() > 1.50 * w->GetRestLength()) {
				// Irreversible extension
				w->SetRestLength(w->GetLength() / 1.25);
			}
		}
	}

	if (ham_select == "Maxwell") {
		// Update the solute of cell and the rest length of wall
		const double solute = m_viscosity_const * sqrt(cell->GetArea());
		const double update_solute = solute - (solute - cell->GetSolute()) * exp(-0.1 * m_time_step/m_response_time);
		cell->SetSolute(update_solute);

		for (list<Wall*>::iterator i = cell->GetWalls().begin(); i != cell->GetWalls().end(); ++i) {
			Wall* w = *i;

			const double update_rest_length = w->GetLength()
			        - (w->GetLength() - w->GetRestLength())
			                * exp(-0.01 * m_elastic_modulus * m_time_step / m_viscosity_const);
			w->SetRestLength(update_rest_length);
		}
	}
}

} // namespace
} // namespace
