/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for AuxinGrowth model.
 */

#include "AuxinGrowth.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

AuxinGrowth::AuxinGrowth(const CoreData& cd)
{
	Initialize(cd);
}

void AuxinGrowth::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_chemical_count   = p->get<unsigned int>("model.cell_chemical_count");
        m_D.clear();
        m_ka               = p->get<double>("auxin_transport.ka");
        m_tip_source  = p->get<double>("auxin_transport.leaf_tip_source");
        m_sam_efflux       = p->get<double>("auxin_transport.sam_efflux");
        m_transport        = p->get<double>("auxin_transport.transport");

        ptree const& arr_D = p->get_child("auxin_transport.D.value_array");
        unsigned int c = 0;
        for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
                if (c == m_chemical_count) {
                        break;
                }
                m_D.push_back(it->second.get_value<double>());
                c++;
        }
        // Defensive: if fewer than chemical_count parameters were specified, fill up.
        for (unsigned int i = c; i < m_chemical_count; i++) {
                m_D.push_back(0.0);
        }
}

void AuxinGrowth::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	// leaf edge is const source of auxin (Neumann boundary condition: we specify the influx)
	if (w->GetC2()->IsBoundaryPolygon()) {
		if (w->IsAuxinSource()) {
			double const aux_flux = m_tip_source * w->GetLength();
			dchem_c1[0] += aux_flux;
		}
		return;
	}

	if (w->GetC1()->IsBoundaryPolygon()) {
		if (w->IsAuxinSource()) {
			double const aux_flux = m_tip_source * w->GetLength();
			dchem_c2[0] += aux_flux;
		}
		else if (w->IsAuxinSink()) {
			// efflux into Shoot Apical meristem
			// we assume all PINs are directed towards shoot apical meristem
			double const chem = w->GetC2()->GetChemical(0);
			dchem_c2[0] -= m_sam_efflux * chem / (m_ka + chem);
		}
		return;
	}

	// Passive fluxes (Fick's law)
	// only auxin flux for now; flux depends on edge length and concentration difference
	double const phi = w->GetLength() * m_D[0]
	                          * (w->GetC2()->GetChemical(0) - w->GetC1()->GetChemical(0));
	dchem_c1[0] += phi;
	dchem_c2[0] -= phi;

	// Active transport (PIN1 mediated) (Transporters measured in moles, here)
	double const chem_1_0 = w->GetC1()->GetChemical(0);
	double const chem_2_0 = w->GetC2()->GetChemical(0);
	double const trans12 = m_transport * w->GetTransporters1(1) * chem_1_0 / (m_ka + chem_1_0);
	double const trans21 = m_transport * w->GetTransporters2(1) * chem_2_0 / (m_ka + chem_2_0);
	dchem_c1[0] += trans21 - trans12;
	dchem_c2[0] += trans12 - trans21;
}

} // namespace
} // namespace
