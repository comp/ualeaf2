/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport component for the TestCoupling  model.
 */

#include "TestCoupling.h"

#include "../ComponentFactory.h"
#include "bio/Cell.h"
#include "bio/Wall.h"
#include "model/ComponentTraits.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace SimPT_Sim;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;

TestCoupling::TestCoupling(const CoreData& cd)
{
	Initialize(cd);
}

void TestCoupling::Initialize(const CoreData& cd)
{
        m_cd    = cd;

        auto& p = m_cd.m_parameters;
        m_chemical_count   = p->get<unsigned int>("model.cell_chemical_count");

        //m_d     = p->get<double>("test_coupling.D[0]");
        ptree const& arr_D = p->get_child("auxin_transport.D.value_array");
        unsigned int c = 0;
        for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
                if (c == m_chemical_count) {
                        break;
                }
                m_D.push_back(it->second.get_value<double>());
                c++;
        }
        // Defensive: if fewer than chemical_count parameters were specified, fill up.
        for (unsigned int i = c; i < m_chemical_count; i++) {
                m_D.push_back(0.0);
        }

        m_has_boundary_condition = false;

        if (p->get<std::string>( ComponentTraits<CellToCellTransportBoundaryTag>::Label(),"") != "") {
                m_has_boundary_condition = true;
                m_boundary_condition = ComponentFactory::CreateCellToCellTransportBoundary(cd);
                if (m_boundary_condition == nullptr) {
                        throw runtime_error("TestCoupling::Initialize> nullptr for cell2cell_transport boundary codition.");
                }
        }

}

void TestCoupling::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	if ( !(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon()) ) {
		const double apoplast_thickness = 1.;

		const double phi = (w->GetLength() / apoplast_thickness) * m_D[0]
		                                 * ( ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[0] += phi; //LEVELS!
		dchem_c2[0] -= phi; //LEVELS!

		//DDV2012: diffusive hormone - chemical '1'
		const double phi2 = (w->GetLength() / apoplast_thickness) * m_D[1]
		                                 * ( ( w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()) )
			- ( w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[1] += phi2 ; //LEVELS!
		dchem_c2[1] -= phi2 ; //LEVELS!
	} else {
		if (m_has_boundary_condition) {
			m_boundary_condition(w, dchem_c1, dchem_c2);
		}
	}
}

} // namespace
} // namespace
