#ifndef DEFAULT_WALL_CHEMISTRY_BASIC_H_INCLUDED
#define DEFAULT_WALL_CHEMISTRY_BASIC_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Basic WallChemistry component.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {
class Cell;
class Wall;
}

namespace SimPT_Default {
namespace WallChemistry {

using namespace SimPT_Sim;

/**
 * Basic approach with PINflux to wall chemistry.
 */
class Basic
{
public:
	/// Set up empty object.
	Basic();

	/// Initializing constructor.
	Basic(const CoreData& cd);

	/// Initialize from ptree.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Wall* w, double* dw1, double* dw2);

private:
	/// Helper.
	double PINflux(Cell* this_cell, Cell* adjacent_cell, Wall* w);

private:
	CoreData       m_cd;      ///< Core data (mesh, params, sim_time,...).
	double 	       m_k1;
	double 	       m_k2;
	double 	       m_km;
	double         m_kr;
	double         m_r;
};

} // namespace
} // namespace

#endif // end_of_include_guard
