/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * WallChemistry for AuxinGrowth model.
 */

#include "AuxinGrowth.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace WallChemistry {

using namespace std;
using namespace boost::property_tree;

AuxinGrowth::AuxinGrowth()
	: m_k1(0.0), m_k2(0.0), m_km(0.0), m_kr(0.0), m_r(0.0), m_sam_auxin(0.0)
{
}

AuxinGrowth::AuxinGrowth(const CoreData& cd)
	: AuxinGrowth()
{
	Initialize(cd);
}

void AuxinGrowth::Initialize(const CoreData& cd)
{
	m_cd = cd;
		auto& p = m_cd.m_parameters;

		m_k1        = p->get<double>("auxin_transport.k1");
		m_k2        = p->get<double>("auxin_transport.k2");
		m_km        = p->get<double>("auxin_transport.km");
		m_kr        = p->get<double>("auxin_transport.kr");
		m_r         = p->get<double>("auxin_transport.r");
		m_sam_auxin = p->get<double>("auxin_transport.sam_auxin");
}

void AuxinGrowth::operator()(Wall* w, double* dw1, double* dw2)
{
	// Cells polarize available PIN1 to Shoot Apical Meristem
	if (w->GetC2()->IsBoundaryPolygon()) {
		if (w->IsAuxinSink()) {
			dw1[0] = 0.;
			dw2[0] = 0.;

			// assume high auxin concentration in SAM, to convince PIN1 to polarize to it
			// exocytosis regulated
			double const chem = w->GetC1()->GetChemical(1);
			double const receptor = m_sam_auxin * m_r / (m_kr + m_sam_auxin);
			dw1[1] = m_k1 * chem * receptor / (m_km + chem)
				                	- m_k2 * w->GetTransporters1(1);
			dw2[1] = 0.;
		} else {
			dw1[0] = dw2[0] = dw1[1] = dw2[1];
		}
		return;
	}

	if (w->GetC1()->IsBoundaryPolygon()) {
		if (w->IsAuxinSink()) {
			dw1[0] = 0.;
			dw2[0] = 0.;

			// assume high auxin concentration in SAM, to convince PIN1 to polarize to it
			// exocytosis regulated
			double const chem = w->GetC2()->GetChemical(1);
			double const receptor = m_sam_auxin * m_r / (m_kr + m_sam_auxin);
			dw1[1] = 0.;
			dw2[1] = m_k1 * chem * receptor / (m_km + chem)
				                	- m_k2 * w->GetTransporters2(1);
		} else {
			dw1[0] = dw2[0] = dw1[1] = dw2[1];
		}
		return;
	}

	// PIN1 localization at wall 1
	// Chemical 0 is Auxin (intracellular storage only)
	// Chemical 1 is PIN1  (walls and intracellular storage)
	//! \f$ \frac{d Pij/dt}{dt} = k_1 A_j \frac{P_i}{L_ij} - k_2 P_{ij} \f$
	// Note: Pij measured in terms of concentration (mol/L); Pi in terms of quantity (mol)

	// normal cell -- exocytosis regulated
	double const auxin2     = w->GetC2()->GetChemical(0);
	double const chem_1_1   = w->GetC1()->GetChemical(1);
	double const receptor1  = auxin2 * m_r / (m_kr + auxin2);
	double const dPijdt1    = m_k1 * chem_1_1 * receptor1 / (m_km + chem_1_1)
		                			- m_k2 * w->GetTransporters1(1);

	// normal cell -- exocytosis regulated
	double const auxin1     = w->GetC1()->GetChemical(0);
	double const chem_2_1   = w->GetC2()->GetChemical(1);
	double const receptor2  = auxin1 * m_r / (m_kr + auxin1);
	double const dPijdt2    = m_k1 * chem_2_1 * receptor2 / (m_km + chem_2_1)
		                			- m_k2 * w->GetTransporters2(1);

	// PIN1 of neighboring vascular cell inhibits PIN1 endocytosis
	dw1[0] = 0.;
	dw2[0] = 0.;
	dw1[1] = dPijdt1;
	dw2[1] = dPijdt2;
}

} // namespace
} // namespace
