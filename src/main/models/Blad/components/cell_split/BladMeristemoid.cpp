/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Blad model.
 */

#include "BladMeristemoid.h"

#include "bio/Cell.h"

#include "math/RandomEngine.h"

#include <cmath>
#include <iostream>
#include <trng/uniform_dist.hpp>

namespace SimPT_Blad {
namespace CellSplit {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace boost::property_tree;

BladMeristemoid::BladMeristemoid(const CoreData& cd)
{
	Initialize(cd);
}

void BladMeristemoid::Initialize(const CoreData& cd)
{
        m_cd 						= cd;
        const auto& p				= m_cd.m_parameters->get_child("blad");
        m_M_threshold_division_pavement	    = p.get<double>("M_threshold_division");
        m_M_threshold_division_meristemoid = p.get<double>("M_threshold_expansion");
        m_size_threshold_division	= p.get<double>("size_threshold_division");
        m_time_threshold_division 	= p.get<double>("time_threshold_division", 60 * 22);

        m_uniform_generator_noise 	= m_cd.m_random_engine->GetGenerator(trng::uniform_dist<double>(-0.1, 0.1));
}

std::tuple<bool, bool, std::array<double, 3>> BladMeristemoid::operator()(Cell* cell)
{
	const double chem1      = cell->GetChemical(1);
	const double a_area	= cell->GetArea();

	int time_since_division = cell->GetTimeSinceDivision();
	int timestep = m_cd.m_time_data->m_sim_time / (m_cd.m_time_data->m_sim_step + 1);
	time_since_division += timestep;

	bool must_divide        = false;

	if (cell->GetBoundaryType() == BoundaryType::None) {
		int cell_type = cell->GetCellType();

		if (cell_type == 1) {
			// Pavement cell, normal process

			double noise = m_uniform_generator_noise();
			int time_threshold = m_time_threshold_division + (m_time_threshold_division * noise);

			if (( chem1 / a_area ) >= m_M_threshold_division_pavement && time_since_division >= time_threshold && a_area > 50) {
				must_divide = true;
				time_since_division = 0;
			}

		} else if (cell_type == 2) {
			// Meristemoid cell
			int div_counter = cell->GetDivisionCounter();
			//TODO time threshold specific to Meristemoid cell
			if (div_counter < 3  && ( chem1 / a_area ) >= m_M_threshold_division_meristemoid && time_since_division >= m_time_threshold_division && a_area > 50) {
				must_divide = true;
				time_since_division = 0;
			}

		} else if (cell_type == 3) {
			// Stomata cell: can divide only once
			int div_counter = cell->GetDivisionCounter();
			//TODO time threshold specific to Stomata cell

			if (div_counter == 0 && time_since_division >= m_time_threshold_division && a_area > 50) {
				must_divide = true;
				time_since_division = 0;
			}
		}

	}

	cell->SetTimeSinceDivision(time_since_division);

	return std::make_tuple(must_divide, false, std::array<double, 3>());
}

} // namespace
} // namespace

