/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Blad model.
 */

#include "BladMeristemoid.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"

#include "math/RandomEngine.h"

#include <trng/bernoulli_dist.hpp>

namespace SimPT_Blad {
namespace CellHousekeep {

using boost::property_tree::ptree;

BladMeristemoid::BladMeristemoid(const CoreData& cd)
{
	Initialize(cd);
}

void BladMeristemoid::Initialize(const CoreData& cd)
{
	m_cd 							= cd;
	const auto& p					= m_cd.m_parameters->get_child("blad");

	m_M_threshold_expansion	    	= p.get<double>("M_threshold_expansion");
	m_relative_growth				= p.get<double>("relative_growth");

	m_bernoulli_generator_meristem 	= m_cd.m_random_engine->GetGenerator(trng::bernoulli_dist<bool>(0.0025, true, false));
	m_bernoulli_generator_stomata 	= m_cd.m_random_engine->GetGenerator(trng::bernoulli_dist<bool>(0.075, true, false));

	m_max_meristem_size				= 1000 / 2.25;
	m_max_stomata_size				= 1000 / 2.25;
}

void BladMeristemoid::operator()(Cell* cell)
{
	const double chem1      = cell->GetChemical(1);
	const double t_area     = cell->GetTargetArea();
	const double a_area		= cell->GetArea();
	const int cell_type		= cell->GetCellType();

	// Check if the cell should transition to another type of cell
	if (cell_type == 1) {
		// Pavement cell, check if transition to Meristemoid cell happens
		if (m_bernoulli_generator_meristem()) {
			cell->SetCellType(2);
			cell->SetDivisionCounter(0);
		}
	} else if (cell_type == 2) {
		// Meristemoid cell, check if transition to Stomata cell happens
		if (m_bernoulli_generator_stomata()) {
			cell->SetCellType(3);
			cell->SetDivisionCounter(0);
		}
	}

	if (cell->GetBoundaryType() == BoundaryType::None) { //THIS LINE IS VALID FOR ALL TYPES!
		if (cell_type == 1) {
			// Pavement cell
			if ( (chem1 / a_area) > m_M_threshold_expansion ) {
				const double incr = m_relative_growth * a_area; // This is for a 240-minute time step!
				const double update_t_area = t_area + incr;
				cell->SetTargetArea(update_t_area);
			}
		} else if (cell_type == 2) {
			// Meristemoid cell
			//if (a_area < m_max_meristem_size) {
				const double incr = (m_relative_growth / 2) * a_area; // This is for a 240-minute time step!
				const double update_t_area = t_area + incr;
				cell->SetTargetArea(update_t_area);
			//}
		} else if (cell_type == 3) {
			// Stomata cell
			//if (a_area < m_max_stomata_size) {
				const double incr = (m_relative_growth / 2) * a_area; // This is for a 240-minute time step!
				const double update_t_area = t_area + incr;
				cell->SetTargetArea(update_t_area);
			//}
		}
	}
	// TODO different thresholds for different cell types: also different criteria, cf. above
}

} // namespace
} // namespace
