/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters for Blad model.
 */

#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "Blad.h"

namespace SimPT_Blad {
namespace CellDaughters {

Blad::Blad(const CoreData& cd)
{
	Initialize(cd);
}

void Blad::Initialize(const CoreData& cd)
{
	m_cd = cd;
}

void Blad::operator()(const CellAttributes& parent_attributes, Cell* daughter1, Cell* daughter2)
{
	daughter1->IncrementDivisionCounter();
	daughter2->IncrementDivisionCounter();

    // Fix the solute for the daughters
    const auto new_solute       = parent_attributes.GetSolute() / sqrt(2.0);
    daughter1->SetSolute(new_solute);
    daughter2->SetSolute(new_solute);

    // Fix the target area for the daughters
    const auto new_t_area       = parent_attributes.GetTargetArea() / 2.0;
    daughter1->SetTargetArea(new_t_area);
    daughter2->SetTargetArea(new_t_area);

    // Fix the target length for the daughters
    const auto new_t_length     = parent_attributes.GetTargetLength() / sqrt(2.0);
    daughter1->SetTargetLength(new_t_length);
    daughter2->SetTargetLength(new_t_length);

     // Chemicals in cells.

	const double area1 = daughter1->GetArea();
	const double area2 = daughter2->GetArea();
	const double tot_area = area1 + area2;
	const double chemical0 = parent_attributes.GetChemical(0);
	const double chemical1 = parent_attributes.GetChemical(1);
	daughter1->SetChemical(0, chemical0 * (area1 / tot_area));
	daughter2->SetChemical(0, chemical0 * (area2 / tot_area));
	daughter1->SetChemical(1, chemical1 * (area1 / tot_area));
	daughter2->SetChemical(1, chemical1 * (area2 / tot_area));

	// Transporters in walls.
	daughter1->SetTransporters(1, 1., 1.);
	daughter2->SetTransporters(1, 1., 1.);
}

} // namespace
} // namespace
