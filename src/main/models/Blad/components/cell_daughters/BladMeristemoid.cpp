/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters for Blad model.
 */

#include "BladMeristemoid.h"

#include "math/RandomEngine.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"

#include <trng/bernoulli_dist.hpp>

namespace SimPT_Blad {
namespace CellDaughters {

BladMeristemoid::BladMeristemoid(const CoreData& cd)
{
	Initialize(cd);
}

void BladMeristemoid::Initialize(const CoreData& cd)
{
	m_cd = cd;
	m_bernoulli_generator = m_cd.m_random_engine->GetGenerator(trng::bernoulli_dist<bool>(0.5, true, false));
}

void BladMeristemoid::operator()(const CellAttributes& parent_attributes, Cell* daughter1, Cell* daughter2)
{
	daughter1->IncrementDivisionCounter();
	daughter2->IncrementDivisionCounter();

	// Check parent type of cell
	int parent_type = parent_attributes.GetCellType();
	if (parent_type == 2) {
		// Parent was Meristemoid cell
		// Randomly select which of the 2 daughters stays type 1 and which reverses to type 0
		if (m_bernoulli_generator()) {
			// Choose cell 1 to revert
			daughter1->SetCellType(1);
		} else {
			// Choose cell 2 to rever
			daughter2->SetCellType(1);
		}
	}

    // Fix the solute for the daughters
    const auto new_solute       = parent_attributes.GetSolute() / sqrt(2.0);
    daughter1->SetSolute(new_solute);
    daughter2->SetSolute(new_solute);

    // Fix the target area for the daughters
    const auto new_t_area       = parent_attributes.GetTargetArea() / 2.0;
    daughter1->SetTargetArea(new_t_area);
    daughter2->SetTargetArea(new_t_area);

    // Fix the target length for the daughters
    const auto new_t_length     = parent_attributes.GetTargetLength() / sqrt(2.0);
    daughter1->SetTargetLength(new_t_length);
    daughter2->SetTargetLength(new_t_length);

    // Chemicals in cells and transporters in walls.
	const double area1 = daughter1->GetArea();
	const double area2 = daughter2->GetArea();
	const double tot_area = area1 + area2;

	daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
	daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
	daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
	daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));

	daughter1->SetTransporters(1, 1., 1.);
	daughter2->SetTransporters(1, 1., 1.);
}

} // namespace
} // namespace
