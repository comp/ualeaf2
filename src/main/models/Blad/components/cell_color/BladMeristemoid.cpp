/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellColor for Blad model.
 */

#include "BladMeristemoid.h"

#include "bio/Cell.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Blad {
namespace CellColor {

using namespace std;

BladMeristemoid::BladMeristemoid(const boost::property_tree::ptree& )
{
}

array<double, 3> BladMeristemoid::operator()(SimPT_Sim::Cell* cell)
{
	const int cell_type = cell->GetCellType();

	switch (cell_type) {
	case 0:
		return array<double, 3> {{1.0, 1.0, 1.0}};
	case 1:
		return array<double, 3> {{1.0, 0.0, 0.0}};
	case 2:
		return array<double, 3> {{0.0, 1.0, 0.0}};
	case 3:
		return array<double, 3> {{0.0, 0.0, 1.0}};
	default:
		return array<double, 3> {{0.0, 0.0, 0.0}};
	}
}

} // namespace
} // namespace



