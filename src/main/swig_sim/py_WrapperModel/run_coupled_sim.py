#!/usr/bin/env python

# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.

import logging
import numpy as np
import scipy as sp
import scipy.integrate as spi
from pypts.tissue import Tissue
from pypts.io import from_SimPT
from pypts_sim import ModelDiffusion
import simPT

log = logging.getLogger(__name__)
#log.setLevel(logging.INFO)
logging.basicConfig(level=logging.DEBUG,
            format='%(levelname)s - %(filename)s:%(lineno)s - %(message)s')

def integrate(pysim, timestep=1.0, dt=0.01):
    """
    Uses the current state of the tissue as initial state at t_0 and
    integrates du/dt = f(u,t) for t from 0 to timestep.
    This function uses ode integrators from scipy.integrate
    """
    # Get state vector
    u = pysim.get_variables()

    # scipy expects a function f(t,y), wrap get_residual(y) with a lambda:
    f = lambda t,y: pysim.get_residual(y)
    r = spi.ode(f).set_integrator('vode')
    r.set_initial_value(u, 0.0)
    while r.successful() and r.t < timestep:
        r.integrate(r.t + dt)
        res_norm = np.linalg.norm(pysim.get_residual(r.y))
        log.info('t = {0}, |du/dt| = {1}'.format(r.t, res_norm))

    # Set state with new vector
    pysim.set_variables(r.y)

def step(pysim, sptsim, coupled=True):
    """
    Does one simulation step: pysim, coupling, sptsim, coupling.
    Returns the resulting tissue of both simulations in a tuple.
    """
    # =========================================================================
    # Advance SimPT root model one step forward
    log.info('Running SimPT simulation step')
    sptsim.TimeStep()

    # Retrieve simulation state in XML form
    sptret = sptsim.GetXMLState()
    if sptret.status != simPT.SUCCESS:
        raise RuntimeError('SimPT error: {}'.format(sptret.message))
    sim_state_xml = sptret.value

    # Retrieve simulation state in object form
    sptret = sptsim.GetState()
    if sptret.status != simPT.SUCCESS:
        raise RuntimeError('SimPT error: {}'.format(sptret.message))
    sim_state = sptret.value

    # Retrieve tissue (and associated time & step) from SimPT
    t_spt = from_SimPT(sim_state_xml)
    t_spt.time = sim_state.GetTime()
    t_spt.step = sim_state.GetTimeStep()

    if coupled:
        # Get cell information from simulation state in object form
        mesh_state = sim_state.GetMeshState()
        cell_attr = mesh_state.CellAttributeContainer()
        # The first four cells are the topmost part of the root and interface
        # with the leaf
        spt_areas = np.array(cell_attr.GetAllDouble('area')[4:8])
        chem_0 = np.array(cell_attr.GetAllDouble('chem_0')[4:8])
        chem_1 = np.array(cell_attr.GetAllDouble('chem_1')[4:8])

        # =====================================================================
        # Transfer chem 1 values from SimPT to PyPTS
        # Note: values are amounts and must be rescaled with the areas
        # Note: chem 0 is NOT transferred from root -> leaf
        log.info('Transfer values SimPT -> PyPTS')
        py_areas = np.array([
            pysim.tissue.get_area_of_cell(c_idx) for c_idx in [28,29,30,31]
        ])
        pysim.tissue.cells_attributes['chem_0'][[28,29,30,31]] = np.zeros(4)
        pysim.tissue.cells_attributes['chem_1'][[28,29,30,31]] = py_areas * chem_1 / spt_areas

    # =========================================================================
    # Advance Python leaf model one step forward
    log.info('Running PyPTS simulation step')
    integrate(pysim, timestep=1.0, dt=0.1)

    if coupled:
        # =====================================================================
        # Transfer chem 0 values from PyPTS to SimPT
        # Note: values are amounts and must be rescaled with the areas
        # Note: chem 1 is NOT transferred from leaf -> root
        log.info('Transfer values PyPTS -> SimPT')
        cell_attr.SetDouble(0, 'chem_0', spt_areas[0] * pysim.tissue.cells_attributes['chem_0'][24] / py_areas[0])
        cell_attr.SetDouble(1, 'chem_0', spt_areas[1] * pysim.tissue.cells_attributes['chem_0'][25] / py_areas[1])
        cell_attr.SetDouble(2, 'chem_0', spt_areas[2] * pysim.tissue.cells_attributes['chem_0'][26] / py_areas[2])
        cell_attr.SetDouble(3, 'chem_0', spt_areas[3] * pysim.tissue.cells_attributes['chem_0'][27] / py_areas[3])
        cell_attr.SetDouble(0, 'chem_1', 0.0)
        cell_attr.SetDouble(1, 'chem_1', 0.0)
        cell_attr.SetDouble(2, 'chem_1', 0.0)
        cell_attr.SetDouble(3, 'chem_1', 0.0)
        sim_state.SetMeshState(mesh_state)
        sptsim.Initialize(sim_state)

    return (t_spt, pysim.tissue)

def main():
    import argparse
    # =========================================================================
    # Check command line options
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--leaf', '-l', dest='leaf_file_name',
        help='Input HDF5 file for the PyPTS simulation; the leaf'
    )
    parser.add_argument(
        '--root', '-r', dest='root_file_name',
        help='Input XML file for the SimPT simulation; the root'
    )
    parser.add_argument(
        '--out', '-o',
        help='Base name of the output files: <BASE>_simpt.h5, <BASE>_pypts.h5'
    )
    parser.add_argument(
        '--num_steps', '-n', type=int,
        help='Number of integration steps'
    )
    parser.add_argument('--blind_mode', '-b',
        action='store_true', dest='blind_mode', default=False,
        help='Blind mode: don\'t show any live plots'
    )
    args = parser.parse_args()

    if args.leaf_file_name is None:
        parser.error('Input leaf file is required')
    if args.root_file_name is None:
        parser.error('Input root file is required')
    if args.out == None:
        parser.error('Base output name is required')
    if args.num_steps == None:
        parser.error('Provide a valid number of simulation steps')

    # =========================================================================
    # Set up the simulation in Python
    #
    # Open the tissue from a file
    tissue = Tissue(args.leaf_file_name)

    # Create model equations
    pysim = ModelDiffusion(tissue, {'src_cells': [28, 29, 30, 31]})

    # Set the initial values of the cell-based morphogens
    c_chem0 = np.zeros(pysim.tissue.num_cells)
    c_chem1 = np.zeros(pysim.tissue.num_cells)
    pysim.tissue.cells_attributes['chem_0'] = c_chem0
    pysim.tissue.cells_attributes['chem_1'] = c_chem1

    log.info('Number of equations: {0}'.format(pysim.get_variables().shape[0]))

    # Save initial state at step_0, time = 0.0
    pysim.tissue.save(args.out + '_pypts.h5', 0, 0.0)

    # =========================================================================
    # Set up the simulation in SimPT
    sptsim = simPT.SimWrapper()
    r = sptsim.Initialize(args.root_file_name)
    if r.status != simPT.SUCCESS:
        raise RuntimeError('Error initializing SimPT: {}'.format(r.message))

    # =========================================================================
    # Start a live plot
    if not args.blind_mode:
        import matplotlib.cm as cm
        import matplotlib.pyplot as plt
        import matplotlib.colors as colors
        from pypts.tviz import plot

        fig,ax = plt.subplots(nrows=2, ncols=2, figsize=(14,14))
        for _ax in ax.flat:
            _ax.set_aspect(1.0)
            _ax.hold(True)
            _ax.invert_yaxis()
        plt.show(False)
        plt.draw()
        
    # ========================================================================= 
    # Go!
    for step_idx in xrange(1, args.num_steps + 1):
        # Advance both simulations one step ahead
        t_spt, t_py = step(pysim, sptsim, coupled=True)

        # Save results
        t_py.save(args.out + '_pypts.h5', step_idx, 1.0 * step_idx)
        t_spt.save(args.out + '_simpt.h5', t_spt.step, t_spt.time)

        if not args.blind_mode:
            # Update the live plot
            norm = colors.Normalize(vmin=0.0, vmax=40.0)

            spt_areas = np.array([t_spt.get_area_of_cell(i) for i in t_spt.cells_idx])
            py_areas = np.array([t_py.get_area_of_cell(i) for i in t_py.cells_idx])
            plot(t_spt, axes=ax[0,0], show_cells=True, color_cells_by=t_spt.cells_attributes['chem_0']/spt_areas, norm_cells=norm, cmap_cells=cm.viridis)
            plot(t_py, axes=ax[0,1], show_cells=True, color_cells_by=t_py.cells_attributes['chem_0']/py_areas, norm_cells=norm, cmap_cells=cm.viridis)
            plot(t_spt, axes=ax[1,0], show_cells=True, color_cells_by=t_spt.cells_attributes['chem_1']/spt_areas, norm_cells=norm, cmap_cells=cm.viridis)
            plot(t_py, axes=ax[1,1], show_cells=True, color_cells_by=t_py.cells_attributes['chem_1']/py_areas, norm_cells=norm, cmap_cells=cm.viridis)
            ax[0,0].set_title('Root, chem 0')
            ax[0,1].set_title('Leaf, chem 0')
            ax[1,0].set_title('Root, chem 1')
            ax[1,1].set_title('Leaf, chem 1')
            for _ in ax.flat: _.invert_yaxis()
            fig.canvas.draw()

    if not args.blind_mode:
        plt.close(fig)

if __name__ == "__main__":
    main()

