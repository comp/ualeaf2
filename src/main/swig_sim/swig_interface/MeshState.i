/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * SWIG interface of MeshState.
 */

%module simPT

%include "std_pair.i"
%include "std_string.i"
%include "std_vector.i"
%include "AttributeContainer.i"

%ignore SimPT_Sim::MeshState::PrintToStream(std::ostream&) const;
%ignore SimPT_Sim::MeshState::NodeAttributeContainer() const;
%ignore SimPT_Sim::MeshState::CellAttributeContainer() const;
%ignore SimPT_Sim::MeshState::WallAttributeContainer() const;

%{
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include "bio/MeshState.h"
%}

%include "../../cpp_sim/bio/MeshState.h"

