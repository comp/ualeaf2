/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * SWIG interface of AttributeContainer.
 */

%module simPT

%include "std_pair.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

namespace std {
	%template(PairInt)           pair<int, int>;
	%template(VectorInt)	     vector<int>;
	%template(VectorDouble)	     vector<double>;
	%template(VectorSizeT)	     vector<size_t>;
	%template(VectorString)	     vector<string>;
	%template(MapStringInt)      map<string, vector<int> >;
	%template(MapStringDouble)   map<string, vector<double> >;
	%template(MapStringString)   map<string, vector<string> >;
}

%ignore SimPT_Sim::AttributeContainer::Print(std::ostream&) const;

%{
#include <map>
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include "bio/AttributeContainer.h"
%}

%include "../../cpp_sim/bio/AttributeContainer.h"

namespace SimPT_Sim {

%define ATTRIBUTE_CONTAINER_WRAP(postfix, T...)
%template(Add          ## postfix)  AttributeContainer::Add<T>;
%template(Get          ## postfix)  AttributeContainer::Get<T>;
%template(GetAll       ## postfix)  AttributeContainer::GetAll<T>;
%template(GetNames     ## postfix)  AttributeContainer::GetNames<T>;
%template(IsName       ## postfix)  AttributeContainer::IsName<T>;
%template(Set          ## postfix)  AttributeContainer::Set<T>;
%template(SetAll       ## postfix)  AttributeContainer::SetAll<T>;
%enddef

ATTRIBUTE_CONTAINER_WRAP(Int,    int        )
ATTRIBUTE_CONTAINER_WRAP(Double, double     )
ATTRIBUTE_CONTAINER_WRAP(String, std::string)

}
