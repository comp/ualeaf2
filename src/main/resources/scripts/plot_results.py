#!/usr/bin/python


"""
Elise Kuylen, 2016
Script to create comparative plots for the following leaf properties:
    * cell numbers
    * average cell areas
    * total leaf areas
    * absolute difference target vs actual areas
    * relative difference target vs actual areas
"""


from __future__ import division
import argparse
import h5py
import matplotlib.pyplot as plt
import numpy as np
from pypts.tissue import Tissue
from pypts.tissue import TissueSeries
from pypts.tviz import plot
import sys


def plot_results(xs, ys, xlabel, ylabel, title, legend, markers, log_scale=False):
    """
    """
    if  len(xs) != len(ys) :
        print "x and y dimensions different!"
        print len(xs)
        print len(ys)
        return

    for i in range(len(xs)):
        plt.plot(xs[i], ys[i], markers[i])

    if log_scale:
        plt.yscale('log')
        plt.xlim((0, max(xs[0]) + 6))

    if (title == 'leaf_areas'):
        plt.ylim((0, 10**3))

    plt.xlabel(xlabel, fontsize=18)
    plt.ylabel(ylabel, fontsize=18)
    #plt.title(title)
    plt.xticks(np.arange(0, max(xs[0])+6, 5.0), fontsize=16)
    plt.yticks(fontsize=16)
    plt.legend(legend, loc=2)

    plt.savefig(title)
    plt.clf()


def main(leafs, title, legend):
    """
    """
    num_leafs = len(leafs)

    # experimental data
    # Kheibarshekan et al., 
    # Model-Based Analysis of Arabidopsis Leaf Epidermal Cells Reveals Distinct Division and Expansion Patterns for Pavement and Guard Cells
    x_paper = range(5, 26)
    num_cells_paper = [11.6, 19.4, 35.6, 44.4, 48., 63.5, 65.3, 66.7, 69.1, 72.7, 73., 71.3, 72.7, 73.7, 72., 70.2, 73.7, 72., 73.7, 72.3, 73.7]
    num_cells_paper = [10 ** (2 + (x / 31.)) for x in num_cells_paper]
    cell_areas_paper = [26.5, 28.9, 30.7, 34.9, 40.9, 39.9, 53.3, 54.3, 55.7, 57.9, 63.5, 65.3, 66.7, 67.7, 69.1, 67.4, 65.6, 70.5, 69.7, 68.1, 69.6]
    cell_areas_paper = [10 ** (1 + (x / 33)) for x in cell_areas_paper]
    leaf_areas_paper = [4.2, 12.3, 26.5, 35.6, 43.4, 54.3, 65.3, 67.4, 70.9, 74.8, 79.7, 79.7, 81.8, 83.3, 83.6, 80.4, 81.8, 83.6, 84.7, 82.2, 84.3]
    leaf_areas_paper = [10 ** (-2 + (x / 24)) for x in leaf_areas_paper]

    num_cells = [ [] for i in range(num_leafs) ]
    cell_areas = [ [] for i in range(num_leafs) ]
    total_areas = [ [] for i in range(num_leafs) ]
    abs_diffs = [ [] for i in range(num_leafs) ]
    rel_diffs = [ [] for i in range(num_leafs) ]
    
    # Read data from h5 files
    for i in range(num_leafs):
        print leafs[i]
        tissue_series = TissueSeries(leafs[i])
        for j in range(1, 127):
            tissue = tissue_series[j]
            # get number of cells (x 8 since every cell in blad model represents 8 cells in reality)
            curr_num_cells = tissue.num_cells * 8
            num_cells[i].append(curr_num_cells)

            # get average cell area (1 length unit = 1.5 micron)
            areas = [x * 2.25 for x in tissue.cells_attributes['area']]
            curr_avg_area = np.mean(areas) / 8
            cell_areas[i].append(curr_avg_area)

            # leaf area (divide by 10**6 since paper has leaf area in mm**2 instead of micron)
            curr_total_area = sum(areas) / (10 ** 6)
            total_areas[i].append(curr_total_area)

            # target vs actual areas
            #target_areas = [x * 2.25 for x in tissue.cells_attributes['target_area']]
            #abs_diff = np.mean([abs(x - y) for x,y in zip(areas, target_areas)])
            #rel_diff = np.mean([abs(x - y) / y for x,y in zip(areas, target_areas)])
            #abs_diffs[i].append(abs_diff)
            #rel_diffs[i].append(rel_diff)

            #if j == 96:
            #    # 16 days after sowing
            #    areas = [x / 8. for x in areas]
            #   plt.hist(areas, 50, normed=1, facecolor='green', alpha=0.75)
            #    plt.xlabel('cell size')
            #    plt.ylabel('distribution')
            #    plt.title(title + ': cell size division, 16 d.a.s.')
            #    plt.savefig(title + ': cell size division, 16 d.a.s.')
            #    plt.clf()

    # timesteps to 'days after sowing'
    timesteps = range(25, 151)
    timesteps = [x / 6. for x in timesteps]

    # construct plots
    xs_no_paper = [timesteps] * num_leafs
    xs_with_paper = [x_paper] + xs_no_paper

    markers_no_paper = ['k+', 'kx', 'k.', 'k*']
    markers_with_paper = ['ko'] + markers_no_paper

    legend_paper = 'data from Kheibarshekan Asl et al. (2011)'

    plot_results(xs_with_paper, [num_cells_paper] + num_cells, 'days after sowing', 'number of cells', 'cell_numbers', [legend_paper] + legend, markers_with_paper, True)
    plot_results(xs_with_paper, [cell_areas_paper] + cell_areas, 'days after sowing', r'average cell area ($\mu m^2$)', 'cell_areas', [legend_paper] + legend, markers_with_paper, True)
    plot_results(xs_with_paper, [leaf_areas_paper] + total_areas, 'days after sowing', r'leaf area ($mm^2$)','leaf_areas', [legend_paper] + legend, markers_with_paper, True)

    #plot_results(xs_no_paper, abs_diffs, 'days after sowing', 'absolute difference', title + ': absolute difference actual-target areas', legend, markers_no_paper)
    #plot_results(xs_no_paper, rel_diffs, 'days after sowing', 'relative difference', title + ': relative difference actual-target areas', legend, markers_no_paper)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--leafs', nargs='+')
    parser.add_argument('--title')
    parser.add_argument('--legend', nargs='+')

    args = parser.parse_args()
    main(args.leafs, args.title, args.legend)
