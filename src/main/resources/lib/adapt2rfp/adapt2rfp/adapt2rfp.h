#ifndef ADAPT2RFP_H_INCLUDED
#define ADAPT2RFP_H_INCLUDED
/**
 * @file
 * The interface and implementation of the adapt2rfp project.
 * @author  J. Broeckhove, Kurt Vanmechelen, 2011, UA/CoMP
 */

#include <map>
#include <functional>
#include <type_traits>

namespace UA_CoMP_Adapt2rfp {

#if( 1500 <= _MSC_VER  &&  -MSC_VER < 1600 )
	using std::tr1::function;
	using std::tr1::bad_function_call;
#else
    using std::function;
    using std::bad_function_call;
#endif
    using std::map;

    namespace Implementation {

        //----------------------------------------------------------------------------
        // Pick the type for CallSignature.
        //----------------------------------------------------------------------------
        /**
         * Primary class template for the Pick class that deals with call
         * type transformation of an individual argument or return type.
         * This is aimed at dealing with the situation
         * where a callable entity has an argument or return by value which
         * needs to be transformed into an argument (or return) by address.
         * For example void f(double) --> void f(double*) where T1 is
         * double and T2 double*. It is OK to make this transformation in
         * the context of adapt2rfp because we interpose a dereference of
         * address before handing over the argument to the target function.
         * So we need to handle
         * \li T1 --> T2 whenever these are non-pointer types with T1 convertible to T2,
         * in which case we let built-in conversion take place
         * \li T1 --> T2 whenever both are pointer types with identical base types, in
         * which case the argument simply passes through
         * \li T1 --> T2 whenever T1 is a pointer type whose base type is convertible to T2,
         * in which case we dereference the pointer and let built-in conversion take place
         * This is implemented via a primary template to handle the generic situation and
         * two specializations that handle cases where pointer types are involved.
         */
        template<typename T1, typename T2>
        struct Pick
        {

            typedef T1 type;
            /// Provide an eval function that simply converts for non-pointer types.
            struct Arg { static T2 eval(T1 t1) {return t1;} };
        };

        /**
         * Specialization of Pick for identical pointer types. For more information
         * see the primary Pick class template.
         */
        template<typename T1>
        struct Pick<T1*, T1*>
        {
            typedef T1* type;
            /// Provide an eval function that does nothing for identical pointers.
            struct Arg { static T1* eval(T1* t1) {return t1;} };
        };

        /**
         * Specialization of Pick for pointer and non-pointer type. For more information
         * see the primary Pick class template.
         */
        template<typename T1, typename T2>
        struct Pick<T1*, T2>
        {
            typedef T1 type;
            /// Provide an eval function that dereferences pointer and passes on value for conversion.
            struct Arg { static T2 eval(T1* t1) {return *t1;} };
        };

        //----------------------------------------------------------------------------
        // CallSignature class template.
        //----------------------------------------------------------------------------
        /**
         * Primary class template for dealing with type transformation of the
         * call type, that is return type and full argument list.
         * The primary is empty, the template only has specializations, for
         * increasing numbers of parameters in the call signature.
         */
        template<typename F1, typename F2>
        struct CallSignature {}; // Empty body to trigger documentation, for compiler no body is fine

        /**
         * CallSignature for 0-parameter signatures.
         */
        template<typename R, typename S>
        struct CallSignature<R(), S()>
        {
            typedef R      ret_t;
            typedef ret_t(type)();
        };

        /**
         * CallSignature for 1-parameter signatures.
         */
        template<typename R, typename S, typename P1, typename Q1>
        struct CallSignature<R(P1), S(Q1)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef ret_t(type)(arg1_t);
        };

        /**
         * CallSignature for 2-parameter signatures.
         */
        template<typename R, typename S, typename P1, typename Q1, typename P2, typename Q2>
        struct CallSignature<R(P1,P2), S(Q1,Q2)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef ret_t(type)(arg1_t, arg2_t);
        };

        /**
         * CallSignature for 3-parameter signatures.
         */
        template<typename R,  typename S, typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3>
        struct CallSignature<R(P1,P2,P3), S(Q1,Q2,Q3)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t);
        };

        /**
         * CallSignature for 4-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4>
        struct CallSignature<R(P1,P2,P3,P4), S(Q1,Q2,Q3,Q4)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t);
        };

        /**
         * CallSignature for 5-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5>
        struct CallSignature<R(P1,P2,P3,P4,P5), S(Q1,Q2,Q3,Q4,Q5)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef typename Pick<P5, Q5>::type   arg5_t;
            typedef typename Pick<P5, Q5>::Arg    arg5_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t, arg5_t);
        };

        /**
         * CallSignature for 6-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6>
        struct CallSignature<R(P1,P2,P3,P4,P5,P6), S(Q1,Q2,Q3,Q4,Q5,Q6)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef typename Pick<P5, Q5>::type   arg5_t;
            typedef typename Pick<P5, Q5>::Arg    arg5_f;
            typedef typename Pick<P6, Q6>::type   arg6_t;
            typedef typename Pick<P6, Q6>::Arg    arg6_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t, arg5_t, arg6_t);
        };

        /**
         * CallSignature for 7-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7>
        struct CallSignature<R(P1,P2,P3,P4,P5,P6,P7), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef typename Pick<P5, Q5>::type   arg5_t;
            typedef typename Pick<P5, Q5>::Arg    arg5_f;
            typedef typename Pick<P6, Q6>::type   arg6_t;
            typedef typename Pick<P6, Q6>::Arg    arg6_f;
            typedef typename Pick<P7, Q7>::type   arg7_t;
            typedef typename Pick<P7, Q7>::Arg    arg7_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t, arg5_t, arg6_t, arg7_t);
        };

        /**
         * CallSignature for 8-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8>
        struct CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef typename Pick<P5, Q5>::type   arg5_t;
            typedef typename Pick<P5, Q5>::Arg    arg5_f;
            typedef typename Pick<P6, Q6>::type   arg6_t;
            typedef typename Pick<P6, Q6>::Arg    arg6_f;
            typedef typename Pick<P7, Q7>::type   arg7_t;
            typedef typename Pick<P7, Q7>::Arg    arg7_f;
            typedef typename Pick<P8, Q8>::type   arg8_t;
            typedef typename Pick<P8, Q8>::Arg    arg8_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t, arg5_t, arg6_t, arg7_t, arg8_t);
        };

        /**
         * CallSignature for 9-parameter signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8,
                 typename P9, typename Q9>
        struct CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8,P9), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9)>
        {
            typedef R      ret_t;
            typedef typename Pick<P1, Q1>::type   arg1_t;
            typedef typename Pick<P1, Q1>::Arg    arg1_f;
            typedef typename Pick<P2, Q2>::type   arg2_t;
            typedef typename Pick<P2, Q2>::Arg    arg2_f;
            typedef typename Pick<P3, Q3>::type   arg3_t;
            typedef typename Pick<P3, Q3>::Arg    arg3_f;
            typedef typename Pick<P4, Q4>::type   arg4_t;
            typedef typename Pick<P4, Q4>::Arg    arg4_f;
            typedef typename Pick<P5, Q5>::type   arg5_t;
            typedef typename Pick<P5, Q5>::Arg    arg5_f;
            typedef typename Pick<P6, Q6>::type   arg6_t;
            typedef typename Pick<P6, Q6>::Arg    arg6_f;
            typedef typename Pick<P7, Q7>::type   arg7_t;
            typedef typename Pick<P7, Q7>::Arg    arg7_f;
            typedef typename Pick<P8, Q8>::type   arg8_t;
            typedef typename Pick<P8, Q8>::Arg    arg8_f;
            typedef typename Pick<P9, Q9>::type   arg9_t;
            typedef typename Pick<P9, Q9>::Arg    arg9_f;
            typedef ret_t(type)(arg1_t, arg2_t, arg3_t, arg4_t, arg5_t, arg6_t, arg7_t, arg8_t, arg9_t);
        };

        //----------------------------------------------------------------------------
        // Wrapper class template.
        //----------------------------------------------------------------------------
        /**
         * The Wrapper class holds the target function that actually executes the
         * call as a data member and has a static member that forwards calls to
         * this target function. It is the address of this static member function
         * that is the raw function pointer used for the callback. The Wrapper
         * only implements specializations, for increasing number of parameters
         * in the call signature.
         */
        template<typename F1, typename F2, unsigned int C, unsigned int I>
        struct Wrapper {}; // Empty body to trigger documentation, for compiler no body is fine

        /**
         * Wrapper for 0-parameter call signatures.
         */
        template<typename R, typename S, unsigned int C, unsigned int I>
        struct Wrapper<R(), S(), C, I>
        {
            typedef CallSignature<R(), S()> TT;
            static function<typename TT::type> fgTarget;
            static R call()
            {
                return fgTarget();
            }
        };

        /// Static data member definition for Wrapper for 0-parameters.
        template<typename R, typename S, unsigned int C, unsigned int I>
        function<typename CallSignature<R(), S()>::type>
        Wrapper<R(), S(), C, I>::fgTarget;

        /**
         * Wrapper for 1-parameter call signatures.
         */
        template<typename R, typename S, typename P1, typename Q1, unsigned int C, unsigned int I>
        struct Wrapper<R(P1), S(Q1), C, I>
        {
            typedef CallSignature<R(P1), S(Q1)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1)
            {
            	return fgTarget(TT::arg1_f::eval(p1));
            }
        };

        /// Static data member definition for Wrapper for 1-parameters.
        template<typename R, typename S, typename P1, typename Q1, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1), S(Q1)>::type>
        Wrapper<R(P1), S(Q1), C, I>::fgTarget;

        /**
         *  Wrapper for 2-parameter call signatures.
         */
        template<typename R, typename S, typename P1, typename Q1, typename P2, typename Q2,
                 unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2), S(Q1,Q2), C, I>
        {
            typedef CallSignature<R(P1,P2), S(Q1,Q2)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2));
            }
        };

        /// Static data member definition for Wrapper for 2-parameters.
        template<typename R, typename S, typename P1, typename Q1, typename P2, typename Q2,
                 unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2), S(Q1,Q2)>::type>
        Wrapper<R(P1,P2), S(Q1,Q2), C, I>::fgTarget;

        /**
         * Wrapper for 3-parameter call signatures.
         */
        template<typename R,  typename S, typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3), S(Q1,Q2,Q3), C, I>
        {
            typedef CallSignature<R(P1,P2,P3), S(Q1,Q2,Q3)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3));
            }
        };

        /// Static data member definition for Wrapper for 3-parameters.
        template<typename R,  typename S, typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3), S(Q1,Q2,Q3)>::type>
        Wrapper<R(P1,P2,P3), S(Q1,Q2,Q3), C, I>::fgTarget;

        /**
         * Wrapper for 4-parameter call signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4), S(Q1,Q2,Q3,Q4), C, I>
        {
        	typedef CallSignature<R(P1,P2,P3,P4), S(Q1,Q2,Q3,Q4)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4));
            }
        };

        /// Static data member definition for Wrapper for 4-parameters.
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4), S(Q1,Q2,Q3,Q4)>::type>
        Wrapper<R(P1,P2,P3,P4), S(Q1,Q2,Q3,Q4), C, I>::fgTarget;

        /**
         * Wrapper for 5-parameter call signatures.
         */
        template<typename R, typename S,  typename P1, typename Q1, typename P2, typename Q2,
        		typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
        		unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4,P5), S(Q1,Q2,Q3,Q4,Q5), C, I>
        {
            typedef CallSignature<R(P1,P2,P3,P4,P5), S(Q1,Q2,Q3,Q4,Q5)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4), TT::arg5_f::eval(p5));
            }
        };

        /// Static data member definition for Wrapper for 5-parameters.
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4,P5), S(Q1,Q2,Q3,Q4,Q5)>::type>
        Wrapper<R(P1,P2,P3,P4,P5), S(Q1,Q2,Q3,Q4,Q5), C, I>::fgTarget;

        /**
         * Wrapper for 6-parameter call signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4,P5,P6), S(Q1,Q2,Q3,Q4,Q5,Q6), C, I>
        {
        	typedef CallSignature<R(P1,P2,P3,P4,P5,P6), S(Q1,Q2,Q3,Q4,Q5,Q6)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4), TT::arg5_f::eval(p5), TT::arg6_f::eval(p6));
            }
        };

        /// Static data member definition for Wrapper for 6-parameters.
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4,P5,P6), S(Q1,Q2,Q3,Q4,Q5,Q6)>::type>
        Wrapper<R(P1,P2,P3,P4,P5,P6), S(Q1,Q2,Q3,Q4,Q5,Q6), C, I>::fgTarget;

        /**
         * Wrapper for 7-parameter call signatures.
         */
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4,P5,P6,P7), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7), C, I>
        {
            typedef CallSignature<R(P1,P2,P3,P4,P5,P6,P7), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6, P7 p7)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4), TT::arg5_f::eval(p5), TT::arg6_f::eval(p6),
                                TT::arg7_f::eval(p7));
            }
        };

        /// Static data member definition for Wrapper for 7-parameters.
        template<typename R,  typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4,P5,P6,P7), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7)>::type>
        Wrapper<R(P1,P2,P3,P4,P5,P6,P7), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7), C, I>::fgTarget;

        /**
         * Wrapper for 8-parameter call signatures.
         */
        template<typename R, typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8,
                 unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4,P5,P6,P7,P8), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8), C, I>
        {
            typedef CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6, P7 p7, P8 p8)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4), TT::arg5_f::eval(p5), TT::arg6_f::eval(p6),
                                TT::arg7_f::eval(p7), TT::arg8_f::eval(p8));
            }
        };

        /// Static data member definition for Wrapper for 8-parameters.
        template<typename R, typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8,
                 unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8)>::type>
        Wrapper<R(P1,P2,P3,P4,P5,P6,P7,P8), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8), C, I>::fgTarget;

        /**
         * Wrapper for 9-parameter call signatures.
         */
        template<typename R, typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8,
                 typename P9, typename Q9, unsigned int C, unsigned int I>
        struct Wrapper<R(P1,P2,P3,P4,P5,P6,P7,P8,P9), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9), C, I>
        {
            typedef CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8,P9), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9)> TT;
            static function<typename TT::type> fgTarget;
            static R call(P1 p1, P2 p2, P3 p3, P4 p4, P5 p5, P6 p6, P7 p7, P8 p8, P9 p9)
            {
                return fgTarget(TT::arg1_f::eval(p1), TT::arg2_f::eval(p2), TT::arg3_f::eval(p3),
                                TT::arg4_f::eval(p4), TT::arg5_f::eval(p5), TT::arg6_f::eval(p6),
                                TT::arg7_f::eval(p7), TT::arg8_f::eval(p8), TT::arg9_f::eval(p9));
            }
        };

        /// Static data member definition for Wrapper for 9-parameters.
        template<typename R, typename S,  typename P1, typename Q1, typename P2, typename Q2,
                 typename P3, typename Q3, typename P4, typename Q4, typename P5, typename Q5,
                 typename P6, typename Q6, typename P7, typename Q7, typename P8, typename Q8,
                 typename P9, typename Q9, unsigned int C, unsigned int I>
        function<typename CallSignature<R(P1,P2,P3,P4,P5,P6,P7,P8,P9), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9)>::type>
        Wrapper<R(P1,P2,P3,P4,P5,P6,P7,P8,P9), S(Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9), C, I>::fgTarget;

        //----------------------------------------------------------------------------
        // WrapperMapFiller and WrapperMap
        //----------------------------------------------------------------------------
        /**
         * The WrapperMapFiller class template is built to recursively instantiate
         * a number of C copies, each instance distinguished by the value of
         * template parameter I that controls the recursion. Each instance itself
         * instantiates a Wrapper class (again all of them distinguished by template
         * parameter I). It then enters the address of that Wrapper's static member
         * function "call" (<-- raw function pointer) and the address of the
         * Wrapper's static data member (<-- target function) into the map as a pair.
         * All of this happens in the WrapperMapFiller constructor call which
         * gets executed for each generated instance.
         */
        template<typename F1, typename F2, unsigned int C, unsigned int I>
        struct WrapperMapFiller
        {
            /**
             * Supply the the entries to the map.
             */
            WrapperMapFiller(map<F1*, function<F2>*>& wmap)
            {
                wmap[&Wrapper<F1, F2, C, I>::call] = &Wrapper<F1, F2, C, I>::fgTarget;
                WrapperMapFiller<F1, F2, C, I - 1> irrelevant_name(wmap);
            }
        };

        /**
         * Termination of the recursive instantiation triggered by the
         * WrapperMapFiller class template.
         */
        template<typename F1, typename F2, unsigned int C>
        struct WrapperMapFiller<F1, F2, C, 0> { WrapperMapFiller(map<F1*, function<F2>*>&) {} };

        /**
         * WrapperMap is the data structure that contains all the wrapper instances
         * that are generated at compile time. The number of wrappers that gets
         * generated is determined by template parameter C (stands for capacity).
         * The entries of the map are pairs of (raw function pointer, target function).
         */
        template<typename F1, typename F2, unsigned int C>
        struct WrapperMap : public map<F1*, function<F2>*>
        {
            /**
             * Instantiation of constructor triggers instantiation of
             * the recursive WrapperMapFiller algorithm that fills the map.
             */
        	WrapperMap() { WrapperMapFiller<F1, F2, C, C>(*this); }
        };

        //----------------------------------------------------------------------------
        // InUseMapFiller and InUseMap
        //----------------------------------------------------------------------------
        /**
         * The InUseMapFiller class template functions w.r.t the InUseMap in a
         * similar way as does the WrapperMapFiller to the WrapperMap. In this
         * case the static member function address is entered together with a
         * a flag indicating that the raw pointer is not currently in use.
         */
        template<typename F1, typename F2, unsigned int C, unsigned int I>
        struct InUseMapFiller
        {
            /**
             * Supply the the entries to the map.
             */
            InUseMapFiller(map<F1*, bool>& imap)
            {
                imap[&Wrapper<F1, F2, C, I>::call] = false;
                InUseMapFiller<F1, F2, C, I - 1> irrelevant_name(imap);
            }
        };

        /**
         * Termination of the recursive instantiation triggered by the
         * WrapperMapFiller class template.
         */
        template<typename F1, typename F2, unsigned int C>
        struct InUseMapFiller<F1, F2, C, 0> { InUseMapFiller(map<F1*, bool>&) {} };


        /**
         * The InUseMap functions in combination with the WrapperMap. The InUseMap
         * holds flags indicating which entries of the WrapperMap are currently
         * in use meaning the corresponding raw function pointer has been acquired
         * by an adaptor and should not be reused until that adaptor has released
         * the entry.
         */
        template<typename F1, typename F2, unsigned int C>
        struct InUseMap : public map<F1*, bool>
        {
        	/**
        	 * Instantiation of constructor triggers instantiation of
        	 * the recursive InUseMapFiller algorithm that fills the map.
        	 */
        	InUseMap() { InUseMapFiller<F1, F2, C, C>(*this); }
        };

        //----------------------------------------------------------------------------
        // WrapperPool
        //----------------------------------------------------------------------------
        /**
         * The WrapperPool is the class that manages the wrappers through
         * the WrapperMap and InUseMap. It has a straightforward interface
         * allowing you to find out whether wrappers are available, how many,
         * acquiring (= taking temporary exclusive ownership) a wrapper,
         * releasing it (= relinquishing ownership), binding it to a target
         * function, querying whether a raw pointer is managed by the pool, etc.
         */
        template<typename F1, typename F2, unsigned int C = 5>
        class WrapperPool
        {
        public:

            /**
             * Expose the function type of the raw function pointers.
             */
            typedef F1 FunctionType;

            /**
             * Return the capacity of the pool (number of entries it holds) which
             * is also the maximum number of Adaptors that can be instantiated.
             */
            static unsigned int getCapacity() { return C; }

            /**
             * Return the number of wrappers that is still available which is also
             * the number of Adaptors that can still be instantiated at this point.
             */
            static unsigned int getAvailable()
            {
                unsigned int count = 0;
                IMap& imap = getIMapInstance();
                for (typename IMap::iterator it = imap.begin(); it != imap.end(); ++it)
                {
                    if ( (*it).second == false ) count++;
                }
                return count;
            }

            /**
             * Returns true iff the pointer is associated with a wrapper in this pool.
             */
            static bool isInPool(F1* p) { return (getIMapInstance().count(p) == 1); }

            /**
             * Returns true iff the pointer argument is (a) isInPool and (b) being used.
             */
            static bool isInUse(F1* p) { return isInPool(p) && (getIMapInstance()[p]==true); }

            /**
             * Returns true iff the pointer argument is (a) inUse and (b) its target function
             * is not empty. This guarantees that a call through the pointer will succeed.
             */
            static bool hasTarget(F1* p) { return isInUse(p) && (*(getWMapInstance()[p]) != 0); }

            /**
             * Acquires ownership of a wrapper, that is, flags the wrapper as being in use
             * and returns its associated call pointer. If no wrappers are available (all of
             * them are in use) then return a null pointer.
             */
            static F1* acquire()
            {
                F1* p = 0;
                IMap& imap = getIMapInstance();
                for (typename IMap::iterator it = imap.begin(); it != imap.end(); ++it)
                {
                    if ( (*it).second == false )
                    {
                        (*it).second = true;
                        p = (*it).first;
                        *(getWMapInstance()[p]) = function<F2>();
                        break;
                    }
                }
                return p;
            }

            /**
             * Releases the wrapper associated with the pointer argument and for good
             * measure set the target function to a null (empty) function. If the
             * pointer is not in the pool, do nothing and return false. Otherwise
             * return true. Calling release twice in succession is not a problem.
             */
            static bool release(F1* p)
            {
                bool stat = isInPool(p);
                if (stat)
                {
                    getIMapInstance()[p] = false;
                    *(getWMapInstance()[p]) = function<F2>();
                }
                return stat;
            }

            /**
             * Sets the the target function for the wrapper associated with pointer
             * argument. If the pointer is not in use, a null pointer is returned.
             * Otherwise the pointer itself is returned.
             */
            static F1* bind(F1* p, function<F2> f)
            {
                if (isInUse(p)) { *(getWMapInstance()[p]) = f; }
                else { p = 0; }
                return p;
            }

            /**
             * Acquires a wrapper and binds its target to the function argument.
             * If acquiring a wrapper fails return a null pointer; if it succeeds
             * return the raw function pointer associated with the wrapper.
             */
            static F1* adapt(function<F2> f) { return bind(acquire(), f); }

        private:

            /// Inhibit default constructor to emphasize static character of the class.
            WrapperPool();

            /// Inhibit copy constructor to emphasize static character of the class.
            WrapperPool(WrapperPool const&);

            /// Inhibit copy assignment to emphasize static character of the class.
            WrapperPool& operator=(WrapperPool const&);

            /// Typedef for short hand notation.
            typedef WrapperMap<F1, F2, C> WMap;

            /// WrapperMap as a Meyer's singleton.
            static WMap& getWMapInstance() { static WMap wmap; return wmap; }

            /// Typedef for shorthand notation.
            typedef InUseMap<F1, F2, C> IMap;

            /// InUseMap as a Meyer's singleton.
            static IMap& getIMapInstance() { static IMap imap; return imap; }
        };

    } //end of namespace Implementation

    //----------------------------------------------------------------------------
    // Adaptor
    //----------------------------------------------------------------------------
    /**
     * The Adaptor class is an abstraction for managing access to the raw
     * pointers and dealing with acquisition/release when leaving scope
     * and so on. It supplies "resource acquisition is initialization"
     * semantics, the resource being (exclusive) ownership of an entry in the
     * WrapperMap. The implementation interacts with the WrapperPool in a
     * straightforward manner.
     */
    template< typename F1, typename F2 = F1, unsigned int C = 5>
    class Adaptor
    {
    private:

        /// Inhibit the copy constructor.
        Adaptor(Adaptor const&);

        /// Inhibit the copy assignment.
        Adaptor& operator=(Adaptor const&);

        /// Typedef for shorthand notation of target call signature.
        typedef typename Implementation::CallSignature<F1, F2>::type  BF2;

        /// Typedef for shorthand notation in the implementations.
        typedef Implementation::WrapperPool<F1, BF2, C> WPool;

        /// Raw function pointer, bound to target function through WrapperPool.
        F1* fPtr;

    public:

        /**
         * Expose the function type of the raw function pointers.
         */
        typedef F1 FunctionType;

        /**
         * Instantiate an adaptor that binds a target function to a raw function pointer.
         * @param   f       target function adaptor binds to (defaults to empty function)
         */
        explicit Adaptor(function<BF2> f = function<BF2>()) : fPtr(WPool::adapt(f)) {}

        /**
         * Release the entry in the WrapperPool.
         */
        ~Adaptor() { WPool::release(fPtr); }
        
        /*
         * Get the raw function pointer of which this adaptor is the exclusive owner.
         * @return          raw function pointer
         */
        F1* get() const { return fPtr; }

        /**
         * Conversion of adaptor to raw function pointer.
         * @return          raw function pointer
         */
        operator F1*() const { return fPtr; }

        /**
         * Set the target function f and return the adaptor itself (allows you to chain
         * expressions). The absence of a default is intentional.
         * @param   f       target function you are binding the adaptor to
         * @return          adaptor reference
         */
        Adaptor& bind(function<BF2> f) { WPool::bind(fPtr, f); return *this; }

        /**
         * Dissolve binding with target function (used when you need to ensure that calls
         * will not be forwarded to that target function). Achieves this by binding to
         * empty function.
         * @return          adaptor reference
         */
        Adaptor& unbind() { WPool::bind(fPtr, function<BF2>()); return *this; }

        /**
         * Check whether target function of the adaptor is non-empty.
         * @return          true iff target function non-empty
         */
        bool hasTarget() const { return WPool::hasTarget(fPtr); }

        /**
         * Return maximum number of Adaptors that can be instantiated simultaneously.
         * @return          number of adaptors that can co-exist at any one time
         */
        static unsigned int getCapacity() { return C; }

        /**
         * Return the number of Adaptors that can still be instantiated at this point.
         * @return          number of adaptors still available at this time
         */
        static unsigned int getAvailable() { return WPool::getAvailable(); }
    };

} //end of namespace UA_CoMP_Adapt2rfp

#endif  // end of include guard

