/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef VTK_SIMPT_READER_H
#define VTK_SIMPT_READER_H

// VTK includes
#include "vtkPolyDataAlgorithm.h"
// STD includes
#include <vector>
#include <stdexcept>
// HDF5 includes
#include <hdf5.h>

/**
 * Implements a ParaView reader for SimPT files based on the HDF5 data format
 *
 * This reader produces an irregular collection of polygons (= SimPT cells)
 * with the associated cell/edge based data. The vtk algorithm that generates
 * vtkPolyData is therefore used as a base class. Look at vtkH5PartReader for
 * some reference on how things work.
 */
class VTK_EXPORT vtkSimPTReader : public vtkPolyDataAlgorithm {
    public:
        static vtkSimPTReader *New();

        vtkTypeMacro(vtkSimPTReader,vtkPolyDataAlgorithm);

        /**
         * Prints object's information to output stream os
         */
        void PrintSelf(ostream& os, vtkIndent indent);

        /**
         * VTK macro to generate the member:
         * virtual char* GetFileName();
         */
        vtkSetStringMacro(FileName);

        /**
         * VTK macro to generate the member:
         * virtual void SetFileName(const char*);
         */
        vtkGetStringMacro(FileName);

    protected:
        /*
         * As is customary for vtk classes, construction and destruction is
         * handled by ::New() and ::Delete() members.
         * VTK takes care of this, see:
         *  vtkStandardNewMacro(vtkSimPTReader);
         * in vtkSimPTReader.cpp
         */
        vtkSimPTReader();
        ~vtkSimPTReader();

        virtual int RequestInformation(vtkInformation* request, \
                                       vtkInformationVector** inputVector, \
                                       vtkInformationVector* outputVector);

        virtual int RequestData(vtkInformation* request, \
                                vtkInformationVector** inputVector, \
                                vtkInformationVector* outputVector);

        /**
         * Determines if the file specified by file_name can be read by this
         * reader. TODO: I'm not sure this is actually necessary and/or used
         */
        //virtual int CanReadFile(const char* file_name);

        /// The file name variable that will be set by the macros
        /// vtk[G,S]etStringMacro
        char* FileName;                                                         

    private:
        /// Not implemented, vtk takes care of these
        vtkSimPTReader(const vtkSimPTReader&);
        void operator=(const vtkSimPTReader&);                                  

        /// Current time step (index)
        int TimeStep;
        /// Time values
        std::vector<double> TimeStepValues;
        std::vector<int> m_time_steps_idx;
        /// ID of the HDF5 file as used by libhdf5
        hid_t m_file_id;
};

#endif /* VTK_SIMPT_READER_H */

