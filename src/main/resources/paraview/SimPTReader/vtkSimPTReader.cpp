/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// Local includes
#include "vtkSimPTReader.h"
#include "hdf5util.h"
// VTK includes
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkPoints.h"
#include "vtkLine.h"
#include "vtkPolygon.h"
#include "vtkCellArray.h"
#include "vtkDoubleArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkSmartPointer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
// STD includes
#include <algorithm>
#include <vector>
#include <string>
#include <list>
#include <map>

using std::string;
using std::vector;
using std::list;
using std::map;
using std::runtime_error;
using std::to_string;

/**
 * Fallback for boost::starts_with since I don't want to introduce boost as a
 * dependency for just one function.
 */
bool starts_with(const std::string & str, const std::string & prefix) {
    return (prefix.size() <= str.size()
            && std::equal(prefix.begin(), prefix.end(), str.begin()));
}

/**
 * Predicate that returns true if given values a and b are equal within a
 * specified tolerance tol
 */
struct ToleranceCheck : public std::binary_function<double, double, bool> {
    double m_tol;
    ToleranceCheck(double tol) : m_tol(tol) {}
    result_type operator()(first_argument_type a, \
                           second_argument_type b) const {
        return fabs(a-b) <= m_tol;
    }
};


vtkObjectFactoryNewMacro(vtkSimPTReader);


vtkSimPTReader::vtkSimPTReader() :
    FileName(0), TimeStep(0) {
    // Turn on reporting of vtkDebugMacro statements
    //this->DebugOn();
    // Readers have no input and one output
    this->SetNumberOfInputPorts(0);
    this->SetNumberOfOutputPorts(1);
}


vtkSimPTReader::~vtkSimPTReader() {
    if (this->FileName) {
        vtkDebugMacro(<< "Closing SimPT file: " << this->FileName);
        H5Fclose(m_file_id);
    }
    this->SetFileName(0);
    //this->DebugOff();
}


void vtkSimPTReader::PrintSelf(ostream& os, vtkIndent indent) {
    this->Superclass::PrintSelf(os, indent);
    os << indent << "FileName: " \
       << (this->FileName ? this->FileName : "(none)") << "\n";
}


int vtkSimPTReader::RequestInformation(vtkInformation* vtkNotUsed(request),
                                       vtkInformationVector** vtkNotUsed(inputVector),
                                       vtkInformationVector* outputVector) {
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    // TODO: what's this?
    //outInfo->Set(vtkStreamingDemandDrivenPipeline::MAXIMUM_NUMBER_OF_PIECES(), -1);

    // [HDF5] =================================================================
    // Try to open the HDF5 file specified by this->FileName in read-only  mode
    //vtkDebugMacro(<< "Opening SimPT file: " << this->FileName);
    m_file_id = H5Fopen(this->FileName, H5F_ACC_RDONLY, H5P_DEFAULT);
        if (m_file_id < 0) { vtkErrorMacro(<< "Could not open SimPT file: " << this->FileName); return 0; }

    // Sizes
    size_t num_time_steps = 0;

    // RAW data that will be filled up with HDF5 readers and fed to VTK
    double* time_steps_data = 0;
    int* time_steps_idx_data = 0;

    // Attempt to read raw data from HDF5
    try {
		size_t time_steps_dim1 = 0;
		size_t time_steps_idx_dim1 = 0;
		read_array_rank1(m_file_id, "time_steps", &time_steps_data, H5T_NATIVE_DOUBLE, &time_steps_dim1);
		read_array_rank1(m_file_id, "time_steps_idx", &time_steps_idx_data, H5T_NATIVE_INT, &time_steps_idx_dim1);
		if (time_steps_dim1 != time_steps_idx_dim1) {
			throw runtime_error("Number of steps in \'time_steps\' and \'time_steps_idx\' datasets don\'t match.");
		} else {
			num_time_steps = time_steps_dim1;
		}
    } catch (std::exception& e) {
        vtkErrorMacro(<< e.what());
        return 1;
    }

    // Check if time_steps are strictly monotonically increasing (as should be
    // the case for proper time stepping) Use time_steps_idx for time_steps
    // instead if that's not the case
	bool use_time_steps_idx_instead_of_time_steps = false;
	for (size_t idx = 1; idx < num_time_steps; ++idx) {
		if (time_steps_data[idx] <= time_steps_data[idx-1]) {
			use_time_steps_idx_instead_of_time_steps = true;
			break;
		}
	}

    // Copy time step values (and indices) to the this->TimeStepValues vector
    this->TimeStepValues.resize(num_time_steps);
    this->m_time_steps_idx.resize(num_time_steps);
    for (size_t idx = 0; idx < num_time_steps; ++idx) {
		if (use_time_steps_idx_instead_of_time_steps) {
			this->TimeStepValues[idx] = static_cast<double>(time_steps_idx_data[idx]);
			this->m_time_steps_idx[idx] = time_steps_idx_data[idx];
		} else {
			this->TimeStepValues[idx] = time_steps_data[idx];
			this->m_time_steps_idx[idx] = time_steps_idx_data[idx];
		}
    }
    
    // Clean up temp storage
    delete[] time_steps_data;
    delete[] time_steps_idx_data;
    // [end of HDF5] ==========================================================
    
    // [VTK] ==================================================================
    // Set the time step values for all time steps in vtk
    outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), &this->TimeStepValues[0], this->TimeStepValues.size());
    // Set the available time range in vtk
    double time_range[2] = {this->TimeStepValues.front(), this->TimeStepValues.back()};
    outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), time_range, 2);

    // Return success
    return 1;
}


// ============================================================================
int vtkSimPTReader::RequestData(vtkInformation* vtkNotUsed(request),
                                vtkInformationVector** vtkNotUsed(inputVector),
                                vtkInformationVector* outputVector) {
    // TODO
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    // The output dataobject of this filter/reader is vtkPolyData. Try a safe
    // downcast from vtkDataObject and get a shorthand for it
    vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    /*
     * If an update of time step was requested, find the correct time value
     * (index into array of time step values)
     * TODO: I'm not 100% sure this covers all cases but it seems to work pretty good.
     */
    if (outInfo->Has(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP())) {
        // Which time step value (in seconds) was requested?
        const double requestedTimeValue = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());
        // Find time step value within a certain tolerance
        // TODO: still a bit unsure why the the first time step value has to be subtracted
        this->TimeStep = std::find_if(this->TimeStepValues.begin(),
                                      this->TimeStepValues.end(),
                                      std::bind2nd(ToleranceCheck(1.0e-6), requestedTimeValue))
                         - this->TimeStepValues.begin();
        // Set the requested time step on the output information
        outInfo->Set(vtkDataObject::DATA_TIME_STEP(), requestedTimeValue);
    }

    // [HDF5] =================================================================
    // Open HDF5 group /step_n for the requested time step
    // (don't forget to translate TimeStep into the right /step_n group through time_step_idx
    // i.e: the n in '/step_n' should be n = time_steps_idx[paraview time step])
    // TODO: do bounds checking!!! this->TimeStep < num_time_steps = m_time_steps_idx.size()
    if (this->TimeStep >= this->m_time_steps_idx.size()) {
        vtkErrorMacro(<< "TimeStep out of bounds (" << this->TimeStep << ")");
        return 1;
    }
    string step_grp_name = string("/step_") + to_string(this->m_time_steps_idx[this->TimeStep]);
    hid_t step_grp = H5Gopen(m_file_id, step_grp_name.c_str(), H5P_DEFAULT);
        if (step_grp < 0) { vtkErrorMacro(<< "Could not open time step group " << step_grp_name); return 1; }

    // Sizes
    size_t num_nodes = 0;
	size_t nodes_xy_dim1 = 0, nodes_xy_dim2 = 0;
	size_t cells_id_dim1 = 0;
	size_t cells_num_nodes_dim1 = 0;
	size_t cells_nodes_dim1 = 0, cells_nodes_dim2 = 0;
    size_t num_cells = 0;
    size_t max_num_cells_nodes = 0;
    size_t num_cells_chemicals = 0;

    // RAW data that will be filled up with HDF5 readers and fed to VTK
    double* nodes_xy_data = 0;
    int* cells_id_data = 0;
    int* cells_num_nodes_data = 0;
    int* cells_nodes_data = 0;
	// Containers for attributes
	map<string, vector<int> > nodes_attri;
	map<string, vector<double> > nodes_attrd;
	map<string, vector<int> > cells_attri;
	map<string, vector<double> > cells_attrd;

    // Attempt to read raw data for this step from HDF5
	// TODO: add step_n in error reporting!!!
    try {
		/*
		 * Read the /step_n/nodes_xy dataset that contains the 2D point geometry
		 * Check dimensions of the nodes_xy dataset:
		 * Must be a rank-two dataset with dimensions N x 2
		 * N is the number of points (= nodes)
		 * 2 represents xy coordinates in R^2
		 */
		read_array_rank2(step_grp, "nodes_xy", &nodes_xy_data, H5T_NATIVE_DOUBLE, &nodes_xy_dim1, &nodes_xy_dim2);
		if (2 != nodes_xy_dim2) {
			throw runtime_error("Second dimention of \'nodes_xy\' must equal 2.");
		} else {
			num_nodes = nodes_xy_dim1;
		}

		// Find datasets called 'nodes_attr_<name>' (I.e.: node attributes)
		list<string> nodes_attr_names = find_matching_links(step_grp, [](string name)->bool{
			return starts_with(name, "nodes_attr_");
		});

		// Try to read all matching node attribute datasets
		for (string long_name : nodes_attr_names) {
            // Magic number 11 is the length of the prefix "nodes_attr_" that
            // needs to be stripped away
			string short_name = long_name.substr(11, string::npos);

			// Check if "long_name" refers to a valid rank-1 dataset in step_grp
			H5O_info_t obj_info;
			H5Oget_info_by_name(step_grp, long_name.c_str(), &obj_info, H5P_DEFAULT);
			if (H5O_TYPE_DATASET == obj_info.type) {
				// Open the dataset to get all info
				hid_t dset = H5Dopen(step_grp, long_name.c_str(), H5P_DEFAULT);

				// Make sure dimensions match
				hid_t dspace = H5Dget_space(dset);
				// Check if rank = 1
				const int ndims = H5Sget_simple_extent_ndims(dspace);
				if (1 != ndims) {
					H5Sclose(dspace);
					break;
				}
				// Check if number of attribute values = number of nodes
				hsize_t dims = 0;
				H5Sget_simple_extent_dims(dspace, &dims, 0);
				if (num_nodes != dims) {
					H5Sclose(dspace);
					break;
				}

				// Get the type of the attribute from HDF5 (either int or double)
				hid_t dtype = H5Dget_type(dset);
				H5T_class_t dtype_class = H5Tget_class(dtype);
				H5Tclose(dtype);
				if (H5T_INTEGER == dtype_class) {
					// Read the data directly into a vector
					vector<int> attr_values(num_nodes);
					H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, attr_values.data());
					// Add attribute and its values to the nodes
					nodes_attri[short_name] = attr_values;
				} else if (H5T_FLOAT == dtype_class) {
					// Read the data directly into a vector
					vector<double> attr_values(num_nodes);
					H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, attr_values.data());
					// Add attribute and its values to the nodes
					nodes_attrd[short_name] = attr_values;
				} else {
					// Unsupported datatype: ignore (or generate a warning)
				}

				// Clean up
				H5Dclose(dset);
			}
		}

		/*
		 * Read the IDs of the cells from /step_n/cells_id
		 */
		read_array_rank1(step_grp, "cells_id", &cells_id_data, H5T_NATIVE_INT, &cells_id_dim1);
		num_cells = cells_id_dim1;

		/*
		 * Read the number of nodes for each cell from /step_n/cells_num_nodes
		 * cells_num_nodes dataset must be rank-one
		 */
		read_array_rank1(step_grp, "cells_num_nodes", &cells_num_nodes_data, H5T_NATIVE_INT, &cells_num_nodes_dim1);
		if (num_cells != cells_num_nodes_dim1) {
			throw runtime_error("Number of cells in \'cells_num_nodes\' and \'cells_id\' don't match.");
		}

		/*
         * Read the array with node indices for the cells from
         * /step_n/cells_nodes Each row i represents a cell made up of
         * cells_num_nodes[i] nodes cells_nodes dataset mst be rank two with
         * dimensions (num_cells x max_num_nodes_per_cell)
		 */
		read_array_rank2(step_grp, "cells_nodes", &cells_nodes_data, H5T_NATIVE_INT, &cells_nodes_dim1, &cells_nodes_dim2);
		if (num_cells != cells_nodes_dim1) {
			throw runtime_error("Number of cells in \'cells_nodes\' and \'cells_id\' don't match.");
		} else {
			max_num_cells_nodes = cells_nodes_dim2;
		}

		// Find datasets called 'cells_attr_<name>' (I.e.: cell attributes)
		list<string> cells_attr_names = find_matching_links(step_grp, [](string name)->bool{
			return starts_with(name, "cells_attr_");
		});

		// Try to read all matching cell attribute datasets
		for (string long_name : cells_attr_names) {
            // Magic number 11 is the length of the prefix "cells_attr_" that
            // needs to be stripped away
			string short_name = long_name.substr(11, string::npos);

			// Check if "long_name" refers to a valid rank-1 dataset in step_grp
			H5O_info_t obj_info;
			H5Oget_info_by_name(step_grp, long_name.c_str(), &obj_info, H5P_DEFAULT);
			if (H5O_TYPE_DATASET == obj_info.type) {
				// Open the dataset to get all info
				hid_t dset = H5Dopen(step_grp, long_name.c_str(), H5P_DEFAULT);

				// Make sure dimensions match
				hid_t dspace = H5Dget_space(dset);
				// Check if rank = 1
				const int ndims = H5Sget_simple_extent_ndims(dspace);
				if (1 != ndims) {
					H5Sclose(dspace);
					break;
				}
				// Check if number of attribute values = number of cells
				hsize_t dims = 0;
				H5Sget_simple_extent_dims(dspace, &dims, 0);
				if (num_cells != dims) {
					H5Sclose(dspace);
					break;
				}

                // Get the type of the attribute from HDF5 (either int or
                // double)
				hid_t dtype = H5Dget_type(dset);
				H5T_class_t dtype_class = H5Tget_class(dtype);
				H5Tclose(dtype);
				if (H5T_INTEGER == dtype_class) {
					// Read the data directly into a vector
					vector<int> attr_values(num_cells);
					H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, attr_values.data());
					// Add attribute and its values to the cells
					cells_attri[short_name] = attr_values;
				} else if (H5T_FLOAT == dtype_class) {
					// Read the data directly into a vector
					vector<double> attr_values(num_cells);
					H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, attr_values.data());
					// Add attribute and its values to the cells
					cells_attrd[short_name] = attr_values;
				} else {
					// Unsupported datatype: ignore (or generate a warning)
				}

				// Clean up
				H5Dclose(dset);
			}
		}
    } catch (std::exception& e) {
        vtkErrorMacro(<< e.what());
        return 1;
    }

    // Clean up HDF5 handles
    H5Gclose(step_grp);
    // [end of HDF5] ==========================================================


    // [VTK] ==================================================================
    // Create a vtkPoints object and add the node's coordinates
    // (Note that vtkPoints are 3D points => fill up the z-coord with zeros)
    vtkSmartPointer<vtkPoints> nodes = vtkSmartPointer<vtkPoints>::New();
    for (size_t node_idx = 0; node_idx < num_nodes; ++node_idx) {
        // x & y are read from nodes_xy_data, the z coordinate is always zero
        const double x = nodes_xy_data[2 * node_idx];
        const double y = nodes_xy_data[2 * node_idx + 1];
        const double z = 0.0;
        nodes->InsertNextPoint(x, y, z);
    }
    // Attach nodes to the output
    output->SetPoints(nodes);

    /*
     * A vtkCellArray is the collection of all polygons, i.e.: the whole tissue
     * of SimPT cells One vtkPolygon in the above array corresponds to one
     * SimPT cell
     *
     * Squeeze the topology data from cells_num_nodes and cells_nodes
     * in vtk's polygons
     */
    vtkSmartPointer<vtkCellArray> cell_polys = vtkSmartPointer<vtkCellArray>::New();
    // Append polygons to the cell array
    for (size_t cell_idx = 0; cell_idx < num_cells; ++cell_idx) {
		// New vtkPolygon to represent the SimPT cell
		vtkSmartPointer<vtkPolygon> cell_poly = vtkSmartPointer<vtkPolygon>::New();

		// Set number of nodes in current cell
		size_t cell_num_nodes = cells_num_nodes_data[cell_idx];
		cell_poly->GetPointIds()->SetNumberOfIds(cell_num_nodes);

		// Loop though these cell's nodes and append their ids
		for (size_t cell_node_idx = 0; cell_node_idx < cell_num_nodes; ++cell_node_idx) {
			cell_poly->GetPointIds()->SetId(cell_node_idx, cells_nodes_data[cell_idx * max_num_cells_nodes + cell_node_idx]);
		}
		// Add polygon to list of polygons
		cell_polys->InsertNextCell(cell_poly);
    }
    // Attach polygons to the output
    output->SetPolys(cell_polys);

	/*
	 * IDs of cells in SimPT:
	 */
	// A vtkIntArray for SimPT cell IDs
	vtkSmartPointer<vtkIntArray> cells_id = vtkSmartPointer<vtkIntArray>::New();
	cells_id->SetName("cell ID");
	// Copy ID for each cell
	for (size_t cell_idx = 0; cell_idx < num_cells; ++cell_idx) {
		cells_id->InsertNextValue(cells_id_data[cell_idx]);
	}
	// Add array of cell IDs to output
	output->GetCellData()->AddArray(cells_id);

	/*
	 * Loop over node attributes ... TODO: document!
	 */
	for (auto attr : nodes_attri) {
        // A vtkIntArray for int valued node attributes
        vtkSmartPointer<vtkIntArray> attr_values = vtkSmartPointer<vtkIntArray>::New();
        attr_values->SetName(attr.first.c_str());

        // Copy values for each node
        for (size_t node_idx = 0; node_idx < num_nodes; ++node_idx) {
            attr_values->InsertNextValue(attr.second[node_idx]);
        }
        // Add array of node attribute values to output
		output->GetPointData()->AddArray(attr_values);
	}
	for (auto attr : nodes_attrd) {
        // A vtkDoubleArray for double valued node attributes
        vtkSmartPointer<vtkDoubleArray> attr_values = vtkSmartPointer<vtkDoubleArray>::New();
        attr_values->SetName(attr.first.c_str());

        // Copy values for each node
        for (size_t node_idx = 0; node_idx < num_nodes; ++node_idx) {
            attr_values->InsertNextValue(attr.second[node_idx]);
        }
        // Add array of node attribute values to output
        output->GetPointData()->AddArray(attr_values);
	}

	/*
	 * Loop over cell attributes ... TODO: document!
	 */
	for (auto attr : cells_attri) {
        // A vtkIntArray for int valued cell attributes
        vtkSmartPointer<vtkIntArray> attr_values = vtkSmartPointer<vtkIntArray>::New();
        attr_values->SetName(attr.first.c_str());

        // Copy values for each cell
        for (size_t cell_idx = 0; cell_idx < num_cells; ++cell_idx) {
			attr_values->InsertNextValue(attr.second[cell_idx]);
        }
        // Add array of cell attribute values to output
        output->GetCellData()->AddArray(attr_values);
	}
	for (auto attr : cells_attrd) {
        // A vtkDoubleArray for double valued cell attributes
        vtkSmartPointer<vtkDoubleArray> attr_values = vtkSmartPointer<vtkDoubleArray>::New();
        attr_values->SetName(attr.first.c_str());

        // Copy values for each cell
        for (size_t cell_idx = 0; cell_idx < num_cells; ++cell_idx) {
			attr_values->InsertNextValue(attr.second[cell_idx]);
        }
        // Add array of cell attribute values to output
        output->GetCellData()->AddArray(attr_values);
	}

    // TEMP CODE: TODO Make this more general!!!
    // I want to visualize the PIN flux vectors as glyphs in ParaView.
    // To this end, I need the ability to associate 2D vector data with cells
    // Proposal: cells_vec_data in HDF5, rank-3 array: (num_cells, num_vector_quantities, 2)
    // Translation to vtk: Arrays of 3 (z=0) components.
    //if (cells_vectors_data) {
        //vtkSmartPointer<vtkDoubleArray> cells_vectors = vtkSmartPointer<vtkDoubleArray>::New();
        //cells_vectors->SetName("cells_vectors");
        //cells_vectors->SetNumberOfComponents(3);
        //for (int cell_idx = 0; cell_idx < num_cells; ++cell_idx) {
            //double cell_vector[] = {cells_vectors_data[2*cell_idx], cells_vectors_data[2*cell_idx+1], 0.0};
            //cells_vectors->InsertNextTuple(cell_vector);
        //}
        //output->GetCellData()->AddArray(cells_vectors);
    //}
    // END TEMP

    // ========================================================================
    // Clean up temp data allocated for reading from HDF5
    delete[] nodes_xy_data;
    delete[] cells_id_data;
    delete[] cells_num_nodes_data;
    delete[] cells_nodes_data;

    // Return success
    return 1;
}


// ============================================================================
//int vtkSimPTReader::CanReadFile(const char* file_name) {
    //// Return success
    //return 1;
//}

