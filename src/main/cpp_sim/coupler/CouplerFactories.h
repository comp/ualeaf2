#ifndef SIMPT_SIM_COUPLER_FACTORIES_H_
#define SIMPT_SIM_COUPLER_FACTORIES_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CouplerFactories.
 */

#include "ICoupler.h"
#include "util/misc/FunctionMap.h"

namespace SimPT_Sim {

/**
 * A coupler factory map that is used to register names for these couplers.
 */
class CouplerFactories : public SimPT_Sim::Util::FunctionMap<std::shared_ptr<ICoupler>()>
{
public:
	using MapType = SimPT_Sim::Util::FunctionMap<std::shared_ptr<ICoupler>()>;

public:
	CouplerFactories();

	virtual ~CouplerFactories() {}
};

} // namespace

#endif // end_of_include_guard
