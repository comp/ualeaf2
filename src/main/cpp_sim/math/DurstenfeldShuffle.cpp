/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MeshProps.
 */

#include "DurstenfeldShuffle.h"

#include "RandomEngine.h"

#include <trng/uniform_int_dist.hpp>
#include <vector>

using namespace std;

namespace SimPT_Sim {

void DurstenfeldShuffle::Fill(vector<unsigned int>& v, RandomEngine& r)
{
        v[0] = 0U;
        for (unsigned int i = 1; i < v.size(); i++) {
                const auto j = r.GetGenerator(trng::uniform_int_dist(0U, i))();
                v[i] = v[j];
                v[j] = i;
        }

}

} // namespace
