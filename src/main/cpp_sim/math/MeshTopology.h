#ifndef SIMPT_MATH_MESH_TOPOLOGY_H_INCLUDED
#define SIMPT_MATH_MESH_TOPOLOGY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MeshTopology.
 */

#include "CSRMatrix.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <tuple>

namespace SimPT_Sim {

using boost::property_tree::ptree;
class Mesh;

/**
 * Helper functions for mesh topology.
 */
class MeshTopology
{
public:
        /**
         * Return the incidence of nodes and cells as a binary CSR matrix A with
         * dimensions: #nodes x #cells.
         * Rows are nodes, columns are cells. A nonzero element at position (i,j)
         * indicates that a node i is incident with cell j.
         *
         * The notion of "incidence" in the context of this function is defined as:
         * "Node i belongs to the polygon of a cell j".
         */
        static CSRMatrix NodeCellNodeIncidence(std::shared_ptr<Mesh> mesh);

        /**
         * Return the incidence of nodes and nodes as a binary CSR matrix A with
         * dimensions: #nodes x #nodes.
         * Rows and columns are nodes. A nonzero element at position (i,j)
         * indicates that a node i is incident with node j.
         *
         * The notion of "incidence" in the context of this function is defined as:
         * "Node i shares an edge with node j".
         */
        static CSRMatrix NodeEdgeNodeIncidence(std::shared_ptr<Mesh> mesh);
};

} // namespace

#endif // end_of_include_guard

