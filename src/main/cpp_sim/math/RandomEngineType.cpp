/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of RandomEngineType.
 */

#include "RandomEngineType.h"

#include <map>

using namespace std;
using namespace SimPT_Sim::RandomEngineType;

namespace {

std::map<TypeId, string> g_type_id_to_name {
	make_pair(TypeId::lcg64,           "lcg64"),
	make_pair(TypeId::lcg64_shift,     "lcg64_shift"),
	make_pair(TypeId::mrg2,            "mrg2"),
	make_pair(TypeId::mrg3,            "mrg3"),
	make_pair(TypeId::yarn2,           "yarn2"),
	make_pair(TypeId::yarn3,           "yarn3")
};

std::map<string, TypeId> g_name_to_type_id {
	make_pair("lcg64",                 TypeId::lcg64),
	make_pair("lcg64_shift",           TypeId::lcg64_shift),
	make_pair("mrg2",                  TypeId::mrg2),
	make_pair("mrg3",                  TypeId::mrg3),
	make_pair("yarn2",                 TypeId::yarn2),
	make_pair("yarn3",                 TypeId::yarn3)
};

}

namespace SimPT_Sim {
namespace RandomEngineType {

bool IsExisting(std::string s)
{
	return g_name_to_type_id.count(s) == 1;
}

bool IsExisting(TypeId b)
{
	return g_type_id_to_name.count(b) == 1;
}

string ToString(TypeId b)
{
	string ret = "";
	if (g_type_id_to_name.count(b) == 1) {
		ret = g_type_id_to_name[b];
	}
	return ret;
}

TypeId FromString(string s)
{
	TypeId ret = TypeId::mrg2;
	if (g_name_to_type_id.count(s) == 1) {
		ret = g_name_to_type_id[s];
	}
	return ret;
}

} // namespace
} // namespace




