#ifndef SIMPT_MATH_DURSTENFELD_SHUFFLE_H_INCLUDED
#define SIMPT_MATH_DURSTENFELD_SHUFFLE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for DurstenfeldShuffle.
 */

#include "RandomEngine.h"

#include<vector>

namespace SimPT_Sim {

class RandomEngine;

/**
 * Generate a random permuted sequence of integers 0,1,..., n.
 * Durstenfeld implementation of Fisher-Yates shuffle (cfr. Wikipedia page).
 */
class DurstenfeldShuffle
{
public:
        static void Fill(std::vector<unsigned int>& indices, RandomEngine& random_engine);

};

} // namespace

#endif // end_of_include_guard

