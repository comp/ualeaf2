#ifndef CHAINHULL_H_INCLUDED
#define CHAINHULL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ChainHull.
 */

#include <array>
#include <vector>

namespace SimPT_Sim {

/**
 * Compute the convex hull of a set of two-dimensional points.
 */
class ChainHull
{
public:
	static std::vector<std::array<double, 2> > Compute(const std::vector<std::array<double, 2> >& polygon);

	/**
	 * Point class needed by 2D convex hull code.
	 */
	class Point
	{
	public:
		///
		Point(float xx = 0.0, float yy = 0.0) : x(xx), y(yy) {}

		///
		double GetX() const { return x; }

		///
		double GetY() const { return y; }

	private:
		float x;
		float y;
	};

private:
	/**
	 * Andrew's monotone chain 2D convex hull algorithm.
	 * @param P     array of 2D points pre-sorted by increasing x- and y-coordinates
	 * @param n     number of points in P[]
	 * @param H     array of the convex hull vertices (max is n)
	 * @return      number of points in H[]
	 */
	static int chainHull_2D(Point* P, int n, Point* H);
};

/**
 * Required to sort points (e.g. Qt's qSort()).
 */
bool operator<(ChainHull::Point const& p1, ChainHull::Point const& p2);

} // namespace

#endif // end_of_include_guard
