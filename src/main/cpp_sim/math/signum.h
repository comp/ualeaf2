#ifndef MATH_SIGNUM_H_INCLUDED
#define MATH_SIGNUM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Math signum function.
 */

#include <type_traits>

namespace SimPT_Sim {
namespace Util {

/**
 * Overload for unsigned types.
 * Posted on stackoverflow, March 28, 2012, by Joe Wreschnig
 */
template <typename T> inline constexpr
int signum(T x, std::false_type)
{
	return T(0) < x;
}

/**
 * Overload for signed types.
 * Posted on stackoverflow, March 28, 2012, by Joe Wreschnig
 */
template <typename T> inline constexpr
int signum(T x, std::true_type)
{
	return (T(0) < x) - (x < T(0));
}

/**
 * Select particular overload via tag dispatch.
 * Posted on stackoverflow, March 28, 2012, by Joe Wreschnig
 */
template <typename T> inline constexpr
int signum(T x)
{
	return signum(x, std::is_signed<T>());
}

} // Util
} // SimPT_Sim

#endif // end_of_include_guard
