#ifndef PTREE_FILE_H_INCLUDED
#define PTREE_FILE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeFile.
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <string>

namespace SimPT_Sim {
namespace Util {

/**
 * Utility interface for dealing with ptree xml or xml.gz files.
 */
class PTreeFile
{
public:
	///
	static void Compress(const std::string& in_path, const std::string& out_path);

	///
	static void Decompress(const std::string& in_path, const std::string& out_path);

	///
	static bool IsGzipped(const std::string& path);

	/// Signal availability of boost iostreams with gzip capability.
	static constexpr bool IsBoostGzipAvailable();

	/// Indicates whether the file has the appropriate extension.
	static bool IsPTreeFile(const std::string& path);

	/// Base name excluding label (label encodes time step = chars from last "_" to first "." ).
	static std::string GetBaseNameExcludingLabel(const std::string& path);

	/// Base name including label (label encodes time step = chars from last "_" to first "." ).
	static std::string GetBaseNameIncludingLabel(const std::string& path);

	/// Return complete suffix (everything following the first ".").
	static std::string GetCompleteSuffix(const std::string& path);

	///
	static std::string GetLabel(const std::string& path);

	///
	static void Read(const std::string& path, boost::property_tree::ptree& pt);

	///
	static void ReadXml(const std::string& path, boost::property_tree::ptree& pt);

	///
	static void ReadXmlGz(const std::string& path, boost::property_tree::ptree& pt);

	///
	static void Write(const std::string& path, const boost::property_tree::ptree& pt);

	///
	static void WriteXml(const std::string& path, const boost::property_tree::ptree& pt);

	///
	static void WriteXmlGz(const std::string& path, const boost::property_tree::ptree& pt);
};

} // namespace
} // namespace

#endif // end-of-include-guard
