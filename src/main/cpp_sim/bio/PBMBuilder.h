#ifndef PBMBUILDER_H_INCLUDED
#define PBMBUILDER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PBMBBuilder.
 */

#include <memory>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {

class Mesh;
class Sim;

/**
 * Class that directs ptree based mesh building process.
 */
class PBMBuilder
{
public:
	std::shared_ptr<Mesh> Build(const boost::property_tree::ptree& mesh_pt);

private:
	void BuildBoundaryPolygon(const boost::property_tree::ptree& cells_pt);

	void BuildCells(const boost::property_tree::ptree& cells_pt);

	void BuildNodes(const boost::property_tree::ptree& nodes_pt);

	void BuildWalls(const boost::property_tree::ptree& walls_pt);

	void ConnectCellsToWalls(const boost::property_tree::ptree& cells_pt);

private:
	std::shared_ptr<Mesh>   m_mesh;
};

}

#endif // end_of_include_guard

