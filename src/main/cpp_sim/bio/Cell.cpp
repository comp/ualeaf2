/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Cell.
 */

#include "Cell.h"

#include "AreaMoment.h"
#include "CellAttributes.h"
#include "Edge.h"
#include "GeoData.h"
#include "Node.h"

#include "math/array3.h"
#include "math/constants.h"
#include "util/container/CircularIterator.h"
#include "util/container/ConstCircularIterator.h"

#include <iomanip>
#include <list>
#include <string>
#include <tuple>
#include <vector>

namespace SimPT_Sim { class Node; }
namespace SimPT_Sim { class Wall; }

using namespace std;
using namespace boost::property_tree;
using boost::optional;
using namespace SimPT_Sim::Container;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim;

namespace {

/**
 * Returns true if lines (v1)---(v2) and (v3)---(v4) have a common point, false
 * otherwise. Even though the points vX are 3D coordinates, only their x,y
 * components are used.
 */
bool is_intersecting(const array<double, 3>& v1, const array<double, 3>& v2,
		const array<double, 3>& v3, const array<double, 3>& v4)
{
	double const denominator = (v4[1] - v3[1]) * (v2[0] - v1[0]) - (v4[0] - v3[0]) * (v2[1] - v1[1]);
	double const ua = ((v4[0] - v3[0]) * (v1[1] - v3[1]) - (v4[1] - v3[1]) * (v1[0] - v3[0]))
	                / denominator;
	double const ub = ((v2[0] - v1[0]) * (v1[1] - v3[1]) - (v2[1] - v1[1]) * (v1[0] - v3[0]))
	                / denominator;

    return (0.0 < ua) && (ua < 1.0) && (0.0 < ub) && (ub < 1.0);
}

} // namespace anonymous

namespace SimPT_Sim {

Cell::Cell(int index, unsigned int chem_count)
	: CellAttributes(chem_count), m_index(index),
	  m_dirty_area(true), m_dirty_centroid(true), m_dirty_geo_data(true)
{
}

void Cell::AddWall(Wall* w)
{
	// Add Wall to Cell's list (already registered with Mesh by Mesh::BuildWall)
	m_walls.push_back(w);
}

void Cell::CalculateArea() const
{
	double a   = 0.0;

	auto cit = make_const_circular(m_nodes, m_nodes.begin());
	do {
		const double i_x = (*(*cit))[0];
		const double i_y = (*(*cit))[1];
		const double n_x = (*(*next(cit)))[0];
		const double n_y = (*(*next(cit)))[1];

		a += i_x * n_y - n_x * i_y;
	} while (++cit != m_nodes.begin());

	m_geo_data.m_area           = abs(a) / 2.0;
	m_dirty_area                = false;
}

void Cell::CalculateCentroid() const
{
	double a = 0.0;        // area
	double x = 0.0;        // centroid - x component
	double y = 0.0;        // centroid - y component

	auto cit = make_const_circular(m_nodes, m_nodes.begin());
	do {
		const double i_x = (*(*cit))[0];
		const double i_y = (*(*cit))[1];
		const double n_x = (*(*next(cit)))[0];
		const double n_y = (*(*next(cit)))[1];

		a += i_x * n_y - n_x * i_y;
		x += (n_x + i_x) * (i_x * n_y - n_x * i_y);
		y += (n_y + i_y) * (i_x * n_y - n_x * i_y);

	} while (++cit != m_nodes.begin());

	m_geo_data.m_area      = 0.5 * abs(a);
	m_geo_data.m_centroid  = array<double, 3> {{ x / (3.0 * a), y / (3.0 * a), 0.0}};
	m_dirty_area           = false;
	m_dirty_centroid       = false;
}

void Cell::CalculateGeoData() const
{
	double a  = 0.0;        // area (actually twice the area)
	double x  = 0.0;        // centroid - x component
	double y  = 0.0;        // centroid - y component
	double xx = 0.0;        // area moment of inertia - xx component
	double xy = 0.0;        // area moment of inertia - xy component
	double yy = 0.0;        // area moment of inertia - yy component

	auto cit = make_const_circular(m_nodes, m_nodes.begin());
	do {
		const double i_x = (*(*cit))[0];
		const double i_y = (*(*cit))[1];
		const double n_x = (*(*next(cit)))[0];
		const double n_y = (*(*next(cit)))[1];

		a  += i_x * n_y - n_x * i_y;
		x  += (n_x + i_x) * (i_x * n_y - n_x * i_y);
		y  += (n_y + i_y) * (i_x * n_y - n_x * i_y);
		xx += (i_x * i_x + n_x * i_x + n_x * n_x) * (i_x * n_y - n_x * i_y);
		xy += (n_x * i_y - i_x * n_y) * (i_x * (2 * i_y + n_y) + n_x * (i_y + 2 * n_y));
		yy += (i_x * n_y - n_x * i_y) * (i_y * i_y + n_y * i_y + n_y * n_y);

	} while (++cit != m_nodes.begin());

	m_geo_data.m_area        = abs(a) / 2.0;
	m_geo_data.m_centroid    = array<double, 3> {{ x / (3.0 * a), y / (3.0 * a), 0.0}};
	m_geo_data.m_area_moment = AreaMoment(xx / 12.0, xy / 24.0, yy / 12.0);

	m_dirty_area             = false;
	m_dirty_centroid         = false;
	m_dirty_geo_data         = false;
}

ptree Cell::GeometryToPtree() const
{
        ptree ret;
        ret.put("id", m_index);
        ret.put("area", GetArea());
        ret.put("at_boundary", HasBoundaryWall());

        for (const auto& node : m_nodes) {
                ret.add("node_array.node", node->GetIndex());
        }
        for (const auto& wall : m_walls) {
                ret.add("wall_array.wall", wall->GetIndex());
        }

        return ret;
}

double Cell::GetArea() const
{
	if (m_dirty_area) {
		CalculateArea();
	}
	return m_geo_data.m_area;
}

double Cell::GetSignedArea() const
{
        // Should be handled similar to area but first we need to test if it solves
        // the problem of cell inversion; the sole reason it's been introduced.
	double a   = 0.0;

	auto cit = make_const_circular(m_nodes, m_nodes.begin());
	do {
		const double i_x = (*(*cit))[0];
		const double i_y = (*(*cit))[1];
		const double n_x = (*(*next(cit)))[0];
		const double n_y = (*(*next(cit)))[1];

		a += i_x * n_y - n_x * i_y;
	} while (++cit != m_nodes.begin());

	return 0.5 * a;
}

array<double, 3> Cell::GetCentroid() const
{
	if (m_dirty_centroid) {
		CalculateCentroid();
	}
	return m_geo_data.m_centroid;
}

double Cell::GetCircumference() const
{
	double circumference = 0.0;
	auto cit = make_const_circular(m_nodes);
	do {
		const double dx = (*(*next(cit)))[0] - (*(*cit))[0];
		const double dy = (*(*next(cit)))[1] - (*(*cit))[1];
		circumference += sqrt(dx * dx + dy * dy);
	} while (++cit != m_nodes.begin());
	return circumference;
}

GeoData Cell::GetGeoData() const
{
	if (m_dirty_geo_data) {
		CalculateGeoData();
	}
	return m_geo_data;
}

double Cell::GetSumTransporters(unsigned int ch) const
{
	double sum = 0.0;
	for (const auto& wall : m_walls) {
		assert( (wall->GetC1() == this || wall->GetC2() == this) && "Error in cell <-> wall!" );
		sum += (wall->GetC1() == this) ? wall->GetTransporters1(ch) : wall->GetTransporters2(ch);
	}
	return sum;
}

bool Cell::HasEdge(const Edge& edge) const
{
	bool status = false;
	auto cit = make_const_circular(m_nodes, m_nodes.begin());
	do {
		if (Edge(*cit, *next(cit)) == edge) {
			status = true;
			break;
		}
	} while(++cit != m_nodes.begin());
	return status;
}

bool Cell::HasBoundaryWall() const
{
	bool status = false;
	for (const auto& wall : m_walls) {
		if (wall->IsAtBoundary()) {
			status = true;
			break;
		}
	}
	return status;
}

bool Cell::HasNeighborOfTypeZero() const
{
	int prod = 1;
	for (const auto& wall : m_walls) {

/*		prod *= ( wall->GetC1()->GetIndex() != this->GetIndex()
			? ( wall->GetC1()->GetCellType() ) : ( wall->GetC2()->GetCellType() ) );

*/
		if ( wall->GetC1()->GetIndex() != this->GetIndex() ) {
			if ( wall->GetC1()->GetIndex() != -1 ) {
				prod *= wall->GetC1()->GetCellType();
			}
		} else {
			if ( wall->GetC2()->GetIndex() != -1 ) {
				prod *= wall->GetC2()->GetCellType();
			}
		}
	}
	return ( prod == 0 );
}

bool Cell::IsWallNeighbor(Cell* cell) const
{
	bool status = false;
	for (const auto& wall : m_walls) {
		if ((cell != this) && (wall->GetC1() == cell || wall->GetC2() == cell)) {
			status = true;
			break;
		}
	}
	return status;
}

void Cell::Move(const array<double, 3>& a)
{
	for (auto& node : m_nodes) {
		*node += a;
	}
}

bool Cell::MoveCreatesTinyAngles(Node* /*moving_node*/, array<double, 3> /*new_pos*/) {
	// iterate over nodes
	/*for (auto it = m_nodes.begin(); it != m_nodes.end(); it++) {
		auto it2 = it++;
		if(it2 == m_nodes.end()) {
			it2 = m_nodes.begin();
		}
		auto it3 = it2++;
		if(it3 == m_nodes.end()) {
			it3 = m_nodes.begin();
		}

		// calculate old angle
		array<double, 3> edge1 = *(*it2) - *(*it);
		array<double, 3> edge2 = *(*it3) - *(*it2);

		double oldAngle = Angle(edge1, edge2);
		//if (oldAngle < 1.6) {
			double newAngle = oldAngle;

			if(*it == moving_node) {
				edge1 = *(*it2) - new_pos;
				newAngle = Angle(edge1, edge2);

			} else if (*it2 == moving_node) {
				edge1 = new_pos - *(*it);
				edge2 = *(*it3) - new_pos;
				newAngle = Angle(edge1, edge2);

			} else if (*it3 == moving_node) {
				edge2 = new_pos - *(*it2);
				newAngle = Angle(edge1, edge2);
			}

			if (newAngle < 0.2) {
				return true;
			}
		//}
	}*/

	return false;
}

bool Cell::MoveSelfIntersects(Node* moving_node, array<double, 3> new_pos)
{
	const auto moving_node_it = find(m_nodes.begin(), m_nodes.end(), moving_node);

	// Only makes sense if there are nodes and moving_node is among them
	if (!m_nodes.empty() && moving_node_it != m_nodes.end()) {

		array<double, 3> neighbor_of_moving[2];
		const auto moving_it = make_const_circular(m_nodes, moving_node_it);
		neighbor_of_moving[0] = *(*next(moving_it));
		neighbor_of_moving[1] = *(*prev(moving_it));

		// Compare the two new edges against edge starting
		// at node *it, with it looping over m_nodes
		auto it = make_const_circular(m_nodes);
		do {
			// loop over the two neighbors of moving node
			for (int j = 0; j < 2; j++) {
				// do not compare to self
				if (*it == moving_node || *next(it) == moving_node) {
					continue;
				}
				array<double, 3> v3 = *(*it);
				array<double, 3> v4 = *(*next(it));
				if (is_intersecting(new_pos, neighbor_of_moving[j], v3, v4)) {
					return true;
				}
			}
		} while(++it != m_nodes.begin());
	}
	return false;
}

ostream& Cell::Print(ostream& os) const
{
	os << "Cell: [ index = " << m_index << " ]" << endl;

	os << "Nodes: { " << endl << "\t ";
	for (auto const& node : m_nodes) {
		os << node->GetIndex() << " && ";
	}
	os << endl <<"} " << endl;
	os << "Walls: { " << endl << "\t ";
	for (auto const& wall : m_walls) {
		wall->Print(os);
		os << ", ";
	}
	os << endl << "} " << endl;

	CellAttributes::Print(os);

	return os;
}

void Cell::ReadPtree(const ptree& cell_pt)
{
	if ( !IsBoundaryPolygon() ) {
		const ptree& attributes_pt = cell_pt.get_child("attributes");
		CellAttributes::ReadPtree(attributes_pt);
	}
}

void Cell::ReassignWall(Wall* w, Cell* to)
{
	// Remove wall from this cell's list
	m_walls.remove(w);
	// Reassign it to the "to" cell
	to->m_walls.push_back(w);
}

void Cell::SetGeoDirty()
{
	m_dirty_area = true;
	m_dirty_centroid = true;
	m_dirty_geo_data = true;
}

void Cell::SetTransporters(unsigned int ch, double conc)
{
	for (auto const& wall : m_walls) {
		assert( (wall->GetC1() == this || wall->GetC2() == this) && "Error in cell <-> wall!" );

		if (wall->GetC1() == this) {
			wall->SetTransporters1(ch,conc);
		} else {
			wall->SetTransporters2(ch, conc);
		}
	}
}

void Cell::SetTransporters(unsigned int ch, double , double lat) //DDV: for now this makes sense only if conc = 1
{
	for (auto w = GetWalls().begin(); w != GetWalls().end(); ++w) {

		std::array<double, 3> ref{{1., 0., 0.}};
		std::array<double, 3> wa = *((*w)->GetN2()) - *((*w)->GetN1());
		double t2 = SignedAngle(wa, ref);

		if (this->GetCellType() == 1)
		{
			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 1.);
				(*w)->SetTransporters2(ch, 0.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 2) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 1.);
				(*w)->SetTransporters2(ch, 0.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 3) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 4) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 5) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 6) {
			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		} else if (this->GetCellType() == 7) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
		} else if (this->GetCellType() == 8) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 1.);
				(*w)->SetTransporters2(ch, 0.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
		} else if (this->GetCellType() == 9) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 1.);
				(*w)->SetTransporters2(ch, 0.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}

			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
		}
	}
}

void Cell::SetTransportersLight(unsigned int ch, double , double lat) //DDV: for now this makes sense only if conc = 1
{
	for (list<Wall *>::iterator w = GetWalls().begin(); w != GetWalls().end(); w++) {

		std::array<double, 3> ref{{1., 0., 0.}};
		std::array<double, 3> wa = *((*w)->GetN2()) - *((*w)->GetN1());
		double t2 = SignedAngle(wa, ref);

		if (this->GetCellType() == 1)
		{
			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 1.);
				(*w)->SetTransporters2(ch, 0.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		}
		else if (this->GetCellType() == 2) {

			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
		}
		else if (this->GetCellType() == 3)  {
			if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
			{ //UPPER WALL
				(*w)->SetTransporters1(ch, 0.);
				(*w)->SetTransporters2(ch, 1.);
			}
			else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
			{ //LOWER WALL
				if (this->HasNeighborOfTypeZero())
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 1.);
				}
				else
				{
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
			{
				if ((*w)->GetC1() == this)
				{ //LEFT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else
				{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, lat);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
			{
				if ((*w)->GetC1() == this)
				{ //RIGHT WALL WRT C1
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, lat);
				}
				else
				{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 0.);
				}
			}
			else if (this->GetCellType() == 4) {

				if ((t2 > (-pi() / 4)) && (t2 <= (pi() / 4)))
				{ //UPPER WALL
					(*w)->SetTransporters1(ch, 1.);
					(*w)->SetTransporters2(ch, 0.);
				}
				else if ((t2 <= (-pi() * 3 / 4)) || (t2 > (pi() * 3 / 4)))
				{ //LOWER WALL
					(*w)->SetTransporters1(ch, 0.);
					(*w)->SetTransporters2(ch, 1.);
				}

				else if ((t2 > (-pi() * 3 / 4)) && (t2 <= (-pi() / 4)))
				{
					if ((*w)->GetC1() == this)
					{ //LEFT WALL WRT C1
						(*w)->SetTransporters1(ch, lat);
						(*w)->SetTransporters2(ch, 0.);
					}
					else
					{ //RIGHT WALL WRT C2 (therefore switch transporters 1 and 2)
						(*w)->SetTransporters1(ch, 0.);
						(*w)->SetTransporters2(ch, 0.);
					}
				}
				else if ((t2 > (pi() / 4)) && (t2 <= (pi() * 3 / 4))/*t1 > 0.8*/)
				{
					if ((*w)->GetC1() == this)
					{ //RIGHT WALL WRT C1
						(*w)->SetTransporters1(ch, 0.);
						(*w)->SetTransporters2(ch, 0.);
					}
					else
					{ //LEFT WALL WRT C2 (therefore switch transporters 1 and 2)
						(*w)->SetTransporters1(ch, 0.);
						(*w)->SetTransporters2(ch, lat);
					}
				}
			}
		}
	}
}

ptree Cell::ToPtree() const
{
	ptree ret = GeometryToPtree();
	if ( !IsBoundaryPolygon()) {
		ret.push_back(make_pair("attributes", CellAttributes::ToPtree()));
	}

	return ret;
}

ostream& operator<<(ostream& os, const Cell& c)
{
	c.Print(os);
	return os;
}

}
