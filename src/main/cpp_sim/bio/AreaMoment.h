#ifndef AREA_MOMENT_H_INCLUDED
#define AREA_MOMENT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for AreaMoment.
 */

namespace SimPT_Sim {

/**
 * Structure with functionality for calculations of the area moment of inertia.
 */
struct AreaMoment
{
	AreaMoment()
		: m_xx(0.0), m_xy(0.0), m_yy(0.0)
	{
	}

	AreaMoment(double xx, double xy, double yy)
		: m_xx(xx), m_xy(xy), m_yy(yy)
	{
	}

	AreaMoment& operator+=(const AreaMoment& rhs)
	{
		m_xx += rhs.m_xx; m_xy += rhs.m_xy; m_yy += rhs.m_yy;
		return *this;
	}

	AreaMoment& operator-=(const AreaMoment& rhs)
	{
		m_xx -= rhs.m_xx; m_xy -= rhs.m_xy; m_yy -= rhs.m_yy;
		return *this;
	}

	double   m_xx;
	double   m_xy;
	double   m_yy;
};

inline AreaMoment operator+(const AreaMoment& i1, const AreaMoment& i2)
{
	return AreaMoment(i1.m_xx + i2.m_xx, i1.m_xy + i2.m_xy, i1.m_yy + i2.m_yy);
}

inline AreaMoment operator-(const AreaMoment& i1, const AreaMoment& i2)
{
	return AreaMoment(i1.m_xx - i2.m_xx, i1.m_xy - i2.m_xy, i1.m_yy - i2.m_yy);
}

inline std::ostream& operator<<(std::ostream& os, const AreaMoment& i)
{
	os << "AreaMoment: {" << std::endl;
	os << "\t " << i.m_xx << " , " << i.m_xy << " , " << i.m_yy << std::endl;
	os << "}";
	return os;
}

} // namespace

#endif // end-of-include-guard
