#ifndef EDGE_H_INCLUDED
#define EDGE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Edge.
 */

#include <iosfwd>

namespace SimPT_Sim {

class Node;

/**
 * An Edge connects two nodes and is ambidextrous.
 */
class Edge
{
public:
	/// Construct edge using two node pointers.
	Edge(Node* f = nullptr, Node* s = nullptr)
			: m_first(f), m_second(s) {}

	/// Ambidextrous equivalence.
	bool operator==(const Edge& e) const
	{
		return ((m_first == e.m_first && m_second == e.m_second) || (m_first == e.m_second && m_second == e.m_first));
	}

	/// Get the first node of the edge.
	Node* GetFirst() const
	{
		return m_first;
	}

	/// Get the second node of the edge.
	Node* GetSecond() const
	{
		return m_second;
	}

	/// Query whether edge is fixed
	bool IsFixed() const;

	/// Insert the edge in an output stream.
	std::ostream& Print(std::ostream& os) const;

private:
	Node* m_first;
	Node* m_second;
};

inline std::ostream& operator<<(std::ostream& os, const Edge& e)
{
	e.Print(os);
	return os;
}

}

#endif //end_of_include_guard



