#ifndef MBMBUILDER_H_INCLUDED
#define MBMBUILDER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MBMBBuilder.
 */

#include <memory>

namespace SimPT_Sim {

class Mesh;
class MeshState;
class Sim;

/**
 * MBMBDirector directs the Mesh building process using a MeshState object.
 */
class MBMBuilder
{
public:
	///
	std::shared_ptr<Mesh> Build(const MeshState& mesh_state);

private:
	///
	void BuildNodes(const MeshState& mesh_state);

	///
	void BuildCells(const MeshState& mesh_state);

	///
	void BuildBoundaryPolygon(const MeshState& mesh_state);

	///
	void BuildWalls(const MeshState& mesh_state);

	///
	unsigned int CalculateNumChemicals(const MeshState& mesh_state);

	///
	void ConnectCellsToWalls(const MeshState& mesh_state);

private:
	///
	std::shared_ptr<Mesh> m_mesh;
};

}

#endif // end_of_include_guard

