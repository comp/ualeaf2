#ifndef NEIGHBOR_NODES_H_INCLUDED
#define NEIGHBOR_NODES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NeighborNodes.
 */

#include <iosfwd>

namespace SimPT_Sim {

class Node;
class Cell;

/**
 * Structure of neighboring nodes: two neighboring nodes from standpoint
 * of a given cell with an ordering "first" and "second" that corresponds
 * with the node traversal order in that cell.
 */
class NeighborNodes
{
public:
	/// Plain constructor.
	NeighborNodes(Cell* c = nullptr, Node* n1 = nullptr, Node* n2 = nullptr);

	/// Return the cell of this Neighbor pair.
	Cell* GetCell() const;

	/// Return first node of this Neighbor pair.
	Node* GetNb1() const;

	/// Return second node of this Neighbor pair.
	Node* GetNb2() const;

	/// Equality test.
	bool operator==(const NeighborNodes& nn) const;

	/// Inequality test.
	bool operator!=(const NeighborNodes& nn) const;

private:
	Cell*   m_cell;
	Node*   m_nb1;
	Node*   m_nb2;
};

/// Insert Neighbor into output stream.
std::ostream& operator<<(std::ostream& os, const NeighborNodes& n);

}

#endif //end_of_include_guard

