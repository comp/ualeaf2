#ifndef REDUCE_CELL_WALLS_H_INCLUDED
#define REDUCE_CELL_WALLS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for ReduceCellWalls.
 */

#include "Cell.h"
#include "Wall.h"

namespace SimPT_Sim {

/**
 * Traverse walls of cell and apply functor.
 */
template<typename R> R ReduceCellWalls(Cell* cell, std::function<R(Cell*, Cell*, Wall*)> f)
{
	R sum = R();
	for (auto const& wall : cell->GetWalls()) {
		sum += (wall->GetC1() == cell) ?
			f(wall->GetC1(), wall->GetC2(), wall) : f(wall->GetC2(), wall->GetC1(), wall);
	}
	return sum;
}

}

#endif // end_of_include_guard
