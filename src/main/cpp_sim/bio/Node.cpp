/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Node.
 */

#include "Node.h"

#include "NodeAttributes.h"

#include <list>


using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Sim {

Node::Node(unsigned int index, const array<double, 3>& src, bool at_boundary)
	: m_position(src), m_at_boundary(at_boundary), m_index(index)
{}

ostream& Node::Print(ostream& os) const
{
	os << "Node:   ";
	os << m_index << "  {" << (*this)[0] << ", " << (*this)[1] << ", " << (*this)[2] << "}";
	return os;
}

void Node::ReadPtree(const ptree& node_pt)
{
	NodeAttributes::ReadPtree(node_pt.get_child("attributes"));
}

ptree Node::ToPtree() const
{
	ptree ret;
	ret.put("id", GetIndex());
	ret.put("x", (*this)[0]);
	ret.put("y", (*this)[1]);
	ret.put("boundary", IsAtBoundary());
	ret.put_child("attributes", NodeAttributes::ToPtree());
	return ret;
}

ostream& operator<<(ostream& os, Node const& n)
{
	n.Print(os);
	return os;
}

}
