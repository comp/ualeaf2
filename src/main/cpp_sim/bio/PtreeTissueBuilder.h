#ifndef PTREE_TISSUE_BUILDER_H_INCLUDED
#define PTREE_TISSUE_BUILDER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PBMBBuilder.
 */

#include <boost/property_tree/ptree.hpp>
#include <memory>

namespace SimPT_Sim {

class AttributeStore;
class Mesh;
class Tissue;

/**
 * Class directs ptree based tissue building process.
 */
class PtreeTissueBuilder
{
public:
	/**
	 * Build a tissue data set.
	 * @see Tissue
	 */
	Tissue Build(const boost::property_tree::ptree& pt);

private:
	/**
	 * Build (old style) mesh.
	 */
	std::shared_ptr<SimPT_Sim::Mesh> BuildMesh(
		const boost::property_tree::ptree& mesh_pt);

	/**
	 * Build attribute store for cell or wall or ...
	 * @param   entity_id      to which entity (wall, cell, ...) the store relates.
	 * @param   index_pt       index descriptor i property tree format
	 * @param   values_pt      values in property tree format.
	 */
	std::shared_ptr<AttributeStore> BuildAttributeStore(
		const std::string& entity_id,
		const boost::property_tree::ptree& index_pt,
		const boost::property_tree::ptree& values_pt);

private:
	void BuildBoundaryPolygon(const boost::property_tree::ptree& cells_pt);

	void BuildCells(const boost::property_tree::ptree& cells_pt);

	void BuildNodes(const boost::property_tree::ptree& nodes_pt);

	void BuildWalls(const boost::property_tree::ptree& walls_pt);

	void ConnectCellsToWalls(const boost::property_tree::ptree& cells_pt);

private:
	std::shared_ptr<SimPT_Sim::Mesh>  m_mesh;
	std::shared_ptr<AttributeStore>    m_cell_attributes;
	std::shared_ptr<AttributeStore>    m_node_attributes;
	std::shared_ptr<AttributeStore>    m_wall_attributes;
};

} //namespace

#endif // end_of_include_guard

