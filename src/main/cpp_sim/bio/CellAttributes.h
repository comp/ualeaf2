#ifndef CELL_ATTRIBUTES_H_
#define CELL_ATTRIBUTES_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CellAttributes.
 */

#include "bio/Wall.h"
#include "bio/BoundaryType.h"

#include <iosfwd>
#include <vector>

namespace SimPT_Sim {

class Mesh;
class Node;

/**
 * Attributes of Cell.
 */
class CellAttributes
{
public:
	///
	CellAttributes(unsigned int chem_count);

	///
	virtual ~CellAttributes() {};

	///
	BoundaryType GetBoundaryType() const { return m_boundary_type; }

	///
	int GetCellType() const { return m_cell_type; }

	///
	double GetChemical(unsigned int c) const;

	///
	std::vector<double> GetChemicals() const { return m_chem; }

	///
	bool IsDead() const { return m_dead; }

	///
	int GetDivisionCounter() { return m_div_counter; }

	///
	int GetDivisionTime() { return m_div_time; }

	///
	int GetTimeSinceDivision() { return m_time_since_div; }

	///
	double GetSolute() const { return m_solute; }

	///
	double GetStiffness() const { return m_stiffness; }

	///
	double GetTargetArea() const { return m_target_area; }

	///
	double GetTargetLength() const { return m_target_length; }

	///
	void IncrementDivisionCounter() { ++m_div_counter; }

	///
	bool IsFixed() const  { return m_fixed; }

	///
	virtual std::ostream& Print(std::ostream& os) const;

	///
	virtual void ReadPtree(const boost::property_tree::ptree& cell_pt);

	///
	BoundaryType SetBoundaryType(BoundaryType b) { return m_boundary_type = b; }

	///
	void SetCellType(int ct) { m_cell_type = ct; }

	///
	void SetChemical(unsigned int chem, double conc);

	///
	void SetChemicals(std::vector<double> const& chem);

	///
	void SetDead(bool dead) { m_dead = dead; }

	///
	void SetDivisionCounter(int div_counter) { m_div_counter = div_counter; }

	///
	void SetDivisionTime(double div_time) { m_div_time = div_time; }

	///
	void SetTimeSinceDivision(int time_since_div) { m_time_since_div = time_since_div; }

	///
	void SetSolute(double s) { m_solute = s; }

	///
	void SetStiffness(double stiffness) { m_stiffness = stiffness; }

	///
	void SetTargetArea(double a) { m_target_area = a; }

	///
	void SetTargetLength(double l) { m_target_length = l; }

	/**
	 * Convert the cell attributes to a ptree.
	 * The format of a cell attributes ptree is as follows:
	 *
	 * <boundary>[the boundary type]</boundary>
	 * <dead>[true if the cell is dead]</dead>
	 * <division_count>[the number of divisions of the cell]</division_count>
	 * <fixed>[true if the cell is fixed]</fixed>
	 * <stiffness>[the stiffness]</stiffness>
	 * <target_area>[the target area]</target_area>
	 * <target_length>[the target length]</target_length>
	 * <type>[the cell type]</type>
	 * <chemical_array> (for every chemical this cell has)
	 * 	<chemical>[the value of the chemical]</chemical>
	 * </chemical_array>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

protected:
	BoundaryType         m_boundary_type;     ///<
	int                  m_cell_type;         ///<
	std::vector<double>  m_chem;              ///<
	bool                 m_dead;              ///< For future use, when apoptosis gets set up.
	int                  m_div_counter;       ///< Keep track of divisions in the cell ancestry.
	int                  m_div_time;          ///<
	int                  m_time_since_div;
	bool                 m_fixed;             ///<
	double               m_solute;            ///<
	double               m_stiffness;         ///< Stiffness like in Hogeweg (2000).
	double               m_target_area;       ///<
	double               m_target_length;     ///<
};

std::ostream& operator<<(std::ostream& os, CellAttributes const& v);

} // namespace

#endif // end_of_include_guard
