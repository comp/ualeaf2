#ifndef ATTRIBUTE_STORE_H_INCLUDED
#define ATTRIBUTE_STORE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of AttributeStore.
 */

////////////////////////////////////////////////////////////////////////////
// WARNING: FILE ALSO GETS PROCESSED BY SWIG. SWIG (VERSIONS EARLY 2013)  //
// DOES NOT ACCEPT TWO CONSECUTIVE >> TEMPLATE TYPE PARAMETERLIST ENDING  //
// BRACKETS WITHOUT INTERVENING BLANK (EVEN THOUGH C++11 DOES ACCPT THIS) //
////////////////////////////////////////////////////////////////////////////

#include <boost/property_tree/ptree.hpp>
#include <algorithm>
#include <cstddef>
#include <functional>
#include <map>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

namespace SimPT_Sim {

/**
 * Store and manage attributes.
 */
class AttributeStore
{
public:
	/**
	 * Default constructor creates empty AttributeStore.
	 */
	AttributeStore() = default;

	/**
	 * Creates an AttributeStore from an attribute property tree.
	 * An attribute property tree has the following structure:
	 *
	 * <attribute>
	 *    <name>testint</name>
	 *    <type>int</type>
	 *    <default>255</default>
	 * </attribute>
	 * <attribute>
	 *    <name>a_bool</name>
	 *    <type>bool</type>
	 *    <default>true</default>
	 * </attribute>
	 * <attribute>
	 *    <name>another_int</name>
	 *    <type>int</type>
	 *    <default>10</default>
	 * </attribute>
	 * <attribute>
	 *    <name>testdouble</name>
	 *    <type>double</type>
	 *    <default>3.14</default>
	 * </attribute>
	 * <attribute>
	 *    <name>teststring</name>
	 *    <type>string</type>
	 *    <default>hellodynamic world</default>
	 * </attribute>
	 *
	 * The ordering (in types and or names in inconsequential.
	 */
	AttributeStore(const boost::property_tree::ptree& pt);

	/**
	 * Reference to the appropriate (type and name) attribute container.
	 * Throws runtime error if name not present in index.
	 */
	template<typename T>
	std::vector<T>& Container(const std::string& name);

	/**
	 * Constant reference to the appropriate (type and name) attribute container.
	 * Throws runtime error if name not present in index.
	 */
	template<typename T>
	const std::vector<T>& Container(const std::string& name) const;

	/**
	 * Return all attribute values at position i in the container in property tree format.
	 * The format is:
	 * <name1>value1</name1>
	 * <name2>value2</name2>
	 * etc.
	 */
	boost::property_tree::ptree GetAttributeValues(std::size_t pos) const;

	/**
	 * Return default value for attribute of type and name.
	 * Throws runtime error if name not present in index.
	 */
	template<typename T>
	T  GetDefaultValue(const std::string& name) const;

	/**
	 * Return a vector containing the names of attributes of type T.
	 */
	template<typename T>
	std::vector<std::string> GetAttributeNames() const;

	/**
	 * Return original attribute ptree the AttributeStore was created from.
	 * We have this method to recover the original in terms of ordering, comments etc.
	 * @see AttributeStore(const boost::property_tree::ptree& pt) for format.
	 */
	boost::property_tree::ptree GetIndexPtree() const;

	/**
	 * Produces representation of index structure in a ptree descriptor.
	 * This method does note preserve ordering and loses comments wrt original
	 * attribute property tree.
	 */
	boost::property_tree::ptree GetIndexDump() const;

	/**
	 * Set container of type and name to values iff that container is currently empty and values
	 * is size consistent with the current containers in store. This means either the current
	 * container is empty or it has the same size as values.
	 *
	 * Throws runtime error if name not present in index or values not size consistent.
	 */
	template<typename T>
	void Initialize(const std::string& name, const std::vector<T>& values);

	/**
	 * Set container of type and name to values iff that container is currently empty and values
	 * is size consistent with the current containers in store. This means either the current
	 * container is empty or it has the same size as values.
	 *
	 * Throws runtime error if name not present in index or values not size consistent.
	 */
	template<typename T>
	void Initialize(const std::string& name, std::vector<T>&& values);

	/**
	 * Returns true iff there is an attribute of type and name.
	 */
	template<typename T>
	bool IsAttribute(const std::string& name) const;

	/**
	 * Returns true iff no attributes are defined.
	 */
	bool IsEmpty() const;

	/**
	 * Strict size consistent iff all containers have the same size.
	 *
	 * @return  tuple with bool (true or false for consistent), size_t (size of containers
	 * but undefined if not consistent).
	 */
	std::tuple<bool, std::size_t> IsStrictSizeConsistent() const;

	/**
	 * Weak size consistent iff all containers with non-zero size have the same size.
	 */
	bool IsWeakSizeConsistent(std::size_t N) const;

private:
	/**
	 * Check whether containers of type T all have size N.
	 */
	template<typename T>
	bool SizeCheckStrict(std::size_t N) const;

	/**
	 * Check whether all containers of type T have either size N or size zero.
	 */
	template<typename T>
	bool SizeCheckWeak(std::size_t N) const;

private:
	template<typename T>
	using MapType = std::map<std::string, std::pair<std::vector<T>, T> >;

	/**
	 * Return the mapping of names to containers and default values for given type.
	 */
	template<typename T>
	MapType<T>& IndexMap();

	/**
	 * Return the mapping of names to containers and default values for given type.
	 */
	template<typename T>
	const MapType<T>& IndexMap() const;

	MapType<bool>                  m_map_b;   ///< IndexMap of type bool
	MapType<int>                   m_map_i;   ///< IndexMap of type int
	MapType<double>                m_map_d;   ///< IndexMap of type double
	MapType<std::string>           m_map_s;   ///< IndexMap of type std::string
	boost::property_tree::ptree    m_ptree;   ///< Attribute ptree used for creation.
};

/**
 * Insertion of text representation in ostream.
 */
std::ostream& operator<<(std::ostream& os, const AttributeStore& a);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF PRIVATE ACCESS METHODS (need to be seen by compiler prior to first use).
/////////////////////////////////////////////////////////////////////////////////////////////////////

template<>
inline AttributeStore::MapType<bool>& AttributeStore::IndexMap<bool>()
{
	return m_map_b;
}


template<>
inline const AttributeStore::MapType<bool>& AttributeStore::IndexMap<bool>() const
{
	return m_map_b;
}


template<>
inline AttributeStore::MapType<int>& AttributeStore::IndexMap<int>()
{
	return m_map_i;
}


template<>
inline const AttributeStore::MapType<int>& AttributeStore::IndexMap<int>() const
{
	return m_map_i;
}


template<>
inline AttributeStore::MapType<double>& AttributeStore::IndexMap<double>()
{
	return m_map_d;
}


template<>
inline const AttributeStore::MapType<double>& AttributeStore::IndexMap<double>() const
{
	return m_map_d;
}


template<>
inline AttributeStore::MapType<std::string>& AttributeStore::IndexMap<std::string>()
{
	return m_map_s;
}


template<>
inline const AttributeStore::MapType<std::string>& AttributeStore::IndexMap<std::string>() const
{
	return m_map_s;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF PUBLIC METHODS
/////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline std::vector<T>& AttributeStore::Container(const std::string& name)
{
	const auto c = IndexMap<T>().find(name);
	if (c == IndexMap<T>().end()) {
		throw std::runtime_error("AttributeStore::Attributes(const std::string& name): name not in index: " + name);
	}
	return c->second.first;
}


template<typename T>
inline const std::vector<T>& AttributeStore::Container(const std::string& name) const
{
	const auto c = IndexMap<T>().find(name);
	if (c == IndexMap<T>().end()) {
		throw std::runtime_error("AttributeStore::Attributes(const std::string& name): name not in index: " + name);
	}
	return c->second.first;
}


template<typename T>
inline std::vector<std::string> AttributeStore::GetAttributeNames() const
{
	std::vector<std::string> names;
	for (const auto& c : IndexMap<T>()) {
		names.push_back(c.first);
	}
	return names;
}

inline boost::property_tree::ptree AttributeStore::GetIndexPtree() const
{
	return m_ptree;
}


template<typename T>
inline T AttributeStore::GetDefaultValue(const std::string& name) const
{
	const auto c = IndexMap<T>().find(name);
	if (c == IndexMap<T>().end()) {
		throw std::runtime_error("AttributeStore::GetDefaultValue(const std::string& name): name not in index: " + name);
	}
	return c->second.second;
}


template<typename T>
inline void AttributeStore::Initialize(const std::string& name, const std::vector<T>& values)
{
	if ( IsWeakSizeConsistent(values.size()) ) {
		Container<T>(name) = values;
	} else {
		throw std::runtime_error("AttributeStore::Initialize: name not in index or size inconsistent " + name);
	}
}


template<typename T>
inline void AttributeStore::Initialize(const std::string& name, std::vector<T>&& values)
{
	if ( IsWeakSizeConsistent(values.size()) ) {
		Container<T>(name) = std::move(values);
	} else {
		throw std::runtime_error("AttributeStore::Initialize: name not in index or size inconsistent " + name);
	}
}


template<typename T>
inline bool AttributeStore::IsAttribute(const std::string& name) const
{
	return IndexMap<T>().find(name) != IndexMap<T>().end();
}


inline std::tuple<bool, std::size_t> AttributeStore::IsStrictSizeConsistent() const
{
	bool status = true;
	size_t s = 0;

	if (!IndexMap<bool>().empty()) {
		s = IndexMap<bool>().cbegin()->second.first.size();
		status = SizeCheckStrict<bool>(s) && SizeCheckStrict<int>(s)
			&& SizeCheckStrict<double>(s) && SizeCheckStrict<std::string>(s);
	} else  if (!IndexMap<int>().empty()) {
		s = IndexMap<int>().cbegin()->second.first.size();
		status = SizeCheckStrict<int>(s) && SizeCheckStrict<double>(s) && SizeCheckStrict<std::string>(s);
	} else if (!IndexMap<double>().empty()) {
		s = IndexMap<int>().cbegin()->second.first.size();
		status = SizeCheckStrict<double>(s) && SizeCheckStrict<std::string>(s);
	} else if (!IndexMap<std::string>().empty()){
		s = IndexMap<int>().cbegin()->second.first.size();
		status = SizeCheckStrict<std::string>(s);
	}

	return std::make_tuple(status, s);
}


inline bool AttributeStore::IsWeakSizeConsistent(std::size_t N) const
{
	// Short circuit avoids redundant evaluations.
	return SizeCheckWeak<int>(N) && SizeCheckWeak<double>(N) && SizeCheckWeak<std::string>(N);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF PRIVATE METHODS.
/////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline bool AttributeStore::SizeCheckStrict(std::size_t N) const
{
	bool status = true;
	for (const auto& e : IndexMap<T>()) {
		const auto s = e.second.first.size();
		if ( s != N ) {
			status = false;
			break;
		}
	}
	return status;
}


template<typename T>
inline bool AttributeStore::SizeCheckWeak(std::size_t N) const
{
	bool status = true;
	for (const auto& e : IndexMap<T>()) {
		const auto s = e.second.first.size();
		if ( !( s == N || s == 0) ) {
			status = false;
			break;
		}
	}
	return status;
}

} // namespace

#endif // include guard

