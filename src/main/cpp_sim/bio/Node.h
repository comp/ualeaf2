#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Node.
 */

#include "NodeAttributes.h"
#include "math/math.h"
#include "util/container/SegmentedVector.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <iosfwd>

namespace SimPT_Editor {
	class EditableMesh;
}

namespace SimPT_Sim {

/**
 * Node in cell wall.
 */
class Node : public NodeAttributes
{
public:
	///
	virtual ~Node() {};

	///
	unsigned int GetIndex() const { return m_index; }

	///
	bool IsAtBoundary() const { return m_at_boundary; }

	///
	std::ostream& Print(std::ostream& os) const;

	///
	virtual void ReadPtree(const boost::property_tree::ptree& node_pt);

	///
	void SetAtBoundary(bool b) { m_at_boundary = b; }

	/**
 	 * Convert the node to a ptree.
	 * The format of a node ptree is as follows:
	 *
	 * <id>[the index of the node]</id>
	 * <x>[the x-coordinate of the node's position]</x>
	 * <y>[the y-coordinate of the node's position]</y>
	 * <boundary>[true if the node is at the boundary of the mesh]</boundary>
	 * <attributes>
	 *	NodeAttributes::ToPtree()
	 * </attributes>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

public:
	///
	operator std::array<double, 3>() { return m_position; }

	///
	operator const std::array<double,3>() const { return m_position; }

	///
	Node& operator+=(const std::array<double,3>& a)
	{
		m_position += a;
		return *this;
	}

	///
	Node& operator-=(const std::array<double,3>& a)
	{
		m_position -= a;
		return *this;
	}

	///
	std::array<double,3> operator+(const Node& n) const
	{
		return m_position + n.m_position;
	}

	///
	std::array<double,3> operator-(const Node& n) const
	{
		return m_position - n.m_position;
	}

	///
	double& operator[](size_t i) { return m_position[i]; }

	///
	double operator[](size_t i) const { return m_position[i]; }

private:
	std::array<double, 3>  m_position;      ///< Node position
	bool                   m_at_boundary;   ///< Included for for performance
	unsigned int	       m_index;         ///< Node index

private:
	friend class Mesh;
	template<typename T, size_t N>
	friend class SimPT_Sim::Container::SegmentedVector;
	friend class SimPT_Editor::EditableMesh;

private:
	Node(unsigned int index, const std::array<double, 3>& src, bool at_boundary);
	Node(const Node &src);
	Node& operator=(const Node&);
};

std::ostream& operator<<(std::ostream& os, const Node& n);

}

#endif //end_of_include_guard
