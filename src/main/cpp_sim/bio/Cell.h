#ifndef CELL_H_INCLUDED
#define CELL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Cell.
 */

#include "CellAttributes.h"
#include "GeoData.h"
#include "util/container/SegmentedVector.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <list>
#include <memory>
#include <vector>

namespace SimPT_Editor {
	class EditableMesh;
}

namespace SimPT_Sim {

using boost::property_tree::ptree;
class Edge;
class Mesh;
class Node;
class Wall;

/**
 * A cell contains walls and nodes. The nodes should be ordered in counter-clockwise order.
 * The cell id is an unsigned integer, except for the boundary polygon, which has an id of -1.
 */
class Cell: public CellAttributes
{
public:
	///
	virtual ~Cell() {}

	///
	void AddWall(Wall* w);

	/// Convert the cell geometry to a ptree.
	ptree GeometryToPtree() const;

	/// Return the area of the cell.
	double GetArea() const;

	/// Return the signed area of the cell.
	double GetSignedArea() const;

	/// Return the centroid position.
	std::array<double, 3> GetCentroid() const;

	/// Return the circumference along the edges.
	double GetCircumference() const;

	/// Return GeData (area, centroid, area moment of inertia).
	GeoData GetGeoData() const;

	/// Return the index.
	int GetIndex() const { return m_index; }

	/// Access the nodes of cell's polygon.
	const std::vector<Node*>& GetNodes() const { return m_nodes; }

	/// Access the nodes of cell's polygon.
	std::vector<Node*>& GetNodes() { return m_nodes; }

	/// Sum transporters at this cell's side of the walls
	double GetSumTransporters(unsigned int ch) const;

	/// Access the cell's walls.
	const std::list<Wall*>& GetWalls() const { return m_walls; }

	/// Access the cell's walls.
	std::list<Wall*>& GetWalls() { return m_walls; }

	///
	bool HasEdge(const Edge& edge) const;

	///
	bool HasBoundaryWall() const;

	///
	bool HasNeighborOfTypeZero() const;

	///
	bool IsBoundaryPolygon() const { return m_index == -1; }

	/// Strict neighbor (you're never your own neighbor)
	bool IsWallNeighbor(Cell* cell) const;

	/// Strict neighbor (you're never your own neighbor)
	//bool IsWallNeighbor(Cell* cell) const { return IsWallNeighbor(cell); }

	///
	void Move(const std::array<double, 3>& a);

	/// Check for self-intersection when moving_node gets displaced
	bool MoveSelfIntersects(Node* moving_node, std::array<double, 3> new_pos);

	/// Check if displacement of moving_nodes causes the cell to have extremely acute angles
	bool MoveCreatesTinyAngles(Node* moving_node, std::array<double, 3> new_pos);

	///
	virtual std::ostream& Print(std::ostream& os) const;

	///
	virtual void ReadPtree(const ptree& cell_pt);

	///
	void ReassignWall(Wall* w, Cell* to);

	///
	void SetGeoDirty();

	///
	void SetTransporters(unsigned int chem, double conc);

	///
	void SetTransporters(unsigned int chem, double conc, double lat);

	///
	void SetTransportersLight(unsigned int chem, double conc, double lat);


	/// Convert the cell (geometry and attributes) to a ptree.
	virtual ptree ToPtree() const;

private:
	friend class Mesh;
	template<typename T, size_t N>
	friend class SimPT_Sim::Container::SegmentedVector;
	friend class SimPT_Editor::EditableMesh;

private:
	Cell(int index, unsigned int chem_count);      ///< Only Mesh or CompactStorage<Cell> can construct Cell
	Cell(const Cell& src) = delete;                ///< No copying.
	Cell& operator=(const Cell& src) = delete;     ///< No assignment.

private:
	/// Compute geometric properties of cell: area.
	void CalculateArea() const;

	/// Compute geometric properties of cell: centroid.
	void CalculateCentroid() const;

	/// Compute geometric properties of cell: area, centroid and area moment.
	void CalculateGeoData() const;

private:
	int                    m_index;
	std::vector<Node*>     m_nodes;
	std::list<Wall*>       m_walls;

private:
	mutable bool           m_dirty_area;
	mutable bool           m_dirty_centroid;
	mutable bool           m_dirty_geo_data;
	mutable GeoData        m_geo_data;
};

std::ostream& operator<<(std::ostream& os, Cell const& v);

} // namespace

#endif // end_of_include_guard
