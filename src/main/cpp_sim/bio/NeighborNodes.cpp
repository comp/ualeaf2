/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NeighborNodes.
 */

#include "Cell.h"
#include "Node.h"
#include "NeighborNodes.h"
#include <iostream>

using std::ostream;

namespace SimPT_Sim {

NeighborNodes::NeighborNodes(Cell* c, Node* n1, Node* n2)
		: m_cell(c), m_nb1(n1), m_nb2(n2) {}

Cell* NeighborNodes::GetCell() const
{
	return m_cell;
}

Node* NeighborNodes::GetNb1() const
{
	return m_nb1;
}

Node* NeighborNodes::GetNb2() const
{
	return m_nb2;
}

bool NeighborNodes::operator==(const NeighborNodes& nn) const
{
	return m_cell == nn.m_cell && m_nb1 == nn.m_nb1 && m_nb2 == nn.m_nb2;
}

bool NeighborNodes::operator!=(const NeighborNodes& nn) const
{
	return !operator==(nn);
}

ostream& operator<<(ostream& os, const NeighborNodes& n)
{
	os << " {" << n.GetCell()->GetIndex() << " "
		<< n.GetNb1()->GetIndex() << " " << n.GetNb2()->GetIndex() << "}";
	return os;
}

}
