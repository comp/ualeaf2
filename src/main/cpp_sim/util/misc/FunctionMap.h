#ifndef INC_FUNCTION_MAP_H
#define INC_FUNCTION_MAP_H
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation FunctionMap.
 */

#include <functional>
#include <initializer_list>
#include <list>
#include <map>
#include <string>
#include <utility>

namespace SimPT_Sim {
namespace Util {

/**
 * A map to hold std::functions.
 */
template<typename S>
class FunctionMap
{
public:
	// Type of function stored in the map.
	using FunctionType = std::function<S>;

private:
	///
	using  MapType = std::map<const std::string, const FunctionType>;

public:
	/// Construct an empty map.
	FunctionMap() {}

	/// Construct map from initializer_list.
	FunctionMap(std::initializer_list<typename MapType::value_type> l) : m_map(l) {}

	/// Destructor must be virtual.
	virtual ~FunctionMap() {}

	/// Return function for given name.
	FunctionType Get(const std::string& name) const
	{
		return (m_map.count(name) != 0) ? m_map.at(name) : FunctionType();
	}

	/// Check whether function name is present.
	bool IsValid(const std::string& name) const
	{
		return (m_map.count(name) != 0) && (m_map.at(name));
	}

	/// Register a function.
	bool Register(const std::string& name, const FunctionType& f)
	{
		bool status = false;
		if ((m_map.count(name) == 0) && (f)){
			m_map.insert(std::make_pair(name, f));
			status = true;
		}
		return status;
	}

	/// List the names.
	std::list<std::string> List() const
	{
		std::list<std::string> l;
		for (const auto& e : m_map) {
			l.push_back(e.first);
		}
		return l;
	}

private:
	MapType  m_map;
};

} // namespace
} // namespace

#endif // end-of-include-guard

