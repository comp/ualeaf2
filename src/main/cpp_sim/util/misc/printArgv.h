#ifndef PRINTARGV_H_INCLUDED
#define PRINTARGV_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file.
 * Print (argc, argv) info.
 */

#include <iostream>

namespace SimPT_Sim {
namespace Util {

void printArgv(int argc, char* argv[], std::ostream& os)
{
	os << "argc   : " << argc << std::endl;
	for (int i = 0; i < argc; i++) {
		os << "argv[" << i << "]: " << argv[i] << std::endl;
	}
	return;
}

} // namespace
} // namespace

#endif // end-of-include-guard
