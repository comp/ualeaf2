#ifndef UTIL_LOG_DEBUG_H_INCLUDED
#define UTIL_LOG_DEBUG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Macro defs for debug and logging.
 */

#include <string>
#include <QDebug>
#include <QString>

// Macro to take advantage of PRETTY_FUNCTION over plain FUNCTION with gcc.
#if defined(__GNUC__)
#define SIMPT_FUNCTION_NAME __PRETTY_FUNCTION__
#else
#define SIMPT_FUNCTION_NAME __FUNCTION__
#endif

#define VL_HERE SIMPT_FUNCTION_NAME

inline QDebug operator<<(QDebug dbg, std::string const& s)
{
    dbg.nospace() << s.c_str();
    return dbg.space();
}


#if defined( QT_NO_DEBUG_OUTPUT )
#define VL_DEBUG() qDebug()
#else
#define VL_DEBUG() (qDebug().nospace() << QString("%1").arg(SIMPT_FUNCTION_NAME).toStdString().c_str() << ">").space()
#endif

#endif // end-of-include-guard
