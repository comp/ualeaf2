#ifndef UTIL_CLOCK_MAN_UTILS_H_INCLUDED
#define UTIL_CLOCK_MAN_UTILS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence. You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TimeKeeper::Utils.
 */

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Utilities to tag clocks and to reformat number of ticks to a string.
 */
struct Utils
{
	/// Procude string in hh:mm:ss format.
	static std::string ToColonString(std::chrono::seconds d)
	{
		using namespace std;
		using namespace std::chrono;

		ostringstream oss;
		hours     hh = duration_cast < hours > (d);
		minutes   mm = duration_cast < minutes > (d % hours(1));
		seconds   ss = duration_cast < seconds > (d % minutes(1));

		oss << right << setfill('0') << setw(2) << hh.count() << ":" << setw(2) << mm.count()
			<< ":" << setw(2) << ss.count();
		return oss.str();
	}

	/// Produce string in hh:mm:ss:mus format
	static std::string ToColonString(std::chrono::milliseconds d)
	{
		using namespace std;
		using namespace std::chrono;

		ostringstream oss;
		hours         hh    = duration_cast < hours > (d);
		minutes       mm    = duration_cast < minutes > (d % hours(1));
		seconds       ss    = duration_cast < seconds > (d % minutes(1));
		milliseconds  milli = duration_cast < milliseconds > (d % seconds(1));

		oss << right << setfill('0') << setw(2) << hh.count() << ":" << setw(2) << mm.count()
			<< ":" << setw(2) << ss.count() << ":" << setw(3) << milli.count();
		return oss.str();
	}

	/// Produce string in hh:mm:ss:ms:mus format.
	static std::string ToColonString(std::chrono::microseconds d)
	{
		using namespace std;
		using namespace std::chrono;

		ostringstream oss;
		hours         hh    = duration_cast < hours > (d);
		minutes       mm    = duration_cast < minutes > (d % hours(1));
		seconds       ss    = duration_cast < seconds > (d % minutes(1));
		milliseconds  milli = duration_cast < milliseconds > (d % seconds(1));
		microseconds  micro = duration_cast < microseconds > (d % milliseconds(1));

		oss << right << setfill('0') << setw(2) << hh.count() << ":" << setw(2) << mm.count()
			<< ":" << setw(2) << ss.count() << ":" << setw(3) << milli.count() << ":" << setw(3) << micro.count();
		return oss.str();
	}

	/// Produce string in hh:mm:ss:ms:mus:ns format.
	static std::string ToColonString(std::chrono::nanoseconds d)
	{
		using namespace std;
		using namespace std::chrono;

		ostringstream oss;
		hours          hh    = duration_cast < hours > (d);
		minutes        mm    = duration_cast < minutes > (d % hours(1));
		seconds        ss    = duration_cast < seconds > (d % minutes(1));
		milliseconds   milli = duration_cast < milliseconds > (d % seconds(1));
		microseconds   micro = duration_cast < microseconds > (d % milliseconds(1));
		nanoseconds    nano  = duration_cast < nanoseconds > (d % microseconds(1));

		oss << right << setfill('0') << setw(2) << hh.count() << ":" << setw(2) << mm.count()
			<< ":" << setw(2) << ss.count() << ":" << setw(3) << milli.count() << ":" << setw(3) << micro.count()
			<< ":" << setw(3) << nano.count();
		return oss.str();
	}
};

} // namespace
} // namespace

#endif // end of include guard
