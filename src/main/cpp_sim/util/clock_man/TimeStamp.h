#ifndef UTIL_CLOCK_MAN_TIMESTAMP_H_INCLUDED
#define UTIL_CLOCK_MAN_TIMESTAMP_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence. You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * TimeStamp class.
 */
#include <string>
#include <ctime>
#include <chrono>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Provides wall-clock time stamp using the time call.
 * The time is that of the constructor call.
 */
class TimeStamp
{
public:
	/// Constructor marks the time for the time stamp.
	TimeStamp() : m_tp(std::chrono::system_clock::now()) {}

	/// Returns string with the time stamp after eliminating newline.
	std::string ToString() const
	{
		std::time_t t = std::chrono::system_clock::to_time_t(m_tp);
		std::string str = std::ctime(&t);
		str[str.length() - 1] = ' ';
		return str;
	}

	/// Returns time stamp as a time_t.
	std::time_t ToTimeT() const
	{
		return std::chrono::system_clock::to_time_t(m_tp);
	}

private:
	std::chrono::system_clock::time_point m_tp;
};

/**
 * TimeStamp helper inserts string representation in output stream.
 */
inline std::ostream&
operator<<(std::ostream& os, TimeStamp t) {
	return (os << t.ToString());
}

} // namespace
} // namespace

#endif  // include guard
