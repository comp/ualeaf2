#ifndef UTIL_CLOCK_MAN_CLOCK_CLIB_H_INCLUDED
#define UTIL_CLOCK_MAN_CLOCK_CLIB_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for ClockCLib.
 */

#include <chrono>
#include <ctime>
#include <stdexcept>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Clock based on C library clock_t clock() which returns an
 * approximate processor time in ticks. There are CLOCKS_PER_SEC
 * ticks per second and POSIX requires this be 1000000.
 */
class ClockCLib
{
public:
	typedef std::chrono::milliseconds duration;
	typedef duration::rep rep;
	typedef duration::period period;
	typedef std::chrono::time_point<ClockCLib, duration> time_point;

	static time_point now()
	{
		std::clock_t t = std::clock();
		if ( static_cast<std::clock_t>(-1) == t ) {
			throw std::runtime_error("Error in ClockCLib");
		}

		constexpr double conv  = 1.0 / CLOCKS_PER_SEC;
		constexpr double ratio = static_cast<double>(period::den)/static_cast<double>(period::num);
		const duration d(static_cast<rep>(static_cast<double>(t) * conv * ratio));
		return time_point(d);
	}
};

} // namespace
} // namespace

#endif // end of include guard
