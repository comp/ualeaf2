/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Info on git revision and commit date.
 */

#include "RevisionInfo.h"

#cmakedefine GIT_FOUND

namespace SimPT_Sim {
namespace Util {

std::string RevisionInfo::CommitDate()
{
#ifdef GIT_FOUND
	return @SIMPT_WC_LAST_CHANGED_DATE@;
#else
	return "@SIMPT_WC_LAST_CHANGED_DATE@";
#endif
}

std::string RevisionInfo::CompoundId()
{
	return HashId() + " " + CommitDate();
}

std::string RevisionInfo::HashId()
{

	return "@SIMPT_WC_REVISION_HASH@";
}

std::string RevisionInfo::Id(const std::string& type)
{
	std::string s;
	if (type == "hash")
		s = HashId();
	else if (type == "date")
		s = CommitDate();
	else
		s = CompoundId();
	return s;
}

} // namespace
} // namespace
