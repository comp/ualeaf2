#ifndef SIMPT_RGB2HSV_H_INCLUDED
#define SIMPT_RGB2HSV_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Rgb color def to Hsv color def: r,g,b values are from 0 to 1
 * and  h = [0,360], s = [0,1], v = [0,1] (if s == 0, then h = -1)
 */

#include <array>

namespace SimPT_Sim {
namespace Color {

std::array<double, 3> Rgb2Hsv(double r, double g, double b);

} // namespace
} // namespace

#endif // end_of_include_guard
