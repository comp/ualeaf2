 /*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NodeMover (Metropolis algorithm).
 */

#include "NodeMover.h"

#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "math/DurstenfeldShuffle.h"
#include "math/MeshGeometry.h"
#include "math/RandomEngine.h"
#include "model/ComponentFactoryProxy.h"
#include "sim/CoreData.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

#include <trng/uniform_dist.hpp>
#include <trng/uniform01_dist.hpp>
#include <trng/normal_dist.hpp>
#include <omp.h>
#include <algorithm>
#include <array>
#include <set>
#include <unordered_set>

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim{

NodeMover::NodeMover()
	: m_mesh(nullptr), m_tissue_energy(0.0)
{
}

NodeMover::NodeMover(const CoreData& cd)
{
	Initialize(cd);
}

void NodeMover::CalculateCellEnergies()
{
	m_tissue_energy = 0.0;
	for (auto c : m_mesh->GetCells()) {
		const double e = m_hamiltonian(c);
		m_cell_energies[c] = e;
		m_tissue_energy += e;
	}
	const double e_bp = m_hamiltonian(m_mesh->GetBoundaryPolygon());
	m_cell_energies[m_mesh->GetBoundaryPolygon()] = e_bp;
	m_tissue_energy += e_bp;
}

void NodeMover::CalculateCellEnergiesDelta()
{
	m_tissue_energy = 0.0;
	for (auto c : m_mesh->GetCells()) {
		m_tissue_energy += m_hamiltonian(c);
	}
	m_tissue_energy += m_hamiltonian(m_mesh->GetBoundaryPolygon());
}

vector<size_t> NodeMover::GetLookaheadsByEdges(const vector<unsigned int>& shuffled_indices, const CSRMatrix& nni) const
{
        const auto& nodes = m_mesh->GetNodes();
        const size_t num_nodes = nodes.size();

        // Contains indices (offsets) of the first block elements
        vector<size_t> block_offsets;
        block_offsets.push_back(0); // First block starts at index 0

        // Loop over (shuffled) nodes
        vector<int> touched_nodes;
        for (size_t i = 0; i < num_nodes; ++i) {
                const int n_i = shuffled_indices[i];
                // Check if node was already touched
                if (find(touched_nodes.begin(), touched_nodes.end(), n_i) != touched_nodes.end()) {
                        // Current node belongs to previously touched nodes: clear the
                        // touched nodes and remember the beginning of a new block
                        touched_nodes.clear();
                        block_offsets.push_back(i);
                }
                // Add neighbors of current node to `touched_nodes`. Neighbors n_j of
                // node n_i are retrieved from the node-node incidence matrix.
                // (Note that n_i itself does not have to be added: once it's in the
                // lookahead it won't be visited anymore)
                for (size_t j = nni.row_ptr[n_i]; j < nni.row_ptr[n_i + 1]; ++j) {
                        const int n_j = nni.col_ind[j];
                        touched_nodes.push_back(n_j);
                }
        }
        // Add "sentinel" value to the end to make lookahead calculation easier
        block_offsets.push_back(num_nodes);

        // Calculate lookaheads from offsets
        vector<size_t> lookaheads;
        for (size_t i = 0; i < block_offsets.size() - 1; ++i) {
                lookaheads.push_back(block_offsets[i+1] - block_offsets[i]);
        }

        return lookaheads;
}

vector<size_t> NodeMover::GetLookaheadsByCells(const vector<unsigned int>& shuffled_indices, const CSRMatrix& nci) const
{
        const auto & nodes = m_mesh->GetNodes();
        const size_t num_nodes = nodes.size();

        // Contains indices (offsets) of the first block elements
        vector<size_t> block_offsets;
        block_offsets.push_back(0); // First block starts at index 0

        // Loop over (shuffled) nodes
        vector<int> touched_cells;
        for (size_t i = 0; i < num_nodes; ++i) {
                const int n_i = shuffled_indices[i];
                // Check if node touches cells that were already touched
                if (any_of(nci.col_ind.begin() + nci.row_ptr[n_i],
                                   nci.col_ind.begin() + nci.row_ptr[n_i + 1],
                                   [&](int c_i){ return find(touched_cells.begin(), touched_cells.end(), c_i) != touched_cells.end(); })) {
                        // Current node shares cells with already touched cells: clear the
                        // touched cells and remember the beginning of a new block
                        touched_cells.clear();
                        block_offsets.push_back(i);
                }
                // Add current node's cells to touched cells
                // Loop over all cells incident with node n_i
                // Checking for equality is not necessary: if cell was there, the touched_cells are empty
                // if it was not, they will be added, but they are unique already.
                for (size_t j = nci.row_ptr[n_i]; j < nci.row_ptr[n_i + 1]; ++j) {
                        const int c_i = nci.col_ind[j];
                        touched_cells.push_back(c_i);
                }

        }
        // Add "sentinel" value to the end to make lookahead calculation easier
        block_offsets.push_back(num_nodes);

        // Calculate lookaheads from offsets
        vector<size_t> lookaheads;
        for (size_t i = 0; i < block_offsets.size() - 1; ++i) {
                lookaheads.push_back(block_offsets[i+1] - block_offsets[i]);
        }

        return lookaheads;
}

void NodeMover::Initialize(const CoreData& cd)
{
        assert((cd.Check() == true) && "CoreData not OK." );

        m_cd                = cd;
        m_cell_energies.clear();

        const auto model_group       = m_cd.m_parameters->get<string>("model.group", "Default");
        const auto factory           = ComponentFactoryProxy::Create(model_group);
        const bool allow_use_delta   = m_cd.m_parameters->get<bool>("cell_mechanics.mc_allow_use_delta", true);

        if (allow_use_delta) {
                m_delta_hamiltonian = factory->CreateDeltaHamiltonian(m_cd);
                assert((m_delta_hamiltonian) && ("DeltaHamiltonian produces nullptr."));
        }
        // Even if allow_use_delta == true, we need the hamiltonian to calculate
        // a baseline energy to determine a convergence rate.
        m_hamiltonian       = factory->CreateHamiltonian(m_cd);
        assert((m_hamiltonian) && ("Hamiltonian produces nullptr."));

        m_mesh              = m_cd.m_mesh.get();
        m_tissue_energy     = 0.0;

        // Initialize the various RNGs.
        omp_set_dynamic(false);
        for (size_t i = 0; i < static_cast<size_t>(omp_get_max_threads()); ++i) {
                m_mc_move_generator.push_back(factory->CreateMoveGenerator(m_cd));
                m_normal_generator.push_back(m_cd.m_random_engine->GetGenerator(trng::normal_dist<double>(0.0, 1.0), i));
                m_uniform_generator.push_back(m_cd.m_random_engine->GetGenerator(trng::uniform01_dist<double>(), i));
        }

        if (UsesDeltaHamiltonian()) {
                CalculateCellEnergiesDelta();
        } else {
                CalculateCellEnergies();
        }
}

bool NodeMover::IsGeometricallyAcceptable(Node* node, const std::array<double, 3>& move) const
{

	// -------------------------------------------------------------------------------------------------------------
	// Check for self-intersection of the cell polygon in all cells owning the node
	// Angle constraint: if an angle in the cell is already acute & is made more acute by move, do not execute move
	// -------------------------------------------------------------------------------------------------------------

	const auto& owners = m_mesh->GetNodeOwningNeighbors(node);
	bool non_intersects = true;
	bool no_acute_angles = true;

	for (const auto& owner : owners) {
		if ((*owner.GetCell()).MoveSelfIntersects(node, *(node)+move)) {
			non_intersects = false;
			break;
		}
		if ((*owner.GetCell()).MoveCreatesTinyAngles(node, *(node)+move)) {
			no_acute_angles = false;
			break;
		}
	}

	//-----------------------------------------------------------------------------
        // TODO: check for flipping of the cell polygons that are triangles
	//-----------------------------------------------------------------------------

	return non_intersects && no_acute_angles;
}

NodeMover::MoveInfo NodeMover::ComputeMoveInfo(Node* n, double step_size, double temperature)
{
	MoveInfo info;

	if (n->IsFixed()) {
	        info = MoveInfo(n);
	} else {
	        const auto m     = m_mc_move_generator[omp_get_thread_num()]() * step_size;
                double dh        = 0.0;
                double stiff     = 0.0;
                for (const auto& owner : m_mesh->GetNodeOwningNeighbors(n)) {
                        dh    += m_delta_hamiltonian(owner, n, m);
                        stiff += owner.GetCell()->GetStiffness();
                }
                const bool dh_accept = dh < -stiff;
                const bool mc_accept = m_uniform_generator[omp_get_thread_num()]() < exp((-dh - stiff) / temperature);
                info = MoveInfo(n, dh_accept, mc_accept, dh, m);
	}

	return info;
}

NodeMover::MetropolisInfo NodeMover::MoveExecution(const MoveInfo& move)
{
	MetropolisInfo info;
	const auto n = move.node;

	// We need not explicitly check node->IsFixed because fixed nodes have both accept values false.
	if ( (move.dh_accept || move.mc_accept) && IsGeometricallyAcceptable(n, move.displacement)) {
		*n += move.displacement;
		for (auto& owner : m_mesh->GetNodeOwningNeighbors(n)) {
			owner.GetCell()->SetGeoDirty();
		}
		for (auto& wall : m_mesh->GetNodeOwningWalls(n)) {
			wall->SetLengthDirty();
		}
		m_tissue_energy += move.dh;
		info = move.dh_accept ? MetropolisInfo(1U, 0U, move.dh, 0.0) : MetropolisInfo(0U, 1U, 0.0, move.dh);
	}

	return info;
}

NodeMover::MetropolisInfo NodeMover::MoveNode(Node* n, double step_size, double temperature)
{
        // Find the node owners (cells) and node owning walls
        const auto& owners          = m_mesh->GetNodeOwningNeighbors(n);
        const auto& owningWalls     = m_mesh->GetNodeOwningWalls(n);

        // Sum the old (current) energy of node owners (cells)
        double old_energy = 0.0;
        double stiff  = 0.0;
        for (const auto& owner : owners) {
                Cell* const cell = owner.GetCell();
                old_energy += m_cell_energies[cell];
                stiff  += cell->GetStiffness();
        }

        // Temporarily move the node.
        const array<double,3> move = m_mc_move_generator[omp_get_thread_num()]() * step_size;
        *n += move;
        for (const auto& owner : owners) {
                owner.GetCell()->SetGeoDirty();
        }
        for (auto& wall : owningWalls) {
                wall->SetLengthDirty();
        }

        // Recompute energies. We do not check validity of the move (i.e. is it
        // non-intersecting) at this point. So, the numbers below might be garbage
        // (in case of intersection the energy caculations produce garbage numbers).
        // But even if, on the basis of the garbage numbers the energy or mc criteria
        // accept the move, it will be rejected because of the intersection.
        double new_energy = 0.0;
        map<Cell*, double> new_energies_cache;
        for (auto& owner : owners) {
                Cell* cell = owner.GetCell();
                const double e = m_hamiltonian(cell);
                new_energies_cache[cell] = e;
                new_energy += e;
        }
        const double dh = new_energy - old_energy;

        // Accept or Reject node movement, provided it is non-intersecting.
        MetropolisInfo info;
        const bool dh_accept = (dh < -stiff);
        const bool mc_accept = m_uniform_generator[omp_get_thread_num()]() < exp((-dh - stiff)/temperature);
        if ((dh_accept || mc_accept) && IsGeometricallyAcceptable(n, move)) {
                // ACCEPT: adjust energy vector.
                for (auto& owner : owners) {
                        Cell* cell = owner.GetCell();
                        m_cell_energies[cell] = new_energies_cache[cell];
                }
                m_tissue_energy += dh;
                info = dh_accept ? MetropolisInfo(1U, 0U, dh, 0.0) : MetropolisInfo(0U, 1U, 0.0, dh);
        } else {
                // REJECT: move the node back to original position.
                *n -= move;
                for (auto& owner : owners) {
                        owner.GetCell()->SetGeoDirty();
                }
                for (auto& wall : owningWalls) {
                        wall->SetLengthDirty();
                }
        }

        return info;
}

NodeMover::MetropolisInfo NodeMover::MoveNodeDelta(Node* n, double step_size, double temperature)
{
        // Find the node owners (cells) and node owning walls.
        const auto& owners       = m_mesh->GetNodeOwningNeighbors(n);
        const auto& owningWalls  = m_mesh->GetNodeOwningWalls(n);

        // Sum energy differences of node owners (cells) due to (potential) node move.
        const array<double,3> move = m_mc_move_generator[omp_get_thread_num()]() * step_size;
        double dh    = 0.0;
        double stiff = 0.0;
        for (const auto& owner : owners) {
                dh    += m_delta_hamiltonian(owner, n, move);
                stiff += owner.GetCell()->GetStiffness();
        }

        // Acceptance criteria for Metropolis.
        const bool dh_accept = (dh < -stiff);
        const bool mc_accept = m_uniform_generator[omp_get_thread_num()]() < exp((-dh - stiff)/temperature);

        // Accept (and the execute it) or Reject (and then do nothing).
        MetropolisInfo info;
        if ((dh_accept || mc_accept) && IsGeometricallyAcceptable(n, move)) {
                *n += move;
                for (auto& owner : owners) {
                        owner.GetCell()->SetGeoDirty();
                }
                for (auto& wall : owningWalls) {
                        wall->SetLengthDirty();
                }
                m_tissue_energy += dh;
                info = dh_accept ? MetropolisInfo(1U, 0U, dh, 0.0) : MetropolisInfo(0U, 1U, 0.0, dh);
        }

        return info;
}

NodeMover::MetropolisInfo NodeMover::SweepOverNodes(double step_size, double temperature)
{
        // -------------------------------------------------------------------------
        // Select the move method.
        // -------------------------------------------------------------------------
        const auto move_method = UsesDeltaHamiltonian() ? &NodeMover::MoveNodeDelta : &NodeMover::MoveNode;

        // -------------------------------------------------------------------------
        // Shuffle the indices for the sweep.
        // -------------------------------------------------------------------------
	const auto& nodes = m_mesh->GetNodes();
	vector<unsigned int> shuffled_indices(nodes.size());
	DurstenfeldShuffle::Fill(shuffled_indices, *m_cd.m_random_engine);

        // -------------------------------------------------------------------------
        // Loop of all indices, move, accumulate info.
        // -------------------------------------------------------------------------
	MetropolisInfo sweep_info;
	for (const auto& i : shuffled_indices) {
		const auto node = nodes[i];
		if (!node->IsFixed()) {
		        sweep_info += (this->*move_method)(node, step_size, temperature);
		}
	}

	return sweep_info;
}


NodeMover::MetropolisInfo NodeMover::SweepOverNodes(const CSRMatrix& nci, double step_size, double temperature)
{
        // -------------------------------------------------------------------------
        // Shuffle the indices for the sweep.
        // -------------------------------------------------------------------------
        vector<unsigned int>   shuffled_indices(m_mesh->GetNodes().size());
        DurstenfeldShuffle::Fill(shuffled_indices, *m_cd.m_random_engine);

        // -------------------------------------------------------------------------
        // Calculate data for parallelization:
        // -------------------------------------------------------------------------
        vector<size_t> cell_lookaheads { GetLookaheadsByCells(shuffled_indices, nci) };

        // -------------------------------------------------------------------------
        // The outer "for" loops over lookahead blocks, the inner "parallel for"
        // loops over indices in each block.
        // Accumulation of MetropolisInfo needs OpenMP reduction which does not
        // work for struct fields. Hence the temporary variables for the reduction.
        // -------------------------------------------------------------------------
        unsigned int info_mdn   = 0U;
        unsigned int info_mup   = 0U;
        double info_sdn         = 0.0;
        double info_sup         = 0.0;
        const auto& nodes       = m_mesh->GetNodes();

        for (size_t curr_offset = 0, block_idx = 0; block_idx < cell_lookaheads.size(); ++block_idx) {
                const size_t lookahead = cell_lookaheads[block_idx];
                #pragma omp parallel for \
                        reduction(+:info_mdn, info_mup, info_sdn, info_sup) \
                        schedule(static) \
                        default(none), firstprivate(curr_offset, step_size, temperature) \
                        shared(shuffled_indices, nodes)
                for (size_t j = 0; j < lookahead; ++j) {
                        const auto node = nodes[shuffled_indices[curr_offset + j]];
                        MetropolisInfo info;
                        if (!node->IsFixed()) {
                                info = MoveNodeDelta(node, step_size, temperature);
                        }
                        info_mdn += info.down_count;
                        info_mup += info.up_count;
                        info_sdn += info.down_dh;
                        info_sup += info.up_dh;
                }
                curr_offset += lookahead;
        }

        return MetropolisInfo(info_mdn, info_mup, info_sdn, info_sup);
}


NodeMover::MetropolisInfo NodeMover::SweepOverNodes(const CSRMatrix& nci, const CSRMatrix& nei, double step_size, double temperature)
{
        // -------------------------------------------------------------------------
        // Shuffle the indices for the sweep.:
        // -------------------------------------------------------------------------
        vector<unsigned int>   shuffled_indices(m_mesh->GetNodes().size());
        DurstenfeldShuffle::Fill(shuffled_indices, *m_cd.m_random_engine);

	// -------------------------------------------------------------------------
	// Calculate data for parallelization:
	// -------------------------------------------------------------------------
	vector<size_t> cell_lookaheads { GetLookaheadsByCells(shuffled_indices, nci) };
	vector<size_t> edge_lookaheads { GetLookaheadsByEdges(shuffled_indices, nei) };
	size_t cell_num_blocks { cell_lookaheads.size() };
	size_t edge_num_blocks { edge_lookaheads.size() };

	// -------------------------------------------------------------------------
	// The loop to compute energy related numbers. Because the move is executed
	// later on, we need to store the move info. Outer "for" loops  over lookahead
	// blocks, inner "parallel for" loops over indices in each block.
	// -------------------------------------------------------------------------
	vector<MoveInfo>  moves(shuffled_indices.size());
	const auto&       nodes = m_mesh->GetNodes();

	for (size_t curr_offset = 0, block_idx = 0; block_idx < edge_num_blocks; ++block_idx) {
		const size_t lookahead = edge_lookaheads[block_idx];
                #pragma omp parallel for \
	                schedule(static) \
	                default(none), firstprivate(curr_offset, step_size, temperature), shared(shuffled_indices, moves, nodes)
                for (size_t j = 0; j < lookahead; ++j) {
                        const size_t i = curr_offset + j;
                        moves[i] = ComputeMoveInfo(nodes[shuffled_indices[i]], step_size, temperature);
                }
		curr_offset += lookahead;
	}

        // -------------------------------------------------------------------------
	// Apply node moves that comply with the requirements.
        // Accumulation of MetropolisInfo needs OpenMP reduction which does not
        // work for struct fields. Hence the temporary variables for the reduction.
        // -------------------------------------------------------------------------
        unsigned int info_mdn   = 0U;
        unsigned int info_mup   = 0U;
        double info_sdn         = 0.0;
        double info_sup         = 0.0;

        for (size_t curr_offset = 0, block_idx = 0; block_idx < cell_num_blocks; ++block_idx) {
                const size_t lookahead = cell_lookaheads[block_idx];
                #pragma omp parallel for \
                        reduction(+:info_mdn, info_mup, info_sdn, info_sup) \
                        schedule(guided) \
	                default(none), firstprivate(curr_offset), shared(moves)
                for (size_t j = 0; j < lookahead; ++j) {
                        const MetropolisInfo info = MoveExecution(moves[curr_offset + j]);
                        info_mdn += info.down_count;
                        info_mup += info.up_count;
                        info_sdn += info.down_dh;
                        info_sup += info.up_dh;
                }
                curr_offset += lookahead;
        }

	return MetropolisInfo(info_mdn, info_mup, info_sdn, info_sup);
}

} // namespace
