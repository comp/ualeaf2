/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CellHousekeeper.
 */

#include "CellHousekeeper.h"

#include "bio/Mesh.h"
#include "model/ComponentFactoryProxy.h"
#include "sim/CoreData.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {

CellHousekeeper::CellHousekeeper()
{
}

CellHousekeeper::CellHousekeeper(const CoreData& cd)
{
	Initialize(cd);
}

ptree CellHousekeeper::Exec() const
{
	for (auto cell :  m_cd.m_mesh->GetCells()) {
		m_cell_housekeep(cell);
	}
	return ptree();
}

void CellHousekeeper::Initialize(const CoreData& cd)
{
	assert(cd.Check() && "CoreData not ok.");
	m_cd = cd;
        const auto model_group = m_cd.m_parameters->get<string>("model.group", "");
        const auto factory = ComponentFactoryProxy::Create(model_group);
        m_cell_housekeep = factory->CreateCellHousekeep(m_cd);
}

} // namespace
