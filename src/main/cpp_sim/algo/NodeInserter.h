#ifndef NODE_INSERTER_H_INCLUDED
#define NODE_INSERTER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NodeInserter.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {

class Edge;
class Mesh;
class Node;
class Sim;

/**
 * Insertion of nodes in existing edges (contributes to the cell mechanics).
 */
class NodeInserter
{
public:
	/// Straight initialization of empty object.
	NodeInserter();

	/// Initializing constructor.
	NodeInserter(const CoreData& cd);

	/// Initializes based on values in property tree.
	void Initialize(const CoreData& cd);

	/// Insert nodes in those edges that have been stretched.
	unsigned int InsertNodes();

private:
	/// Insert nodes when Edges get stretched.
	void InsertNode(Edge& edge);

	/// Update neighbor info after inserting a node.
	void UpdateNeighbors(Edge& edge, Node* new_node);

private:
	CoreData    m_cd;                 ///< Core data (mesh, params, sim_time,...).
	Mesh*       m_mesh;               /// Bare pointer to mesh for convenience.
	double      m_target_node_distance;
	double      m_yielding_threshold;
};

} // namespace

#endif // end_of_include_guard
