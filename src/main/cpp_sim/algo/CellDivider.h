#ifndef CELL_DIVIDER_H_INCLUDED
#define CELL_DIVIDER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CellDivider.
 * WARNING: THIS CLASS IS USED IN THE SIMULATOR BUT ALSO IN THE MESH EDITOR.
 * WARNING: IN THE LATTER CASE SEVERAL DATA MEMBERS< MOST NOTABLY THE
 * WARNING: CoreData MEMBER m_cd HAS DEFAULT VALUE WHICH MEANS ALL OF THE
 * WARNING: POINTS IN m_cd CAN NOT BE DEREFERENCED.
 */

#include "model/ComponentInterfaces.h"
#include "sim/CoreData.h"

#include <array>
#include <functional>
#include <list>
#include <memory>
#include <vector>

namespace SimPT_Sim {

class Cell;
class Edge;
class Mesh;
class Node;
class Sim;
class Wall;
using boost::property_tree::ptree;

/**
 * Class for handling cell division.
 */
class CellDivider
{
public:
	/**
	 * Constructor initializes with CoreData.
	 */
	CellDivider(const CoreData& cd);

	/**
	 * Constructor initializes with mesh (for use with tissue_editor).
	 */
	CellDivider(Mesh* mesh);

	/**
	 * Execute cell divisions.
	 */
	unsigned int DivideCells();

	/**
	 * Divide cell over a pre-defined axis (only when this object has
	 * been initialized with core data).
	 * @param	cell		The given cell.
	 * @param	axis		The axis.
	 */
	void DivideOverAxis(Cell* cell, const std::array<double, 3>& axis);

	/**
	 * A geometric split of cell (see tissue editor's EditableMesh class).
	 */
	std::list<Cell*> GeometricSplitCell(Cell* cell, Node* node1, Node* node2);

	/**
	 * Initializes based on the values of a given mesh.
	 * Since a mesh doesn't contain any parameters as a sim has, the default
	 * values in the constructor will be used.
	 * @param	mesh		The given mesh.
	 */
	void Initialize(Mesh* mesh);

	/**
	 * Initializes based on the values of given core data.
	 * If the target_distance isn't specified, then it will be defined as infinite.
	 * @param	cd		The given core data.
	 */
	void Initialize(const CoreData& cd);

private:
	///
	void AddSharedWallNodes(std::array<std::array<double, 3>, 2>& new_node_vector,
		std::array<Node*, 2>& new_node_index,
		std::vector<Node*>& parent_nodes, std::vector<Node*>& daughter_nodes);

	///
	void AddSplitWalls(Cell* this_cell, Cell* neighbor_cell, int i,
		std::array<Node*, 2>& new_node_index,
		std::array<Wall*, 4>& div_wall, std::array<double, 2>& orig_length,
		std::array<double, 2>& orig_rest_length,
        std::array<double, 2>& orig_rest_length_init);

	/// Determine how cell intersects with axis defined by vector from and to.
	void ComputeIntersection(Cell* cell, std::vector<Node*> intersect_indexes,
		const std::array<double, 3>& from, const std::array<double, 3>& to,
		std::array<std::array<double, 3>, 2>& new_node_vector,
		std::array<Node*, 2>& new_node_index, std::array<int, 2>& new_node_flag,
		std::array<Edge, 2>& divided_edges);

	///
	Node* InsertNewNodeAtIntersect(Cell* this_cell, Cell* neighbor_cell,
		std::array<double, 3> node_vec, const Edge& intrsct_edge);

	///
	Cell* SplitCell(Cell* this_cell, std::vector<Node*> intersect_indexes,
		const std::array<double, 3>& from, const std::array<double, 3>& to);

private:
	CoreData                      m_cd;                        ///< Not initialized for tissue_editor!
	CellDaughtersComponent        m_cell_daughters;            ///< CellDaughter algorithmic component.
	CellSplitComponent            m_cell_split;                ///< CellDaughter algorithmic component.
	double                        m_collapse_node_threshold;   ///<
	bool                          m_copy_wall;                 ///<
	Mesh*                         m_mesh;                      ///< Bare pointer (for use in tissue_editor).
	std::string                   m_model_name;                ///<
	double                        m_target_node_distance;      ///<
	std::function<double()>       m_uniform_generator;         ///<
};

} // namespace

#endif // end_of_include_guard
