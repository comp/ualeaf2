#ifndef COUPLED_SIM_H_INCLUDED
#define COUPLED_SIM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CoupledSim.
 */

#include "CoreData.h"
#include "event/CoupledSimEvent.h"
#include "sim/Sim.h"
#include "SimInterface.h"

#include "util/clock_man/Timeable.h"
#include "util/misc/Subject.h"

#include <boost/property_tree/ptree.hpp>
#include <string>
#include <unordered_map>
#include <vector>

namespace SimPT_Sim {

class ICoupler;

/**
 * Coupled Simulator: multiple simulators and couplers.
 */
class CoupledSim : public SimPT_Sim::SimInterface,
                   public SimPT_Sim::Util::Subject<SimPT_Sim::Event::CoupledSimEvent>,
                   public std::enable_shared_from_this<CoupledSim>
{
public:
	/// Constructor does almost no initialization work (@see CoupledSim::Initialize)
	CoupledSim();

	/// Destructor is virtual.
	virtual ~CoupledSim() {}

        /// @see SimInterface.
        CoreData& GetCoreData() override final { return m_cd; }

	/// @see SimInterface.
	const boost::property_tree::ptree& GetParameters() const;

	/// @see SimInterface.
	std::string GetProjectName() const;

	/// @see SimInterface.
	std::string GetRunDate() const;

	/// @see SimInterface.
	int GetSimStep() const;

	/// @see SimInterface.
	Timings GetTimings() const;

        /// Increase the current time step counter by one.
        void IncrementStepCount() { ++m_sim_step; }

	/// Initialize with full configuration (complete setup prior to first use).
	void Initialize(const boost::property_tree::ptree& pt,
		const std::vector<std::shared_ptr<Sim>>& simulators);

	/// @see SimInterface.
	bool IsAtTermination() const;

	/// @see SimInterface.
	bool IsStationary() const;

	/// @see SimInterface.
	void Reinitialize(const boost::property_tree::ptree& parameters);

	/// @see SimInterface.
	void TimeStep();

private:
	std::unordered_map<std::string, std::shared_ptr<Sim>>  m_simulators;    ///< Sub simulations.
	std::vector<std::shared_ptr<ICoupler>>                 m_couplers;      ///< Couplers.
	CoreData                                               m_cd;            ///< Core data (params, mesh, sim_time,...).
	std::string                                            m_project_name;  ///< Name identifies the simulation project.
	std::string                                            m_run_date;      ///< Date this run of simulation started.
	unsigned int                                           m_sim_step;      ///< Stepcount for this simulation.
	unsigned int                                           m_sim_ODE_steps; ///< Number of ODE steps per time step.
	Timings                                                m_timings;       ///< Timing durations for parts of the computations
};

} // namespace

#endif // end_of_include_guard
