#ifndef SIM_STEP_CONTROLLER_H_INCLUDED
#define SIM_STEP_CONTROLLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time slice propagation.
 */

#include "SimPhase.h"

#include <mutex>

namespace SimPT_Sim {

class Sim;

/**
 * Simulator: mesh & parameters, model & algorithms.
 */
class TimeSlicer
{
public:
	/// Constructor does almost no initialization work.
	TimeSlicer(std::mutex& time_step_mutex, Sim* sim);

	/// No copy-constructor.
	TimeSlicer(const TimeSlicer& other) =delete;

	/// No assignment operator.
	TimeSlicer& operator=(const TimeSlicer& other) =delete;

	/// Destructor does some wrapping up.
	~TimeSlicer();

        /// Increment time step count because slices have covered a full step.
        void IncrementStepCount();

	/// Actualy propagate over a time slice.
	void Go(double time_slice, SimPhase phase = SimPhase::NONE);

private:
	std::lock_guard<std::mutex>    m_guard;            ///< RAII for timestep mutex.
	Sim*                           m_sim;              ///< back pointer to Sim object.
};

} // namespace

#endif // end_of_include_guard
