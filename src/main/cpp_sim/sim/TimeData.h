#ifndef SIMPT_SIM_TIME_DATA_H_INCLUDED
#define SIMPT_SIM_TIME_DATA_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time data used during model execution.
 */


namespace SimPT_Sim {

/**
 * Time data: current simulated time, number of step in simulation history.
 */
struct TimeData
{

        TimeData(): m_sim_step(0U), m_sim_time(0.0) {}

        unsigned int                          m_sim_step;                      ///< Number of time steps taken.
        double                                m_sim_time;                      ///< Current simulated time.
};

} // namespace

#endif // end_of_include_guard
