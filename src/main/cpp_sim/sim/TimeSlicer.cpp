/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time slice propagator.
 */

#include "TimeSlicer.h"

#include "Sim.h"

namespace SimPT_Sim {

TimeSlicer::TimeSlicer(std::mutex& time_step_mutex, Sim* sim)
        : m_guard(time_step_mutex), m_sim(sim)
{
        m_sim->TimeSliceSetup(m_guard);
}

TimeSlicer::~TimeSlicer()
{
        m_sim->TimeSliceWrapup(m_guard);
}

void TimeSlicer::IncrementStepCount()
{
        ++(m_sim->GetCoreData().m_time_data->m_sim_step);
}

void TimeSlicer::Go(double time_slice, SimPhase phase)
{
        m_sim->TimeSliceGo(m_guard, time_slice, phase);
}

} // namespace

