#ifndef COUPLED_SIM_EVENT_H_INCLUDED
#define COUPLED_SIM_EVENT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for CoupledSimEvent.
 */

#include "SimEventType.h"

#include <memory>

namespace SimPT_Sim { class CoupledSim; }


namespace SimPT_Sim {
namespace Event {

/**
 * An event transmitted by a Coupled Simulator.
 * Really a POD but packaged to force users to initialize all data members.
 */
class CoupledSimEvent
{
public:
	/// Convenience typedef for clients of this class.
	typedef std::shared_ptr<SimPT_Sim::CoupledSim> Source;

	/// Type of events triggered by sim.
	typedef SimEventType Type;

	/// Initialize all data members
	CoupledSimEvent(Source sim, int step, Type type) : m_source(sim), m_step(step), m_type(type) {}

	/// Get the data member value.
	Source GetSource() const { return m_source; }

	/// Get step.
	int GetStep() const { return m_step; }

	/// Get Event type.
	Type GetType() const { return m_type;}

private:
	Source   m_source;      ///< Simulator reporting the event.
	int      m_step;        ///< Number of steps in sim.
	Type     m_type;        ///< Kind of event.
};

} // namespace
} // namespace

#endif
