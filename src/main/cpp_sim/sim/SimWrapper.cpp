/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimWrapper.
 */

#include "SimWrapper.h"

#include "Sim.h"
#include "SimState.h"
#include "fileformats/PTreeFile.h"
#include "util/misc/XmlWriterSettings.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <sstream>

namespace SimPT_Sim {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

SimWrapper::SimWrapper()
	: m_sim(make_shared<Sim>())
{
}

SimWrapperResult<SimState> SimWrapper::GetState() const
{
	try {
		return { SUCCESS, string(), m_sim->GetState() };
	}
	catch (std::exception & e) {
		return { FAILURE, e.what(), SimState() };
	}
	catch ( ... ) {
		return { FAILURE, "Unknown exception", SimState() };
	}
}

SimWrapperResult<string> SimWrapper::GetXMLState() const
{
	try {
		stringstream ss;
		write_xml(ss, m_sim->ToPtree(), XmlWriterSettings::GetTab());
		return { SUCCESS, string(), ss.str() };
	}
	catch (std::exception & e) {
		return { FAILURE, e.what(), string() };
	}
	catch ( ... ) {
		return { FAILURE, "Unknown exception", string() };
	}
}

SimWrapperResult<void> SimWrapper::Initialize(SimState state)
{
	try {
		m_sim->Initialize(state);
		return { SUCCESS, string() };
	}
	catch (std::exception & e) {
		return { FAILURE, e.what() };
	}
	catch ( ... ) {
		return { FAILURE, "Unknown exception" };
	}
}

SimWrapperResult<void> SimWrapper::Initialize(const std::string& path)
{
	try {
		ptree pt;
		PTreeFile::Read(path, pt);
		m_sim->Initialize(pt);
		return { SUCCESS, string() };
	}
	catch (std::exception & e) {
		return { FAILURE, e.what() };
	}
	catch ( ... ) {
		return { FAILURE, "Unknown exception" };
	}
}

SimWrapperResult<void> SimWrapper::TimeStep()
{
	try {
		m_sim->TimeStep();
		return { SUCCESS, string() };
	}
	catch (std::exception & e) {
		return { FAILURE, e.what() };
	}
	catch ( ... ) {
		return { FAILURE, "Unknown exception" };
	}
}

} // namespace
