#ifndef SIMSTATE_H_INCLUDED
#define SIMSTATE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header for SimState.
 */

#include "bio/MeshState.h"

#include <boost/property_tree/ptree.hpp>
#include <ostream>

namespace SimPT_Sim {

/**
 * Contains the state of the whole Simulator at a given simulation step.
 * It's a data object to pass information for saving and loading.
 */
class SimState
{
public:
	/// Returns the index of the current time step.
	int GetTimeStep() const;

	/// Returns the value of the current time step in seconds.
	double GetTime() const;

	/// Get the state of the mesh.
	MeshState GetMeshState() const;

	/// Returns the parameters currently used simulation model.
	boost::property_tree::ptree GetParameters() const;

	/// Returns the name of the associated project.
	std::string GetProjectName() const;

	/// Returns the state of the random engine in a ptree.
	boost::property_tree::ptree GetRandomEngineState() const;

	/// Sets the index of the current time step.
	/// @param step Index of the current time step.
	void SetTimeStep(int step);

	/// Sets the value of the current time step in seconds.
	/// @param t Value of the current time step in seconds.
	void SetTime(double t);

	/// Sets the current state of the mesh in the simulation.
	void SetMeshState(MeshState const& mesh_state);

	/// Sets the parameters currently used simulation model.
	/// @param parameters 		Boost property tree of model parameters.
	void SetParameters(boost::property_tree::ptree const& parameters);

	/// Sets the name of the associated project.
	void SetProjectName(std::string project_name);

	/// Sets the state of the random number engine used for generating random
	/// in the simulation.
	void SetRandomEngineState(boost::property_tree::ptree const& re_state);

	/// Prints a text representation to the given output stream.
	/// @param[in,out] out 		The ouput stream to use for printing.
	/// @note This method is mainly used for debug purposes.
	void PrintToStream(std::ostream& out) const;

private:
	double                        m_time;           ///< Simulated time in seconds
	int                           m_time_step;      ///< Time step
	MeshState                     m_mesh_state;     ///< The state of the tissue
	boost::property_tree::ptree   m_parameters;     ///< Simulation parameters at current time
	boost::property_tree::ptree   m_re_state;       ///< State of the random engine at current time
	std::string                   m_project_name;   ///< The name of the project this SimState is being used in
};

} // namespace

#endif // end_of_include_guard

