#ifndef SIMPT_EDITOR_EDITABLE_NODE_ITEM_H_INCLUDED
#define SIMPT_EDITOR_EDITABLE_NODE_ITEM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditableNodeItem
 */

#include "editor/EditableItem.h"
#include "bio/Node.h"

#include <QGraphicsEllipseItem>
#include <QGraphicsScene>

namespace SimPT_Sim {
	class Cell;
	class Wall;
}

namespace SimPT_Editor {

using namespace SimPT_Sim;

/**
 * Editable graphical representation for Node.
 */
class EditableNodeItem : public EditableItem, public QGraphicsEllipseItem
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 *
	 * @param	node		The node which will be represented.
	 * @param	radius		The radius of the node in the scene.
	 */
	EditableNodeItem(SimPT_Sim::Node* node, double radius);

	/**
	 * Destructor.
	 */
	virtual ~EditableNodeItem();

	/**
	 * Checks whether the node is at the boundary of the complex.
	 */
	virtual bool IsAtBoundary() const;

	/**
	 * Checks whether this item represents the given node.
	 *
	 * @param	node		The given node.
	 * @return	True if this item represents the given node.
	 */
	bool Contains(SimPT_Sim::Node* node) const;

	/**
	 * Highlights the node in the canvas.
	 *
	 * @param	highlighted	True if the node should be highlighted.
	 */
	virtual void Highlight(bool highlighted);

	/**
	 * Set the color the item should get when it gets highlighted.
	 *
	 * @param	color		The given color.
	 */
	virtual void SetHighlightColor(const QColor& color = DEFAULT_HIGHLIGHT_COLOR);

	/**
	 * Update the tool tip of this item.
	 *
	 * @param	cells		The cells to which the logical node this item represents belongs.
	 * @param	walls		The walls to which the logical node this item represents belongs.
	 */
	void SetToolTip(std::list<SimPT_Sim::Cell*> cells, std::list<SimPT_Sim::Wall*> walls);

	/**
	 * Get the node associated with this item.
	 *
	 * @return	The node.
	 */
	SimPT_Sim::Node* Node() const;

	/**
	 * Revert the node to its previous position.
	 */
	void Revert();

signals:
	/**
	 * Emitted when this node has been moved in the scene.
	 */
	void Moved();

public slots:
	/**
	 * Update the node.
	 */
	void Update();

private:
	/**
	 * Recalculating the position of the node after moving it in the scene.
	 * Reimplemented from QGraphicsItem::itemChange()
	 *
	 * @param	change		The change of the graphics item
	 * @param	value		The value of the change
	 * @return	QVariant	A return value of the change
	 */
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

	/**
	 * Reimplemented to paint based upon selection or changed mode.
	 */
	virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

private:
	SimPT_Sim::Node* m_node;

	QColor m_highlight_color;
	QPointF m_prev_pos;
	double m_radius;

private:
	static const QColor DEFAULT_HIGHLIGHT_COLOR;
};

} // namespace

#endif // end_of_include_guard
