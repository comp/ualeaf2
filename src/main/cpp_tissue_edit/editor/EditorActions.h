#ifndef SIMPT_EDITOR_EDITOR_ACTIONS_H_INCLUDED
#define SIMPT_EDITOR_EDITOR_ACTIONS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditorActions.
 */

#include <QMenuBar>

namespace SimPT_Editor {

class PTreePanels;
class TissueGraphicsView;
class TissueEditor;
class UndoStack;


/**
 * The actions in the menu bar of the workspace.
 */
class EditorActions : public QMenuBar
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 * @param	parent		The parent of this object.
	 * @param	view		The graphics view associated with the parent.
	 * @param	panels		The ptree panels associated with the parent.
	 * @param	undoStack	The undo stack associated with the parent.
	 */
	EditorActions(TissueEditor* parent, TissueGraphicsView* view,
			PTreePanels* panels, UndoStack* undoStack);

	/// Destructor.
	virtual ~EditorActions();

	/// Fix the toggles of the modes.
	void FixToggle();

	/// Show or hide the menubar.
	void Show(bool visible);

public slots:
	/// A tissue has been opened.
	void LeafOpened();

	/// The tissue has been closed.
	void LeafClosed();

	/// The mode has been changed.
	void ModeChanged();

	/// The tissue has been modified.
	void Modified();

	/// Number of items that were selected.
	void ItemsSelected(unsigned int count);

private:
	/// Initialize.
	void Initialize();

private:
	// Project pull down menu
	QAction* m_new_tissue;
	QAction* m_open_tissue;
	QAction* m_save_tissue;
	QAction* m_close_tissue;
	QAction* m_quit;

	// Selection modes
	QAction* m_display_mode;
	QAction* m_node_mode;
	QAction* m_edge_mode;
	QAction* m_cell_mode;

	// Edit pull down menu.
	QAction* m_delete_item;
	QAction* m_split_edge;
	QAction* m_split_cell;
	QAction* m_create_cell;
	QAction* m_copy_attributes;
	QAction* m_find_and_select;
	QAction* m_generate_regular_pattern;
	QAction* m_generate_voronoi_pattern;
	QAction* m_undo;
	QAction* m_redo;

	// View pull down menu.
	QAction* m_show_attribute_panel;
	QAction* m_show_geometric_panel;
	QAction* m_show_parameters_panel;
	QAction* m_transparent_cells;
	QAction* m_set_display_mode;
	QAction* m_set_background;

	QAction* m_cancel;

	// Pull down menus.
	QMenu* m_menu_project;
	QMenu* m_menu_edit;
	QMenu* m_menu_view;

	// Views.
	TissueEditor*           m_parent;
	TissueGraphicsView*     m_view;
	PTreePanels*            m_panels;
	UndoStack*              m_undo_stack;
};

} // namespace

#endif // end_of_include_guard
