/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SelectByIDWidget.
 */

#include "SelectByIDWidget.h"

#include "../../cpp_simshell/common/InstallDirs.h"
#include "util/misc/StringUtils.h"

#include <cassert>
#include <list>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRegExp>
#include <string>
#include <vector>

namespace SimPT_Editor {

using namespace std;
using namespace SimPT_Sim::Util;

SelectByIDWidget::SelectByIDWidget(QWidget* parent)
	: QWidget(parent), m_line_edit(new QLineEdit()), m_lock(new QPushButton()), m_max_id(0)
{
	QString strict_pos_int_regex = "[1-9]+[0-9]*";
	QString pos_int_regex = "(0|" + strict_pos_int_regex + ")";
	QString range_regex = pos_int_regex + "(-" + pos_int_regex + "(:" + strict_pos_int_regex + ")?)?";
	QString comb_regex = "(" + range_regex + "(," + range_regex + ")*)?";
	m_regex = QRegExp(comb_regex);

	m_lock->setCheckable(true);
	connect(m_line_edit, SIGNAL(returnPressed()), this, SLOT(ProcessInput()));
	connect(m_line_edit, SIGNAL(textChanged(const QString&)), this, SLOT(SetTextBoxStyle()));

	SetupGui();
}

SelectByIDWidget::~SelectByIDWidget() {}

void SelectByIDWidget::SetMaxID(unsigned int maxID)
{
	m_max_id = maxID;
	m_line_edit->setText("");
}

void SelectByIDWidget::ProcessInput() {
	if (m_regex.exactMatch(m_line_edit->text()))
	{
		ParseText();
	} else {
		SetTextBoxStyle(true);
	}
}

void SelectByIDWidget::ParseText()
{
	std::vector<bool> selected(m_max_id + 1, false);

	vector<string> tokens = Tokenize(m_line_edit->text().toStdString(), ",");
	for (const std::string& token : tokens) {
		vector<string> range = Tokenize(token, "-:");

		int from = -1;
		int to = -1;
		int step = -1;
		if (range.size() == 1) {
			from = atoi(range[0].c_str());
			to = from;
			step = 1;
		} else if (range.size() == 2) {
			from = atoi(range[0].c_str());
			to = atoi(range[1].c_str());
			step = 1;
		} else if (range.size() == 3) {
			from = atoi(range[0].c_str());
			to = atoi(range[1].c_str());
			step = atoi(range[2].c_str());
		} else {
			assert("This is not a proper format for selection by ID.");
		}
		assert(from >= 0 && to >= 0 && step > 0 && "The given bounds for the range are not valid.");

		for (int id = from; id <= std::min(m_max_id, to); id = id + step) {
			selected[id] = true;
		}
	}

	std::list<unsigned int> selected_ids;
	for (unsigned int id = 0; id < selected.size(); ++id) {
		if (selected[id]) {
			selected_ids.push_back(id);
		}
	}

	emit IDsSelected(selected_ids, m_lock->isChecked());
}

void SelectByIDWidget::SetTextBoxStyle(bool error)
{
	if (error) {
		m_line_edit->setStyleSheet("QLineEdit{background: #FFCCCC; color: #FF0000;}");
	} else {
		m_line_edit->setStyleSheet("QLineEdit{background: #FFFFFF; color: #000000;}");
	}
}

void SelectByIDWidget::SetupGui()
{
	QLayout* layout = new QHBoxLayout();

	QIcon lockIcon(QString::fromStdString(SimShell::InstallDirs::GetDataDir() + "/icons/Tango/22x22/emblems/emblem-readonly.png"));
	m_lock->setIcon(lockIcon);
	m_lock->setToolTip("Keep/Discard currently selected items");

	layout->addWidget(new QLabel("Search by ID:"));
	layout->addWidget(m_line_edit);
	layout->addWidget(m_lock);

	setLayout(layout);
}

} // namespace
