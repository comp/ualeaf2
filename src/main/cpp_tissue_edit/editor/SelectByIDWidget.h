#ifndef SIMPT_EDITOR_SELECT_BY_ID_WIDGET_H_INCLUDED
#define SIMPT_EDITOR_SELECT_BY_ID_WIDGET_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SelectByIDWidget.
 */

#include <QRegExp>
#include <QWidget>

class QLineEdit;
class QPushButton;

namespace SimPT_Editor {

/**
 * Class for selecting an item by ID.
 * The text box only allows valid (partial) input, checked by the following regex.
 *
 *      ((0|[1-9]+[0-9]*)(-(0|[1-9]+[0-9]*)(:[1-9]+[0-9]*)?)?(,(0|[1-9]+[0-9]*)(-(0|[1-9]+[0-9]*)(:[1-9]+[0-9]*)?)?)*)?
 *
 * Basically, it allows a combination of ranges (separated by a comma) of the form "from-to:step", for which 'to' and 'step' are optional.
 */
class SelectByIDWidget : public QWidget
{
	Q_OBJECT
public:
	SelectByIDWidget(QWidget* parent = nullptr);

	virtual ~SelectByIDWidget();

	/**
	 * Set the maximal ID and reset the text.
	 */
	void SetMaxID(unsigned int maxID);

signals:
	/**
	 * Emitted when IDs have been selected.
	 */
	void IDsSelected(const std::list<unsigned int>& ids, bool locked);

private slots:
	/**
	 * Process the input in the text box.
	 */
	void ProcessInput();

	/**
	 * Set the color and background color of the text box based on whether there was invalid/erroneous input given.
	 */
	void SetTextBoxStyle(bool error = false);

private:
	/**
	 * Parse the text in the text box to extract the selected IDs.
	 */
	void ParseText();

	/**
	 * Setup GUI.
	 */
	void SetupGui();

private:
	QLineEdit*       m_line_edit;
	QPushButton*     m_lock;
	QRegExp          m_regex;
	int             m_max_id;
};

} // namespace

#endif // end-of-include-guard
