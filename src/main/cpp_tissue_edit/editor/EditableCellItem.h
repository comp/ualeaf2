#ifndef SIMPT_EDITOR_EDITABLE_CELL_ITEM_H_INCLUDED
#define SIMPT_EDITOR_EDITABLE_CELL_ITEM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditableCellItem
 */

#include "editor/EditableItem.h"
#include "bio/Cell.h"

#include <QGraphicsPolygonItem>
#include <QGraphicsScene>

namespace SimPT_Editor {

class EditableEdgeItem;
class EditableNodeItem;

/**
 * Editable graphical representation for Cell.
 */
class EditableCellItem : public EditableItem, public QGraphicsPolygonItem
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 * Both the endpoints and the edges should be listed in order of connection.
	 * The endpoints will correspond to the endpoints of the polygon itself.
	 *
	 * @param	nodes		The endpoints of the cell. The points should be in order of connection.
	 * @param	edges		The edges of the cell.
	 * @param	cell		The logical representation of the cell.
	 */
	EditableCellItem(const std::list<EditableNodeItem*>& nodes, const std::list<EditableEdgeItem*>& edges, SimPT_Sim::Cell* cell);

	/**
	 * Destructor.
	 */
	virtual ~EditableCellItem();

	/**
	 * Checks whether the cell is at the boundary of the cell complex.
	 *
	 * @return	True if the cell is at the boundary.
	 */
	virtual bool IsAtBoundary() const;

	/**
	 * Get the logical cell.
	 */
	SimPT_Sim::Cell* Cell() const;

	/**
	 * Get the nodes in this cell.
	 */
	const std::list<EditableNodeItem*>& Nodes() const;

	/**
	 * Get the edges in this cell.
	 */
	const std::list<EditableEdgeItem*>& Edges() const;

	/**
	 * Checks whether this cell contains the given node.
	 *
	 * @param	node		The given node.
	 * @return	True if the cell contains the node.
	 */
	bool ContainsNode(EditableNodeItem* node) const;

	/**
	 * Checks whether this cell contains the given edge.
	 *
	 * @param	edge		The given edge.
	 * @return	True if the cell contains the edge.
	 */
	bool ContainsEdge(EditableEdgeItem* edge) const;

	/**
	 * Highlights the node in the canvas.
	 *
	 * @param	highlighted	True if the cell should be highlighted.
	 */
	virtual void Highlight(bool highlighted);

	/**
	 * Set the color of the cell.
	 * The alpha value will be ignored.
	 *
	 * @param	color		The given color.
	 */
	void SetColor(const QColor& color);

	/**
	 * Set the color the item should get when it gets highlighted.
	 * The alpha value will be ignored.
	 *
	 * @param	color		The given color.
	 */
	virtual void SetHighlightColor(const QColor& color = DEFAULT_HIGHLIGHT_COLOR);

	/**
	 * Set the transparency of a cell.
	 *
	 * @param	transparent	True if the cell should be transparent.
	 */
	void SetTransparent(bool transparent);

public slots:
	/**
	 * Replaces a given edge with two other edges.
	 *
	 * @param	oldEdge		The edge which should be replaced.
	 * @param	edge1		The first replacement edge.
	 * @param	edge2		The second replacement edge.
	 */
	void ReplaceEdgeWithTwoEdges(EditableEdgeItem* oldEdge, EditableEdgeItem* edge1, EditableEdgeItem* edge2);

	/**
	 * Replaces two given edges with one other edge.
	 *
	 * @param	oldEdge1	The first given edge which should be replaced.
	 * @param	oldEdge2	The second given edge which should be replaced.
	 * @param	edge		The replacement edge.
	 */
	void ReplaceTwoEdgesWithEdge(EditableEdgeItem* oldEdge1, EditableEdgeItem* oldEdge2, EditableEdgeItem* edge);

	/**
	 * Update the cell.
	 */
	void Update();

private:
	/// Reimplemented to paint based upon selection or changed mode.
	virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

private:
	SimPT_Sim::Cell* m_cell;
	std::list<EditableEdgeItem*> m_edges;
	std::list<EditableNodeItem*> m_nodes;

	QColor m_color;
	QColor m_highlight_color;

private:
	static const QColor DEFAULT_HIGHLIGHT_COLOR;
};

} // namespace

#endif // end_of_include_guard
