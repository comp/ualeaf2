/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for EditableCellItem
 */

#include "EditableCellItem.h"

#include "editor/EditableEdgeItem.h"
#include "editor/EditableNodeItem.h"

#include <QStyleOptionGraphicsItem>

class QWidget;
namespace SimPT_Sim { class Cell; }

namespace SimPT_Editor {

const QColor EditableCellItem::DEFAULT_HIGHLIGHT_COLOR(230,0,0,255);//(51, 102, 0, 255)

EditableCellItem::EditableCellItem(const std::list<EditableNodeItem*>& nodes, const std::list<EditableEdgeItem*>& edges, SimPT_Sim::Cell* cell)
		: QGraphicsPolygonItem(), m_cell(cell), m_edges(edges), m_nodes(nodes), m_color(55, 255, 255, 255), m_highlight_color(DEFAULT_HIGHLIGHT_COLOR)
{
	Update();
	setZValue(0); //Draw cells beneath all other items.

	//Update the cell when one of its corners has changed.
	for (EditableNodeItem* node : m_nodes) {
		connect(node, SIGNAL(Moved()), this, SLOT(Update()));
	}

	for (EditableEdgeItem* edge : m_edges) {
		connect(edge, SIGNAL(EdgeSplitted(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
			this, SLOT(ReplaceEdgeWithTwoEdges(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
		connect(edge, SIGNAL(EdgeMerged(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
			this, SLOT(ReplaceTwoEdgesWithEdge(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
	}
}

EditableCellItem::~EditableCellItem() {}

bool EditableCellItem::IsAtBoundary() const
{
	return m_cell->HasBoundaryWall();
}

SimPT_Sim::Cell* EditableCellItem::Cell() const
{
	return m_cell;
}

const std::list<EditableNodeItem*>& EditableCellItem::Nodes() const
{
	return m_nodes;
}

const std::list<EditableEdgeItem*>& EditableCellItem::Edges() const
{
	return m_edges;
}

bool EditableCellItem::ContainsNode(EditableNodeItem* node) const
{

	return std::find(m_nodes.begin(), m_nodes.end(), node) != m_nodes.end();
}

bool EditableCellItem::ContainsEdge(EditableEdgeItem* edge) const
{
	return std::find(m_edges.begin(), m_edges.end(), edge) != m_edges.end();
}

void EditableCellItem::Highlight(bool highlighted)
{
	QPen pen(this->pen());
	if (highlighted) {
		m_highlight_color.setAlpha(brush().color().alpha());
		setBrush(m_highlight_color);
	}
	setPen(pen);
}

void EditableCellItem::SetColor(const QColor& color)
{
	int alpha = brush().color().alpha();
	m_color = color;
	m_color.setAlpha(alpha);
}

void EditableCellItem::SetHighlightColor(const QColor& color)
{
	int alpha = brush().color().alpha();
	m_highlight_color = color;
	m_highlight_color.setAlpha(alpha);
}

void EditableCellItem::SetTransparent(bool transparent)
{
	int alpha = (transparent ? 64 : 255);

	m_color.setAlpha(alpha);
	QColor color = brush().color();

	setBrush(QBrush(QColor(color.red(), color.green(), color.blue(), alpha)));
}

void EditableCellItem::ReplaceEdgeWithTwoEdges(EditableEdgeItem* oldEdge, EditableEdgeItem* edge1, EditableEdgeItem* edge2)
{
	EditableNodeItem* connectedNode = edge1->ConnectingNode(edge2);
	EditableNodeItem* nodeEdge1 = (edge1->First() != connectedNode) ? edge1->First() : edge1->Second();
	assert(connectedNode != nullptr && "The two new edges aren't connected.");

	auto foundEdge = std::find(m_edges.begin(), m_edges.end(), oldEdge);
	assert(foundEdge != m_edges.end() && "This cell doesn't contain the old edge. [inconsistency]");
	auto foundNode = std::find(m_nodes.begin(), m_nodes.end(), nodeEdge1);
	assert(foundNode != m_nodes.end() && "This cell doesn't contain the endpoints of the old edge. [inconsistency]");

	//Get next edge.
	foundEdge++;
	if (foundEdge == m_edges.end()) {
		foundEdge = m_edges.begin();
	}

	if ((*foundEdge)->First() == nodeEdge1 || (*foundEdge)->Second() == nodeEdge1) {
		//The next edge should be connected to edge1 (while the previous one should be connected to edge2).
		m_edges.insert(foundEdge, edge2);
		m_edges.insert(foundEdge, edge1);
		m_nodes.insert(foundNode, connectedNode);
	}
	else {
		//The next edge should be connected to edge2 (while the previous one should be connected to edge1).
		m_edges.insert(foundEdge, edge1);
		m_edges.insert(foundEdge, edge2);
		m_nodes.insert(++foundNode, connectedNode);
	}
	m_edges.remove(oldEdge);

	connect(connectedNode, SIGNAL(Moved()), this, SLOT(Update()));
	connect(edge1, SIGNAL(EdgeSplitted(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceEdgeWithTwoEdges(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
	connect(edge1, SIGNAL(EdgeMerged(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceTwoEdgesWithEdge(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
	connect(edge2, SIGNAL(EdgeSplitted(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceEdgeWithTwoEdges(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
	connect(edge2, SIGNAL(EdgeMerged(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceTwoEdgesWithEdge(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));

	Update();
}

void EditableCellItem::ReplaceTwoEdgesWithEdge(EditableEdgeItem* oldEdge1, EditableEdgeItem* oldEdge2, EditableEdgeItem* edge)
{
	EditableNodeItem* connectedNode = oldEdge1->ConnectingNode(oldEdge2);

	assert(connectedNode != nullptr && "The two new edges aren't connected.");
	assert(std::find(m_nodes.begin(), m_nodes.end(), connectedNode) != m_nodes.end() && "This cell doesn't contain the connected node. [inconsistency]");
	assert(std::find(m_edges.begin(), m_edges.end(), oldEdge1) != m_edges.end() && "This cell doesn't contain the first old edge. [inconsistency]");
	assert(std::find(m_edges.begin(), m_edges.end(), oldEdge2) != m_edges.end() && "This cell doesn't contain the second old edge. [inconsistency]");

	m_nodes.remove(connectedNode);

	m_edges.insert(std::find(m_edges.begin(), m_edges.end(), oldEdge1), edge);
	m_edges.remove(oldEdge1);
	m_edges.remove(oldEdge2);

	connect(edge, SIGNAL(EdgeSplitted(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceEdgeWithTwoEdges(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));
	connect(edge, SIGNAL(EdgeMerged(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)),
		this, SLOT(ReplaceTwoEdgesWithEdge(EditableEdgeItem*, EditableEdgeItem*, EditableEdgeItem*)));

	Update();
}

void EditableCellItem::Update()
{
	QPolygonF endpoints;
	for (EditableNodeItem* node : m_nodes) {
		endpoints.append(node->pos());
	}
	setPolygon(endpoints);
}

void EditableCellItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (flags().testFlag(QGraphicsItem::ItemIsSelectable)) {
		setBrush(QBrush(QColor(220, 220, 220, brush().color().alpha())));
	}
	else {
		setBrush(QBrush(m_color));
	}
	Highlight(isSelected());

	QStyleOptionGraphicsItem noRectBorder(*option);
	noRectBorder.state &= !QStyle::State_Selected;
	QGraphicsPolygonItem::paint(painter, &noRectBorder, widget);
}

} // namespace
