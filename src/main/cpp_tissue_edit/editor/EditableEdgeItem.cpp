/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for EditableEdgeItem
 */

#include "EditableEdgeItem.h"

#include "EditableNodeItem.h"

#include <QLineF>
#include <QColor>
#include <QGraphicsPolygonItem>
#include <QStyleOptionGraphicsItem>
#include <cassert>

class QWidget;

namespace SimPT_Editor {



const QColor EditableEdgeItem::DEFAULT_HIGHLIGHT_COLOR(230,0,0,255);//(51, 102, 0, 255)

EditableEdgeItem::EditableEdgeItem(EditableNodeItem* node1, EditableNodeItem* node2)
		: QGraphicsLineItem(), m_endpoint1(node1), m_endpoint2(node2), m_highlight_color(DEFAULT_HIGHLIGHT_COLOR)
{
	Update();
	setZValue(1); //Draw edges on top of cells.

	//Update the edge when one of its endpoints has changed.
	connect(m_endpoint1, SIGNAL(Moved()), this, SLOT(Update()));
	connect(m_endpoint2, SIGNAL(Moved()), this, SLOT(Update()));
}

EditableEdgeItem::~EditableEdgeItem() {}

bool EditableEdgeItem::IsAtBoundary() const
{
	return m_endpoint1->IsAtBoundary() && m_endpoint2->IsAtBoundary();
}

bool EditableEdgeItem::ContainsEndpoint(EditableNodeItem* endpoint) const
{
	return (m_endpoint1 == endpoint || m_endpoint2 == endpoint);
}

bool EditableEdgeItem::ContainsEndpoints(EditableNodeItem* endpoint1, EditableNodeItem* endpoint2) const
{
	return (ContainsEndpoint(endpoint1) && ContainsEndpoint(endpoint2));
}

EditableNodeItem* EditableEdgeItem::ConnectingNode(EditableEdgeItem* edge) const
{
	if (m_endpoint1 == edge->m_endpoint1 || m_endpoint1 == edge->m_endpoint2) {
		return m_endpoint1;
	}
	else if (m_endpoint2 == edge->m_endpoint1 || m_endpoint2 == edge->m_endpoint2) {
		return m_endpoint2;
	}
	else {
		return nullptr;
	}
}

SimPT_Sim::Edge EditableEdgeItem::Edge() const
{
	return SimPT_Sim::Edge(m_endpoint1->Node(), m_endpoint2->Node());
}

void EditableEdgeItem::Highlight(bool highlighted)
{
	QPen pen(this->pen());
	if (highlighted) {
		pen.setBrush(m_highlight_color);
	}
	setPen(pen);
}

void EditableEdgeItem::SetHighlightColor(const QColor& color)
{
	m_highlight_color = color;
}

EditableEdgeItem* EditableEdgeItem::MergeEdge(EditableEdgeItem* edge)
{
	EditableNodeItem* firstNode;
	EditableNodeItem* secondNode;

	EditableNodeItem* connectedNode = ConnectingNode(edge);
	assert(connectedNode != nullptr && "The edges don't have a connecting point.");
	assert(!ContainsEndpoints(edge->First(), edge->Second()) && "The edges are the same.");

	if (m_endpoint1 != connectedNode) {
		firstNode = m_endpoint1;
	}
	else {
		firstNode = m_endpoint2;
	}
	if (edge->First() != connectedNode) {
		secondNode = edge->First();
	}
	else {
		secondNode = edge->Second();
	}

	EditableEdgeItem* newEdge = new EditableEdgeItem(firstNode, secondNode);
	emit EdgeMerged(this, edge, newEdge);

	return newEdge;
}

std::pair<EditableEdgeItem*, EditableEdgeItem*> EditableEdgeItem::SplitEdge(EditableNodeItem* node)
{
	EditableEdgeItem* edge1 = new EditableEdgeItem(m_endpoint1, node);
	EditableEdgeItem* edge2 = new EditableEdgeItem(node, m_endpoint2);
	emit EdgeSplitted(this, edge1, edge2);
	return std::make_pair(edge1, edge2);
}

void EditableEdgeItem::Update()
{
	setLine(QLineF(m_endpoint1->pos(), m_endpoint2->pos()));
}

void EditableEdgeItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	QPen pen(this->pen());
	if (flags().testFlag(QGraphicsItem::ItemIsSelectable)) {
		pen.setBrush(QBrush(QColor(190,190,190,255)));//QColor(127,127,127,255)
		pen.setWidthF(0.7);//0.5
	}
	else {
		pen.setBrush(QBrush(QColor(0,0,0,255)));
		pen.setWidthF(0.1);
	}
	setPen(pen);
	Highlight(isSelected());

	QStyleOptionGraphicsItem noRectBorder(*option);
	noRectBorder.state &= !QStyle::State_Selected;
	QGraphicsLineItem::paint(painter, &noRectBorder, widget);
}

} // namespace
