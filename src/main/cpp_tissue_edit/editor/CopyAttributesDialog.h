#ifndef SIMPT_EDITOR_COPY_ATTRIBUTES_DIALOG_H_INCLUDED
#define SIMPT_EDITOR_COPY_ATTRIBUTES_DIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CopyAttributesDialog
 */

#include <boost/property_tree/ptree.hpp>
#include <QDialog>
#include <QStandardItemModel>
#include <QTableView>

namespace SimPT_Editor {

/**
 * Dialog for selecting the attributes we want to copy.
 */
class CopyAttributesDialog : public QDialog
{
public:
	/**
	 * Constructor.
	 *
	 * @param	pt		The ptree containing the attributes.
	 * @param	parent		The parent widget.
	 */
	CopyAttributesDialog(const boost::property_tree::ptree& pt, QWidget* parent = nullptr);

	/**
	 * Destructor.
	 */
	virtual ~CopyAttributesDialog();

	/**
	 * Get the selected attributes.
	 * The selected attributes will contain the original value, while the other attributes will have an empty value.
	 *
	 * @return		The selected attributes.
	 */
	boost::property_tree::ptree SelectedAttributes();
private:
	/**
	 * Initialize the model with a ptree.
	 * The ptree will be flatten.
	 *
	 * @param	pt		The given ptree.
	 */
	void InitializeModel(const boost::property_tree::ptree& pt);

	/**
	 * Setup the dialog.
	 */
	void SetupGui();

	/**
	 * Datamembers.
	 */
	QTableView* m_table_view;
	QStandardItemModel* m_model;
};

} // namespace

#endif // end-of-include-guard
