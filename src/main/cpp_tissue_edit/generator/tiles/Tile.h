#ifndef SIMPT_EDITOR_TILE_H_INCLUDED
#define SIMPT_EDITOR_TILE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Tile.
 */

#include <QGraphicsPathItem>

namespace SimPT_Editor {

/**
 * Abstract base class for cell pattern tiles
 */
class Tile : public QGraphicsPathItem
{
public:
	/**
	 * Constructor
	 *
	 * @param	polygon		The polygon of the full tile
	 * @param	path		The path to draw the polygon
	 */
	Tile(const QPolygonF &polygon, const QPainterPath &path) : QGraphicsPathItem(path), m_polygon(polygon) {}

	/**
	 * (Virtual) Destructor
	 */
	virtual ~Tile() {}


	/**
	 * Returns to the polygon of this tile
	 *
	 * @return	polygon		A const reference to the tile polygon
	 */
	const QPolygonF &GetPolygon() const { return m_polygon; }


	/**
	 * Creates a new tile to the left of this tile (should normally be the opposite of Tile::Right())
	 * Parent is set to parent of this tile
	 *
	 * @return	Tile*	A new tile to the left of this one (ownership is returned with pointer)
	 */
	virtual Tile *Left() const = 0;

	/**
	 * Creates a new tile to the right of this tile (should normally be the opposite of Tile::Left())
	 * Parent is set to parent of this tile
	 *
	 * @return	Tile*	A new tile to the right of this one (ownership is returned with pointer)
	 */
	virtual Tile *Right() const = 0;

	/**
	 * Creates a new tile on the row under this tile (should normally be the opposite of Tile::Down())
	 * Parent is set to parent of this tile
	 *
	 * @return	Tile*	A new tile to below this one (ownership is returned with pointer)
	 */
	virtual Tile *Up() const = 0;

	/**
	 * Creates a new tile on the row under this tile (should normally be the opposite of Tile::Up())
	 * Parent is set to parent of this tile
	 *
	 * @return	Tile*	A new tile to below this one (ownership is returned with pointer)
	 */
	virtual Tile *Down() const = 0;

protected:
	const QPolygonF m_polygon;
};

} // namespace

#endif // end-of-include-guard
