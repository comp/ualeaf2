/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for RectangularTile.
 */

#include "RectangularTile.h"

namespace SimPT_Editor {

const double RectangularTile::g_side_length = 10.0;
const QPolygonF RectangularTile::g_start_polygon(QRectF(0, 0, g_side_length, g_side_length));

namespace {

QPainterPath PolygonToPath(const QPolygonF &polygon)
{
	QPainterPath path;
	path.moveTo(polygon.first());
	path.lineTo(polygon.at(1));
	path.lineTo(polygon.at(2));
	return path;
}

}

RectangularTile::RectangularTile()
	: RectangularTile(g_start_polygon, nullptr)
{
}

RectangularTile::~RectangularTile()
{
}

RectangularTile *RectangularTile::Left() const
{
	return new RectangularTile(m_polygon.translated(-g_side_length, 0), parentItem());
}

RectangularTile *RectangularTile::Right() const
{
	return new RectangularTile(m_polygon.translated(g_side_length, 0), parentItem());
}

RectangularTile *RectangularTile::Up() const
{
	return new RectangularTile(m_polygon.translated(0, -g_side_length), parentItem());
}

RectangularTile *RectangularTile::Down() const
{
	return new RectangularTile(m_polygon.translated(0, g_side_length), parentItem());
}

RectangularTile::RectangularTile(const QPolygonF &polygon, QGraphicsItem *parentItem)
	: Tile(polygon, PolygonToPath(polygon))
{
	setParentItem(parentItem);
}

} // namespace
