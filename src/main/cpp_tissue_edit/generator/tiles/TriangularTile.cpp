/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for TriangularTile.
 */

#include "TriangularTile.h"

#include <cmath>

namespace SimPT_Editor {

const double TriangularTile::g_side_length = 10.0;
const double TriangularTile::g_height = std::sqrt(3.0) / 2 * g_side_length;
const QPolygonF TriangularTile::g_start_polygon(QPolygonF() << QPointF(0, 0) << QPointF(g_side_length / 2, g_height) << QPointF(g_side_length, 0));

namespace {

QPainterPath PolygonToPath(const QPolygonF &polygon, bool odd)
{
	QPainterPath path;
	if (!odd) {
		path.moveTo(polygon.last());
		path.lineTo(polygon.at(0));
		path.lineTo(polygon.at(1));
	} else {
		path.moveTo(polygon.first());
		path.lineTo(polygon.at(1));
	}
	return path;
}

}

TriangularTile::TriangularTile()
	: Tile(g_start_polygon, PolygonToPath(g_start_polygon, false)), m_odd(false)
{
        setParentItem(nullptr);
}

TriangularTile::TriangularTile(const QPolygonF& polygon, bool odd, QGraphicsItem* parentItem)
        : Tile(polygon, PolygonToPath(polygon, odd)), m_odd(odd)
{
        setParentItem(parentItem);
}

TriangularTile::~TriangularTile()
{
}

TriangularTile* TriangularTile::Left() const
{
	QPolygonF polygon(m_polygon);
	polygon.pop_back();
	polygon.push_front(polygon.back() - QPointF(g_side_length, 0));
	return new TriangularTile(polygon, !m_odd, parentItem());
}

TriangularTile* TriangularTile::Right() const
{
	QPolygonF polygon(m_polygon);
	polygon.pop_front();
	polygon.push_back(polygon.front() + QPointF(g_side_length, 0));
	return new TriangularTile(polygon, !m_odd, parentItem());
}

TriangularTile* TriangularTile::Up() const
{
	QPolygonF polygon(m_polygon);
	if (!m_odd) {
		polygon[1] = polygon[1] - QPointF(0, 2 * g_height);
	} else {
		polygon[0] = polygon[0] - QPointF(0, 2 * g_height);
		polygon[2] = polygon[2] - QPointF(0, 2 * g_height);
	}
	return new TriangularTile(polygon, !m_odd, parentItem());
}

TriangularTile* TriangularTile::Down() const
{
	QPolygonF polygon(m_polygon);
	if (!m_odd) {
		polygon[0] = polygon[0] + QPointF(0, 2 * g_height);
		polygon[2] = polygon[2] + QPointF(0, 2 * g_height);
	} else {
		polygon[1] = polygon[1] + QPointF(0, 2 * g_height);
	}
	return new TriangularTile(polygon, !m_odd, parentItem());
}

} // namespace
