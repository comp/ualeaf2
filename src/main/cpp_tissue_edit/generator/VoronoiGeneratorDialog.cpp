/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for VoronoiGeneratorDialog.
 */

#include "VoronoiGeneratorDialog.h"

#include "editor/PolygonUtils.h"

#include "VoronoiTesselation.h"
#include <cassert>
#include <QBoxLayout>
#include <QBrush>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QGraphicsPolygonItem>
#include <QGraphicsScene>
#include <QPen>
#include "gui/PanAndZoomView.h"

class QColor;
class QWidget;

namespace SimPT_Editor {

using namespace SimShell;
using namespace SimShell::Gui;

const double VoronoiGeneratorDialog::g_scene_margin = 2.0;

VoronoiGeneratorDialog::VoronoiGeneratorDialog(const QPolygonF &boundaryPolygon, double initialScale, QWidget *parent)
	: QDialog(parent)
{
	SetupSceneItems(boundaryPolygon);
	SetupGui(initialScale);
}

VoronoiGeneratorDialog::~VoronoiGeneratorDialog()
{
}

std::list<QPolygonF> VoronoiGeneratorDialog::GetGeneratedPolygons() const
{
	// Make the polygons counterclockwise (in the standard coordinate system).
	std::list<QPolygonF> polygons;
	for (QPolygonF& polygon : m_tesselation->GetCellPolygons()) {
		polygons.push_back(PolygonUtils::Counterclockwise(polygon));
	}

	return !polygons.empty() ? polygons : std::list<QPolygonF>({m_tesselation->polygon()});
}

void VoronoiGeneratorDialog::SetupSceneItems(const QPolygonF &boundaryPolygon)
{
	m_scene = new QGraphicsScene();
	m_tesselation = new VoronoiTesselation(boundaryPolygon);
	m_tesselation->setPen(QPen(QBrush(QColor(51, 102, 0, 255)), 0));
	m_scene->addItem(m_tesselation);
	m_scene->setSceneRect(m_tesselation->boundingRect().adjusted(
		-g_scene_margin, -g_scene_margin, g_scene_margin, g_scene_margin));
}

void VoronoiGeneratorDialog::SetupGui(double scale)
{
	setWindowTitle("Voronoi cell pattern generator");
	setMinimumSize(800, 500);

	// > GUI layout
	QVBoxLayout *layout = new QVBoxLayout();

	//	 > Generator graphics view
	PanAndZoomView *view = new PanAndZoomView();
	view->setParent(this);
	view->ScaleView(scale);
	view->setScene(m_scene);
	view->setRenderHints(QPainter::Antialiasing);

	layout->addWidget(view);
	//	< Generator graphics view

	//	> Dialog button box layout
	QHBoxLayout *controlsLayout = new QHBoxLayout();
	controlsLayout->addStretch();

	//		> Dialog button box
	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	controlsLayout->addWidget(buttonBox);
	//		< Dialog button box

	layout->addLayout(controlsLayout);
	//	< Dialog button box layout

	setLayout(layout);
	// < GUI layout
}

} // namespace
