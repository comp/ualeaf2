#ifndef SIMPT_EDITOR_VORONOI_GENERATOR_DIALOG_H_INCLUDED
#define SIMPT_EDITOR_VORONOI_GENERATOR_DIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for VoronoiGeneratorDialog.
 */

#include <list>
#include <memory>
#include <utility>
#include <vector>
#include <QDialog>
#include <QPolygonF>

class QGraphicsScene;

namespace SimPT_Editor {

class VoronoiTesselation;

/**
 * Dialog for generating patterns in a cell using Voronoi tessellation.
 */
class VoronoiGeneratorDialog : public QDialog
{
public:
	/**
	 * Constructor
	 *
	 * @param	boundaryPolygon		The polygon inside which to generate the cell complex
	 * @param	initialScale		The initial scale of the generator graphics view
	 * @param	parent			The parent of the dialog
	 */
	VoronoiGeneratorDialog(const QPolygonF &boundaryPolygon, double initialScale = 1.0, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~VoronoiGeneratorDialog();


	/**
	 * Retrieves the generated polygons after the dialog has successfully executed
	 *
	 * @return	list<QPolygonF>		The polygons of the generated cells
	 * 					The polygons are open, meaning that the first point and last point aren't equal
	 * 					The points of the polygons are also in counterclockwise order (in a standard right-handed coordinate system)
	 */
	std::list<QPolygonF> GetGeneratedPolygons() const;

private:
	void SetupSceneItems(const QPolygonF &boundaryPolygon);
	void SetupGui(double scale);


	QGraphicsScene *m_scene;
	VoronoiTesselation *m_tesselation;

	static const double g_scene_margin;
};

} // namespace

#endif // end-of-include-guard
