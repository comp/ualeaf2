/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for RegularGeneratorDialog.
 */

#include "generator/RegularGeneratorDialog.h"

#include "editor/PolygonUtils.h"
#include "editor/TransformationWidget.h"
#include "generator/RegularTiling.h"
#include "generator/tiles/DiamondTile.h"
#include "generator/tiles/HexagonalTile.h"
#include "generator/tiles/RectangularTile.h"
#include "generator/tiles/TriangularTile.h"
#include "gui/PanAndZoomView.h"
#include "util/container/circular_iterator.h"

#include <QBoxLayout>
#include <QBrush>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QGraphicsScene>
#include <QGraphicsPolygonItem>
#include <QPen>

#include <cassert>

namespace SimPT_Editor {

using namespace SimShell;
using namespace SimShell::Gui;

/**
 * Interface for tile factories.
 */
class RegularGeneratorDialog::ITileFactory
{
public:
	virtual ~ITileFactory() {}
	virtual Tile *Create() const = 0;
};

/**
 * Concrete factory for tile production.
 */
template <class TileType>
class RegularGeneratorDialog::TileFactory : public RegularGeneratorDialog::ITileFactory
{
public:
	virtual ~TileFactory() {}
	virtual Tile *Create() const { return new TileType(); }
};

const double RegularGeneratorDialog::g_scene_margin = 2.0;
const std::vector<std::pair<std::shared_ptr<const RegularGeneratorDialog::ITileFactory>, std::string>> RegularGeneratorDialog::g_tile_factories{
		std::make_pair(std::make_shared<TileFactory<RectangularTile>>(), "Rectangles"),
		std::make_pair(std::make_shared<TileFactory<TriangularTile>>(), "Triangles"),
		std::make_pair(std::make_shared<TileFactory<DiamondTile>>(), "Diamonds"),
		std::make_pair(std::make_shared<TileFactory<HexagonalTile>>(), "Hexagons")};

RegularGeneratorDialog::RegularGeneratorDialog(const QPolygonF &boundaryPolygon, double initialScale, QWidget *parent)
	: QDialog(parent)
{
	SetupSceneItems(boundaryPolygon);
	SetupGui(initialScale);
}

RegularGeneratorDialog::~RegularGeneratorDialog()
{
}

std::list<QPolygonF> RegularGeneratorDialog::GetGeneratedPolygons() const
{
	std::list<QPolygonF> polygons;
	for (const QPolygonF& polygon : m_tiling->GetPolygons()) {
		auto openPolygon = PolygonUtils::OpenPolygon(polygon);
		for (const QPolygonF& subpolygon : PolygonUtils::ClipPolygon(openPolygon, m_boundary->polygon())) {
			polygons.push_back(PolygonUtils::Counterclockwise(subpolygon));
		}
	}
	return polygons;
}

void RegularGeneratorDialog::UpdateTiling(int index)
{
	delete m_tiling;
	m_tiling = new RegularTiling(g_tile_factories[index].first->Create(), m_boundary->boundingRect(), m_boundary);
	UpdateTransformation();
}

void RegularGeneratorDialog::UpdateTransformation()
{
	QTransform transformation;

	QPointF polygonCenter = m_boundary->boundingRect().center();
	transformation.translate(m_transformation->GetTranslationX(), m_transformation->GetTranslationY());

	transformation.translate(polygonCenter.x(), polygonCenter.y());
	transformation.rotate(m_transformation->GetRotation());
	transformation.scale(m_transformation->GetScalingX(), m_transformation->GetScalingY());
	transformation.translate(-polygonCenter.x(), -polygonCenter.y());

	m_tiling->setTransform(transformation);
}

void RegularGeneratorDialog::SetupSceneItems(const QPolygonF &boundaryPolygon)
{
	m_scene = new QGraphicsScene();

	m_boundary = new QGraphicsPolygonItem(boundaryPolygon);
	m_boundary->setPen(QPen(QBrush(QColor(51, 102, 0, 255)), 0));
	m_scene->addItem(m_boundary);

	m_boundary->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
	m_tiling = new RegularTiling(g_tile_factories[0].first->Create(), m_boundary->boundingRect(), m_boundary);

	m_scene->setSceneRect(m_boundary->boundingRect().adjusted(-g_scene_margin, -g_scene_margin, g_scene_margin, g_scene_margin));
}

void RegularGeneratorDialog::SetupGui(double scale)
{
	setWindowTitle("Regular cell pattern generator");
	setMinimumSize(800, 500);

	// > GUI layout
	QHBoxLayout *layout = new QHBoxLayout();

	//	 > Generator graphics view
	PanAndZoomView *view = new PanAndZoomView();
	view->setParent(this);
	view->ScaleView(scale);
	view->setScene(m_scene);
	view->setRenderHints(QPainter::Antialiasing);

	layout->addWidget(view, 3);
	//	< Generator graphics view

	//	> Tiling selection, sliders and dialog button box layout
	QVBoxLayout *controlsLayout = new QVBoxLayout();

	// 		> Tiling selection
	QComboBox *tilingSelection = new QComboBox();
	for (auto pair : g_tile_factories) {
		tilingSelection->addItem(QString::fromStdString(pair.second));
	}
	tilingSelection->setCurrentIndex(0);
	connect(tilingSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(UpdateTiling(int)));
	controlsLayout->addWidget(tilingSelection);
	// 		< Tiling selection

	QFrame *line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	controlsLayout->addWidget(line);

	// 		> Transformations
	m_transformation = new TransformationWidget(10, m_boundary->boundingRect().width() / 2, m_boundary->boundingRect().height() / 2);
	connect(m_transformation, SIGNAL(TransformationChanged()), this, SLOT(UpdateTransformation()));
	controlsLayout->addWidget(m_transformation);
	// 		< Transformations

	controlsLayout->addStretch();

	//		> Dialog button box
	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	controlsLayout->addWidget(buttonBox);
	//		< Dialog button box

	layout->addLayout(controlsLayout, 1);
	//	< Tiling selection, sliders and dialog button box layout

	setLayout(layout);
	// < GUI layout
}

} // namespace
