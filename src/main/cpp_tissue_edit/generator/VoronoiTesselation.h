#ifndef SIMPT_EDITOR_VORONOI_TESSELATION_H_INCLUDED
#define SIMPT_EDITOR_VORONOI_TESSELATION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for VoronoiTesselation.
 */

#include <QGraphicsPolygonItem>

#include <QBrush>
#include <QPointF>
#include <QRectF>

#include <boost/polygon/point_data.hpp>
#include <boost/polygon/voronoi.hpp>

#include <list>
#include <map>

class QGraphicsLineItem;
class QGraphicsSceneMouseEvent;

namespace SimPT_Editor {

/**
 * Graphics item for Voronoi tesselation of a polygon
 */
class VoronoiTesselation: public QGraphicsPolygonItem
{
public:
	/**
	 * Constructor
	 *
	 * @param	boundaryPolygon		The polygon inside which to generate the cell complex
	 * @param	parent			The QGraphicsItem parent
	 */
	VoronoiTesselation(const QPolygonF &boundaryPolygon, QGraphicsItem *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~VoronoiTesselation();


	/**
	 * Returns the polygons of cell of the Voronoi tesselation
	 *
	 * @return	list<QPolygonF>		The cell polygons of the tesselation. The returned polygons will be open and have their point in counterclockwise order (in a standard righthanded coordinate system)
	 */
	std::list<QPolygonF> GetCellPolygons();

protected:
	/// Reimplementation of QGraphicsItem::mousePressEvent to remove a point from the tesselation
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);

	/// Reimplementation of QGraphicsItem::mouseDoubleClickEvent to add a point to the tesselation
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

private:
	void UpdateTesselation();
	void RedrawLines();

private:
	/**
	 * Represents cell edge bein cut by clipping operation.
	 */
	struct ClippedEdge
	{
		ClippedEdge()
		: edge(QLineF()), clippedP1(false), nextBoundaryP1(QPointF()), clippedP2(false), nextBoundaryP2(QPointF())
		{}

		ClippedEdge(QLineF e, bool c1, QPointF nBP1, bool c2, QPointF nBP2)
		: edge(e), clippedP1(c1), nextBoundaryP1(nBP1), clippedP2(c2), nextBoundaryP2(nBP2)
		{}

		QLineF      edge;
		bool        clippedP1;
		QPointF     nextBoundaryP1;
		bool        clippedP2;
		QPointF     nextBoundaryP2;
	};

	void AddEdge(const boost::polygon::voronoi_diagram<double>::edge_type *edge, QPolygonF &polygon, std::map<const boost::polygon::voronoi_diagram<double>::edge_type*, ClippedEdge> &clippingCache);
	ClippedEdge GetClippedEdge(const boost::polygon::voronoi_diagram<double>::edge_type *edge, std::map<const boost::polygon::voronoi_diagram<double>::edge_type*, ClippedEdge> &clippingCache);
	ClippedEdge ClipEdge(const boost::polygon::voronoi_diagram<double>::edge_type *edge);

private:
	static boost::polygon::point_data<long> ToVoronoiPoint(const QPointF &point);
	static QPointF FromVoronoiPoint(const boost::polygon::voronoi_diagram<double>::vertex_type &point);

private:
	std::vector<QPointF>             m_points;
	std::list<QPolygonF>             m_cell_polygons;
	std::list<QGraphicsLineItem*>    m_lines;

private:
	static const QRectF g_point_rect;
	static const QBrush g_point_brush;
	static const double g_points_precision;
};

} // namespace

#endif // end-of-include-guard
