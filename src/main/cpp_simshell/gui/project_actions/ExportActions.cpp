/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ExportActions implementation.
 */

#include "ExportActions.h"

#include <QAction>
#include <QFileDialog>
#include <QMessageBox>

using namespace std;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

ExportActions::ExportActions(
		const shared_ptr<Ws::IProject>& project,
		const Session::ISession::ExportersType& exporters,
		QWidget* parent)
	: m_project(project), m_parent(parent)
{
	for (auto& exporter : exporters) {
		QAction* a = new QAction(QString::fromStdString(exporter.first) + " ...", this);
		m_actions.push_back(a);
		m_callback_map[a] = exporter;
		connect(a, SIGNAL(triggered()), this, SLOT(SLOT_Trigger()));
	}
}

ExportActions::~ExportActions()
{
}

const vector<QAction*>& ExportActions::GetActions() const
{
	return m_actions;
}

void ExportActions::SLOT_Trigger()
{
	auto& exp           = m_callback_map[sender()];
	auto& exp_name      = exp.first;
	auto& exp_extension = exp.second.extension;
	auto& exp_callback  = exp.second.callback;

	QString dir       = QString::fromStdString(m_project->GetPath());
	QString filters   = QString::fromStdString(exp_name) + " (*."
				+ QString::fromStdString(exp_extension) + ");;All Files (*)";
	QFileDialog* fd   = new QFileDialog(m_parent, "Export " + QString::fromStdString(exp_name),
				dir, filters);
	fd->setFileMode(QFileDialog::AnyFile);
	fd->setAcceptMode(QFileDialog::AcceptSave);

	if (fd->exec() == QDialog::Accepted) {
		QString path;
		QStringList paths = fd->selectedFiles();
		if (!paths.empty()) {
			path = paths.first();
		} else {
			QMessageBox::warning(m_parent, "Error", "No filename given!");
			emit static_cast<QAction*>(sender())->trigger();
			return;
		}

		// extract extension from filename
		QFileInfo fi(path);
		QString extension = fi.suffix();
		if (extension.isEmpty()) {
			extension = QString::fromStdString(exp_extension);
			path += ".";
			path += extension;
		}

		// Perform export action
		(exp_callback)(path.toStdString());
	}
}

} // namespace
} // namespace
} // namespace
