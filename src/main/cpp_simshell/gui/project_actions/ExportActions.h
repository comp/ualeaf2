#ifndef GUI_PROJECTACTIONS_EXPORTACTIONS_H_INCLUDED
#define GUI_PROJECTACTIONS_EXPORTACTIONS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ExportActions header.
 */

#include "session/ISession.h"
#include "workspace/IProject.h"

#include <QObject>
#include <map>
#include <vector>

class QAction;
class QWidget;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

/**
 * For a given Session::ISession::ExportersType instance, generates a list of actions
 * that will trigger file "Export" to happen.
 * This object maintains ownership of those actions, so a long as they are available
 * to the user (e.g. added to a menu), this object should NOT be destroyed.
 * Such a trigger of "Export" will cause a file dialog window to be shown, so the
 * user can choose where to save the exported file.
 */
class ExportActions : public QObject
{
	Q_OBJECT
public:
	ExportActions(const std::shared_ptr<Ws::IProject>&,
		const Session::ISession::ExportersType&,
		QWidget* parent = nullptr);

	/// Virtual destructor.
	virtual ~ExportActions();

	/// Get actions.
	/// This class keeps ownership over the returned objects!
	const std::vector<QAction*>& GetActions() const;

private slots:
	void SLOT_Trigger();

private:
	std::shared_ptr<Ws::IProject>           m_project;                ///< Project
	QWidget*                                m_parent;                 ///<
	std::vector<QAction*>                   m_actions;                ///< List of QAction objects associated with Export callbacks.
	std::map<QObject*, std::pair<std::string, Session::ISession::ExporterType>>
	                                        m_callback_map;           ///< Mapping from QAction objects to file extension and Export callback.
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
