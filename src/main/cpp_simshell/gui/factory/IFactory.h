#ifndef SIMSHELL_GUI_IFACTORY_H_INCLUDED
#define SIMSHELL_GUI_IFACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IFactory.
 */

#include "workspace/IWorkspaceFactory.h"

namespace SimShell {
namespace Gui {

/**
 * Factory for application-specific stuff in generic simshell GUI.
 */
class IFactory
{
public:
	/// Virtual destructor for interface class.
	virtual ~IFactory() {}

	/// Create workspace factory.
	virtual std::shared_ptr<Ws::IWorkspaceFactory> CreateWorkspaceFactory() const = 0;

	/// Get text shown in the "About" box.
	virtual std::string GetAbout() const = 0;

	/// Get application name.
	virtual std::string GetApplicationName() const = 0;

	/// Get organization name.
	virtual std::string GetOrganizationName() const = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
