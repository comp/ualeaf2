#ifndef GUI_PTREECONTAINERPREFERENCESOBSERVER_H_INCLUDED
#define GUI_PTREECONTAINERPREFERENCESOBSERVER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeContainerPreferencesObserver.
 */

#include "PTreeContainer.h"
#include "workspace/event/PreferencesChanged.h"

namespace SimShell {
namespace Gui {

/**
 * PTreeContainer meant for displaying/editing preferences ptrees that can also
 * be modified elsewhere in the code.
 * This class can act as an observer for Ws::Event::PreferencesChanged events.
 * @see Update
 */
class PTreeContainerPreferencesObserver : public PTreeContainer
{
	Q_OBJECT
public:
	PTreeContainerPreferencesObserver(const QString& title, QWidget* parent = nullptr);

	/**
	 * Virtual destructor.
	 */
	virtual ~PTreeContainerPreferencesObserver();

	/**
	 * Call this method to update the maintained ptree.
	 * Typically, you want to register this method as a callback for
	 * Ws::Event::PreferencesChanged events.
	 */
	void Update(const Ws::Event::PreferencesChanged&);
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
