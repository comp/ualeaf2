/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MyGraphicsView.
 */

#include "MyGraphicsView.h"

#include <QWheelEvent>
#include <cmath>

namespace SimShell {
namespace Gui {

using namespace std;

MyGraphicsView::MyGraphicsView(std::shared_ptr<QGraphicsScene> s, QWidget* parent)
	: QGraphicsView(s.get(), parent), m_scene(s)
{
	setInteractive(true); // required for zooming
}

MyGraphicsView::MyGraphicsView(QWidget* parent)
	: QGraphicsView(parent)
{
	setInteractive(true); // required for zooming
}

void MyGraphicsView::wheelEvent(QWheelEvent* event)
{
	double scaleFactor = pow(2.0, -event->delta() / 240.0);
	double factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor > 0.07 && factor < 100.0)
	{
		scale(scaleFactor, scaleFactor);
	}
}

} // end of namespace Gui
} // end of namespace SimShell
