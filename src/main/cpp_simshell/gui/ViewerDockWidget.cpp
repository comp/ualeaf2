/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of ViewerDockWidget.
 */

#include "ViewerDockWidget.h"

#include <QAction>
#include <QCloseEvent>
#include <QMainWindow>

using namespace std;

namespace SimShell {
namespace Gui {

ViewerDockWidget::ViewerDockWidget(Qt::DockWidgetArea area, const shared_ptr<Ws::MergedPreferences>& p,
		QWidget* parent, std::function<void()> on_close)
	: QDockWidget(parent), m_preferences(p), m_on_close(on_close)
{
	setWindowModality(Qt::NonModal);
	static_cast<QMainWindow*>(parent)->addDockWidget(area, this);

	const auto pos_x   = m_preferences->Get<int>("position.x");
	const auto pos_y   = m_preferences->Get<int>("position.y");
	const auto size_x  = m_preferences->Get<int>("size.x");
	const auto size_y  = m_preferences->Get<int>("size.y");
	setGeometry(pos_x, pos_y, size_x, size_y);
}

ViewerDockWidget::~ViewerDockWidget()
{
	auto g = geometry();
	m_preferences->Put("position.x", g.x());
	m_preferences->Put("position.y", g.y());
	m_preferences->Put("size.x", g.width());
	m_preferences->Put("size.y", g.height());
}

void ViewerDockWidget::closeEvent(QCloseEvent* )
{
	if (m_on_close) {
		m_on_close();
	}
}

} // end namespace Gui
} // end namespace SimShell
