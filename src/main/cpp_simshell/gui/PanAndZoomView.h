#ifndef PAN_AND_ZOOM_VIEW_H_INCLUDED
#define PAN_AND_ZOOM_VIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PanAndZoomView
 */

#include <QGraphicsView>

namespace SimShell {
namespace Gui {

/**
 * QGraphicsView with the ability to pan and zoom.
 */
class PanAndZoomView : public QGraphicsView
{
public:

	/**
	 * Constructor
	 *
	 * @param	zoomMin		The minimum zooming factor
	 * @param	zoomMax		The maximum zooming factor
	 * @param	parent		The widget's parent
	 */
	PanAndZoomView(double zoomMin = 0.07, double zoomMax = 100.0, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~PanAndZoomView();

	/**
	 * Scales the view, bounded by the minimum and maximum zooming factor
	 * @param	factor	The scaling factor
	 */
	void ScaleView(double factor);

	/**
	 * Gets the current scaling of the view
	 * (assuming the view was only scaled with ScaleView() and the
	 * horizontal and vertical scaling are the same)
	 * @param	double	The scaling factor of the view
	 */
	double Scaling();

	/**
	 * Resets the view to the normal zoom level
	 */
	void ResetZoom();

	/**
	 * Overriding keyPressEvent to detect when control key is pressed
	 * @param	event	The key event
	 */
	virtual void keyPressEvent(QKeyEvent *event);

	/**
	 * Overriding releasePressEvent to detect when control key is released
	 * @param	event	The key event
	 */
	virtual void keyReleaseEvent(QKeyEvent *event);

	/**
	 * Overriding enterEvent to grab focus when the view is entered
	 * @param	event	The key event
	 */
	virtual void enterEvent(QEvent *event);

	/**
	 * Overriding wheelEvent to scroll the view when control key is held down
	 * @param	event
	 */
	virtual void wheelEvent(QWheelEvent *event);

private:
	double m_zoom_min;
	double m_zoom_max;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
