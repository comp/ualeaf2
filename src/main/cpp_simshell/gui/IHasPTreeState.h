#ifndef GUI_IHASPTREESTATE_H_INCLUDED
#define GUI_IHASPTREESTATE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * HasPtreeState interface.
 */

#include <boost/property_tree/ptree.hpp>

namespace SimShell {
namespace Gui {

/**
 * Interface for objects that have non-critical state (metadata) information representable as a ptree.
 *
 * Typically used for remembering non-critical information, such as user interface layout, between sessions.
 */
class IHasPTreeState
{
public:
	virtual ~IHasPTreeState() {}

	/**
	 * Return (possibly empty) ptree containing information about the current state of the object.
	 * Returned ptree can be used as parameter for SetPTreeState at a later point in time.
	 */
	virtual boost::property_tree::ptree GetPTreeState() const = 0;

	/**
	 * Set state of the object. Argument should be a ptree retrieved by GetPTreeState at an earlier moment in time.
	 *
	 * Implementation should be able to deal with missing information, e.g. a missing subtree should not cause this method to throw an error.
	 * An empty ptree as parameter should cause nothing to happen.
	 *
	 * The setting of the ptree state of an object must only serve as an enhancement to the usability of the program.
	 */
	virtual void SetPTreeState(const boost::property_tree::ptree&) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
