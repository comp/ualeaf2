/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * MyDockWidget implementation.
 */

#include "MyDockWidget.h"

#include "common/PTreeQtState.h"


using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

MyDockWidget::MyDockWidget(QWidget* parent)
	: QDockWidget(parent)
{
}

ptree MyDockWidget::GetPTreeState() const
{
	ptree result;
	result.put_child("widget", PTreeQtState::GetWidgetState(this));
	result.put_child("dock", PTreeQtState::GetDockWidgetState(this));
	return result;
}

void MyDockWidget::SetPTreeState(const ptree& state)
{
	auto widget_state = state.get_child_optional("widget");
	if (widget_state)
		PTreeQtState::SetWidgetState(this, widget_state.get());

	auto dock_state = state.get_child_optional("dock");
	if (dock_state) {
		PTreeQtState::SetDockWidgetState(this, dock_state.get());
	}
}

} // namespace Gui
} // namespace SimShell
