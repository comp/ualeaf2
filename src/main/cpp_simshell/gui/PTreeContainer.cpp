/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeContainer.
 */

#include "PTreeContainer.h"

#include "MyDockWidget.h"
#include "PTreeEditorWindow.h"
#include "PTreeMenu.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

PTreeContainer::PTreeContainer(QString const & title, QWidget* parent)
	: QWidget(parent), HasUnsavedChanges(title.toStdString()), m_opened(false), m_title(title)
{
	m_window = new PTreeEditorWindow();
	m_window->setWindowTitle(m_title);
	m_dock = new MyDockWidget(nullptr);
	m_dock->setWindowTitle(m_title);
	m_dock->setWidget(m_window);
	m_menu = new PTreeMenu(m_title, QString(), nullptr);

	connect(m_menu, SIGNAL(ItemTriggered(QString const &)), this, SLOT(OpenPath(QString const &)));
	connect(m_window, SIGNAL(ApplyTriggered(boost::property_tree::ptree const &)), this, SLOT(RefreshMenu(boost::property_tree::ptree const &)));
	connect(m_window, SIGNAL(ApplyTriggered(boost::property_tree::ptree const &)), this, SIGNAL(Applied(boost::property_tree::ptree const &)));
	connect(m_window, SIGNAL(StatusChanged(QString const &)), this, SIGNAL(StatusChanged(QString const &)));

	SetChildren({
		m_window,
		m_menu
	});
}

PTreeContainer::~PTreeContainer()
{
	delete m_window;
	delete m_dock;
	delete m_menu;
}

QDockWidget* PTreeContainer::GetDock() const
{
	return m_dock;
}

std::string PTreeContainer::GetEditPath() const
{
	return m_window->GetEditPath();
}


PTreeMenu* PTreeContainer::GetMenu() const {
	return m_menu;
}

ptree PTreeContainer::GetPTreeState() const
{
	ptree result;
	result.put_child("editor_window", m_window->GetPTreeState());
	result.put_child("dock", m_dock->GetPTreeState());
	return result;
}

void PTreeContainer::InternalForceClose()
{
	m_opened = false;
	m_dock->setWindowTitle(m_title);
}

bool PTreeContainer::InternalIsClean() const
{
	return true;
}

bool PTreeContainer::InternalSave()
{
	return true;
}

bool PTreeContainer::IsOpened() const
{
	return m_opened;
}

bool PTreeContainer::Open(boost::property_tree::ptree const & pt, QString const & edit_path)
{
	if (m_window->OpenPTree(pt, edit_path))
	{
		m_menu->OpenPTree(pt);
		m_dock->setWindowTitle(m_title + ": " + edit_path);
		m_opened = true;
		return true;
	}
	return false;
}

void PTreeContainer::OpenPath(QString const & path)
{
	m_window->OpenPath(path);
	m_dock->show();
	m_dock->setWindowTitle(m_title + ": " + path);
}

void PTreeContainer::Redo() {
	m_window->Redo();
}

void PTreeContainer::RefreshMenu(boost::property_tree::ptree const & pt)
{
	m_menu->OpenPTree(pt);
}

void PTreeContainer::SetPTreeState(const ptree& state)
{
	auto editor_window_state = state.get_child_optional("editor_window");
	if (editor_window_state)
		m_window->SetPTreeState(editor_window_state.get());

	auto dock_state = state.get_child_optional("dock");
	if (dock_state) {
		m_dock->SetPTreeState(dock_state.get());
	}
}

void PTreeContainer::SetOnlyEditData(bool b)
{
	m_window->SetOnlyEditData(b);
}

void PTreeContainer::Undo() {
	m_window->Undo();
}

} // namespace Gui
} // namespace SimShell
