/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for LogWindow.
 */

#include "LogWindow.h"

#include <QPlainTextEdit>
#include <sstream>

using namespace std;
using namespace SimPT_Sim::Util;

namespace SimShell {
namespace Gui {

LogWindow::LogWindow(QWidget* parent)
	: QDockWidget(parent)
{
	m_log_widget = new QPlainTextEdit(this);
	m_log_widget->setReadOnly(true);
	m_log_widget->setMaximumBlockCount(100);
	m_log_widget->setCenterOnScroll(true);
	setWindowTitle("Log");
	setWidget(m_log_widget);
	show();
	raise();
}

LogWindow::~LogWindow()
{
}

void LogWindow::AddLine(const string& line)
{
	m_log_widget->appendPlainText(QString::fromStdString(line));
}

} // namespace Gui
} // namespace SimShell
