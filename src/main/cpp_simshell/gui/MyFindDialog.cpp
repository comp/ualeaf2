/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MyFindDialog.
 */

#include "MyFindDialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>

namespace SimShell {
namespace Gui {
MyFindDialog::MyFindDialog(QWidget* parent)
	: QDialog(parent)
{
	m_label = new QLabel("Search for:", this);
	m_input = new QLineEdit;
	m_label->setBuddy(m_input);

	m_match_case = new QCheckBox("Match case");

	m_close_button = new QPushButton("Close");
	m_find_button  = new QPushButton("Find");
	m_find_button->setDefault(true);
	m_find_button->setAutoDefault(true);

	QHBoxLayout* input_layout = new QHBoxLayout;
	input_layout->addWidget(m_label);
	input_layout->addWidget(m_input);

	QHBoxLayout* button_layout = new QHBoxLayout;
	button_layout->addWidget(m_close_button);
	button_layout->addWidget(m_find_button);

	QVBoxLayout* layout = new QVBoxLayout;
	layout->addLayout(input_layout);
	layout->addWidget(m_match_case);
	layout->addLayout(button_layout);

	setLayout(layout);
	setWindowTitle("Find");
	setModal(false);

	connect(m_close_button, SIGNAL(clicked()), this, SLOT(close()));
	connect(m_find_button, SIGNAL(clicked()), this, SLOT(Find()));
}

void MyFindDialog::CorrectFocus()
{
	m_input->setFocus();
	m_find_button->setDefault(true);
}

void MyFindDialog::Find()
{
	emit FindNext(m_input->text(), m_match_case->isChecked());
}

} // namespace Gui
} // namespace SimShell
