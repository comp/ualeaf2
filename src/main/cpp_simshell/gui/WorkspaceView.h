#ifndef WORKSPACE_VIEW_H_INCLUDED
#define WORKSPACE_VIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkspaceView.
 */

#include "MyTreeView.h"
#include "workspace/IProject.h"

#include <QAction>
#include <QItemSelectionModel>

namespace SimShell {
namespace Gui {

class WorkspaceQtModel;

/**
 * TreeView widget that uses WorkspaceQtModel as a model.
 *
 * Owner of a number of actions concerning selected items.
 * Also displays a context menu for selected items.
 */
class WorkspaceView : public MyTreeView
{
	Q_OBJECT
public:
	WorkspaceView(QWidget* parent = 0);
	virtual ~WorkspaceView() {}

	/**
	 * Set the model.
	 *
	 * Must be of dynamic type WorkspaceQtModel or the model is set to zero.
	 */
	virtual void setModel(QAbstractItemModel*);

	/**
	 * Get 'open' action object.
	 *
	 * A 'trigger' signal coming from the action object will cause the selected item to be opened.
	 */
	QAction* GetOpenAction() const;

	/**
	 * Get 'rename' action object.
	 *
	 * This action will only be enabled if a project item is selected in the view; tissue files cannot be renamed.
	 */
	QAction* GetRenameAction() const;

	/**
	 * Get 'remove' action object.
	 *
	 * A 'trigger' signal coming from the action object will cause the selected item to be removed.
	 */
	QAction* GetRemoveAction() const;

private slots:
	/**
	 * Close selected item.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Close();

	/**
	 * Open selected file.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Open();

	void SLOT_ProjectWidgetAction();

	/**
	 * Display dialog asking for a new name for the selected project or tissue file.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_RenameDialog();

	/**
	 * Remove selected item from workspace.
	 *
	 * If no selection is made, nothing happens.
	 */
	void SLOT_Remove();

	/**
	 * Handler called when this view's current item has changed.
	 * @param selected    Selected items.
	 * @param deselected  Deselected items.
	 */
	void SLOT_SelectionChanged(QItemSelection const & selected, QItemSelection const & deselected);

signals:
	/**
	 * Emitted when user renamed a project
	 */
	void ProjectRenamed(const std::string& old_name, const std::string& new_name);

	/**
	 * Emitted when user selected "delete" option on a project.
	 */
	void ProjectRemoved(const std::string& project);

protected:
	/**
	 * Reimplemented from QWidget.
	 * Displays a context menu with trivial actions for the selected item.
	 */
	virtual void contextMenuEvent(QContextMenuEvent*);

	/**
	 * Reimplemented from QTreeView.
	 * If double click happened on a tissue file, it is opened,
	 * otherwise, the handler of the base class is called
	 * (for expanding/collapsing project items)
	 */
	virtual void mouseDoubleClickEvent(QMouseEvent*);

private:
	WorkspaceQtModel* model; // this is here so i don't have to cast model() every time.

	QAction* action_open;
	QAction* action_rename;
	QAction* action_remove;
	QAction* action_close;

	std::shared_ptr<QWidget> project_widget;
	std::string project_widget_project_name;
	struct CallbackMapEntry {
		Ws::IProject::WidgetCallback::Type  callback;
		std::string                         project_name;
	};
	std::map<QObject*, CallbackMapEntry> object_to_callback_map;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
