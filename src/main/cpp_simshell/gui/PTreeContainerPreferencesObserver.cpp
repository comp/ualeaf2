/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeContainerPreferencesObserver.
 */

#include "PTreeContainerPreferencesObserver.h"
#include <QMessageBox>

namespace SimShell {
namespace Gui {

PTreeContainerPreferencesObserver::PTreeContainerPreferencesObserver(QString const & title, QWidget* parent)
	: PTreeContainer(title, parent) {}

PTreeContainerPreferencesObserver::~PTreeContainerPreferencesObserver()
{
}

void PTreeContainerPreferencesObserver::Update(const Ws::Event::PreferencesChanged& e)
{
	if (!IsClean()) {
		// TODO: (extremely low priority since this case will never occur) Make it possible for the user to save his unsaved changes elsewhere.
		if (QMessageBox::warning(this, "Preferences updated",
			"Another entity has updated the preferences contained in this window to "
			"a newer version. You have unsaved changes. Do you want to discard them "
			"and use the most recent version, or keep your changes and ignore the"
			"newer version?",
			QMessageBox::Discard | QMessageBox::Ignore, QMessageBox::Discard)
				== QMessageBox::Ignore)
		{
			return;
		}
	}

	auto state = GetPTreeState();
	ForceClose();
	Open(e.GetPreferences());
	SetPTreeState(state);
}

} // namespace Gui
} // namespace SimShell

