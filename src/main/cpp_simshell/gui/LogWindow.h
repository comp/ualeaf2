#ifndef SIMSHELL_GUI_LOGWINDOW_H_INCLUDED
#define SIMSHELL_GUI_LOGWINDOW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header for LogWindow.
 */

#include "ViewerDockWidget.h"
#include <functional>

class QPlainTextEdit;
class QWidget;

namespace SimShell {
namespace Gui {

/**
 * A viewer that displays sim events in a log in a dock window.
 */
class LogWindow : public QDockWidget
{
public:
	LogWindow(QWidget* parent = nullptr);

	virtual ~LogWindow();

	void AddLine(const std::string& line);

private:
	QPlainTextEdit*  m_log_widget;
	QStringList      m_lines;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
