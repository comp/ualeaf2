#ifndef GUI_RUNNINGCONTROLLER_H_INCLUDED
#define GUI_RUNNINGCONTROLLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * SessionController header.
 */

#include "session/ISession.h"
#include "gui/EnabledActions.h"
#include "workspace/IProject.h"

#include <QWidget>
#include <memory>

class QAction;
class QTimer;

namespace SimShell {
namespace Gui {
namespace Controller {

/**
 * Simple state machine with 3 states:
 * "disabled", "enabled-not-running" and "enabled-running".
 *
 * Resembles the boolean "simulation running" property of an opened project.
 * Owner of actions to start/stop the simulation of an opened project, or
 * performing a single or multiple step(s).
 */
class SessionController : public QWidget
{
	Q_OBJECT
public:
	SessionController(QWidget* parent);

	void Disable();

	void Enable(const std::shared_ptr<Ws::IProject>& project);

	QAction* GetActionRun() const;

	QAction* GetActionMultiStep() const;

	QAction* GetActionSingleStep() const;

	bool IsEnabled() const;

	bool IsRunning() const;

	void SetRunning(bool);

private slots:
	void SLOT_ToggleRunning(bool);

	void SLOT_ToggleMultiStep(bool);

	void SLOT_TimeStep();

private:
	// Because of Qt wanting to match parameter types of signals and slots by string comparison
        typedef SimShell::Session::ISession::InfoMessageReason InfoMessageReason;

private slots:
	/// Slot to receive an simulation error from the project
	void SimulationError(const std::string& error);

        /// Slot to receive an simulation info message from the project
	void SimulationInfo(const std::string& message, const InfoMessageReason& reason);

private:
	void ToggleRunning(bool, int);

	QAction*                                    m_action_run;
	QAction*                                    m_action_multi_step;
	QAction*                                    m_action_single_step;
	EnabledActions                              m_enabled_actions;
	std::shared_ptr<Ws::IProject>               m_project;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
