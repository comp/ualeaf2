#ifndef GUI_WORKSPACECONTROLLER_H_INCLUDED
#define GUI_WORKSPACECONTROLLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * WorkspaceController header.
 */

#include "ProjectController.h"

#include "gui/EnabledActions.h"
#include "gui/HasUnsavedChanges.h"
#include "gui/IHasPTreeState.h"
#include "workspace/IWorkspace.h"
#include "workspace/IWorkspaceFactory.h"

#include <QWidget>
#include <memory>

namespace SimShell {
namespace Gui {

class PTreeContainerPreferencesObserver;
class WorkspaceView;
class WorkspaceQtModel;

namespace Controller {

class AppController;

/**
 * Controller for workspace.
 *
 * Simple state machine that has 2 states: "opened" and "closed".
 * In opened state, it is owner of a workspace object.
 * Owner of WorkspaceView and a dock windows containing workspace preferences.
 *
 * The "opened" state also has a sub-state machine called ProjectController.
 */
class WorkspaceController : public QWidget,
                            public IHasPTreeState,
                            public HasUnsavedChangesPrompt
{
	Q_OBJECT
public:
	WorkspaceController(std::shared_ptr<Ws::IWorkspaceFactory> f, AppController* m);

	bool Open(const std::string& path);

	QAction* GetActionNewProject() const;

	QAction* GetActionRefreshWorkspace() const;

	QDockWidget* GetPreferencesDock() const;

	QMenu* GetPreferencesMenu() const;

	/// @see IHasPTreeState
	virtual boost::property_tree::ptree GetPTreeState() const;

	WorkspaceView* GetView() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	ProjectController& Project();

	const ProjectController& Project() const;

	/// @see IHasPTreeState
	virtual void SetPTreeState(const boost::property_tree::ptree&);

	/// @return Whether we are in opened state.
	operator bool() const;

	Ws::IWorkspace* operator->();
	const Ws::IWorkspace* operator->() const;

	std::shared_ptr<Ws::IWorkspace> operator*();
	std::shared_ptr<const Ws::IWorkspace> operator*() const;

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual void InternalPreForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private slots:
	void SLOT_ApplyPreferences(const boost::property_tree::ptree &);
	void SLOT_NewProjectDialog();
	void SLOT_RefreshWorkspace();

private:
	void InitActions();
	void InitBaseClasses();
	void InitWidgets();

private:
	std::shared_ptr<Ws::IWorkspaceFactory>              m_factory;
	AppController*                                      m_main_controller;

	// These shared ptrs will only contain objects if we are in opened state.
	std::shared_ptr<WorkspaceQtModel>                   m_model;

	// Widgets
	WorkspaceView*                                      m_view;
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_container_preferences;   ///< Collection of menu, dock window, and editor associated with workspace preferences.

	// Actions and menus
	QAction*                                            m_a_new_project;
	QAction*                                            m_a_refresh_workspace;

	// Sub-state
	ProjectController                                   m_project_controller;

	EnabledActions                                      m_enabled_actions;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
