#ifndef GUI_QTMODEL_CHECKABLETREEMODEL_H_INCLUDED
#define GUI_QTMODEL_CHECKABLETREEMODEL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CheckableTreeModel header.
 */

#include <boost/property_tree/ptree.hpp>
#include <QAbstractItemModel>
#include <vector>

namespace SimShell {
namespace Gui {

/**
 * A hierarchical tree-like model of checkable items.
 * After construction, the structure remains fixed; The only part of state that can be modified by the user is the checked/unchecked state of items.
 */
class CheckableTreeModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	/**
	 * Create model. The items are constructed from a ptree with the following structure:
	 *
	 * \code
	 * 	<some_item0>
	 * 		<checked>false</checked>
	 * 		<children>
	 * 			<some_item1>
	 * 				<checked>true</checked>
	 * 			</some_item1>
	 * 		</children>
	 * 	</some_item0>
	 * 	<some_item2>
	 * 		<checked>true</checked>
	 * 		<children/>
	 * 	</some_other_item2>
	 * \endcode
	 * The <checked> node must be present for every item, the <children> node is optional.
	 *
	 * At a later moment, the checked/unchecked state of items can be retrieved by the method ToPTree().
	 * The returned ptree will have the same structure, only the (boolean) values in the <checked> nodes may differ.
	 *
	 * @param tree    A Ptree of form discussed above.
	 * @param parent  Qt parent object that will take ownership of this object.
	 * @see ToPTree()
	 * @throw ptree_bad_path if malformed ptree.
	 * @throw ptree_bad_data if malformed ptree.
	 */
	CheckableTreeModel(const boost::property_tree::ptree& tree, QObject* parent = nullptr);

	virtual ~CheckableTreeModel();

	/// @see QAbstractItemModel
	virtual int columnCount(QModelIndex const&) const;

	/// @see QAbstractItemModel
	virtual QVariant data(const QModelIndex& index, int role) const;

	/// @see QAbstractItemModel
	virtual Qt::ItemFlags flags(const QModelIndex& index) const;

	/// @see QAbstractItemModel
	virtual QVariant headerData(int section, Qt::Orientation o, int role) const;

	/// @see QAbstractItemModel
	virtual QModelIndex index(int row, int column, QModelIndex const& parent) const;

	/// @see QAbstractItemModel
	virtual QModelIndex parent(QModelIndex const& index) const;

	/// @see QAbstractItemModel
	virtual int rowCount(QModelIndex const& parent) const;

	/// @see QAbstractItemModel
	virtual bool setData(QModelIndex const& index, const QVariant& value, int role);

	/**
	 * Get checked/unchecked state of items, represented by a ptree of the form discussed in the constructor.
	 * @see CheckableTreeModel()
	 */
	boost::property_tree::ptree ToPTree();

private:
	/**
	 * Private class for CheckableTreeModel.
	 */
	struct Item {
		~Item();

		Item*               parent;
		std::vector<Item*>  children;
		QString             data;
		bool                checked;
	};

	Item* m_root;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
