#ifndef PTREE_UNDO_COMMANDS_H_INCLUDED
#define PTREE_UNDO_COMMANDS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTree undo commands.
 */

#include "PTreeModel.h"
#include <QUndoCommand>

namespace SimShell {
namespace Gui {

class Item;

/**
 * Undo command that represents a single edit operation for a key value of PTreeModel.
 */
class PTreeModel::EditKeyCommand : public QUndoCommand
{
public:
	/**
	 * @param m          Model in which the operation happened.
	 * @param i          Item of which the key was edited.
	 * @param x          QModelIndex referring to item of which key was edited.
	 *                   (necessary to emit dataChanged() during undo() and redo())
	 * @param old_value  Value of key before it was edited.
	 * @param new_value  New value that was given to key.
	 */
	EditKeyCommand(PTreeModel* m, Item* i, QModelIndex const & x,
			QVariant const & old_value, QVariant const & new_value);
	virtual ~EditKeyCommand();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void undo();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void redo();

private:
	PTreeModel*             model;
	Item*                   item;
	QPersistentModelIndex   index;
	QVariant                old_value;
	QVariant                new_value;
};

/**
 * Undo command that represents a single edit operation for a data value of PTreeModel.
 */
class PTreeModel::EditDataCommand : public QUndoCommand
{
public:
	/**
	 * @param m          Model in which the operation happened.
	 * @param i          Item of which the data value was edited.
	 * @param x          QModelIndex referring to item of which data was edited.
	 *                  (necessary to emit dataChanged() during undo() and redo())
	 * @param old_value  Value of data before it was edited.
	 * @param new_value  New value that was given to data.
	 */
	EditDataCommand(PTreeModel* m, Item* i, QModelIndex const & x,
			QVariant const & old_value, QVariant const & new_value);
	virtual ~EditDataCommand();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void undo();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void redo();

private:
	PTreeModel*             model;
	Item*                   item;
	QPersistentModelIndex   index;
	QVariant                old_value;
	QVariant                new_value;
};

/**
 * Undo command that represents a single insertion operation of rows into a PTreeModel.
 * See QUndoCommand documentation for details.
 */
class PTreeModel::InsertRowsCommand : public QUndoCommand
{
public:
	/**
	 * @param m             Model in which the operation happened.
	 * @param parent        Parent item into which rows were inserted.
	 * @param parent_index  QModelIndex referring to parent item. (necessary to
	 *                      emit beginInsertRows() and endInsertRows() signals
	 *                      during undo() and redo())
	 * @param row           Index of row in parent before which rows were inserted.
	 * @param count         Number of rows that were inserted.
	 */
	InsertRowsCommand(PTreeModel* m, Item* parent,
			QModelIndex const & parent_index, int row, int count);

	/**
	 * @param m             Model in which the operation happened.
	 * @param parent        Parent item into which rows were inserted.
	 * @param parent_index  QModelIndex referring to parent item. (necessary to
	 *                      emit beginInsertRows() and endInsertRows() signals
	 *                      during undo() and redo())
	 * @param row           Index of row in parent before which rows were inserted.
	 * @param t             ptree object of which the children represent the rows that were inserted.
	 */
	InsertRowsCommand(PTreeModel* m, Item* parent,
			QModelIndex const & parent_index, int row, boost::property_tree::ptree const& t);
	virtual ~InsertRowsCommand();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void undo();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void redo();

private:
	PTreeModel*             model;
	Item*                   parent;
	QPersistentModelIndex   parent_index;
	int                     row;
	int                     count;
	bool                    undone;
	Item**                  items;
};

/**
 * Undo command that represents a single removal operation of rows from a PTreeModel.
 * See QUndoCommand documentation for details.
 */
class PTreeModel::RemoveRowsCommand : public QUndoCommand
{
public:
	/**
	 * @param m             Model in which the operation happened.
	 * @param parent        Parent item from which rows were removed.
	 * @param parent_index  QModelIndex referring to parent item. (necessary
	 *                      to emit beginRemoveRows() and endRemoveRows() signals
	 *                      during undo() and redo())
	 * @param row           Index of first row of removed rows in parent.
	 * @param count         Number of rows that were removed.
	 */
	RemoveRowsCommand(PTreeModel* m, Item* parent, QModelIndex const & parent_index, int row, int count);
	virtual ~RemoveRowsCommand();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void undo();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void redo();

private:
	PTreeModel*             model;
	Item*                   parent;
	QPersistentModelIndex   parent_index;
	int                     row;
	int                     count;
	bool                    undone;
	Item**                  items;
};

/**
 * Undo command that represents a single move operation of rows inside a PTreeModel.
 * See QUndoCommand documentation for details.
 */
class PTreeModel::MoveRowsCommand : public QUndoCommand
{
public:
	/**
	 * @param m                 Model in which the operation happened.
	 * @param old_parent        Parent item from which rows were moved.
	 * @param old_parent_index  QModelIndex referring to old parent item.
	 *                          (necessary to emit signals during undo() and redo())
	 * @param old_row           Old row.
	 * @param new_parent        Parent item into which rows were moved.
	 * @param new_parent_index  QModelIndex referring to new parent item.
	 *                          (necessary to emit signals during undo() and redo())
	 * @param new_row           New row.
	 * @param count             Number of rows that were moved.
	 */
	MoveRowsCommand(PTreeModel* m, Item* old_parent, QModelIndex const& old_parent_index, int old_row,
	        Item* new_parent, QModelIndex const& new_parent_index, int new_row, int count);
	virtual ~MoveRowsCommand();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void undo();

	/**
	 * See QUndoCommand documentation for details.
	 */
	virtual void redo();

private:
	PTreeModel*             model;
	Item*                   old_parent;
	Item*                   new_parent;
	QPersistentModelIndex   old_parent_index;
	QPersistentModelIndex   new_parent_index;
	int                     old_row;
	int                     new_row;
	int                     count;
	Item**                  items;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
