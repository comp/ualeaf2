/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeMenu.
 */

#include "PTreeMenu.h"

#include <QAction>
#include <QCloseEvent>
#include <QDockWidget>
#include <QSignalMapper>

#include "PTreeEditorWindow.h"

namespace SimShell {
namespace Gui {

using boost::property_tree::ptree;

PTreeMenu::PTreeMenu(QString const& t, QString const& path_prefix, QWidget* parent)
    : QMenu(t, parent), HasUnsavedChanges("Menu: " + t.toStdString()), m_opened(false), m_path(path_prefix)
{
    m_signal_mapper = new QSignalMapper(this);
    connect(m_signal_mapper, SIGNAL(mapped(QString const&)), this, SIGNAL(ItemTriggered(QString const&)));

    AddAllAction();
}

PTreeMenu::~PTreeMenu()
{
}

void PTreeMenu::AddAllAction()
{
    m_action_all = new QAction("All...", this);
    connect(m_action_all, SIGNAL(triggered()), m_signal_mapper, SLOT(map()));
    m_signal_mapper->setMapping(m_action_all, m_path);
    addAction(m_action_all);
}

void PTreeMenu::InternalForceClose()
{
	m_opened = false;

	for (auto action : actions()) {
		m_signal_mapper->removeMappings(action);
	}
	clear(); // also deletes action objects

	AddAllAction();
}

bool PTreeMenu::InternalIsClean() const
{
	return true;
}

bool PTreeMenu::InternalSave()
{
	return true;
}

bool PTreeMenu::IsOpened() const
{
	return m_opened;
}

bool PTreeMenu::OpenPTree(ptree const& pt, int depth)
{
    if (SaveAndClose()) {
	    OpenPTreeInternal(pt, depth);
	    m_opened = true;
	    return true;
    } else {
	    return false;
    }
}

void PTreeMenu::OpenPTreeInternal(boost::property_tree::ptree const& pt, int remaining_depth)
{
    addSeparator();

    // other actions for each subtree
	if (remaining_depth != 0) {
		for (auto const & subtree : pt) {
			if (subtree.second.empty()) {
				continue; // don't display single items in menu
			}
			QString item_name = QString::fromStdString(subtree.first);
			QString item_path = (m_path == "") ? item_name : m_path + '.' + item_name;

			if (remaining_depth > 1 || remaining_depth == -1) {
				PTreeMenu* menu = new PTreeMenu(item_name, item_path, this);
				menu->OpenPTreeInternal(subtree.second,
				                (remaining_depth == -1) ? -1 : remaining_depth - 1);
				addMenu(menu);

				// forward signals all the way up to the root menu
				connect(menu, SIGNAL(ItemTriggered(QString const&)),
						this, SIGNAL(ItemTriggered(QString const&)));
			} else {
				QAction* action = new QAction(item_name + "...", this);
				connect(action, SIGNAL(triggered()), m_signal_mapper, SLOT(map()));
				m_signal_mapper->setMapping(action, item_path);
				addAction(action);
			}
		}
	}
}

} // end of namespace Gui
} // end of namespace SimShell
