#ifndef MAIN_PTREE_EDITOR_H_INCLUDED
#define MAIN_PTREE_EDITOR_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeEditor.
 */

#include "gui/HasUnsavedChangesPrompt.h"

#include <boost/property_tree/ptree.hpp>
#include <QMainWindow>
#include <string>

class QTabWidget;
class QMenu;
class QStatusBar;
class QToolBar;

namespace SimShell {
namespace Gui {

class MyFindDialog;
class PTreeView;
class PTreeModel;

} // namespace Gui
} // namespace SimShell

namespace SimPT_Shell {
namespace Gui {

using namespace SimShell::Gui;

/**
 * Stand-alone Property Tree editor.
 */
class PTreeEditor : public QMainWindow, public HasUnsavedChangesPrompt
{
	Q_OBJECT
public:
	PTreeEditor(QWidget* parent = 0);
	virtual ~PTreeEditor() {}

	/**
	 * Open file.
	 * If a file is already opened, it is closed first.
	 * @param path  Path to file to open.
	 * @return true if succesful.
	 */
	bool OpenPath(std::string const & path);

	/**
	 * Save opened file to a given path.
	 * If no file is opened, nothing happens.
	 * @param path  Path to save file to.
	 * @return true if application is in clean state and opened_path is equal to given argument.
	 */
	bool SavePath(std::string const & path);

	/// Test whether app is in opened state.
	virtual bool IsOpened() const;

private slots:
	/**
	 * Save opened file to opened path. If opened file is untitled (new file), will show a save dialog.
	 * @return true if application is in clean state and opened_path is an existing file on disk.
	 * @see PromptOnClose::Save().
	 */
	void SLOT_Save();

	/**
	 * Close file. Confirmation dialog will be shown if there are unsaved changes.
	 * @return true if application is in unopened state.
	 */
	void SLOT_PromptClose();

	/**
	 * Create new file for editing.
	 */
	void SLOT_New();

	/**
	 * Display dialog to open file.
	 */
	void SLOT_OpenDialog();

	/**
	 * Display dialog to save file.
	 * @return true if application is in clean state and opened_path is an existing file on disk.
	 */
	bool SLOT_SaveAsDialog();

	/**
	 * Set fullscreen mode.
	 */
	void SLOT_SetFullscreenMode(bool);

	/**
	 * Display 'about' dialog.
	 */
	void SLOT_AboutDialog();

	/**
	 * Handles PTreeView cleanChanged events.
	 */
	void SLOT_SetClean(bool);

protected:
	/**
	 * Re-implemented from QMainWindow.
	 * Closes file if file is opened, possibly displaying 'save changes?'-dialog.
	 * Event is accepted only if this results in the application remaining in an unopened state.
	 * @see QWidget::closeEvent
	 */
	virtual void closeEvent(QCloseEvent *event);

	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private:
	QAction*     action_new;
	QAction*     action_open;
	QAction*     action_save;
	QAction*     action_save_as;
	QAction*     action_close;
	QAction*     action_quit;
	QAction*     action_view_toolbar;
	QAction*     action_view_statusbar;
	QAction*     action_fullscreen;
	QAction*     action_about;

	QToolBar*    toolbar;
	QStatusBar*  statusbar;

	std::string      opened_path; // empty but ptree_model != 0 means an untitled new file is opened.
	PTreeView*       ptree_view;
	PTreeModel*      ptree_model; // 0 if app is in unopened state

	boost::property_tree::ptree     root_pt;
	std::string                     subtree_key;

	static const QString g_caption;
	static const QString g_caption_with_file;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
