#ifndef PTREE_COMPARISON_H_INCLUDED
#define PTREE_COMPARISON_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeComparison.
 */

#include "util/misc/Exception.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <utility>

namespace SimPT_Shell {

using boost::property_tree::ptree;

/**
 * Collection of functions for recursively comparing ptrees, with some 'array' semantics applied.
 *
 * Because all data in a ptree is both ordered (sequential) and unordered (mapped by a key) at the same time,
 * we apply the semantics that, if a subtree path ends with "_array", the order of its children is semantically taken into account.
 * For 2 'array' subtrees to be equal, we thus require all children to be present in the same order in both subtrees.
 * If a subtree path does not end with "_array", we just want all values with a certain key to be found under the same key in the subtree it is compared with.
 *
 * This is all done recursively: For instance, the ordered items of an array can, in its turn, be arrays or unordered subtrees,
 * and the unordered items of an indexed subtree can be arrays or unordered subtrees.
 * The right compare method is selected on each recursion.
 */
class PTreeComparison
{
public:
	/**
	 * Test if root values of both ptrees are equal, with the possibility
	 * of having some tolerance for small differences in case both root
	 * values are floats.
	 *
	 * Does NOT recursively compare children of the given ptrees.
	 *
	 * @param pt1                 First ptree.
	 * @param pt2                 Second ptree.
	 * @param acceptable_diff     Largest tolerated difference between
	 *                            root values if both are float type.
	 * @return  Whether root values tested equal.
	 */
	static bool CompareRootValues(ptree const& pt1, ptree const& pt2, double acceptable_diff);

	/**
	 * Test if ordered list of children of both ptrees are equal,
	 * i.e. lenghts must be the same, and every item in one ptree must
	 * occur in the other at the same index.
	 *
	 * Root values of children are compared using CompareRootValues.
	 * On top of that, recursively compares children of both ptrees:
	 * When a key under which a child ptree is found ends with the suffix
	 * "_array", CompareArray is used. Otherwise, CompareNonArray is used.
	 *
	 * @param pt1                First ptree.
	 * @param pt2                Second ptree.
	 * @param acceptable_diff    Largest tolerated difference between root
	 *                           values of children in both ptrees.
	 * @return  Whether both ptrees tested equal.
	 */
	static bool CompareArray(ptree const& pt1, ptree const& pt2, double acceptable_diff);

	/**
	 * Test if every child in first ptree with certain key can also be
	 * found in second ptree under the same key, and vice versa.
	 *
	 * Comparison is RECURSIVE, see PTreeComparison::CompareArray for details.
	 *
	 * @param pt1                First ptree.
	 * @param pt2                Second ptree.
	 * @param acceptable_diff    Largest tolerated difference between root
	 *                           values of children in both ptrees.
	 * @return  Whether both ptrees tested equal.
	 */
	static bool CompareNonArray(ptree const& pt1, ptree const& pt2, double acceptable_diff);
};

} // namespace

#endif // end-of-include-guard
