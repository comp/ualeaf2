#ifndef SIMSHELL_PTREEQTSTATE_H_INCLUDED
#define SIMSHELL_PTREEQTSTATE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeQtState.
 */

#include <map>
#include <boost/property_tree/ptree.hpp>
#include <QMainWindow>

class QDockWidget;
class QTreeView;
class QWidget;

namespace SimShell {

/**
 * Utility for representing state of various Qt widgets as a ptree.
 *
 * I'm not going into detail of what such a ptree looks like, you shouldn't construct one yourself.
 * Just call GetState() and use the result for SetState() at another point in time.
 *
 * @see IHasPTreeState
 */
class PTreeQtState
{
public:
	/**
	 * Get ptree representation of expand/collapsed state of items in given QTreeView.
	 */
	static boost::property_tree::ptree GetTreeViewState(const QTreeView*);

	/**
	 * Set expand/collapsed state of items in given QTreeView as represented by given ptree.
	 */
	static void SetTreeViewState(QTreeView*, const boost::property_tree::ptree&);

	/**
	 * Get ptree representation of typical widget properties (hidden/shown, size, position, ...)
	 */
	static boost::property_tree::ptree GetWidgetState(const QWidget*);

	/**
	 * Set state of given widget as represented by given ptree.
	 */
	static void SetWidgetState(QWidget*, const boost::property_tree::ptree&);

	/**
	 * Get ptree representatoin of dock widget-specific properties.
	 */
	static boost::property_tree::ptree GetDockWidgetState(const QDockWidget*);

	/**
	 * Set state of given dock widget as represented by given ptree.
	 */
	static void SetDockWidgetState(QDockWidget*, const boost::property_tree::ptree&);

	/**
	 * Get DockWidgetArea of dock widget in main window as a ptree.
	 */
	static boost::property_tree::ptree GetDockWidgetArea(const QMainWindow*, QDockWidget*);

	/*
	 * Set DockWidgetArea as a ptree of dock widget in main window.
	 */
	static void SetDockWidgetArea(QMainWindow*, QDockWidget*, const boost::property_tree::ptree&);

private:
	static const std::map<Qt::DockWidgetArea, std::string> g_dock_widget_area_to_string;
	static const std::map<std::string, Qt::DockWidgetArea> g_string_to_dock_widget_area;
};

} // namespace

#endif // end-of-include-guard
