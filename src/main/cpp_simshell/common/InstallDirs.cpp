/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for FileSys class.
 */

#include "InstallDirs.h"

#include <string>
#include <sstream>
#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QApplication>

using namespace std;

namespace SimShell {

std::string 	InstallDirs::g_data_dir;
std::string 	InstallDirs::g_root_dir;
std::string 	InstallDirs::g_workspace_dir;
bool            InstallDirs::g_initialized = false;


inline void InstallDirs::Check()
{
	if (!g_initialized) {
		Initialize();
	}
}

void InstallDirs::Initialize()
{
	//------- Retrieving Install Root Path
	{
		QDir dir(QApplication::applicationDirPath());

	#if defined(__APPLE__)
		if (dir.dirName() == "MacOS") {
			// app
			//	-Contents		<-Root Path
			//		-MacOS
			//		     -binaries
			dir.cdUp();
		} else
	#endif
		if (dir.dirName().toLower() == "debug" || dir.dirName().toLower() == "release") {
			//x/exec		<-Root Path
			//	-bin
			//		-release/debug
			//			-binaries
			dir.cdUp();
			dir.cdUp();
		} else 
	#if defined(__WIN32__)
		if (dir.dirName() != "bin") {
				// Do Nothing, binaries in root folder
		} else
	#endif
		{
			//x/exec		<-Root Path
			//	-bin
			//		-binaries
			dir.cdUp();
		}

		const bool  exists = dir.exists();
		const string path  = dir.absolutePath().toStdString();
		g_root_dir = (exists) ? dir.absolutePath().toStdString() : "";
	}
	//------- Retrieving Data Dir
	{
		QDir dir(QString::fromStdString(g_root_dir));
		const bool  exists     = dir.cd("data");
		g_data_dir = (exists) ? dir.absolutePath().toStdString() : "";
	}
	//------- Retrieving Workspace
	{
		QDir dir(QString::fromStdString(g_data_dir));
		const bool exists  = dir.cd("simPT_Default_workspace");
		const string path  = dir.absolutePath().toStdString();
		g_workspace_dir = (exists) ? dir.absolutePath().toStdString() : "";
	}

	g_initialized = true;
}

string InstallDirs::GetDataDir()
{
	Check();
	return g_data_dir;
}

string InstallDirs::GetRootDir()
{
        Check();
        return g_root_dir;
}

string InstallDirs::GetWorkspaceDir()
{
	Check();
	return g_workspace_dir;
}

} // namespace


