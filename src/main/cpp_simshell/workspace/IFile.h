#ifndef WS_IFILE_H_INCLUDED
#define WS_IFILE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IFile.
 */

#include "MergedPreferences.h"
#include "session/ISession.h"

#include <memory>
#include <string>

class QAction;

namespace SimShell {
namespace Ws {

/**
 * Interface for files in a project in a workspace.
 *
 * A file can serve as a starting point for a session (=opened state of a project).
 *
 * A file can also specify actions specific for its subtype.
 */
class IFile
{
public:
	/**
	 * First argument: filename.
	 * Second argument: path of file, including filename.
	 */
	using ConstructorType = std::function<std::shared_ptr<IFile>(const std::string&, const std::string&)>;

public:
	/// Virtual destructor.
	virtual ~IFile() {}

	/// Create simulation entities from file.
	virtual std::shared_ptr<Session::ISession>
	CreateSession(
		std::shared_ptr<SimShell::Ws::IProject> proj,
		std::shared_ptr<SimShell::Ws::IWorkspace> ws) const = 0;

	/// Get Qt actions that can be triggered on this type of file.
	virtual std::vector<QAction*> GetContextMenuActions() const = 0;

	/// Get file path.
	virtual const std::string& GetPath() const = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
