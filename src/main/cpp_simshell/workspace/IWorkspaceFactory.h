#ifndef WS_IWORKSPACEFACTORY_H_INCLUDED
#define WS_IWORKSPACEFACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IWorkspaceFactory.
 */
#include "IWorkspace.h"

namespace SimShell {
namespace Ws {

/**
 * Interface for factories creating an application-specific workspace.
 */
class IWorkspaceFactory
{
public:
	/// Virtual destructor.
	virtual ~IWorkspaceFactory() {}

	/// Get path to template workspace.
	/// The template workspace is a workspace containing example projects.
	virtual std::string GetWorkspaceTemplatePath() const = 0;

	/// Get default workspace name.
	/// This string will determine the default path shown in the "Switch Workspace..."-wizard:
	/// <user's home dir/<default workspace name>.
	virtual std::string GetDefaultWorkspaceName() const = 0;

	/// Open workspace with given path and return interface to it.
	virtual std::shared_ptr<IWorkspace> CreateWorkspace(const std::string& path) = 0;

	/// Initialize empty workspace at given path.
	virtual void InitWorkspace(const std::string& path) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
