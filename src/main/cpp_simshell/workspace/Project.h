#ifndef WS_PROJECT_H_INCLUDED
#define WS_PROJECT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Project.
 */

#include "IProject.h"
#include "Preferences.h"
#include "session/ISession.h"
#include "util/FileSystemWatcher.h"

namespace SimShell {
namespace Ws {

/**
 * Abstraction of project in workspace on file system.
 *
 * Template parameters:
 * 	- FileType: Any class that has a public static data member
 * 	named 'Constructor' of (function) type IFile::ConstructorType in it.
 * 	The 'Constructor' function should return any IFile implementation based
 * 	on the file name and path. (Typically the extension is used to determine
 * 	the type actually constructed)
 * 	- index_file: String containing the filename of the project index file.
 * 	Possibly a hidden file to discourage manual editing.
 * 	The project index file only contains the project user data (see IUserData),
 * 	but may contain the list of project files in the future.
 */
template <class FileType, const std::string& index_file>
class Project : public IProject,
                public Preferences
{
public:
	/// Constructor.
	/// @param path   Path to project.
	Project(const std::string& path,
	        const std::string& prefs_file,
	        const std::shared_ptr<IWorkspace>& w);

	/// Move constructor.
	Project(Project<FileType, index_file>&&);

	/// Virtual destructor.
	virtual ~Project() {}

	/// @see IProject
	virtual FileIterator Add(const std::string& name);

	/// @see IProject
	virtual std::shared_ptr<IFile> Back();

	/// @see IProject
	virtual std::shared_ptr<const IFile> Back() const;

	/// @see IProject
	virtual FileIterator begin();

	/// @see IProject
	virtual ConstFileIterator begin() const;

	/// @see IProject
	virtual FileIterator end();

	/// @see IProject
	virtual ConstFileIterator end() const;

	/// @see IProject
	virtual void Close();

	/// @see IProject
	virtual FileIterator Find(const std::string& name);

	/// @see IProject
	virtual ConstFileIterator Find(const std::string& name) const;

	/// @see IProject
	virtual std::shared_ptr<IFile> Front();

	/// @see IProject
	virtual std::shared_ptr<const IFile> Front() const;

	/// @see IProject
	virtual std::shared_ptr<IFile> Get(const std::string& name);

	/// @see IProject
	virtual std::shared_ptr<const IFile> Get(const std::string& name) const;

	/// @see IProject
	virtual const std::string& GetIndexFile() const;

	/// @see IProject
	virtual const std::string& GetPath() const;

	/// @see IProject
	virtual std::shared_ptr<Session::ISession> GetSession() const;

	/// @see IProject
	virtual std::string GetSessionFileName() const;

	/// @see IUserData
	virtual const boost::property_tree::ptree& GetUserData(const std::string& user) const;

	/// @see IProject
	virtual bool IsLeaf(const std::string& name) const;

	/// @see IProject
	virtual bool IsOpened() const;

	/// @see IProject
	virtual bool IsWatchingDirectory() const;

	/// @see IProject
	virtual void Open();

	/// @see IProject
	virtual void Open(const std::string& name);

	/// @see IProject
	virtual void Open(FileIterator);

	/// @see IProject
	virtual void Refresh();

	/// @see IProject
	virtual void Remove(const std::string& name);

	/// @see IProject
	virtual void Remove(FileIterator);

	/// @see IProject
	virtual Session::ISession& Session() const;

	/// @see IProject
	virtual void SetWatchingDirectory(bool);

	/// @see IUserData
	virtual void SetUserData(const std::string& user, const boost::property_tree::ptree&);

private:
	/// Forbid copy constructor.
	Project(const Project<FileType, index_file>&);

	/// Forbid copy assignment.
	Project<FileType, index_file>& operator=(const Project<FileType, index_file>&);

	/// Writes preferences to index_file in project directory.
	void Store();

protected:
	std::string                           m_path;               ///< Path of project directory.
	std::shared_ptr<IWorkspace>           m_workspace;          ///< Workspace to which project belongs.
	std::shared_ptr<Session::ISession>    m_session;            ///< Opened state.
	std::string                           m_session_name;       ///< Name of opened file.
	FileMap                               m_files;              ///< Mapping from file name to file objects.
	mutable boost::property_tree::ptree   m_user_data;          ///< User data. Mutable because a GetUserData() with non-existing argument will create an empty user data ptree for that argument.
	Util::FileSystemWatcher               m_filesystem_watcher; ///< Watches project directory for changes.
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
