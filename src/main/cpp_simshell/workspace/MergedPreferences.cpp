/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MergedPreferences.
 */

#include "MergedPreferences.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Ws {

shared_ptr<MergedPreferences> MergedPreferences::Create(const shared_ptr<Ws::IWorkspace>& w,
                                            const shared_ptr<Ws::IProject>& p)
{
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(w,p));
	w->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	p->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

MergedPreferences::MergedPreferences(const shared_ptr<Ws::IWorkspace>& w,
                         const shared_ptr<Ws::IProject>& p)
	: m_path(), m_workspace(w), m_project(p)
{
}

MergedPreferences::MergedPreferences(const boost::property_tree::ptree::path_type& p, const MergedPreferences& other)
	: m_path(p), m_workspace(other.m_workspace), m_project(other.m_project)
{
}

MergedPreferences::~MergedPreferences()
{
}

shared_ptr<MergedPreferences> MergedPreferences::GetChild(const boost::property_tree::ptree::path_type& p) const
{
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(m_path / p, *this));

	m_workspace->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	m_project->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

shared_ptr<MergedPreferences> MergedPreferences::GetGlobal() const
{
	boost::property_tree::ptree::path_type empty_path;
	auto result = shared_ptr<MergedPreferences>(new MergedPreferences(empty_path, *this));

	m_workspace->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));
	m_project->Subject<Ws::Event::PreferencesChanged>::Register(
		result, bind(&MergedPreferences::ListenPreferencesChanged, result.get(), _1));

	return result;
}

const std::string& MergedPreferences::GetPath() const
{
	return m_project->GetPath();
}

const boost::property_tree::ptree& MergedPreferences::GetProjectPreferencesTree()
{

	return m_project->GetPreferences();

}

void MergedPreferences::ListenPreferencesChanged(const Ws::Event::PreferencesChanged&)
{
	Notify({shared_from_this()});
}

} // namespace Ws
} // namespace SimShell
