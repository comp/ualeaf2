#ifndef WS_IWORKSPACE_H_INCLUDED
#define WS_IWORKSPACE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IWorkspace.
 */

#include "IPreferences.h"
#include "IUserData.h"
#include "event/PreferencesChanged.h"
#include "event/WorkspaceChanged.h"
#include "util/misc/Subject.h"

#include <boost/property_tree/ptree.hpp>
#include <map>

namespace SimShell {
namespace Ws {

class IProject;
class IWorkspace;

/**
 * Interface for workspace-like behavior.
 *
 * A workspace is a container of projects, indexed by name.
 * Projects can be created, removed and renamed.
 *
 * Additionally, workspace also has associated with it a tree of workspace preferences,
 * and a tree of user data.
 *
 * @see IPreferences IUserData
 */
class IWorkspace : public virtual IPreferences,
                   public IUserData,
                   public SimPT_Sim::Util::Subject<Event::WorkspaceChanged>
{
public:
	/// Virtual destructor.
	virtual ~IWorkspace() {}

	class ProjectMapEntry {
	public:
		ProjectMapEntry(const std::shared_ptr<IProject>& p, const std::string& t)
			: m_project(p), m_type(t) {}

		inline IProject* operator->() { return m_project.get(); }
		inline const IProject* operator->() const { return m_project.get(); }

		inline IProject& operator*() { return *m_project.get(); }
		inline const IProject& operator*() const { return *m_project.get(); }

		inline std::shared_ptr<const IProject> Project() const { return m_project; }
		inline std::shared_ptr<IProject> Project() { return m_project; }

		inline std::string Type() { return m_type; }

	private:
		std::shared_ptr<IProject>  m_project;
		std::string                m_type;
	};

	using ProjectMap = std::map<std::string, ProjectMapEntry>;
	using ProjectIterator = typename ProjectMap::iterator;
	using ConstProjectIterator = typename ProjectMap::const_iterator ;

	/// Add existing project to workspace.
	/// Workspace must already contain directory with project before calling this method.
	/// @param type   Type of project.
	/// @param name   Name of project (= name of directory).
	/// @return iterator to added project
	virtual ProjectIterator Add(const std::string& type, const std::string& name) = 0;

	/// Get last project.
	/// @return Last project or nullptr if there are no projects in workspace.
	virtual std::shared_ptr<IProject> Back() = 0;

	/// Get last project.
	/// @return Last project or nullptr if there are no projects in workspace.
	virtual std::shared_ptr<const IProject> Back() const = 0;

	/// Get iterator pointing to first project.
	virtual ProjectIterator begin() = 0;

	/// Get iterator pointing to first project.
	virtual ConstProjectIterator begin() const = 0;

	/// Get first project.
	/// @return First project or nullptr if there are no projects in workspace.
	virtual std::shared_ptr<IProject> Front() = 0;

	/// Get first project.
	/// @return First project or nullptr if there are no projects in workspace.
	virtual std::shared_ptr<const IProject> Front() const = 0;

	/// Get iterator pointing to one position after last project.
	virtual ProjectIterator end() = 0;

	/// Get iterator pointing to one position after last project.
	virtual ConstProjectIterator end() const = 0;

	/// Get iterator to project with given name.
	/// @return Iterator to project or Workspace::end() if not found.
	virtual ProjectIterator Find(const std::string& name) = 0;

	/// Get iterator to project with given name.
	/// @return Iterator to project or Workspace::end() if not found.
	virtual ConstProjectIterator Find(const std::string& name) const = 0;

	/// Get iterator to project with given name.
	/// @return True iff project exists.
	virtual bool IsProject(const std::string& name) const = 0;

	/// Get project with given name.
	/// @throw Exception if no project with given name exists.
	virtual std::shared_ptr<IProject> Get(const std::string& name) = 0;

	/// Get project with given name.
	/// @throw Exception if no project with given name exists.
	virtual std::shared_ptr<const IProject> Get(const std::string& name) const = 0;

	/// Get name of index file for this type of workspace.
	virtual const std::string& GetIndexFile() const = 0;

	/// Get path workspace was initialized with.
	virtual const std::string& GetPath() const = 0;

	/// Create new project in workspace.
	/// A directory will be created for you.
	/// @return iterator to newly created project
	/// @throw Exception if another project with given name exists.
	virtual ProjectIterator New(const std::string& type, const std::string& name) = 0;

	/// Refresh workspace.
	/// Will generate WorkspaceChanged events for newly discovered projects and projects that dissappeared.
	virtual void Refresh() = 0;

	/// Remove project.
	/// @param it  Iterator pointing to project to remove.
	virtual void Remove(ProjectIterator it) = 0;

	/// Remove project.
	/// @param name  Name of project to remove.
	virtual void Remove(const std::string& name) = 0;

	/// Rename project.
	/// @return iterator to renamed project
	virtual ProjectIterator Rename(ProjectIterator, const std::string& new_name) = 0;

	/// Rename project.
	/// @return iterator to renamed project
	virtual ProjectIterator Rename(const std::string& old_name, const std::string& new_name) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
