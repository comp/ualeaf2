#ifndef WS_EVENT_PROJECTCHANGED_H_INCLUDED
#define WS_EVENT_PROJECTCHANGED_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for ProjectChanged.
 */

namespace SimShell {
namespace Ws {
namespace Event {

/**
 * Event used to inform some observer that a project has changed.
 *
 * Really a POD but packaged to force users to initialize all data members.
 */
class ProjectChanged
{
public:
	enum Type {
		LeafAdded,
		LeafRemoved
	};

	ProjectChanged(Type type, const std::string& name)
		: m_type(type), m_name(name) {}

	/// Get type.
	Type GetType() const { return m_type;}

	/// Get name of added/removed sim data file.
	const std::string& GetName() const { return m_name; }

private:
	Type         m_type;
	std::string  m_name;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
