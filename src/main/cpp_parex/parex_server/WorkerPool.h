#ifndef SIMPT_PAREX_WORKER_POOL_H_INCLUDED
#define SIMPT_PAREX_WORKER_POOL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkerPool
 */

#include "WorkerRepresentative.h"

#include <QUdpSocket>
#include <QTcpSocket>
#include <QObject>
#include <QString>
#include <QSignalMapper>
#include <QTimer>
#include <list>

namespace SimPT_Parex {

/**
 * A pool handling the workers that will do the actual work
 */
class WorkerPool: public QObject
{
	Q_OBJECT
public:
	virtual ~WorkerPool();

	/**
	 * Gets a process doing nothing if one is available, otherwise returns 0.
	 * This makes the worker unavailable for others, so be sure to give him work.
	 */
	WorkerRepresentative *getProcess();

	/**
	 * A static method to get the instance of the workerPool.
	 *
	 * Workers broadcasting their location will only be picked up after this has been called at least once!
	 */
	static WorkerPool *globalInstance();

	/**
	 * Checks to see if a worker is ready to do some work
	 */
	bool WorkerAvailable();

	/**
	 * Set the minimum number of workers
	 */
	void SetMinNumWorkers(int);

	/**
	 * Delete an exploration on every node
	 */
	void Delete(std::string name);

signals:
	/**
	 * Emitted when a worker is ready for work.
	 */
	void NewWorkerAvailable();

	/**
	 * Emitted when a worker has reconnected after being disconnected.
	 */
	void WorkerReconnected(WorkerRepresentative*);

public slots:
	/**
	 * When a worker disconnects, this slot should be fired.
	 */
	void ReleaseWorker(QObject*);
	void ReleaseWorker(WorkerRepresentative*);

	/**
	 * When a worker has finished his work, this slot should be fired.
	 */
	void MakeProcessAvailable(QObject*);
	void MakeProcessAvailable(WorkerRepresentative*);

private slots:
	void handleDatagrams();
	void Initialize();
	void StartWorkers();
	void PeriodicCheck();

private:
	void StartWorker();
	WorkerPool();

	QUdpSocket *m_socket;
	QTcpSocket *m_tcpSocket;

	QSignalMapper *m_mapper;
	QSignalMapper *m_release_mapper;
	std::list<WorkerRepresentative*> m_available_list;
	std::list<WorkerRepresentative*> m_busy_list;
	std::list<QString> m_connected_list;
	bool m_initialized;
	QTimer *m_initialize_timer;

	int m_min_nodes;

	QTimer *m_check_timer;
};

} // namespace

#endif // end-of-include-guard
