/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of ClientHandler
 */

#include "ClientHandler.h"

#include "ExplorationManager.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/ServerClientProtocol.h"

#include <QString>
#include <QTcpSocket>
#include <iostream>

class QTcpSocket;

namespace SimPT_Parex {

class ExplorationManager;
using namespace std;

ClientHandler::ClientHandler(QTcpSocket* socket,
		const std::shared_ptr<ExplorationManager>& explorationManager, QObject* parent)
	: QObject(parent), m_protocol(nullptr), m_exploration_manager(explorationManager)
{
	assert(socket && "socket cannot be a nullptr");
	assert(explorationManager.get() && "explorationManager cannot be a nullptr");

	m_protocol = new ServerClientProtocol(socket, parent);

	connect(m_protocol, SIGNAL(Ended()), this, SLOT(deleteLater()));
	connect(m_protocol, SIGNAL(Error(const std::string&)),
			this, SLOT(DisplayError(const std::string&)));

	// Connects for registering received Explorations
	connect(m_protocol, SIGNAL(ExplorationReceived(const Exploration*)),
			this, SLOT(RegisterExploration(const Exploration*)));
	connect(m_exploration_manager.get(), SIGNAL(Updated()), this, SLOT(Refresh()));

	connect(m_protocol, SIGNAL(DeleteExploration(const std::string&)),
			this, SLOT(DeleteExploration(const std::string&)));
	connect(m_protocol, SIGNAL(ExplorationNamesRequested()),
			this, SLOT(SendExplorationNames()));
	connect(m_protocol, SIGNAL(Subscribe(const std::string&)),
			this, SLOT(Subscribe(const std::string&)));
	connect(m_protocol, SIGNAL(Unsubscribe(const std::string&)),
			this, SLOT(Unsubscribe(const std::string&)));
	connect(m_protocol, SIGNAL(StopTask(const std::string&, int)),
			m_exploration_manager.get(), SLOT(StopTask(const std::string&, int)));
	connect(m_protocol, SIGNAL(RestartTask(const std::string&, int)),
			m_exploration_manager.get(), SLOT(RestartTask(const std::string&, int)));
}

void ClientHandler::DisplayError(const std::string& error) const
{
	cerr << "ClientHandler Error: " << error << endl;
}

void ClientHandler::DeleteExploration(const std::string& name)
{
	Unsubscribe(name);
	m_exploration_manager->DeleteExploration(name);
}

void ClientHandler::Refresh()
{
	if (m_subscribed_exploration != "") {
		const ExplorationProgress *progress
			= m_exploration_manager->GetExplorationProgress(m_subscribed_exploration);
		if (progress) {
			 m_protocol->SendStatus(*progress);
		}
	} else {
		m_protocol->SendStatusDeleted("");
	}
}

void ClientHandler::RegisterExploration(const Exploration* exploration)
{
	cout << "ClientHandler::RegisterExploration" << endl;
	m_exploration_manager->RegisterExploration(exploration);
	Subscribe(exploration->GetName());
}

void ClientHandler::SendExplorationNames()
{
	m_protocol->SendExplorationNames(m_exploration_manager->GetExplorationNames());
}

void ClientHandler::Subscribe(const std::string& name)
{
	m_subscribed_exploration = name;
	Refresh();
}

void ClientHandler::Unsubscribe(const std::string& name)
{
	if (m_subscribed_exploration == name){
		m_subscribed_exploration = "";
	}
	Refresh();
}

} // namespace
