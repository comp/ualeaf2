#ifndef SIMPT_PAREX_WORKER_REPRESENTATIVE_H_INCLUDED
#define SIMPT_PAREX_WORKER_REPRESENTATIVE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkerRepresentative
 */

#include "parex_protocol/ServerNodeProtocol.h"
#include "parex_protocol/SimResult.h"
#include "parex_protocol/SimTask.h"

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <iostream>

namespace SimPT_Parex {

/**
 * A worker taken as representative for multiple workers (handles the communication with the node).
 */
class WorkerRepresentative: public QObject
{
	Q_OBJECT
public:
	/// Constructor for a worker without work.
	WorkerRepresentative(const QHostAddress &addr, quint16 port);

	/// Constructor for a reconnecting worker with finished work.
	WorkerRepresentative(const QHostAddress &addr, quint16 port, int taskId, std::string taskName);

	/// Destructor
	virtual ~WorkerRepresentative();

	/// Gets the address of the worker.
	const QHostAddress* GetSenderAddress() const;

	/// Gets the port of the worker.
	int GetSenderPort() const;

	/// Gets the name current tasks' exploration.
	std::string GetExplName() const;

	/// Gets the id of the currently executing task.
	int GetTaskId() const;

	/// Stop the current task
	void StopTask();

	/// Delete an exploration
	void Delete(const std::string name);

signals:
	/// Emitted when the worker is ready to do a new job.
	void ReadyToWork();

	/// Emitted when the worker has finished his job and has the result ready.
	void FinishedWork(const SimResult&);

	/// Emitted when the worker has disconnected.
	void Disconnected();

public slots:
	/// Tries to connect to the worker and sets up the signals.
	void Setup();

	/// Pass the job to the worker.
	void DoWork(const SimTask&);

private slots:
	void Connected();
	void Finished(const SimResult&);
	void DisplayError(QAbstractSocket::SocketError error) const;
	void DisplayError(const std::string &s) const;

private:
	void Init();

private:
	QHostAddress*		m_addr;
	quint16 		m_port;
	QTcpSocket* 		m_socket;
	ServerNodeProtocol* 	m_protocol;
	int			m_currentTaskId;
	std::string		m_currentTaskName;
};

} // namespace

#endif // end-of-include-guard
