/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SendPage.
 */

#include "SendPage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QDoubleValidator>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QRegExp>
#include <QRegExpValidator>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <locale>
#include <memory>
#include <sstream>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

SendPage::SendPage(const std::shared_ptr<Exploration> &exploration)
	: m_exploration(exploration)
{
	setTitle("Send exploration");
	setSubTitle("Give a name for the exploration.");

	QVBoxLayout* layout = new QVBoxLayout;

	m_name = new QLineEdit;
	QRegExpValidator* regex = new QRegExpValidator(QRegExp("[A-Za-z\\d][A-Za-z\\d\\s]*"));
	m_name->setValidator(regex);
	layout->addWidget(m_name);

	setLayout(layout);

	setFinalPage(true);
	setButtonText(QWizard::FinishButton, "Send");

	connect(m_name, SIGNAL(textChanged(const QString&)), this, SIGNAL(completeChanged()));
}


void SendPage::initializePage()
{
	m_name->setText(QString::fromStdString(m_exploration->GetName()));
	m_name->selectAll();
}


bool SendPage::isComplete() const
{
	return m_name->text() != "";
}


bool SendPage::validatePage()
{
	m_exploration->SetName(m_name->text().toStdString());

	return true;
}

} // namespace
