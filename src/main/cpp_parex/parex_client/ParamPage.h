#ifndef SIMPT_PAREX_PARAM_PAGE_H_INCLUDED
#define SIMPT_PAREX_PARAM_PAGE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ParamPage.
 */

#include <QWizard>
#include <QWizardPage>
#include <boost/property_tree/ptree.hpp>
#include <map>

class QComboBox;
class QLineEdit;
class QListWidget;
class QRadioButton;

namespace SimPT_Parex {

class Exploration;

/**
 * Set the parameters for the exploration
 */
class ParamPage : public QWizardPage
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	exploration	A reference to the variable that contains the pointer of the exploration the user chose
	 */
	ParamPage(const std::shared_ptr<Exploration> &exploration);

	/**
	 * Destructor
	 */
	virtual ~ParamPage() {}


private slots:
	/**
	 * Change view to new checked radio button
	 */
	void UpdateOriginal(bool checked);
	void UpdateList(bool checked);
	void UpdateLoop(bool checked);

	/**
	 * Called when new parameter is selected to update the view
	 */
	void SelectParameter(int index);

private:
	enum { Page_Start, Page_Path, Page_Param, Page_Files, Page_Send, Page_Template_Path };

private:
	virtual void initializePage();
	virtual bool validatePage();

	virtual int nextId() const { return Page_Send; }

	/**
	 * Save the current displayed parameter
	 */
	bool SaveParameter();

	const std::shared_ptr<Exploration> &m_exploration;

	QRadioButton *m_original_select;
	QRadioButton *m_list_select;
	QRadioButton *m_loop_select;
	QLineEdit *m_original;
	QLineEdit *m_from;
	QLineEdit *m_to;
	QLineEdit *m_step;
	QLineEdit *m_list;
	QComboBox *m_parameters;
	int m_current_index;
};

} // namespace

#endif // end_of_include_guard
