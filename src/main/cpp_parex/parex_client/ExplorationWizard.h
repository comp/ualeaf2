#ifndef SIMPT_PAREX_EXPLORATION_WIZARD_H_INCLUDED
#define SIMPT_PAREX_EXPLORATION_WIZARD_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExplorationWizard.
 */

#include <QWizard>
#include <QWizardPage>

#include <boost/property_tree/ptree.hpp>

#include <map>

class QComboBox;
class QLineEdit;
class QListWidget;
class QRadioButton;

namespace SimPT_Parex {

class Exploration;

/**
 * Wizard to set up an exploration
 *
 * Contains private classes that represent all the pages of the wizard
 */
class ExplorationWizard : public QWizard
{
public:
	/**
	 * Constructor
	 *
	 * @param	preferences		The (workspace) preferences needed to create a new Exploration
	 * @param	lastExploration		The last recorded exploration that was sent (nullptr if last was successful)
	 * @param	parent			The widget parent of the wizard
	 */
	ExplorationWizard(const boost::property_tree::ptree &preferences, const std::shared_ptr<const Exploration> &lastExploration, QWidget *parent = nullptr);

	/// Destructor
	virtual ~ExplorationWizard() {}


	/// Returns the Exploration generated from the wizard
	std::shared_ptr<const Exploration> GetExploration();

private:
	enum { Page_Start, Page_Path, Page_Param, Page_Files, Page_Send, Page_Template_Path };

	std::shared_ptr<Exploration>      m_exploration;
	boost::property_tree::ptree       m_preferences;
};

} // namespace

#endif // end_of_include_guard
