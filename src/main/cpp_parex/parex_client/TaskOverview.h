#ifndef SIMPT_PAREX_TASK_OVERVIEW_H_INCLUDED
#define SIMPT_PAREX_TASK_OVERVIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TaskOverview
 */

#include "parex_protocol/ExplorationProgress.h"

#include <QWidget>
#include <map>
#include <memory>

class QStandardItemModel;
class QTimer;
class QTableView;

namespace SimPT_Parex {

/**
 * Overview of tasks
 */
class TaskOverview: public QWidget
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param exploration		The current exploration
	 */
	TaskOverview();

	/**
	 * Destructor
	 */
	virtual ~TaskOverview();

	/**
	 * Update the current exploration
	 *
	 * @param	explorationProgress	The subscribed exploration
	 */
	void UpdateExploration(const std::shared_ptr<const ExplorationProgress> &explorationProgress);

signals:
	/**
	 * Send message to stop a certain task
	 *
	 * @param task		Id of task
	 */
	void StopTask(int task);

	/**
	 * Send message to restart a stopped task
	 *
	 * @param task		Id of task
	 */
	void StartTask(int task);

protected:
	/**
	 * Overriding QWidget::showEvent to start the refresh timer
	 */
	virtual void showEvent(QShowEvent *event) override;

	/**
	 * Overriding QWidget::hideEvent to stop the refresh timer
	 */
	virtual void hideEvent(QHideEvent *event) override;

private slots:
	void Refresh();

private:

	std::shared_ptr<const ExplorationProgress> m_exploration_progress;

	QTableView *m_table;
	QStandardItemModel *m_model;
	QTimer *m_refresh_timer;
};

} // namespace

#endif // end_of_include_guard
