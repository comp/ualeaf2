/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PathPage
 */

#include "PathPage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <cctype>
#include <iostream>
#include <memory>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

PathPage::PathPage(std::shared_ptr<Exploration>& exploration, boost::property_tree::ptree& preferences)
	: m_exploration(exploration), m_preferences(preferences)
{
	// Set title + text
	setTitle("Select sim data file");
	setSubTitle("Please select a sim data file to use as basis for the exploration.");

	// Set main layout
	QHBoxLayout* layout = new QHBoxLayout;

	QLabel* label = new QLabel("Path");
	m_file_path = new QLineEdit;
	m_file_path->setReadOnly(true);
	QPushButton* buttonBrowse = new QPushButton("Browse...");

	layout->addWidget(label);
	layout->addWidget(m_file_path);
	layout->addWidget(buttonBrowse);

	setLayout(layout);

	connect(buttonBrowse, SIGNAL(clicked()), this, SLOT(BrowseFile()));
}

void PathPage::BrowseFile()
{
	QSettings settings;

	QString path;
	if (settings.contains("last_leaf_file"))
		path = settings.value("last_leaf_file").toString();
	else
		path = settings.value("workspace").toString();

	QString file = QFileDialog::getOpenFileName(this, "Browse", path);

	m_ptree.clear();

	if (!QFile(file).exists())
		return;

	try {
		read_xml(file.toStdString(), m_ptree, trim_whitespace);
	} catch (ptree_bad_path& e) {
		QMessageBox::critical(this, "Read Error", "Error in data input file", QMessageBox::Ok);
		return;
	} catch (ptree_bad_data& e) {
		QMessageBox::critical(this, "Read Error", "Error in data input file.", QMessageBox::Ok);
		return;
	} catch (xml_parser_error& e) {
		QMessageBox::critical(this, "Error", "Not a valid data input file.", QMessageBox::Ok);
		return;
	} catch (std::exception& e) {
		QMessageBox::critical(this, "Error", "Could not read data input file.", QMessageBox::Ok);
		return;
	} catch (...) {
		QMessageBox::critical(this, "Error", "Unknown Exception", QMessageBox::Ok);
		return;
	}

	m_file_path->setText(file);
	settings.setValue("last_leaf_file", file);

	emit completeChanged();
}

bool PathPage::isComplete() const
{
	return !m_ptree.empty();
}

bool PathPage::validatePage()
{
	m_exploration = std::make_shared<ParameterExploration>("Untitled", m_ptree, m_preferences);
	return true;
}

} // namespace

