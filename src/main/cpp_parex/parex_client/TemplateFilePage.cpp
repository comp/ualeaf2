/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for TemplateFilePage.
 */

#include "TemplateFilePage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QDoubleValidator>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QRegExp>
#include <QRegExpValidator>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <algorithm>
#include <cctype>
#include <iostream>
#include <fstream>
#include <functional>
#include <locale>
#include <memory>
#include <sstream>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

TemplateFilePage::TemplateFilePage(std::shared_ptr<Exploration> &exploration, boost::property_tree::ptree &preferences)
	: m_exploration(exploration), m_preferences(preferences)
{
	// Set title + text
	setTitle("Select template sim data file");
	setSubTitle("Select a sim data file template to use as basis for the exploration. Parameters should be entered as $param_name$. The params file should be a CSV file with the param_names as headers.");

	// Set main layout
	QVBoxLayout* mainlayout = new QVBoxLayout;
	QHBoxLayout* layout = new QHBoxLayout;
	QLabel* label = new QLabel("Template");
	m_file_path = new QLineEdit;
	m_file_path->setReadOnly(true);
	QPushButton* buttonBrowse = new QPushButton("Browse...");

	layout->addWidget(label);
	layout->addWidget(m_file_path);
	layout->addWidget(buttonBrowse);
	mainlayout->addLayout(layout);

	layout = new QHBoxLayout;

	QLabel* labelparams = new QLabel("Params");
	m_params_path = new QLineEdit;
	m_params_path->setReadOnly(true);
	QPushButton* buttonBrowseParams = new QPushButton("Browse...");

	layout->addWidget(labelparams);
	layout->addWidget(m_params_path);
	layout->addWidget(buttonBrowseParams);

	mainlayout->addLayout(layout);

	setLayout(mainlayout);

	connect(buttonBrowse, SIGNAL(clicked()), this, SLOT(BrowseLeafFile()));
	connect(buttonBrowseParams, SIGNAL(clicked()), this, SLOT(BrowseParamsFile()));
}

void TemplateFilePage::BrowseLeafFile()
{
	QSettings settings;

	QString path;
	if (settings.contains("last_template_file"))
		path = settings.value("last_template_file").toString();
	else
		path = settings.value("workspace").toString();

	QString file = QFileDialog::getOpenFileName(this, "Browse", path);

	m_ptree.clear();

	if (!QFile(file).exists())
		return;

	try {
		read_xml(file.toStdString(), m_ptree, trim_whitespace);
	} catch (ptree_bad_path& e) {
		QMessageBox::critical(this, "Read Error", "Error in file", QMessageBox::Ok);
		return;
	} catch (ptree_bad_data& e) {
		QMessageBox::critical(this, "Read Error", "Error in file.", QMessageBox::Ok);
		return;
	} catch (xml_parser_error& e) {
		QMessageBox::critical(this, "Error", "Not a valid file.", QMessageBox::Ok);
		return;
	} catch (std::exception& e) {
		QMessageBox::critical(this, "Error", "Could not read file.", QMessageBox::Ok);
		return;
	} catch (...) {
		QMessageBox::critical(this, "Error", "Unknown Exception", QMessageBox::Ok);
		return;
	}

	m_file_path->setText(file);
	settings.setValue("last_template_file", file);

	emit completeChanged();
}

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

std::vector<std::string> splitCSVLine(std::string &line){
	std::vector<std::string> v;
	std::stringstream ls(line);
	std::string value;

	while(std::getline(ls, value, ',')) {
		v.push_back(trim(value));
	}
	return v;
}

void TemplateFilePage::BrowseParamsFile()
{
	QSettings settings;
	QString path;
	if (settings.contains("last_params_file"))
		path = settings.value("last_params_file").toString();
	else
		path = settings.value("workspace").toString();

	QString file = QFileDialog::getOpenFileName(this, "Browse", path);

	if (!QFile(file).exists())
		return;

	try {

		std::ifstream fs(file.toStdString());
		std::string line;
		if (!std::getline(fs, line)) {
			QMessageBox::critical(this, "Error", "First line of params file is invalid.", QMessageBox::Ok);
			return;
		}
		m_params.clear();
		m_param_names=splitCSVLine(line);


		if (m_param_names.empty()) {
			QMessageBox::critical(this, "Error", "First line of params file is invalid.", QMessageBox::Ok);
			return;
		}
		int linecount=0;

		while (std::getline(fs, line)) {
			std::vector<std::string> values=splitCSVLine(line);
			linecount++;
			if (values.empty())
				continue;
			if (values.size()!= m_param_names.size()) {
				QMessageBox::critical(this, "Error", QString::fromStdString("Params file contains invalid input, line "+std::to_string(linecount)+"."), QMessageBox::Ok);
				return;
			}
			m_params.push_back(values);
		}


		fs.close();

	} catch (std::exception& e) {
		QMessageBox::critical(this, "Error", "Could not read file.", QMessageBox::Ok);
		return;
	} catch (...) {
		QMessageBox::critical(this, "Error", "Unknown Exception", QMessageBox::Ok);
		return;
	}

	m_params_path->setText(file);
	settings.setValue("last_params_file", file);

	emit completeChanged();
}

bool TemplateFilePage::isComplete() const
{
	return !m_ptree.empty();
}

bool TemplateFilePage::validatePage()
{
	std::vector<std::pair<std::string, ptree>> files;
	std::map<std::string, int> param_map;
	for(size_t i=0; i<m_param_names.size(); ++i ) {
		param_map[m_param_names[i]]=i;
	}

	int count=0;
	for (auto param: m_params) {
		ptree pt=m_ptree;

		std::function<void(ptree&)> iterate_over=[&iterate_over, &param, &param_map](ptree &p) ->  void {
			for (auto& child: p) {
				if (child.second.empty()){
					std::string value=trim(child.second.data());

					if (value.length()>1 && value[0]=='$' && value[value.length()-1]=='$') {
						//This is a template value ($paramname$)
						value=value.substr(1, value.length()-2);
						auto found=param_map.find(value);

						if (found!=param_map.end()){
							child.second.put_value(param[found->second]);
						}
					}
				} else {
					iterate_over(child.second);
				}
			}
		};

		iterate_over(pt);
		count++;

		files.push_back(std::make_pair("Param_"+std::to_string(count), pt));
	}

	m_exploration = std::make_shared<FileExploration>("Untitled", files, m_preferences);
	return true;
}

} // namespace
