#ifndef SIMPT_PAREX_CLIENT_H_INCLUDED
#define SIMPT_PAREX_CLIENT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Client
 */

#include "Status.h"
#include "parex_protocol/ClientProtocol.h"

#include <QAction>
#include <QMainWindow>
#include <QString>
#include <QPushButton>
#include <QSettings>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>

namespace SimPT_Parex {

class ClientProtocol;
class Exploration;
class ExplorationPtreeView;
class ExplorationSelection;
class TaskOverview;


/**
 * Client for Parameter Exploration
 */
class Client : public QMainWindow
{
	Q_OBJECT
public:
	/// Constructor
	Client();

	/// Destructor
	virtual ~Client();

private slots:

	/// Connect to server
	void Connect();

	/// Disconnect from server
	void Disconnect();

	/// Subscribe for updates for Exploration
	void Subscribe();

	/// Start new exploration wizard and send exploration
	void StartExploration();

	/// Delete a running exploration
	void DeleteExploration();

	/// Download a exploration
	void DownloadExploration();

	/// Stop a task
	void StopTask(int);

	/// Restart a task
	void RestartTask(int);

	/// Start workspace wizard to set up workspace
	void SetWorkspace();

	/// Display the task overview window
	void ShowTaskOverview();

	/// Call when connection is established
	void Connected(std::string name);

	/// Called when exploration names arrive after a connection is established
	void ConnectExplorationNames(const std::vector<std::string> &names);

	/// Called when a status update arrives at the client
	void UpdateStatus(const ExplorationProgress *status);

	/// Called when a message arrives that the current exploration has been deleted
	void ExplorationDeleted();

	/// Call when connection disconnected
	void Disconnected();

private:
	ClientProtocol* 			m_client;
	std::string 				m_workspace_path;
	std::shared_ptr<const Exploration>	m_last_exploration;
	std::string				m_subscribed_exploration;

	Status*				m_status;
	QStatusBar* 			m_statusbar;
	QPushButton*			m_connection_button;

	TaskOverview*			m_task_view;

	QAction*	action_connect;
	QAction*	action_subscribe;
	QAction*	action_start_exploration;
	QAction*	action_delete_exploration;
	QAction*	action_download_exploration;
	QAction*	action_workspace_wizard;
	QAction*	action_edit_workspace;
	QAction*	action_task_overview;

	QSettings	m_settings;

	/// Init the workspace to use
	void InitWorkspace();
};

} // namespace

#endif // end-of-include-guard
