#ifndef SIMPT_PAREX_TEMPLATE_FILE_PAGE_H_INCLUDED
#define SIMPT_PAREX_TEMPLATE_FILE_PAGE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TemplateFilePage.
 */

#include <QWizard>
#include <QWizardPage>
#include <boost/property_tree/ptree.hpp>
#include <map>

class QComboBox;
class QLineEdit;
class QListWidget;
class QRadioButton;

namespace SimPT_Parex {

class Exploration;

/**
 * Select the input template data file
 */
class TemplateFilePage : public QWizardPage
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	exploration	Reference to variable with pointer of the exploration the user chose.
	 * @param	preferences	The preferences of the new exploration.
	 */
	TemplateFilePage(std::shared_ptr<Exploration>& exploration, boost::property_tree::ptree& preferences);

	/**
	 * Destructor
	 */
	virtual ~TemplateFilePage() {}


private slots:
	/**
	 * Display browse dialog for selecting data input file
	 */
	void BrowseLeafFile();


	/**
	 * Display browse dialog for params file
	 */
	void BrowseParamsFile();

private:
	enum { Page_Start, Page_Path, Page_Param, Page_Files, Page_Send, Page_Template_Path };

private:
	virtual bool isComplete() const;
	virtual bool validatePage();
	virtual int nextId() const { return Page_Send; }

private:
	std::shared_ptr<Exploration>&             m_exploration;
	boost::property_tree::ptree&              m_preferences;
	boost::property_tree::ptree               m_ptree;
	std::vector<std::string>                  m_param_names;
	std::vector< std::vector<std::string>>    m_params;
	QLineEdit*                                m_file_path;
	QLineEdit*                                m_params_path;
};

} // namespace

#endif // end_of_include_guard
