#ifndef SIMPT_PAREX_FILES_PAGE_H_INCLUDED
#define SIMPT_PAREX_FILES_PAGE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for FilesPage.
 */

#include <QWizard>
#include <QWizardPage>
#include <boost/property_tree/ptree.hpp>
#include <map>

class QComboBox;
class QLineEdit;
class QListWidget;
class QRadioButton;

namespace SimPT_Parex {

class Exploration;

/**
 * Page to allow file selection.
 */
class FilesPage: public QWizardPage
{
	Q_OBJECT
public:
	/// Constructor
	FilesPage(std::shared_ptr<Exploration>& exploration, boost::property_tree::ptree& preferences);

	/// Destructor
	virtual ~FilesPage() {}

private slots:
	void UpdateRemoveDisabled();
	void AddFiles();
	void RemoveFiles();

private:
	enum { Page_Start, Page_Path, Page_Param, Page_Files, Page_Send, Page_Template_Path };

private:
	virtual bool isComplete() const;
	virtual bool validatePage();
	virtual int nextId() const { return Page_Send; }

private:
	std::shared_ptr<Exploration>&                    m_exploration;
	boost::property_tree::ptree&                     m_preferences;
	std::map<QString, boost::property_tree::ptree>   m_files;
	QListWidget*                                     m_files_widget;
	QPushButton*                                     m_remove_button;
};

} // namespace

#endif // end_of_include_guard
