/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Task Overview
 */

#include "TaskOverview.h"

#include "parex_protocol/Exploration.h"
#include "util/misc/StringUtils.h"

#include <QDateTime>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSignalMapper>
#include <QStandardItemModel>
#include <QTableView>
#include <QTimer>
#include <QVBoxLayout>

using namespace std;
using SimPT_Sim::Util::ToString;

namespace SimPT_Parex {
namespace {

QString RunningTimeToString(unsigned int time)
{
	int seconds = time % 60;
	time /= 60;
	int minutes = time % 60;
	time /= 60;
	int hours = time % 24;
	int days = time / 24;

	return QString::fromStdString(
		ToString(days, 2, '0') + ":" +
		ToString(hours, 2, '0') + ":" +
		ToString(minutes, 2, '0') + ":" +
		ToString(seconds, 2, '0'));
}

QString StateToString(const ExplorationTask &task)
{
	switch(task.GetState()) {
	case TaskState::Waiting:
		if (task.GetNumberOfTries() == 0)
			return "Waiting";
		else
			return "Retrying ("
				+ QString::fromStdString(to_string(task.GetNumberOfTries())) + ")";
	case TaskState::Running:
		return "Running";
	case TaskState::Finished:
		return "Finished";
	case TaskState::Cancelled:
		return "Cancelled";
	case TaskState::Failed:
		return "Failed";
	default:
		assert(false && "This task is in an unknown state.");
		return "Unknown";
	}
}

}

TaskOverview::TaskOverview()
	: m_exploration_progress(nullptr), m_table(nullptr), m_model(nullptr), m_refresh_timer(new QTimer(this))
{
	setMinimumHeight(300);
	setMinimumWidth(600);

	QVBoxLayout* layout = new QVBoxLayout;

	m_table = new QTableView(this);
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_model = new QStandardItemModel(m_table);

	QPushButton* close_button = new QPushButton("Close");
	connect(close_button, SIGNAL(clicked()), this, SLOT(close()));

	layout->addWidget(m_table);
	layout->addWidget(close_button);

	setLayout(layout);

	UpdateExploration(m_exploration_progress);

	connect(m_refresh_timer, SIGNAL(timeout()), this, SLOT(Refresh()));
	m_refresh_timer->setInterval(1000);
}

TaskOverview::~TaskOverview()
{
	m_refresh_timer->stop();
	delete m_table;
}

void TaskOverview::UpdateExploration(const std::shared_ptr<const ExplorationProgress>& explorationProgress)
{
	QStringList header;
	header << "Id" << "Status"<< "Action" << "Time" << "Node";
	m_model->clear();

	m_exploration_progress = explorationProgress;
	if (m_exploration_progress) {
		const Exploration& exploration = m_exploration_progress->GetExploration();

		for (const auto& param : exploration.GetParameters()) {
			header << QString::fromStdString(param).split('.').last();
		}

		m_model->setHorizontalHeaderLabels(header);

		QSignalMapper* stop_signal = new QSignalMapper(this);
		QSignalMapper* restart_signal = new QSignalMapper(this);
		connect(stop_signal, SIGNAL(mapped(int)), this, SIGNAL(StopTask(int)));
		connect(restart_signal, SIGNAL(mapped(int)), this, SIGNAL(StartTask(int)));

		for (unsigned int i = 0; i < exploration.GetNumberOfTasks(); i++) {
			auto task = m_exploration_progress->GetTask(i);
			auto state = task.GetState();
			int j = 0;
			m_model->setItem(i, j++, new QStandardItem(QString::number(i)));
			m_model->setItem(i, j++, new QStandardItem(StateToString(task)));
			m_model->setItem(i, j++, 0);
			m_model->setItem(i, j++, new QStandardItem(RunningTimeToString(task.GetRunningTime())));

			if (state == TaskState::Running || state == TaskState::Waiting) {
				QPushButton* button = new QPushButton("Stop");
				connect(button, SIGNAL(clicked()), stop_signal, SLOT(map()));
				stop_signal->setMapping(button, i);
				m_table->setIndexWidget(m_model->index(i,2), button);
			} else if (state == TaskState::Cancelled) {
				QPushButton* button = new QPushButton("Restart");
				connect(button, SIGNAL(clicked()), restart_signal, SLOT(map()));
				restart_signal->setMapping(button, i);
				m_table->setIndexWidget(m_model->index(i,2), button);
			}
			m_model->setItem(i, j++, new QStandardItem(QString::fromStdString(task.getNodeIp())+":"+QString::number(task.getNodePort())));
			
			for (const std::string &value : exploration.GetValues(i)) {
				m_model->setItem(i, j++, new QStandardItem(QString::fromStdString(value)));
			}
		}
	}

	m_model->setHorizontalHeaderLabels(header);
	m_table->setModel(m_model);
	m_table->resizeColumnsToContents();
	m_table->horizontalHeader()->setStretchLastSection(true);
}

void TaskOverview::showEvent(QShowEvent* event)
{
	m_refresh_timer->start();
	QWidget::showEvent(event);
}

void TaskOverview::hideEvent(QHideEvent* event)
{
	m_refresh_timer->stop();
	QWidget::hideEvent(event);
}

void TaskOverview::Refresh()
{
	if (m_exploration_progress) {
		auto num = m_exploration_progress->GetExploration().GetNumberOfTasks();
		for (unsigned int i = 0; i < num; ++i) {
			m_model->setItem(i, 3, new QStandardItem(
				RunningTimeToString(m_exploration_progress->GetTask(i).GetRunningTime())));
		}
	}
}

} // namespace
