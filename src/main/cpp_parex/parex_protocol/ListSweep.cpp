/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ListSweep.
 */

#include "ListSweep.h"

#include <cmath>

using boost::property_tree::ptree;
using namespace std;

namespace SimPT_Parex {

ListSweep::ListSweep(const vector<string>& values) : m_values(values) {}

ListSweep::ListSweep(const boost::property_tree::ptree& pt)
{
	ReadPtree(pt);
}

string ListSweep::GetValue(unsigned int index) const
{
	assert(index < m_values.size() && "The index doesn't exist.");
	return m_values[index];
}

unsigned int ListSweep::GetNumberOfValues() const
{
	return m_values.size();
}

ptree ListSweep::ToPtree() const
{
	ptree pt;
	for (const string &value : m_values) {
		pt.add("list.value", value);
	}
	return pt;
}

void ListSweep::ReadPtree(const ptree& pt)
{
	m_values.clear();
	for (const auto& entry : pt.get_child("list")) {
		m_values.push_back(entry.second.data());
	}
}

} // namespace
