#ifndef SIMPT_PAREX_EXPLORATION_TASK_H_
#define SIMPT_PAREX_EXPLORATION_TASK_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExplorationTask.
 */

#include <boost/property_tree/ptree.hpp>
#include <QDateTime>

namespace SimPT_Parex {

/**
 * Enumeration describing the state of a single task of the exploration
 */
enum class TaskState
{
	Waiting,	/// The task is waiting to be sent for the first time
	Running,	/// The task is being executed
	Finished,	/// The task was finished
	Cancelled,	/// The task was cancelled or stopped
	Failed		/// The task was resent and failed several times
};

/**
 * Contains information about task in an exploration.
 */
class ExplorationTask
{
public:
	/**
	 * Constructs a task in 'Waiting' state.
	 */
	ExplorationTask();

	/**
	 * Constructor.
	 * Constructs an exploration task with the given pt.
	 * See ToPtree() for more information about the format of such a tree.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	ExplorationTask(const boost::property_tree::ptree& pt);

	/**
	 * Destructor.
	 */
	virtual ~ExplorationTask();

	/**
	 * Change the state of the task.
	 * When the new state is 'Running', the timer will start tracking.
	 * When the new state is 'Finished', the timer will be frozen and the state can't be changed anymore.
	 * @param	state		The new state of the task.
	 */
	void ChangeState(const TaskState& state);

	/**
	 * Gets the state of the task.
	 * @return	The state of this task.
	 */
	TaskState GetState() const { return m_state; }

	/**
	 * Returns the number of tries.
	 */
	unsigned int GetNumberOfTries() const { return m_tries; }

	/**
	 * Returns the time the task has run.
	 * This includes the time from the first 'Running' state of the task till the task has finished.
	 * @return	The time in seconds.
	 */
	unsigned int GetRunningTime() const;

	/**
	 * Increment the tries of running the task.
	 */
	void IncrementTries();

	/**
	 * Convert the task to a ptree. The format of such a ptree is:
	 * 	<state>m_state</state>
	 * 	<start_time>m_start_time</start_time>
	 * 	<running_time>m_running_time</running_time>
	 * 	<tries>m_tries</tries>
	 */
	boost::property_tree::ptree ToPtree() const;


	void setNodeThatContainsResult(const std::string nodeIP, int nodePort) {
		m_node_ip=nodeIP; m_node_port=nodePort;
	}

	std::string getNodeIp() const {  return m_node_ip;}

	int getNodePort() const { return m_node_port; }

private:
	/**
	 * Convert the given ptree to an exploration task.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	void ReadPtree(const boost::property_tree::ptree& pt);

private:
	TaskState        m_state;
	QDateTime        m_start_time;
	unsigned int     m_running_time;
	unsigned int     m_tries;

	std::string      m_node_ip;
	int              m_node_port;
};

} // namespace

#endif // end-of-include-guard
