#ifndef SIMPT_PAREX_EXPLORATION_PROGRESS_H_
#define SIMPT_PAREX_EXPLORATION_PROGRESS_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExplorationProgress
 */

#include "ExplorationTask.h"
#include "SimResult.h"

#include <QObject>
#include <map>
#include <vector>

namespace SimPT_Parex {

class Exploration;
class SimTask;

/**
 * Class describing the progress state of an exploration.
 * The tasks have ID's from 0 to the number of tasks minus 1.
 *
 * The state diagram of a task looks like this:
 *
 * 	======================================================
 *
 * 	Waiting -------1-----> Running -------6-----> Finished
 * 	  | ^   <------2------    |    -------7-----> Failed
 * 	  4 5                     |
 * 	  v |                     |
 * 	Cancelled <---------------3
 *
 *	======================================================
 *
 * 	1. NextTask
 * 	2. GiveBack (didn't sent the exploration) & HandleResult (SimResult::ResultType::Failure)
 * 	3. HandleResult (SimResult::ResultType::Stopped)
 * 	4. CancelWaitingTask
 * 	5. ResendCancelledTask
 * 	6. HandleResult (SimResult::ResultType::Success)
 * 	7. HandleResult (SimResult::ResultType::Failure)
 */
class ExplorationProgress : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 * @param	exploration	The exploration to track (this object takes ownership).
	 */
	ExplorationProgress(const Exploration* exploration);

	/**
	 * Construct an exploration progress with the given pt.
	 * See ToPtree() for more information about the format of such a tree.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	ExplorationProgress(const boost::property_tree::ptree& pt);

	/**
	 * Destructor
	 */
	virtual ~ExplorationProgress();

	/**
	 * Cancels a waiting task.
	 * @param	id		The id of the task to cancel.
	 */
	void CancelWaitingTask(unsigned int id);

	/**
	 * Returns the exploration of this object.
	 */
	const Exploration& GetExploration() const { return *m_exploration; }

	/**
	 * Returns the running tasks.
	 */
	const std::list<unsigned int>& GetRunningTasks() const { return m_running_tasks; }

	/**
	 * Returns the state of the task with the specified id.
	 * @param	id	The id of which to get the task.
	 * @return	The state and information of that task
	 */
	const ExplorationTask& GetTask(unsigned int id) const;

	/**
	 * Returns the number of tasks with the specified state
	 * @param	state		The state of which to get the number of tasks.
	 * @return	The number of tasks with that state.
	 */
	unsigned int GetTaskCount(const TaskState& state) const;

	/**
	 * Give a task back to exploration (when it could not have been sent).
	 * @param	task		The given task. The ownership is transferred
	 *                              to this object, which will delete it.
	 */
	void GiveBack(const SimTask* task);

	/**
	 * Return true if the exploration has finished.
	 * @return	Whether the exploration has finished.
	 */
	bool IsFinished() const;

	/**
	 * Return the next task ready to be simulated.
	 * @return	The next task or nullptr if there's no next task
	 *              available. The ownership is transferred to the caller.
	 */
	SimTask* NextTask();

	/**
	 * Resends a cancelled task.
	 * @param	id		The id of the task to re-send.
	 */
	void ResendCancelledTask(unsigned int id);

	/**
	 * Convert the exploration and progress to a ptree. The format of such a ptree is:
	 * 	<exploration>
 	 * 		m_exploration [see Exploration.h for more information about its format]
	 * 	</exploration>
	 * 	<tasks> (for each task in m_tasks)
	 * 		<task>
	 * 			task [see ExplorationTask.h for more information about their format]
	 * 		</task>
	 * 		[...]
	 *	</tasks>
	 * 	<send_queue> (for every id in m_send_queue, the order will be kept)
	 *		<task_id>id</task_id>
	 *		[...]
	 *	</send_queue>
	 */
	boost::property_tree::ptree ToPtree() const;

public slots:
	/**
	 * Handles the result of a simulation task.
	 * @param	result		The result of the task (this object takes ownership).
	 */
	void HandleResult(const SimResult &result);

signals:
	/**
	 * Signal called when Exploration has changed.
	 */
	void Updated();

private:
	/**
	 * Changes the state of a task.
	 * This includes updating the state of the task and updating the task counts.
	 * @param	task		The information about the task.
	 * @param	state		The state in which the task will be.
	 */
	void ChangeState(ExplorationTask& task, TaskState state);

	/**
	 * Convert the given ptree to an exploration progress.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	void ReadPtree(const boost::property_tree::ptree& pt);

private:
	const Exploration*                 m_exploration;
	std::list<unsigned int>            m_send_queue;
	std::vector<ExplorationTask>       m_tasks;         ///< A vector of task states, index represents the task ID.
	std::map<TaskState, unsigned int>  m_task_counts;   ///< The number of tasks for a given task state.
	std::list<unsigned int>            m_running_tasks;

	static const unsigned int          g_max_tries = 3; ///< Number of tries before a task is marked as 'Failed'.
};

} // namespace

#endif // end-of-include-guard
