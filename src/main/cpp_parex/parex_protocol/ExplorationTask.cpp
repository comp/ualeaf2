/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ExplorationTask.
 */

#include "ExplorationTask.h"

#include <cassert>

using boost::property_tree::ptree;

namespace SimPT_Parex {

ExplorationTask::ExplorationTask()
	: m_state(TaskState::Waiting), m_start_time(), m_running_time(0), m_tries(0), m_node_ip("Unknown"), m_node_port(0)
{
}

ExplorationTask::ExplorationTask(const boost::property_tree::ptree& pt)
{
	ReadPtree(pt);
}

ExplorationTask::~ExplorationTask()
{
}

void ExplorationTask::ChangeState(const TaskState& state)
{
	assert(m_state != TaskState::Finished
		&& m_state != TaskState::Failed
		&& "The task has finished or failed, so changing the state wouldn't make sense.");

	if (m_state != state) {
		if (m_state == TaskState::Running) {
			assert(m_start_time.isValid() && "The start time should be valid.");
			int secs = m_start_time.secsTo(QDateTime::currentDateTime());
			assert(secs >= 0 && "The start time can't be later than the current time.");
			m_running_time += secs;
			m_start_time = QDateTime();
		}
		else if (state == TaskState::Running) {
			assert(!m_start_time.isValid()
				&& "The start time shouldn't have been set when the task wasn't running.");
			m_start_time = QDateTime::currentDateTime();
		}

		m_state = state;
	}
}

unsigned int ExplorationTask::GetRunningTime() const
{
	if (m_state == TaskState::Running) {
		assert(m_start_time.isValid() && "The start time should be valid.");
		int secs = m_start_time.secsTo(QDateTime::currentDateTime());
		assert(secs >= 0 && "The start time can't be later than the current time.");
		return m_running_time + secs;
	} else {
		return m_running_time;
	}
}

void ExplorationTask::IncrementTries()
{
	m_tries += 1;
}

void ExplorationTask::ReadPtree(const boost::property_tree::ptree& pt)
{
	m_state = static_cast<TaskState>(pt.get<int>("state"));
	if (pt.find("start_time") != pt.not_found()) {
		m_start_time = QDateTime::fromString(QString::fromStdString(pt.get<std::string>("start_time")));
	} else {
		m_start_time = QDateTime();
	}
	m_running_time = pt.get<unsigned int>("running_time");
	m_tries = pt.get<unsigned int>("tries");

	m_node_ip = pt.get<std::string>("node_ip");
	m_node_port = pt.get<int>("node_port");
}

ptree ExplorationTask::ToPtree() const
{
	ptree pt;
	pt.put("state", static_cast<int>(m_state));
	if (m_start_time.isValid()) {
		pt.put("start_time", m_start_time.toString().toStdString());
	}
	pt.put("running_time", m_running_time);
	pt.put("tries", m_tries);

	pt.put("node_ip", m_node_ip);
	pt.put("node_port", m_node_port);


	return pt;
}

} // namespace
