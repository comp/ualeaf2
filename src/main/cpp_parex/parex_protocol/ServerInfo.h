#ifndef SIMPT_PAREX_SERVER_INFO_H_
#define SIMPT_PAREX_SERVER_INFO_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ServerInfo
 */

#include <QString>

namespace SimPT_Parex {

/**
 * Class for storing server info used on client-side
 */
class ServerInfo
{
public:
	/**
	 * Constructor
	 * @param name		The name for the server
	 * @param address	The address of the server
	 * @param port		The port used by the server
	 */
	ServerInfo(QString name, QString address, int port);

	/**
	 * Constructor
	 * @param name		The name for the server
	 * @param address	The address of the server
	 * @param port		The port used by the server
	 */
	ServerInfo(std::string name, std::string address, int port);

	/**
	 * Destructor
	 */
	virtual ~ServerInfo();

	/**
	 * Update values for server
	 * @param address	The new address
	 * @param port		The new port number
	 */
	void Update(QString address, int port);

	/**
	 * Return the name of the server
	 * @return 	The name of the server
	 */
	QString GetName();

	/**
	 * Return the address of the server
	 * @return 	The address of the server
	 */
	QString GetAddress();

	/**
	 * Return the port of the server
	 * @return 	The port of the server
	 */
	int GetPort();

private:
	QString 	m_name;
	QString		m_address;
	int		m_port;
};

} // namespace

#endif // end-of-include-guard
