/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for RangeSweep.
 */

#include "RangeSweep.h"

#include <cmath>

using boost::property_tree::ptree;
using namespace std;

namespace SimPT_Parex {

RangeSweep::RangeSweep(double from, double to, double step) : m_from(from), m_to(to), m_step(step)
{
	assert(m_from <= m_to && m_step > 0 && "The given parameters weren't valid.");
}

RangeSweep::RangeSweep(const boost::property_tree::ptree& pt) : m_from(0), m_to(0), m_step(0)
{
	ReadPtree(pt);
}

string RangeSweep::GetValue(unsigned int index) const
{
	assert(index <= GetNumberOfValues() && "The index doesn't exist.");

	ostringstream os;
	os << (m_from + index * m_step);
	return os.str();
}

unsigned int RangeSweep::GetNumberOfValues() const
{
	return static_cast<unsigned int>(std::floor((m_to - m_from) / m_step + 1));
}

ptree RangeSweep::ToPtree() const
{
	ptree pt;

	pt.put("range.from", m_from);
	pt.put("range.to", m_to);
	pt.put("range.step", m_step);

	return pt;
}

void RangeSweep::ReadPtree(const ptree& pt)
{
	m_from = pt.get<double>("range.from");
	m_to = pt.get<double>("range.to");
	m_step = pt.get<double>("range.step");
}

} // namespace
