/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of ClientProtocol
 */

#include "ClientProtocol.h"

#include "Exploration.h"
#include "ExplorationProgress.h"
#include "FileExploration.h"
#include "ParameterExploration.h"
#include "util/misc/StringUtils.h"

#include <boost/property_tree/ptree.hpp>

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Parex {

ClientProtocol::ClientProtocol(ServerInfo* server, QObject *parent)
	: Protocol(server, parent)
{
}

bool ClientProtocol::SendExploration(const Exploration *exploration)
{
	if (!IsConnected())
		return false;

	ptree writer;

	if (dynamic_cast<const ParameterExploration*>(exploration) != nullptr) {
		writer.put_child("parameter_exploration", exploration->ToPtree());
	} else if (dynamic_cast<const FileExploration*>(exploration) != nullptr) {
		writer.put_child("file_exploration", exploration->ToPtree());
	} else {
		assert("Exploration type unknown.");
	}

	SendPtree(writer);
	return true;
}

bool ClientProtocol::DeleteExploration(const std::string &name)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Delete", name);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::RequestExplorationNames()
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.RequestExplorations", "Explorations");

	SendPtree(writer);
	return true;
}

bool ClientProtocol::SubscribeUpdates(const std::string &explorationName)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Subscribe", explorationName);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::UnsubscribeUpdates(const std::string &explorationName)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Unsubscribe", explorationName);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::StopTask(const std::string &name, int task)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Stop.name", name);
	writer.put("Control.Stop.id", to_string(task));

	SendPtree(writer);
	return true;
}

bool ClientProtocol::RestartTask(const std::string &name, int task)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Start.name", name);
	writer.put("Control.Start.id", to_string(task));

	SendPtree(writer);
	return true;
}

void ClientProtocol::ReceivePtree(const ptree &reader)
{
	if (reader.find("Control") != reader.not_found()) {
		ptree control_pt = reader.get_child("Control");

		if (control_pt.find("ExplorationNames") != control_pt.not_found()) {
			std::vector<std::string> names;
			int size = control_pt.get<int>("ExplorationNames.size");
			for (int i = 0; i < size; ++i) {
				names.push_back(control_pt.get<std::string>(
					"ExplorationNames.exploration" + to_string(i) + ".name"));
			}

			emit ExplorationNames(names);
		} else if (control_pt.find("Status") != control_pt.not_found()) {
			ptree status_pt = control_pt.get_child("Status");
			if (status_pt.find("deleted") != status_pt.not_found()) {
				emit ExplorationDeleted();
			} else {
				emit ExplorationStatus(
					new ExplorationProgress(status_pt.get_child("exploration_progress")));
			}
		}
	}
}

} // namespace
