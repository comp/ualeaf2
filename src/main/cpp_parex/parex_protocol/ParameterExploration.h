#ifndef SIMPT_PAREX_PARAMETER_EXPLORATION_H_
#define SIMPT_PAREX_PARAMETER_EXPLORATION_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ParameterExploration.
 */

#include "Exploration.h"

#include <utility>

namespace SimPT_Parex {

class ISweep;

/**
 * Class describing a parameter exploration and its simulation tasks.
 */
class ParameterExploration : public Exploration
{
public:
	/**
	 * Constructor.
	 * @param	name		The name of the exploration.
	 * @param	original	The ptree of the base simulation of the exploration.
	 * @param	preferences	The (workspace) preferences of the exploration.
	 */
	ParameterExploration(const std::string& name, const boost::property_tree::ptree& original,
		const boost::property_tree::ptree& preferences);

	/**
	 * Constructor.
	 * Constructs a new exploration with the given pt.
	 * See ToPtree() for more information about the format of an exploration tree.
	 */
	ParameterExploration(const boost::property_tree::ptree& pt);

	/**
	 * Copy constructor.
	 */
	ParameterExploration(const ParameterExploration& other);

	/**
	 * Assignment operator.
	 */
	ParameterExploration& operator=(const ParameterExploration &other);

	/**
	 * Destructor.
	 */
	virtual ~ParameterExploration();

	/**
	 * Clone function.
	 */
	virtual ParameterExploration* Clone() const;

	/**
	 * Returns all the parameters in the exploration.
	 */
	virtual std::vector<std::string> GetParameters() const;

	/**
	 * Return the values of a certain task.
	 * The index of a value associated with a parameter in the
	 * vector is the same as the index of this parameter.
	 * @param	index		The task index.
	 */
	virtual std::vector<std::string> GetValues(unsigned int index) const;

	/**
	 * Returns the number of tasks the exploration currently contains.
	 */
	virtual unsigned int GetNumberOfTasks() const;

	/**
	 * Creates a task with the parameters on the given index.
	 * This index is based on the Cartesian product of the exploration parameters.
	 */
	virtual SimTask* CreateTask(unsigned int index) const;

	/**
	 * Returns all the possible parameters for the exploration.
	 */
	virtual std::vector<std::string> GetPossibleParameters() const;

	/**
	 * Returns the original value of the parameter.
	 * The user must first verify that the parameter is associated
	 * with this sweep type by calling GetSweepType.
	 * @return	The value of the parameter in the original simulation ptree.
	 * @throws	ptree_bad_path		When the parameter doesn't exist.
	 */
	std::string GetOriginalValue(const std::string& parameter) const;

	/**
	 * Returns the sweep associated with the given parameter.
	 * @return	The associated sweep, nullptr if there is no a sweep for this parameter.
	 */
	const ISweep* GetSweep(const std::string& parameter) const;

	/**
	 * Returns all the sweeps.
	 */
	const std::list<std::pair<std::string, ISweep*>>& GetSweeps() const { return m_sweeps; }

	/**
	 * Set a sweep for the given parameter.
	 * The ownership of the sweep will be transferred to the exploration.
	 */
	void SetSweep(const std::string& parameter, ISweep* sweep);

	/**
	 * Removes the sweep (if any) for the given parameter.
	 */
	void RemoveSweep(const std::string& parameter);


	/**
	 * Convert the exploration to a ptree.
	 *
	 * The format of an exploration ptree is as follows:
	 *	<name>m_name</name>
	 *	<preferences>m_preferences</preferences> (== the format of the workspace preferences)
	 *	<original>m_original</original> (== the format of a normal tissue file)
	 *	<sweeps>
	 *		<sweep> (for each sweep in m_sweeps)
	 *			<parameter>parameter</parameter>
	 *			sweep [see Sweep.h for more information about the format]
	 *		</sweep>
	 *		[...]
	 *	</sweeps>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	/**
	 * Convert the given ptree to an exploration.
	 * Note that only the datamembers for this class will be read, not the Exploration-class.
	 * @throws	ptree_bad_path		When the parameter doesn't exist.
	 */
	virtual void ReadPtree(const boost::property_tree::ptree& pt);

private:
	boost::property_tree::ptree                  m_original;
	std::list<std::pair<std::string, ISweep*>>   m_sweeps;
};

} // namespace

#endif // end-of-include-guard
