#ifndef SIMPT_PAREX_NODE_ADVERTISER_H_INCLUDED
#define SIMPT_PAREX_NODE_ADVERTISER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for the NodeAdvertiser.
 */

#include <string>
#include <QObject>

class QTimer;
class QUdpSocket;

namespace SimPT_Parex {

/**
 * Class advertising an available node over the network
 */
class NodeAdvertiser : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 * @param   port      The port to send UDP advertisements to
	 * @param   verbose   Whether to print information to the standard output or not
	 * @param   parent    The QObject parent of this object
	 */
	NodeAdvertiser(int port, bool verbose = true, QObject *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~NodeAdvertiser();


	/**
	 * Start finding a server to do work for
	 * @param   serverPort   The TCP server port to advertise
	 */
	void Start(int serverPort);

	/**
	 * Start finding the server we already did work for.
	 * @param   serverPort    The TCP server port to advertise
	 * @param   exploration   The name of the exploration the node worked on
	 * @param   taskId        The id of the task the node simulated
	 */
	void Start(int serverPort, const std::string &exploration, int taskId);

	/**
	 * Stop finding a server
	 */
	void Stop();


private slots:
	void SendAdvertisement();

private:
	void BackoffTimer();

private:
	int m_port;
	bool m_verbose;

	QTimer *m_resend_timer;
	QUdpSocket *m_socket;

	int m_server_port;
	std::string m_exploration;
	int m_task_id;

private:
	static const int g_initial_interval;  ///< Start size of interval to search for a server (in milliseconds)
	static const int g_maximal_interval;  ///< Maximum interval to search for a server (in milliseconds)
};

} // namespace

#endif // end-of-include-guard
