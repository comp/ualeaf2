/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for main for simulator command line interface.
 */

#include "sim_cli_mode.h"

#include "cli/CliSimulationTask.h"
#include "cli/CliController.h"
#include "cli/CliConverterTask.h"
#include "converter/FileConverterFormats.h"
#include "converter/IConverterFormat.h"
#include "util/clock_man/ClockTraits.h"
#include "util/clock_man/CumulativeRecords.h"
#include "util/clock_man/TimeStamp.h"
#include "util/misc/Exception.h"
#include "util/revision/RevisionInfo.h"

#include <tclap/CmdLine.h>
#include <tclap/SwitchArg.h>
#include <tclap/ValueArg.h>
#include <tclap/ValuesConstraint.h>
#include <QApplication>
#include <QMetaType>

#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace Modes {

using namespace std;
using namespace TCLAP;
using namespace SimPT_Shell;
using namespace SimPT_Shell::Session;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::Util;

int simPTCLIMode::operator()(int argc, char** argv)
{
	int exit_status = EXIT_SUCCESS;

	try {
		// Needs to be instantiated to access some Qt functionality.
		QCoreApplication  app(argc, argv, false);
		qRegisterMetaType<std::string>("std::string");

		// Parse command line and create task to be executed.
		CmdLine cmd("simPT_sim", ' ', "1.0");
		SwitchArg                rev_Arg("r", "revision", "Revision identification", cmd, false);
		SwitchArg                quiet_Arg("q", "quiet", "Quit mode (no output)", cmd, false);
		ValueArg<unsigned int>   num_steps_Arg("s", "stepcount", "number of steps", false, 0, "NUMBER OF STEPS", cmd);
		ValueArg<string>         file_Arg("f", "file", "Tissue file in project", false, "", "TISSUE FILE", cmd);
		ValueArg<string>         project_Arg("p", "project", "Name of project", false, "unnamed", "PROJECT NAME", cmd);
		ValueArg<string>         workspace_Arg("w", "workspace", "Path to workspace", false, "simPT_Default_workspace", "WORKSPACE PATH", cmd);

		vector<string>           convert_output_formats;
		for (auto f : FileConverterFormats::GetFormats()) {
			if (!f->IsPostProcessFormat())
				convert_output_formats.push_back(f->GetName());
		}

		vector<string>           postprocess_output_formats;
		for (auto f : FileConverterFormats::GetFormats()) {
			if (f->IsPostProcessFormat())
				postprocess_output_formats.push_back(f->GetName());
		}
		
		ValueArg<string>         timestep_filter_Arg("t", "timestep-filter",
		        "Filter timesteps to convert, (list of) ranges are accepted, e.g. \"200-300,600\".", false, {}, "", cmd);

		ValuesConstraint<string> allowedConvertFormats(convert_output_formats);
		ValueArg<string>         convert_Arg("c", "convert",
		        "Convert mode (no simulation): Convert existing files in workspace.",
		        false, "xml", &allowedConvertFormats, cmd);

		ValuesConstraint<string> allowedPostprocessFormats(postprocess_output_formats);
		ValueArg<string>         postprocess_Arg("o", "postprocess",
		        "Postprocess mode (no simulation): Postprocess existing files in workspace.",
		        false, "png", &allowedPostprocessFormats, cmd);

		ValuesConstraint<string> allowedInputFormats(convert_output_formats);
		ValueArg<string>         input_format_Arg("z", "input-format-filter",
		        "Only use a specific input format", false, "", &allowedInputFormats, cmd);

		cmd.parse(argc, argv);

		if (rev_Arg.isSet()) {
			cout << RevisionInfo::CompoundId() << endl;
		}

		if (workspace_Arg.isSet()) {
			// Output preamble.
			cout << "simPT_sim starting up at: " << TimeStamp().ToString() << endl;
			cout << "Executing: " << argv[0]<< endl;
			cout << "Revision: " << RevisionInfo::CompoundId() << endl;

			// Command line arguments.
			const bool            quiet           = quiet_Arg.getValue();
			const string          workspace_path  = workspace_Arg.getValue();
			const string          project_name    = project_Arg.getValue();


			auto c = CliController::Create(workspace_path, quiet);

			if (convert_Arg.isSet() || postprocess_Arg.isSet()) {
				const string timestep_filter     = timestep_filter_Arg.getValue();
				const string output_format_str   = convert_Arg.isSet() ? convert_Arg.getValue() : postprocess_Arg.getValue();
				const string input_format_filter = input_format_Arg.isSet() ? input_format_Arg.getValue() : "";

				IConverterFormat*     output_format = nullptr;
				for (auto f : FileConverterFormats::GetFormats()) {
					if (output_format_str == f->GetName()) {
						output_format = f;
					}
				}
				if (output_format == nullptr) {
					throw Exception("Convert mode selected but no output format given!");
				}

				// Set up task and execute.
				CliConverterTask task(project_name, timestep_filter, output_format, input_format_filter);
				exit_status = c->Execute(task);
			} else {
				const unsigned int    num_steps       = num_steps_Arg.getValue();
				const bool            num_steps_set   = num_steps_Arg.isSet();
				const string          file            = file_Arg.getValue();
				const bool            file_set        = file_Arg.isSet();

				// Setup task.
				CliSimulationTask task(project_name, file_set, file, num_steps_set, num_steps);
				exit_status = c->Execute(task);
			}
			if (exit_status == EXIT_SUCCESS) {
				cout << endl << c->GetTimings() << endl << endl;
			}
			cout << "SimPT_Sim exiting at: " << TimeStamp().ToString() << endl << endl;
		}
	}
	// Almost all the exceptions that we are currently aware of derive
	// from std::exception: TCLAP::ArgException, SimPT_Sim::Util::Exception,
	// boost::property_tree::ptree_error, boost::property_tree::ptree_dad_data,
	// boost::property_tree::ptree_bad_path, boost::property_tree::xml_parser::xml_parser_error
	catch (exception& e) {
		exit_status = EXIT_FAILURE;
		cerr << e.what() << endl;
	}
	catch (...) {
		exit_status = EXIT_FAILURE;
		cerr << "Unknown exception." << endl;
	}

	return exit_status;
}

} // namespace
