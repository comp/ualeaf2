#ifndef SIMPT_MODE_MANAGER_H_
#define SIMPT_MODE_MANAGER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for the mode manager.
 */

#include "parex_client_mode.h"
#include "parex_node_mode.h"
#include "parex_server_mode.h"
#include "sim_cli_mode.h"
#include "sim_gui_mode.h"
#include "util/misc/FunctionMap.h"

#include <tclap/CmdLine.h>
#include <boost/functional/value_factory.hpp>
#include <cstring>
#include <stdexcept>
#include <string>
#include <vector>

namespace Modes {

/**
 * Function representing a certain mode.
 */
using Function = std::function<int(int argc, char** argv)>;

/**
 * Factory produces functions that start the different modes of the various executables.
 */
template<typename executable_type>
class ModeManager
{
public:
	/**
	 * Create a function
	 */
	static Function Create(const std::string& application)
	{
		if (!IsValid(application)) {
			const std::string msg = std::string("Invalid selection:\n");
			throw std::logic_error(msg + application);
		}
		return executable_type::modes.Get(application)();		
	}

	/**
	 * Check whether selection is valid i.e. is known to Factory.
	 */
	static bool IsValid(const std::string& selection)
	{
		return (selection != "") && executable_type::modes.IsValid(selection);
	}

	/**
	 * Lists all the registered applications.
	 */
	static std::vector<std::string> List()
	{
		auto list = executable_type::modes.List();
		return std::vector<std::string> (std::begin(list),std::end(list) );
	}

	/**
	 * Returns the default application.
	 */
	inline static std::string DefaultApplication()
	{
		return executable_type::default_mode;
	}

	static int main(int argc, char** argv)
	{
		using namespace std;
		using namespace TCLAP;

		int exit_status = EXIT_SUCCESS;
		try {
			// Parse command line and create task to be executed.
			CmdLine cmd(executable_type::application_name, ' ', "", false);
			vector<string>           apps(List());
			ValuesConstraint<string> allowedApps(apps);
			ValueArg<string>         application_Arg("m", "mode", "The application mode", false, "compound", &allowedApps, cmd);
			SwitchArg                list_applications("l", "list-modes", "List the available modes", cmd, false);
			SwitchArg                help("h", "help", "Displays usage information and exits.", cmd, false);

			// The unlabeled multi args will be used to get the arguments of the "sub" application.
			UnlabeledMultiArg<string> multi("params","application arguments", false,"");
			cmd.add( multi );
			cmd.parse(argc, argv);

			if (list_applications.getValue()) {
				std::cout << "Available modes:"<<std::endl;
				for (std::string app: apps ) {
					cout <<"   -"<<app<<std::endl;
				}
			} else if (help.getValue() && !application_Arg.isSet()) {
				cmd.getOutput()->usage(cmd);			
			} else {
				const std::string default_app = DefaultApplication();
				vector<string>  params = multi.getValue();

				//Add the -m <mode> to the command line argument (otherwise the --help is wrong for the app.)
				std::string appArg = " -m " + (application_Arg.isSet() ? application_Arg.getValue() : default_app);
				params.insert(params.begin(), std::string(argv[0])+appArg);

				// If the help flag is set together with an application, show the help of the application.
				if (help.getValue()) {
					params.insert(params.begin() + 1, "--help");
				}

				// Convert the arguments back to a plain old char* array
				std::vector<char *> argv_new(params.size() + 1);
				for(std::size_t i=0; i < params.size(); ++i) {
					argv_new[i] = new char[params[i].length() + 1];
					std::strcpy(argv_new[i], params[i].c_str());
				}
				int argc_new = params.size();

				// Start the application, if no application is set, use the default.
				if (application_Arg.isSet()) {
					exit_status = Create(application_Arg.getValue())(argc_new, argv_new.data());
				} else {
					exit_status = Create(default_app)(argc_new, argv_new.data());
				}

				for(int i = 0; i < argc_new; ++i) {
					if (argv_new[i]!=0) {
						delete [] argv_new[i];
					}
				}
			}
		}
		catch (exception& e) {
			exit_status = EXIT_FAILURE;
			cerr << e.what() << endl;
		}
		catch (...) {
			exit_status = EXIT_FAILURE;
			cerr << "Unknown exception." << endl;
		}

		return exit_status;
	}

	/**
	 * Map type that stores the factories for each of the individual classes.
	 */
	using MapType = SimPT_Sim::Util::FunctionMap<Function ()>;

};

} // namespace Modes

#endif // end_of_include_guard
