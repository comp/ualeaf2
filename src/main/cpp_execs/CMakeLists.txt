#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

unset( BUILD_EXECS )

#------------------------------ build simPT_sim ----------------------------
unset( SIMPT_GUI_ICON_SRC )
if( APPLE )
	set( SIMPT_GUI_ICON_SRC  ${CMAKE_SOURCE_DIR}/main/resources/icons/simPT_icon.icns )
endif()

add_executable( simPT_sim      WIN32  
                          simPT_sim.cpp  modes/sim_gui.cpp 
                          modes/sim_cli.cpp simPT.rc ${SIMPT_GUI_ICON_SRC}
)
if( APPLE )
        set_target_properties(simPT_sim
                          PROPERTIES RPATH "${CMAKE_INSTALL_PREFIX}/bin;@loader_path")
endif()
target_link_libraries( simPT_sim   simPTlib_shell )
set( BUILD_EXECS ${BUILD_EXECS} simPT_sim )

#------------------------------------------------------------------------------

#------------------------------ build simPT_editor ----------------------------
if( APPLE AND (SIMPT_QT_VERSION EQUAL 5) )
# DO NOT BUILD THE EDITOR
else() 
    # BUILD THE EDITOR
    unset( SIMPT_EDITOR_ICON_SRC )
    if( APPLE )
    	set( SIMPT_EDITOR_ICON_SRC  ${CMAKE_SOURCE_DIR}/main/resources/icons/editor_logo.icns )
    endif()
    
    add_executable( simPT_editor  WIN32  
    	            simPT_editor.cpp  $<TARGET_OBJECTS:simPTlib_tissue_edit> ${SIMPT_EDITOR_ICON_SRC}  
    )
    if( APPLE )
    	    set_target_properties(simPT_editor
                    PROPERTIES  RPATH "${CMAKE_INSTALL_PREFIX}/bin;@loader_path")
    endif()
    target_link_libraries( simPT_editor    simPTlib_shell )
    set( BUILD_EXECS ${BUILD_EXECS} simPT_editor )
endif()
#------------------------------------------------------------------------------

#--------------------------------- build parex --------------------------------
unset( PAREX_ICON_SRC )
if( APPLE )
	set( PAREX_ICON_SRC  ${CMAKE_SOURCE_DIR}/main/resources/icons/parex_logo.icns )
endif()

add_executable( simPT_parex	 WIN32 
			    simPT_parex.cpp modes/parex_client.cpp modes/parex_node.cpp modes/parex_server.cpp  
			    $<TARGET_OBJECTS:simPTlib_parex> ${PAREX_ICON_SRC}
)
if( APPLE )
	    set_target_properties(simPT_parex
                            PROPERTIES  RPATH "${CMAKE_INSTALL_PREFIX}/bin;@loader_path")
endif()
target_link_libraries( simPT_parex   simPTlib_shell )
set( BUILD_EXECS ${BUILD_EXECS} simPT_parex )

#------------------------------------------------------------------------------


#------------------------------ install -------------------------------------
install( TARGETS  ${BUILD_EXECS} 
		 DESTINATION   ${BIN_INSTALL_LOCATION} )
unset( BUILD_EXECS )
#------------------------------------------------------------------------------

#############################################################################
