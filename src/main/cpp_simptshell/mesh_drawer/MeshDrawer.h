#ifndef MESH_DRAWER_H_
#define MESH_DRAWER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MeshDrawer
 */

#include "exporters/common/GraphicsPreferences.h"

#include <memory>

class QGraphicsScene;
namespace SimPT_Sim { class SimInterface; }

namespace SimPT_Shell {

/**
 * Class drawing a simulator/mesh onto a QGraphicsScene.
 */
class MeshDrawer
{
public:
	/// Constructor
	MeshDrawer();

	/// Constructor
	MeshDrawer(const std::shared_ptr<GraphicsPreferences>&);

	/// Draws the mesh to QGraphicsScene.
	void Draw(std::shared_ptr<SimPT_Sim::SimInterface> sim, QGraphicsScene* scene);

private:
	using prefs_type = GraphicsPreferences;

private:
	std::shared_ptr<prefs_type>    m_preferences;
};

} // namespace

#endif // end-of-include-guard
