/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ArrowItem.
 */

#include "../../cpp_simptshell/mesh_drawer/ArrowItem.h"

#include <cmath>

namespace SimPT_Shell {

using std::sqrt;

ArrowItem::ArrowItem(QGraphicsItem* parent) : QGraphicsLineItem(parent) {}

void ArrowItem::paint(QPainter* p, const QStyleOptionGraphicsItem* , QWidget* )
{
	// construct arrow head
	QPointF const start = line().p1();
	QPointF const end = line().p2();
	QPointF const mid = start + 0.75e0 * (end - start);
	double const vx = end.x() - start.x();
	double const vy = end.y() - start.y();

	double length = sqrt(vx * vx + vy * vy);
	if (length != 0) {
		// perpendicular vector
		double const px = -vy / length;
		double const py = vx / length;

		// Arrow head lines go from end point to points about 3/4 of the total arrow,
		// extending sideways about 1/4 of the arrow length.
		QPointF const arwp1 = mid + QPointF((int) ((length / 4.) * px), (int) ((length / 4.) * py));
		QPointF const arwp2 = mid - QPointF((int) ((length / 4.) * px), (int) ((length / 4.) * py));

		p->setPen(pen());
		// Draw arrow head
		p->drawLine(end, arwp1);
		p->drawLine(end, arwp2);
		// Draw arrow line
		p->drawLine(start, end);
	}
}

} // namespace
