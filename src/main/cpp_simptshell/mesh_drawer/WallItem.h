#ifndef WALLITEM_H_
#define WALLITEM_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WallItem.
 */

#include <QGraphicsScene>
#include <QGraphicsPolygonItem>
#include <array>

namespace SimPT_Sim { class Wall; }

namespace SimPT_Shell {

/**
 * WallItem shows transporter concentrations at one side of the wall.
 */
class WallItem : public QGraphicsPolygonItem
{
public:
	WallItem(SimPT_Sim::Wall* w, int wallnumber, QGraphicsScene* canvas, double outlinewidth,
		double magnification, std::array<double, 3> offset);

	virtual ~WallItem() {}

private:
	void setColor();
	void setColorWallStrength();//DDV
	void setColor4Test();//DDV

private:
	SimPT_Sim::Wall* m_wall;
	int         	  m_wall_number;
	QPolygonF   	  m_poly;
};

} // namespace

#endif // end_of_include_guard
