#ifndef GUI_CONVERSION_LIST_H_INCLUDED
#define GUI_CONVERSION_LIST_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ConversionList
 */

#include "mesh_drawer/MeshDrawer.h"

#include <QWidget>
#include <memory>
#include <set>

class QCheckBox;
class QGraphicsScene;
class QLineEdit;
class QModelIndex;
class QStandardItemModel;
class QTableView;

namespace SimPT_Sim { class SimState; }

namespace SimPT_Shell {

class StepFilterProxyModel;
class LoadedSimState;

/**
 * Widget containing the list of steps in conversion and associated functionality
 */
class ConversionList: public QWidget
{
	Q_OBJECT

public:
	/// Constructor
	ConversionList(QWidget* parent = nullptr);

	/// Destructor
	virtual ~ConversionList();

	///
	struct EntryType {
		int           timestep;
		std::string   files;
	};

	/// Add entry.
	void AddEntry(const EntryType &);

	/// Clear all entries.
	void Clear();

	///
	std::vector<EntryType> GetCheckedEntries() const;

	///
	bool HasCheckedEntries() const;

signals:
	/**
	 * Signal emitted when the selection in the conversion changes
	 *
	 * @param	selected	Whether there are simulation states selected (that can be removed)
	 */
	void SelectionChanged(bool selected);

	/**
	 * Signal emitted when the check state of some SimStates in the conversion has changed
	 */
	void CheckedChanged();

private slots:
	void EmitSelectionChanged();
	void UpdateStepFilter();
	void UpdateFileFilter();
	void UpdateSelectAllCheckState();
	void SelectAll();

private:
	void SetupGui();
	void ResizeColumns();

private:
	QStandardItemModel*    m_model;
	StepFilterProxyModel*  m_filter_model;
	QTableView*            m_table_view;

	QLineEdit*             m_step_filter;
	QLineEdit*             m_file_filter;
	QCheckBox*             m_select_all_check_box;

	Qt::CheckState         m_global_check_state;
};

} // namespace

#endif // end-of-include-guard
