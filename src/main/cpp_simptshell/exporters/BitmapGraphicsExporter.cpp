/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for BitmapGraphicsExporter.
 */

#include "BitmapGraphicsExporter.h"

#include "bio/Mesh.h"
#include "math/MeshGeometry.h"
#include "mesh_drawer/MeshDrawer.h"
#include "sim/Sim.h"
#include "util/clock_man/TimeStamp.h"

#include <QDir>
#include <QGraphicsScene>
#include <QImage>
#include <QPainter>
#include <QPointF>
#include <QPrinter>
#include <QRectF>
#include <QString>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace boost::property_tree;
using namespace std;
using namespace SimPT_Sim;
using SimPT_Sim::ClockMan::TimeStamp;

namespace SimPT_Shell {

bool BitmapGraphicsExporter::Export(shared_ptr<SimPT_Sim::Sim> sim,
	const string& file_path,
	bool overwrite,
	std::shared_ptr<BitmapGraphicsPreferences> prefs)
{
	if (ifstream(file_path).good()) {
		if (overwrite) {
			// Qt doesn't overwrite by default, must delete file first
			QFile::remove(QString::fromStdString(file_path));
		} else {
			return false; // Don't overwrite
		}
	}

	QGraphicsScene canvas;
	MeshDrawer m(prefs);
	m.Draw(sim, &canvas);

	QPointF top_left;
	QPointF bottom_right;
	if (prefs->m_window_preset) {
		top_left      = QPointF(prefs->m_window_x_min, prefs->m_window_y_min);
		bottom_right  = QPointF(prefs->m_window_x_max, prefs->m_window_y_max);
	} else {
		const auto box    = MeshGeometry::BoundingBox(shared_ptr<Mesh>(sim->GetCoreData().m_mesh.get(), [](Mesh*){}));
		const auto min_p  = get<0>(box) - 1.05 * (get<1>(box) - get<0>(box));
		const auto max_p  = get<1>(box) + 1.05 * (get<1>(box) - get<0>(box));
		top_left      = QPointF(get<0>(min_p), get<1>(min_p));
		bottom_right  = QPointF(get<0>(max_p), get<1>(max_p));
	}

	int width;
	int height;
	if (prefs->m_size_preset) {
		width = prefs->m_size_x;
		height = prefs->m_size_y;
	} else {
		width = 800;
		height = 600;
	}

	string format;
	switch (prefs->m_format) {
	case BitmapGraphicsPreferences::Png:
		format = "PNG";
		break;
	case BitmapGraphicsPreferences::Bmp:
		format = "BMP";
		break;
	case BitmapGraphicsPreferences::Jpeg:
		format = "JPEG";
		break;
	}

	bool status = false;
	if (!file_path.empty()) {
		QImage image(QSize(width, height), QImage::Format_RGB32);
		image.fill(QColor(QString::fromStdString(prefs->m_background_color)).rgb());
		QPainter painter(&image);
		canvas.render(&painter, QRectF(), QRectF(top_left, bottom_right));
		status = image.save(QDir::toNativeSeparators(QString::fromStdString(file_path)), format.c_str());
	}

	return status;
}

} // namespace
