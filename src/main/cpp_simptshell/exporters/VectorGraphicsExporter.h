#ifndef VIEW_VECTOR_GRAPHICS_EXPORTER_H_
#define VIEW_VECTOR_GRAPHICS_EXPORTER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for VectorGraphicsExporter.
 */

#include "common/VectorGraphicsPreferences.h"

#include <string>
#include <tuple>

class QGraphicsScene;

namespace SimPT_Sim {
class SimInterface;
}

namespace SimPT_Shell {

/**
 * Functions and data for PDF export of Sim objects.
 */
class VectorGraphicsExporter
{
public:
	/**
	 * Export snapshot of canvas in PNG format.
	 * @param e       	Event containing canvas to render.
	 * @param path    	Path where to save image.
	 * @param overwrite     Whether to overwrite if path already exists.
	 * @param size_x  	Image width
	 * @param size_y  	Image height
	 * @return 		true if successful
	 */
	static bool Export(std::shared_ptr<SimPT_Sim::SimInterface> sim,
		std::string const& file_path,
		bool overwrite = true,
		std::shared_ptr<VectorGraphicsPreferences> prefs = std::make_shared<VectorGraphicsPreferences>());

	/**
	 * File extension associated with this export format.
	 * @return		String containing file extension (without a ".")
	 */
	static std::string GetFileExtension();
};

} // namespace

#endif // end_of_include_guard
