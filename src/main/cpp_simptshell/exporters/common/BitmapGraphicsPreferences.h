#ifndef EXPORTERS_BITMAP_GRAPHICS_PREFERENCES_H_
#define EXPORTERS_BITMAP_GRAPHICS_PREFERENCES_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for BitmapGraphicsPreferences.
 */

#include "exporters/common/GraphicsPreferences.h"
#include "util/misc/Exception.h"

#include <boost/property_tree/ptree_fwd.hpp>

namespace SimPT_Shell
{

/**
 * Preferences for a graphic viewer.
 */
struct BitmapGraphicsPreferences : GraphicsPreferences
{
	enum Format {
		Png, Bmp, Jpeg
	};

	Format       m_format;
	bool         m_size_preset;
	int          m_size_x;
	int          m_size_y;

	BitmapGraphicsPreferences()
	{
		m_format               = Png; // This 'preference' is not read from preferences ptree. Can only be modified in code.
		m_size_preset          = false;
		m_size_x               = 0;
		m_size_y               = 0;
	}

	virtual void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e)
	{
		GraphicsPreferences::Update(e);

		m_size_preset = false;
		try {
			auto size_prefs = e.source->GetChild("size");
			m_size_x = size_prefs->Get<double>("x");
			m_size_y = size_prefs->Get<double>("y");
			m_size_preset = true;
		} catch (boost::property_tree::ptree_error& e) {}
	}
};

} // namespace

#endif
