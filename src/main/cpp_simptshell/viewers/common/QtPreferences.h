#ifndef VIEWERS_QT_PREFERENCES_H_
#define VIEWERS_QT_PREFERENCES_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for QtPreferences.
 */

#include "../../../cpp_simshell/workspace/MergedPreferences.h"
#include "../../exporters/common/GraphicsPreferences.h"

namespace SimPT_Shell
{

/**
 * Preferences for a graphic viewer.
 */
struct QtPreferences : GraphicsPreferences
{
	int          m_stride;

	QtPreferences() : m_stride(0) {}

	virtual void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e)
	{
		GraphicsPreferences::Update(e);
		m_stride = e.source->Get<int>("stride");
	}
};

} // namespace

#endif // end_of_include_guard
