/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for LogWindowViewer.
 */

#include "viewers/LogWindowViewer.h"

#include "sim/CoupledSim.h"
#include "sim/Sim.h"

#include <QPlainTextEdit>
#include <sstream>

using namespace std;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;

namespace SimPT_Shell {

LogWindowViewer::LogWindowViewer(const shared_ptr<SimShell::Ws::MergedPreferences>& p,
                                 SimShell::Gui::Controller::AppController* a, function<void()> )
	: m_preferences(p),
	  m_app(a)
{
}

LogWindowViewer::~LogWindowViewer()
{
}


} // namespace
