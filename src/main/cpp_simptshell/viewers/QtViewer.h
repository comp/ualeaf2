#ifndef VIEW_QT_VIEWER_H_INCLUDED
#define VIEW_QT_VIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for QtViewer.
 */

#include "common/PreferencesObserver.h"
#include "common/QtPreferences.h"

#include "sim/event/SimEventType.h"
#include "gui/ViewerWindow.h"
#include "../mesh_drawer/MeshDrawer.h"

#include <QGraphicsScene>
#include <functional>
#include <memory>
#include <string>

class QWidget;

namespace SimPT_Shell {

/**
 * Graphical viewer showing the mesh at a certain simulation step.
 */
class QtViewer : public SimShell::Gui::ViewerWindow
{
public:
	QtViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p,
	               QWidget*, std::function<void()> on_close);

	virtual ~QtViewer();

	template <typename EventType>
	void Update(const EventType&);

private:
	using prefs_type =  PreferencesObserver<QtPreferences>;

private:
	std::shared_ptr<prefs_type>      m_preferences;
	std::shared_ptr<QGraphicsScene>  m_canvas;
	MeshDrawer                       m_drawer;
};

template <typename EventType>
void QtViewer::Update(const EventType& e)
{
	auto et = e.GetType();
	auto step = e.GetStep();

	const bool check = (et == SimPT_Sim::Event::SimEventType::Forced)
		|| (et == SimPT_Sim::Event::SimEventType::Initialized)
		|| (et == SimPT_Sim::Event::SimEventType::Stepped && (m_preferences->m_stride != 0 && (step % m_preferences->m_stride == 0)))
		|| (et == SimPT_Sim::Event::SimEventType::Done && m_preferences->m_stride == 0);

	if (check) {
		std::string s = std::string("background-color:") + m_preferences->m_background_color + ";";
		setStyleSheet(s.c_str());
		m_canvas->clear();
		m_drawer.Draw(e.GetSource(), m_canvas.get());
	}
}

} // namespace

#endif // end_of_include_guard
