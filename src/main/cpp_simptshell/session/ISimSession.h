#ifndef SIMPT_SESSION_ISIMSESSION_H_INCLUDED
#define SIMPT_SESSION_ISIMSESSION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for sessions.
 */

#include "session/ISession.h"
#include "sim/Sim.h"

namespace SimPT_Shell {
namespace Session {

/**
 * Interface for handling an opened/running simulation.
 */
class ISimSession : public SimShell::Session::ISession
{
	Q_OBJECT
public:
	virtual ~ISimSession() {}

	/// Get the simulator.
	virtual std::shared_ptr<SimPT_Sim::Sim> GetSim() = 0;

	/// Return status.
	virtual std::string GetStatusMessage() const = 0;
};

} // namespace
} // namespace

#endif //  end_of_include_guard
