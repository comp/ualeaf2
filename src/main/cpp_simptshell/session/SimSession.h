#ifndef SIMPT_SESSION_SIM_SESSION_H_INCLUDED
#define SIMPT_SESSION_SIM_SESSION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interfaces for simulator session.
 */

#include "ISimSession.h"
#include "gui/controller/AppController.h"

#include <boost/property_tree/ptree.hpp>
#include <functional>
#include <memory>
#include <mutex>
#include <thread>


namespace SimShell { namespace Ws { class MergedPreferences; } }
namespace SimPT_Sim { class Sim; }
class QMainWindow;
class QTimer;

namespace SimPT_Shell {
namespace Session {

class SimWorker;

using boost::property_tree::ptree;
using namespace SimShell;
using namespace SimShell::Session;
using namespace SimShell::Ws;

/**
 * Collection of instances associated with an opened/running project.
 * Owner of Sim object, viewers and viewer windows.
 */
class SimSession : public ISimSession,
                     public std::enable_shared_from_this<SimSession>
{
	Q_OBJECT
public:
	/// Initializes with a ptree description of the simulator state.
	SimSession(const std::shared_ptr<MergedPreferences>& prefs, const ptree& sim);

	/// Initializes with a SimState description of the simulator state.
	SimSession(const std::shared_ptr<MergedPreferences>& prefs, const SimPT_Sim::SimState& sim);

	/// Destructor virtual.
	virtual ~SimSession();

	/// Return root viewer, i.e. the viewer that owns all other viewers.
	virtual std::shared_ptr<RootViewerType>
	CreateRootViewer(SimShell::Gui::Controller::AppController* parent = nullptr);

	/// @see Session::ISimSession
	virtual std::shared_ptr<SimPT_Sim::Sim> GetSim();

	/// @see Session::ISimSession
	virtual ExportersType GetExporters();

	/// @see Session::ISimSession
	virtual const ptree& GetParameters() const;

	/// @see Session::ISimSession
	virtual std::string GetStatusMessage() const;

	/// Get timing information from simulation and viewers.
	virtual Timings GetTimings() const;

	/// @see Session::ISimSession
	virtual void ForceExport();

	/// @see Session::ISimSession
	virtual void SetParameters(const ptree&);

	/// @see Session::ISimSession
	virtual void StartSimulation(int steps = -1);

	/// @see Session::ISimSession
	virtual void StopSimulation();

public slots:
	/// @see Session::ISimSession
	virtual void TimeStep();

private slots:
	virtual void ExecuteViewUnit(QString);

signals:
	void ExecuteWorkUnit();

private:
	std::shared_ptr<SimPT_Sim::Sim>     m_sim;              ///< Simulator object, never nullptr.
	std::mutex                          m_parameters_mutex; ///< Locked while parameters are being set.
	std::mutex                          m_viewers_mutex;    ///< Locked while viewers are being notified.
	Timings                             m_timings;          ///< Timing parts of the computations.
	std::shared_ptr<MergedPreferences>  m_preferences;      ///< We need this to initialize viewers.

	QThread*                            m_sim_thread;       ///< Simulation thread.
	bool                                m_running;          ///< Whether simulation is currently running.

	int                                 m_steps_limit;      ///< Counts to 0 for of limited number of steps.

	struct {
		bool   updated;       ///< Whether parameters were updated and need saving to disk.
		ptree  pt;            ///< Ptree containing parameters.
	} m_parameter_buffer;         ///< Store parameters until simulator can accept them.
};

} // namespace
} // namespace

#endif // end_of_include_guard
