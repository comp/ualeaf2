/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimSession.
 */

#include "session/SimSession.h"

#include "exporters/BitmapGraphicsExporter.h"
#include "exporters/CsvExporter.h"
#include "exporters/Hdf5Exporter.h"
#include "exporters/PlyExporter.h"
#include "exporters/VectorGraphicsExporter.h"
#include "exporters/XmlExporter.h"
#include "exporters/XmlGzExporter.h"
#include "session/SimWorker.h"
#include "viewer/RootViewerNode.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"
#include "workspace/MergedPreferences.h"

#include <QThread>
#include <QCoreApplication>

namespace SimPT_Shell {
namespace Session {

using namespace std;
using namespace chrono;
using namespace boost::property_tree;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Event;
using namespace SimShell::Gui;
using namespace SimShell::Session;
using namespace SimShell::Ws;
using namespace SimShell::Ws::Event;

SimSession::SimSession(const shared_ptr<MergedPreferences>& prefs, const ptree& sim_pt)
	: m_sim(make_shared<Sim>()),
	  m_preferences(prefs),
	  m_sim_thread(new QThread(this)),
	  m_running(false),
	  m_steps_limit(-1),
	  m_parameter_buffer({ false, ptree() })
{
	m_sim->Initialize(sim_pt); // may throw
}

SimSession::SimSession(const shared_ptr<MergedPreferences>& prefs, const SimState& sim_state)
	: m_sim(make_shared<Sim>()),
	  m_preferences(prefs),
	  m_sim_thread(new QThread(this)),
	  m_running(false),
	  m_steps_limit(0),
	  m_parameter_buffer({ false, ptree() })
{
	m_sim->Initialize(sim_state); // may throw
}

SimSession::~SimSession()
{
	// To process a (potential) pending ExecuteViewUnit
	// event in the eventloop after a StopSimulation
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
	// Signaling closing of project..
	m_sim->Notify({ m_sim, m_sim->GetSimStep(), SimEvent::Type::Done });
}

shared_ptr<SimSession::RootViewerType> SimSession::CreateRootViewer(
	SimShell::Gui::Controller::AppController* parent)
{
	return make_shared<Viewer::RootViewerNode<Sim>>(
		m_preferences->GetChild("viewers"), m_sim, parent);
}

shared_ptr<SimPT_Sim::Sim> SimSession::GetSim()
{
	return m_sim;
}

SimSession::ExportersType SimSession::GetExporters()
{
	return {
		{"BMP", {"bmp",
			[&](const string& file) {
				auto pref = make_shared<BitmapGraphicsPreferences>();
				pref->Update({m_preferences->GetChild("viewers.bitmap_graphics")});
				pref->m_format = BitmapGraphicsPreferences::Bmp;
				BitmapGraphicsExporter::Export(m_sim, file, true, pref);
		}}},
		{"CSV", {CsvExporter::GetFileExtension(),
			[&](const string& file) {
				CsvExporter::Export(m_sim, file);
		}}},
		{"HDF5", {Hdf5Exporter::GetFileExtension(),
			[&](const string& file) {
				Hdf5Exporter::Export(m_sim, file);
		}}},
		{"JPEG", {"jpg",
			[&](const string& file) {
				auto pref = make_shared<BitmapGraphicsPreferences>();
				pref->Update({m_preferences->GetChild("viewers.bitmap_graphics")});
				pref->m_format = BitmapGraphicsPreferences::Jpeg;
				BitmapGraphicsExporter::Export(m_sim, file, true, pref);
		}}},
		{"PLY", {PlyExporter::GetFileExtension(),
			[&](const string& file) {
				PlyExporter::Export(m_sim, file);
		}}},
		{"PDF", {"pdf",
			[&](const string& file) {
				auto pref = make_shared<VectorGraphicsPreferences>();
				pref->Update({m_preferences->GetChild("viewers.vector_graphics")});
				pref->m_format = VectorGraphicsPreferences::Pdf;
				VectorGraphicsExporter::Export(m_sim, file, true, pref);
		}}},
		{"PNG", {"png",
			[&](const string& file) {
				auto pref = make_shared<BitmapGraphicsPreferences>();
				pref->Update({m_preferences->GetChild("viewers.bitmap_graphics")});
				pref->m_format = BitmapGraphicsPreferences::Png;
				BitmapGraphicsExporter::Export(m_sim, file, true, pref);
		}}},
		{"XML", {XmlExporter::GetFileExtension(),
			[&](const string& file) {
				XmlExporter::Export(m_sim, file);
		}}},
		{"XML.GZ", {XmlGzExporter::GetFileExtension(),
			[&](const string& file) {
				XmlGzExporter::Export(m_sim, file);
		}}}
	};
}

const ptree& SimSession::GetParameters() const
{
	return m_sim->GetParameters();
}

string SimSession::GetStatusMessage() const
{
	    return m_sim->GetStatusMessage();
}

SimSession::Timings SimSession::GetTimings() const
{
	auto timings = m_sim->GetTimings();
	timings.Merge(m_timings);
	return timings.GetRecords();
}

void SimSession::ForceExport()
{
	lock_guard<mutex> parameters_guard(m_viewers_mutex);

	using SimPT_Sim::Event::SimEvent;
	SimEvent e(m_sim, m_sim->GetSimStep(), SimEvent::Type::Forced);
	m_sim->Notify(e);
}

void SimSession::ExecuteViewUnit(QString worker_message)
{
	if (worker_message != "") {
		throw Exception(worker_message.toStdString());
	}
	{
		lock_guard<mutex> viewers_guard(m_viewers_mutex);
		m_sim->Notify(SimEvent(m_sim, m_sim->GetSimStep(), SimEvent::Type::Stepped));
	}
	emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stepped);
	--m_steps_limit;

	if (m_running) {
		if ((m_steps_limit == 0) || m_sim->IsAtTermination()) {
			StopSimulation();
		} else {
			{ // Check if parameters were changed.
				lock_guard<mutex> parameters_guard(m_parameters_mutex);
				if (m_parameter_buffer.updated) {
					m_sim->Reinitialize(m_parameter_buffer.pt);
					// Notify viewers (preferences can't be changed during notification).
					{
						lock_guard<mutex> viewers_guard(m_viewers_mutex);
						m_sim->Notify(SimEvent(m_sim, m_sim->GetSimStep(), SimEvent::Type::Forced));
					}
					m_parameter_buffer.updated = false;
				}
			}
			emit ExecuteWorkUnit();
		}
	}
}

void SimSession::SetParameters(const ptree& pt)
{
	lock_guard<mutex> parameters_guard(m_parameters_mutex);

	m_parameter_buffer.pt = pt;
	m_parameter_buffer.updated = true;
}

void SimSession::StartSimulation(int steps)
{
	assert(steps >= -1 && "Steps should be positive or -1");
	m_steps_limit = steps;

	// Create worker instance
	auto sim_worker = new SimWorker(m_sim);
	sim_worker->moveToThread(m_sim_thread);

	// The signals & slots magic
	connect(m_sim_thread, SIGNAL(started()), sim_worker, SLOT(Work()));
	connect(this, SIGNAL(ExecuteWorkUnit()), sim_worker, SLOT(Work()));
	connect(sim_worker, SIGNAL(Worked(QString)), this, SLOT(ExecuteViewUnit(QString)));
	connect(m_sim_thread, SIGNAL(finished()), sim_worker, SLOT(deleteLater()));

	// Get the ball rolling
	m_running = true;
	if (m_parameter_buffer.updated) {
		m_sim->Reinitialize(m_parameter_buffer.pt);
		// Notify viewers (preferences can't be changed during notification).
		{
			lock_guard<mutex> viewers_guard(m_viewers_mutex);
			m_sim->Notify(SimEvent(m_sim, m_sim->GetSimStep(), SimEvent::Type::Forced));
		}
		m_parameter_buffer.updated = false;
	}
	if (m_sim->IsAtTermination()) {
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Terminated);
	} else {
		m_sim_thread->start();
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Started);
	}
}

void SimSession::StopSimulation()
{
	m_running = false;
	m_sim_thread->quit();
	m_sim_thread->wait();
	m_steps_limit = -1;
	if (m_sim->IsAtTermination()) {
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Terminated);
	} else {
		emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stopped);
	}
}

void SimSession::TimeStep()
{
	StartSimulation(1);
}

} // namespace
} // namespace
