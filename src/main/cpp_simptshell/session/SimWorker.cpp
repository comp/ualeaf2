/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of SimWorker.
 */

#include "session/SimWorker.h"

#include <QString>
#include <exception>

using namespace std;

namespace SimPT_Shell {
namespace Session {

SimWorker::SimWorker(shared_ptr<SimPT_Sim::SimInterface> s)
	: m_sim(s)
{
}

void SimWorker::Work()
{
	QString message = "";
	try {
		m_sim->TimeStep(); // May throw Exception.
	}
	catch(const exception& e) {
		message = QString::fromStdString(e.what());
	}
	catch(...) {
		message = "Unknown exception in sim.";
	}

	emit Worked(message);
}

} // namespace
} // namespace
