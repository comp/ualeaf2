#ifndef COUPLED_CLI_CONTROLLER_H_
#define COUPLED_CLI_CONTROLLER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CoupledCliController.
 */

#include "CliSimulationTask.h"

#include "../../cpp_sim/util/clock_man/Timeable.h"
#include "adapt2rfp/adapt2rfp.h"
#include "session/ISession.h"
#include <QObject>
#include <chrono>
#include <memory>
#include <string>

namespace SimPT_Shell {

namespace Session {
	class SimSessionCoupled;
}

namespace Ws {
	class CliWorkspace;
}

/**
 * Command line interface application controller.
 */
class CoupledCliController : public QObject, public SimPT_Sim::ClockMan::Timeable<>
{
	Q_OBJECT
public:
	/**
	 * Virtual destructor.
	 */
	virtual ~CoupledCliController();

	/**
	 * Create controller.
	 * @param workspace   work space that it operates in
	 * @param quiet       mode (verbosity)
	 * @throw Exception   in case of work space problems
	 */
	static std::shared_ptr<CoupledCliController> Create(const std::string& workspace, bool quiet);

	/**
	 * Execute the task defined via prior SetTask call.
	 * @return    status indicates whether task ran OK.
	 */
	int Execute(CliSimulationTask task);

	/**
	 * Execution timings in duration units specified in Timeable base class.
	 * @return      records with timing info
	 */
	Timings GetTimings() const;

	/**
	 * Request termination of the simulation
	 */
	void Terminate();

signals:
	/**
	 * Private signal, to be used for signaling that the simulation should stop
	 */
	void TerminationRequested();

private:
	/**
	 * Constructor (use Create method).
	 * @param workspace_model   work space that it operates in
	 * @param quiet             mode (verbosity)
	 */
	CoupledCliController(std::shared_ptr<Ws::CliWorkspace> workspace_model, bool quiet);

	/**
	 * Query whether operating in quiet mode.
	 */
	bool IsQuiet() const;

	/**
	 * Open a project with a directory and sim data file from the controller workspace.
	 * @param project_name       name of project directory to initialize with
	 * @param file_name          name of file to initialize with
	 */
	std::shared_ptr<Session::SimSessionCoupled> OpenProject(const std::string& project_name, const std::string& file_name);

	/**
	 * Let simulator run until termination condition is met.
	 * @return EXIT_SUCCESS/EXIT_FAILURE.
	 */
	int SimulatorRun(CliSimulationTask task);

	/**
	 * Handler for SIGINT interrupt signal.
	 */
	void SigIntHandler(int sig);

	/**
	 * Handler for Qt errors.
	 */
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	void SigQtHandler(QtMsgType type, const QMessageLogContext&, const QString&);
#else
	void SigQtHandler(QtMsgType type, const char* msg);
#endif

	/**
	 * Display error to user.
	 */
	static void UserError(const std::string& msg);

	/**
	 * Display message to user.
	 */
	static void UserMessage(const std::string& msg);

private:
	typedef SimShell::Session::ISession::InfoMessageReason InfoMessageReason; // Because of Qt wanting to match parameter types of signals and slots by string comparison

private slots:
	/**
	 * Slot to receive the simulation info message from the project
	 */
	void SimulationInfo(const std::string &message, const InfoMessageReason &reason);

	/**
	 * Slot to receive an simulation error from the project
	 */
	void SimulationError(const std::string &error);

private:
	/// Prototype for SIGINT interrupt handler.
	typedef void (SigIntHandlerType)(int);

	/// Adaptor turns std::function into SigIntHandlerType raw function type.
	typedef UA_CoMP_Adapt2rfp::Adaptor<SigIntHandlerType> SigIntAdaptorType;

	/// Prototype for Qt error handler.
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	typedef void (SigQtHandlerType)(QtMsgType, const QMessageLogContext&, const QString&);
#else
	typedef void (SigQtHandlerType)(QtMsgType, const char*);
#endif

	/// Adaptor turns std::function into SigQtHandlerType raw function type.
	typedef UA_CoMP_Adapt2rfp::Adaptor<SigQtHandlerType> SigQtAdaptorType;

private:
	/// Work space with working directories for projects.
	std::shared_ptr<Ws::CliWorkspace> m_workspace_model;

	/// Controller in quiet mode.
	bool m_quiet;

	/// Adapt controller method to raw function pointer for interrupt handler.
	SigIntAdaptorType  m_sig_int_adaptor;

	/// Adapt controller method to raw function pointer for qt error handler.
	SigQtAdaptorType  m_sig_qt_adaptor;

        /// Timing records.
	Timings  m_timings;
};

} // namespace

#endif // end_of_include_guard
