/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CliController.
 */

#include "CliController.h"

#include "converter/FileConversion.h"
#include "converter/StepSelection.h"
#include "session/SimSession.h"
#include "util/misc/log_debug.h"
#include "util/misc/Exception.h"
#include "util/misc/StringUtils.h"
#include "workspace/MergedPreferences.h"

#include <QApplication>
#include <QEventLoop>
#include <QFileInfo>

#include <algorithm>
#include <cassert>
#include <csignal>
#include <sstream>
#include <string>

namespace SimPT_Shell {

using namespace std;
using namespace std::chrono;
using namespace std::placeholders;
using namespace SimPT_Sim::Util;


CliController::CliController(shared_ptr<Ws::CliWorkspace> workspace_model, bool quiet)
	: m_workspace_model(move(workspace_model)), m_quiet(quiet),
	  m_sig_int_adaptor(bind(&CliController::SigIntHandler, this, placeholders::_1)),
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	  m_sig_qt_adaptor(bind(&CliController::SigQtHandler, this, placeholders::_1, placeholders::_2, placeholders::_3))
#else
	  m_sig_qt_adaptor(bind(&CliController::SigQtHandler, this, placeholders::_1, placeholders::_2))
#endif
{
	// register interrupt handler (adaptor auto-converts to function pointer).
	signal(SIGINT, m_sig_int_adaptor);

	// Register Qt error handler (adaptor auto-converts to function pointer).
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	qInstallMessageHandler(m_sig_qt_adaptor);
#else
	qInstallMsgHandler(m_sig_qt_adaptor);
#endif
}

CliController::~CliController()
{
	signal(SIGINT, SIG_DFL);
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	qInstallMessageHandler(0);
#else
	qInstallMsgHandler(0);
#endif
}

shared_ptr<CliController> CliController::Create(const string& workspace, bool quiet)
{
	const string here = string(VL_HERE) + " exception:\n";
	shared_ptr<CliController>  ptr;

	try {
		// QApplication must have been instantiated for the Qt services we use.
		// QApp i.o. QCoreApp because of production of graphics output.
		if (static_cast<QApplication*>(QApplication::instance()) == nullptr) {
			throw Exception(here + "No QApplication instantiated in calling context!");
		}

		// Create workspace model.
		auto workspace_model = make_shared<Ws::CliWorkspace>(workspace);
		if (!quiet) {
			UserMessage("Successfully opened workspace " + workspace_model->GetPath());
		}
		ptr = shared_ptr<CliController>(new CliController(move(workspace_model), quiet));
		if (ptr == nullptr) {
			throw Exception(here + "Produced nullptr for " + workspace);
		}

	} catch (exception& e) {
		UserError(here + e.what());
		throw;
	} catch(...) {
		UserError(here + "Unknown exception!");
	}

	return ptr;
}

shared_ptr<CliController> CliController::Create(shared_ptr<Ws::CliWorkspace> workspace, bool quiet)
{
	const string here = string(VL_HERE) + " exception:\n";
	shared_ptr<CliController>  ptr;
	assert((workspace != nullptr) && "Workspace pointer cannot be nullptr.");

	try {
		// QApplication probably exists because a workspace pointer has been acquired.
		// Still we check.
		if (static_cast<QApplication*>(QApplication::instance()) == nullptr) {
			throw Exception(here + "No QApplication instantiated in calling context!");
		}

		ptr = shared_ptr<CliController>(new CliController(move(workspace), quiet));
		if (ptr == nullptr) {
			throw Exception(here + "Produced nullptr for " + workspace->GetPath());
		}

	} catch (exception& e) {
		UserError(here + e.what());
		throw;
	} catch(...) {
		UserError(here + "Unknown exception!");
	}

	assert((ptr != nullptr) && "CliController pointer cannot be nullptr.");
	return ptr;
}

int CliController::Execute(CliConverterTask task)
{
	int exit_status   = EXIT_SUCCESS;
	Session::SimSession::Stopclock  chrono_total("converter", true);

	StepSelection s;
	s.SetSelectionText(task.GetTimeStepFilter());

	auto project = m_workspace_model->Get(task.GetProjectName());

	std::string inputfilter= task.GetInputFormatFilter();
	std::transform(inputfilter.begin(), inputfilter.end(), inputfilter.begin(), ::tolower);

	auto format = task.GetOutputFormat();
	auto prefs  = SimShell::Ws::MergedPreferences::Create(m_workspace_model, project);
	string prefix = "leaf";

	vector<int> timesteps;
	for (auto& file : *project) {
		bool valid_inputformat=true;

		if (inputfilter!="")
		{
			auto tissue_file = static_pointer_cast<Ws::StartupFileBase>(file.second);
			std::string path = tissue_file->GetPath();
			std::transform(path.begin(), path.end(), path.begin(), ::tolower);

			if (path.length()>=inputfilter.length()) {
				valid_inputformat = path.substr(
				        path.length()-inputfilter.length(), inputfilter.length())==inputfilter;
			}
		}		
		if (valid_inputformat) {
			for (int step : static_pointer_cast<Ws::StartupFileBase>(file.second)->GetTimeSteps()) {
				timesteps.push_back(step);
			}
		}
	}

	vector<int> filtered_timesteps;
	for (int step : timesteps) {
		if (s.Contains(step)) {
			filtered_timesteps.push_back(step);
		}
	}

	// Instantiate file converter object.
	FileConversion converter(
		static_cast<Ws::Project*>(project.get()), filtered_timesteps, prefs,
		format, project->GetPath(), prefix);

	// Execute the converter.
	converter.Run(
	        [&](int i) { UserMessage("Converted " + ToString(i) + " files."); }
	);

	m_timings.Record(chrono_total.GetName(), chrono_total.Get());
	return exit_status;
}


int CliController::Execute(CliSimulationTask task)
{
	int exit_status   = EXIT_SUCCESS;
	Session::SimSession::Stopclock  chrono_total("total", true);

	try {
		exit_status = SimulatorRun(task);
	}
	catch (std::exception& e) {
		exit_status = EXIT_FAILURE;
		throw SimPT_Sim::Util::Exception(e.what());
	}

	m_timings.Record(chrono_total.GetName(), chrono_total.Get());
	return exit_status;
}

CliController::Timings CliController::GetTimings() const
{
	return m_timings.GetRecords();
}

bool CliController::IsQuiet() const
{
	return m_quiet;
}

void CliController::SigIntHandler(int )
{
	signal(SIGINT, SIG_DFL);
	emit TerminationRequested();
}

#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
void CliController::SigQtHandler(QtMsgType type, const QMessageLogContext& /*context*/, const QString& msg)
{
	switch (type) {
	case QtDebugMsg:
		UserMessage("Qt debug: " + msg.toStdString());
		break;
    #if (QT_VERSION >= QT_VERSION_CHECK(5,5,0))
	case QtInfoMsg:
		UserMessage("Qt info: " + msg.toStdString());
		break;
    #endif
	case QtWarningMsg:
		UserMessage("Qt warning: " + msg.toStdString());
		break;
	case QtCriticalMsg:
		UserError("Qt critical: " + msg.toStdString());
		break;
	case QtFatalMsg:
		UserError("Qt fatal: " + msg.toStdString());
		abort();
		break;
	}
}
#else
void CliController::SigQtHandler(QtMsgType type, const char* msg)
{
	switch (type) {
	case QtDebugMsg:
		UserMessage("Qt debug: " + string(msg));
		break;
	case QtWarningMsg:
		UserMessage("Qt warning: " + string(msg));
		break;
	case QtCriticalMsg:
		UserError("Qt critical: " + string(msg));
		break;
	case QtFatalMsg:
		UserError("Qt fatal: " + string(msg));
		abort();
		break;
	}
}
#endif

void CliController::SimulationError(const std::string& error)
{
	UserError(error);
	emit TerminationRequested();
}

void CliController::SimulationInfo(const std::string& message,
        const Session::ISession::InfoMessageReason& reason)
{
	switch (reason) {
	case Session::ISession::InfoMessageReason::Stepped:
		if (!IsQuiet()) {
			UserMessage(message);
		}
		break;
	case Session::ISession::InfoMessageReason::Stopped:
		emit TerminationRequested();
		break;
	case Session::ISession::InfoMessageReason::Terminated:
		emit TerminationRequested();
		break;
	default:
		break;
	}
}

int CliController::SimulatorRun(CliSimulationTask task)
{
	int exit_status               = EXIT_FAILURE;
	const string here             = string(VL_HERE) + "> ";
	const string project_name     = task.GetProjectName();
	auto project                  = m_workspace_model->Get(project_name);
	const string file             = task.IsLeafSet() ? task.GetLeaf() : (--project->end())->first;

	project->Open(file);
	if (project->IsOpened()) {
		// Create root viewer and initialize its subviewers
		auto rootViewer = project->Session().CreateRootViewer();

		connect(&project->Session(), SIGNAL(InfoMessage(const std::string&, const InfoMessageReason&)),
			this, SLOT(SimulationInfo(const std::string&, const InfoMessageReason&)));
		connect(&project->Session(), SIGNAL(ErrorMessage(const std::string&)),
			this, SLOT(SimulationError(const std::string&)));
		if (!IsQuiet()) {
			UserMessage("Opened project " + project_name + " and " + file);
		}

		// Run simulation inside an event loop, so this thread can receive signals from
		// possibly other threads.
		// If TerminationRequested() would for some reason already be emitted during
		// StartSimulation, then the connection to quit will be queued, so that the quit()
		// slot of eventLoop will not be called before the call to exec()
		QEventLoop eventLoop(this);
		connect(this, SIGNAL(TerminationRequested()), &eventLoop, SLOT(quit()), Qt::QueuedConnection);
		project->Session().StartSimulation(task.IsNumStepsSet() ? task.GetNumSteps() : -1);
		eventLoop.exec();

		exit_status = EXIT_SUCCESS;
		// Get timings from simulation.
		m_timings.Merge(project->Session().GetTimings());

		project->Close();
	} else {
		exit_status = EXIT_FAILURE;
		UserError(here + "Failed to open project " + project_name + " and " + file);
	}

	return exit_status;
}

void CliController::Terminate()
{
	emit TerminationRequested();
}

void CliController::UserError(const string& msg)
{
	std::cerr << msg << std::endl;
}

void CliController::UserMessage(const string& msg)
{
	std::cout << msg << std::endl;
}

} // namespace
