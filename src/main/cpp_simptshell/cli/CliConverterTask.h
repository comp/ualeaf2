#ifndef CLI_CONVERTER_TASK_H_
#define CLI_CONVERTER_TASK_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for CliConverterTask.
 */

#include "converter/IConverterFormat.h"

#include <iomanip>
#include <iostream>
#include <string>

namespace SimPT_Shell {

/**
 * A CliConverterTask represents an invocation of the converter from the command line.
 */
class CliConverterTask
{
public:
	/// Straight constructor.
	CliConverterTask(const std::string& project, std::string timestep_filter,
	        IConverterFormat* output_format, std::string inputformat_filter="")
			: m_project_name(project), m_timestep_filter(timestep_filter),
			  m_output_format(output_format), m_inputformat_filter(inputformat_filter)
	{
	}

	/// Plain getter.
	const std::string GetTimeStepFilter() const { return m_timestep_filter; }

	/// Plain getter.
	IConverterFormat* GetOutputFormat() const { return m_output_format; }

	/// Plain getter.
	const std::string GetProjectName() const {return m_project_name; }


	/// Plain getter.
	const std::string GetInputFormatFilter() const {return m_inputformat_filter; }



private:
	std::string             m_project_name;         ///< Name of project to execute.
	std::string             m_timestep_filter;      ///< List of timesteps to convert
	IConverterFormat*       m_output_format;        ///< Output format
	std::string             m_inputformat_filter;   ///< Filter the input format e.g. .xml, .xml.gz or empty to accept all.

};

inline
std::ostream& operator<<(std::ostream& os, const CliConverterTask& task)
{
	os << "CliConverterTask: project = " << task.GetProjectName() << std::endl
		<< " timestep_filter = " << task.GetTimeStepFilter()
		<< ", output_format = " << task.GetOutputFormat()->GetName() << ", inputformat_filter="<<task.GetInputFormatFilter();

	return os;
}

} // namespace

#endif // include guard
