#ifndef CLI_WORKSPACE_MANAGER_H_
#define CLI_WORKSPACE_MANAGER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CliWorkspaceManager.
 */

#include "../workspace/CliWorkspace.h"

#include <memory>
#include <string>

namespace SimPT_Shell {

using namespace SimShell;
using namespace SimPT_Shell;
using namespace SimPT_Shell::Ws;

/**
 * Workspace manager for cli applications.
 */
class CliWorkspaceManager
{
public:
	/**
	 * Return the input file that already existed or has been newly created.
	 * @param   project               project that owns the input file
	 * @param   input_file            input file name
	 * @param   create_if_not_exist   create input file from reference if it did not exist yet
	 * @param   ref_input_file        reference input file in case of creation
	 */
	static SimShell::Ws::IProject::FileIterator OpenLeaf(
		std::shared_ptr<SimShell::Ws::IProject> project, const std::string& input_file,
		bool create_if_not_exists, std::shared_ptr<SimShell::Ws::IFile> ref_input_file);

	/**
	 * Return the sim data file that already existed or has been newly created.
	 * @param   project              project that owns the data file
	 * @param   file_name            sim data file name
	 * @param   create_if_not_exist  create file from reference if it did not exist yet
	 * @param   ref_ws_path          path to reference workspace in case of creation
	 * @param   ref_project          name of reference project in case of creation
	 * @param   ref_file_name        name of reference data file in case of creation
	 */
	static SimShell::Ws::IProject::FileIterator OpenLeaf(
		std::shared_ptr<SimShell::Ws::IProject> project, const std::string& file_name,
		bool create_if_not_exist = true,
		const std::string& ref_ws_path = "", const std::string& ref_project = "",
		const std::string& ref_file_name = "");

	/**
	 * Return the project that already existed or has been newly created.
	 * @param   workspace             workspace that owns project
	 * @param   project               name of the project
	 * @param   create_if_not_exists  create empty project if it does not exist yet
	 */
	static SimShell::Ws::IWorkspace::ProjectIterator OpenProject(
		std::shared_ptr<CliWorkspace> workspace, const std::string& project,
		bool create_if_not_exist = true);

	/**
	 * Return workspace that already existed or has been newly created (in empty state).
	 * @param   base_path             base path to workspace
	 * @param   workspace             name of workspace directory (relative to base path)
	 * @param   create_if_not_exist   creates empty workspace if it does not exist yet
	 */
	static std::shared_ptr<CliWorkspace> OpenWorkspace(
		const std::string& base_path, const std::string& workspace,
		bool create_if_not_exist = true);

private:
	/**
	 * Create a file in an existing project by copying a reference file.
	 * @param    project             project the file will be added to
	 * @param    from_path           file path of reference data to copy
	 * @param    to                  name of data file in the new project
	 */
	static SimShell::Ws::IProject::FileIterator
	CreateLeaf(const std::shared_ptr<SimShell::Ws::IProject>& project,
		const std::string& from_path, const std::string& to);

	/**
	 * Create a new (empty) project in an existing workspace.
	 * @param   workspace             project will be created in the workspace
	 * @param   project               name of the project
	 */
	static SimShell::Ws::IWorkspace::ProjectIterator
	CreateProject(const std::shared_ptr<SimShell::Ws::IWorkspace>& workspace, const std::string& project);

	/**
	 * Create a new cli workspace.
	 * @param   base_path            base path where the workspace will be located
	 * @param   workspace            name of the workspace
	 */
	static std::shared_ptr<Ws::CliWorkspace>
	CreateWorkspace(const std::string& base_path, const std::string& workspace);
};

} // namespace

#endif // end-of-include-guard
