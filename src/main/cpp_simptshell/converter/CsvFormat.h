#ifndef SIMPT_SHELL_CONVERTER_CSV_FORMAT_H_INCLUDED
#define SIMPT_SHELL_CONVERTER_CSV_FORMAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of csv converter format.
 */

#include "IConverterFormat.h"
#include "ExporterFormat.h"
#include "exporters/CsvExporter.h"

namespace SimPT_Shell {

/**
 * CSV converter format specifications.
 */
class CsvFormat : public ExporterFormat<CsvExporter> {
public:
	virtual std::string GetName() const { return "CSV"; }

	virtual bool IsPostProcessFormat() const { return true; }
};

} // namespace

#endif // end_of_include_guard
