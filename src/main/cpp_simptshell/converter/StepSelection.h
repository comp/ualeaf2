#ifndef SIMPT_SHELL_STEP_SELECTION_H_INCLUDED
#define SIMPT_SHELL_STEP_SELECTION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StepSelection
 */

#include <QRegExp>
#include <set>
#include <string>

namespace SimPT_Shell {

/**
 * Class handling user input of ranges of steps.
 * Extracts comma-separated step ranges in the format 'start[-stop[:step]]' from a string.
 */
class StepSelection
{
public:
	/// Constructs a selection with an empty ranges text (ie. everything is selected)
	StepSelection();

	/// Destructor
	virtual ~StepSelection();


	/// Sets the selected ranges of steps
	void SetSelectionText(const std::string &ranges);


	/// Applies the selection to a set of available steps
	std::set<int> ApplySelection(const std::set<int> &available) const;

	/// Checks whether the selection contains a specified step
	bool Contains(int step) const;

public:
	static const QRegExp g_range_regex;      ///< Regex for one range (start[-stop[:step]])
	static const QRegExp g_repeated_regex;   ///< Regex for a series of repeated ranges, separated by , of ;

private:
	bool m_all_selected;
	std::set<int> m_selected_steps;
};

} // namespace

#endif // end_of_include_guard
