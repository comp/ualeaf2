#ifndef SIMPT_SHELL_CONVERTER_ICONVERTERFORMAT_H_
#define SIMPT_SHELL_CONVERTER_ICONVERTERFORMAT_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IConverterFormat
 */

#include "sim/SimState.h"
#include "workspace/MergedPreferences.h"

namespace SimPT_Shell {

/**
 * Interface for file convenvereter formats.
 */
class IConverterFormat
{
public:
	virtual ~IConverterFormat() {}

	virtual bool AppendTimeStepSuffix() const = 0;

	virtual std::string GetExtension() const = 0;

	virtual std::string GetName() const = 0;

	virtual bool IsPostProcessFormat() const=0;

	virtual void PreConvert(const std::string& target_path,
		std::shared_ptr<SimShell::Ws::MergedPreferences>) = 0;

	virtual void Convert(const SimPT_Sim::SimState& src) = 0;

	virtual void PostConvert() = 0;
};

} // namespace

#endif // end_of_include_guard
