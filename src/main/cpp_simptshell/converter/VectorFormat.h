#ifndef SIMPT_SHELL_CONVERTER_VECTOR_FORMAT_H_INCLUDED
#define SIMPT_SHELL_CONVERTER_VECTOR_FORMAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of vector graphics onverter format.
 */

#include "IConverterFormat.h"
#include "TimeStepPostfixFormat.h"

#include "exporters/VectorGraphicsExporter.h"
#include "sim/Sim.h"

namespace SimPT_Shell {

/**
 * Vector graphics converter format specifications.
 */
class VectorFormat : public TimeStepPostfixFormat
{
public:
	using FileFormat = VectorGraphicsPreferences::Format;

	VectorFormat(FileFormat f) : m_format(f) {}

	virtual bool AppendTimeStepSuffix() const { return true; };

	virtual bool IsPostProcessFormat() const { return true; }

	virtual std::string GetExtension() const
	{
		switch (m_format) {
		case FileFormat::Pdf:
			return "pdf";
		case FileFormat::Eps:
			return "eps";
		}
		return std::string();
	}

	virtual std::string GetName() const
	{
		switch (m_format) {
		case FileFormat::Pdf:
			return "PDF";
		case FileFormat::Eps:
			return "EPS";
		}
		return std::string();
	}

	virtual void Convert(const SimPT_Sim::SimState& src);

private:
	FileFormat m_format;
};

} // namespace

#endif // end_of_include_guard
