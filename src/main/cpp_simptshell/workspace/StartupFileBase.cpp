/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StartupFileBase.
 */


#include "StartupFileHdf5.h"
#include "StartupFileXml.h"
#include "StartupFileXmlGz.h"

#include <algorithm>
#include <string>

namespace SimPT_Shell {
namespace Ws {

using namespace std;
using namespace SimPT_Sim::Util;

StartupFileBase::StartupFileBase(const string& path)
	: m_path(path)
{
}

const string& StartupFileBase::GetPath() const
{
	return m_path;
}

const StartupFileBase::ConstructorType StartupFileBase::Constructor(
	[](const string& filename, const string& path) -> shared_ptr<IFile>
	{
		auto endswith = [](const string& s1, const string& s2) {
			if (s2.length() > s1.length()) {
				return false;
			}
			std::string s1lower = s1;
			std::transform(s1lower.begin(), s1lower.end(), s1lower.begin(), ::tolower);
			std::string s2lower = s2;
			std::transform(s2lower.begin(), s2lower.end(), s2lower.begin(), ::tolower);

			return s1lower.compare(s1lower.length()-s2lower.length(), string::npos, s2lower) == 0;
		};

		if (endswith(filename, ".h5")) {
			return make_shared<StartupFileHdf5>(path);
		} else if (endswith(filename, ".xml")) {
			return make_shared<StartupFileXml>(path);
		} else if (endswith(filename, ".xml.gz")) {
			return make_shared<StartupFileXmlGz>(path);
		}
		return nullptr;
	}
);


} // namespace
} // namespace
