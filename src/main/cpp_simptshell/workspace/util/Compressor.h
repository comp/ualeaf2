#ifndef SIMPT_SHELL_WS_UTIL_COMPRESSOR_H_INCLUDED
#define SIMPT_SHELL_WS_UTIL_COMPRESSOR_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Compressor.
 */

#include <QObject>

class QAction;

namespace SimPT_Shell {
namespace Ws {
namespace Util {

/**
 * Compression / decompression actions.
 */
class Compressor : public QObject
{
	Q_OBJECT
public:
	Compressor(const std::string& path);

	QAction* GetActionCompress() const;
	QAction* GetActionDecompress() const;

private slots:
	void SLOT_Compress();
	void SLOT_Decompress();

private:
	QAction*     m_a_compress;
	QAction*     m_a_decompress;
	std::string  m_path;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_include_guard
