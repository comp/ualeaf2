/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StartupFilePtree.
 */

#include "workspace/StartupFilePtree.h"

#include "session/SimSession.h"
#include "session/SimSessionCoupled.h"
#include "workspace/MergedPreferences.h"
#include "util/misc/Exception.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/optional.hpp>

namespace SimPT_Shell {
namespace Ws {

using namespace std;
using namespace boost::property_tree;
using boost::optional;
using namespace SimPT_Sim::Util;
using namespace SimShell::Ws;

StartupFilePtree::StartupFilePtree(const string& path)
	: StartupFileBase(path)
{
}

shared_ptr<SimShell::Session::ISession>
StartupFilePtree::CreateSession(shared_ptr<IProject> proj, shared_ptr<IWorkspace> ws) const
{
	shared_ptr<Session::ISession> session;
	ptree pt;

	auto prefs = MergedPreferences::Create(ws, proj);
	try {
		pt = ToPtree();
	} catch (exception& e) {
		throw Exception("Could not open \"" + m_path + "\": " + e.what());
	}

	optional<ptree&> coupled_pt = pt.get_child_optional("vleaf2.coupled_project");
	if (!coupled_pt) {
		// It is a regular project.
		session = make_shared<Session::SimSession>(prefs, pt);
	} else {
		// It is a coupled project.
		session = make_shared<Session::SimSessionCoupled>(prefs, pt, ws);
	}

	return session;
}

SimPT_Sim::SimState StartupFilePtree::GetSimState(int timestep) const
{
	try {
		ptree pt = ToPtree();
		if (pt.get<int>("vleaf2.sim_step") == timestep) {
			SimPT_Sim::Sim s;
			s.Initialize(pt);
			return s.GetState();
		} else {
			return SimPT_Sim::SimState();
		}
	} catch (exception& e) {
		throw Exception("GetSimStates(): Could not open \"" + m_path + "\": " + e.what());
	}
}

vector<int> StartupFilePtree::GetTimeSteps() const
{
	vector<int> result;
	try {
		ptree pt = ToPtree();
		result.push_back(pt.get<int>("vleaf2.sim_step"));
	} catch (exception& e) {
		throw Exception("GetSimSteps() exception: \"" + m_path + "\": " + e.what());
	}
	return result;
}

} // namespace
} // namespace

