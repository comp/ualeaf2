#ifndef SIMPT_WS_STARTUP_BASE_H_INCLUDED
#define SIMPT_WS_STARTUP_BASE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StartupFileBase.
 */

#include "sim/SimState.h"
#include "workspace/IFile.h"
#include "workspace/IWorkspace.h"

namespace SimPT_Shell {
namespace Ws {

/**
 * Base class representing the file types used to initiate a session.
 *
 * @see SimShell::Session::ISession
 */
class StartupFileBase : public SimShell::Ws::IFile
{
public:
	/// Constructor.
	/// @param path  Path to file.
	StartupFileBase(const std::string& path);

	/// @see SimShell::Ws::IFile::GetPath()
	virtual const std::string& GetPath() const;

	/// Get simstate with given timestep.
	virtual SimPT_Sim::SimState GetSimState(int timestep) const = 0;

	/// Get list of sim steps contained in the file.
	virtual std::vector<int> GetTimeSteps() const = 0;

	static const ConstructorType Constructor;

protected:
	const std::string m_path;
};

} // namespace
} // namespace

#endif  // end_of_include_guard
