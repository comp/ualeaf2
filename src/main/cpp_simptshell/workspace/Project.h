#ifndef SIMPT_WS_SIMPT_PROJECT_H_INCLUDED
#define SIMPT_WS_SIMPT_PROJECT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimPT projects.
 */

#include "../../cpp_simshell/workspace/Project.h"

#include "StartupFileBase.h"
#include "WorkspaceFactory.h"

namespace SimShell {
namespace Ws {

	extern template class Project<SimPT_Shell::Ws::StartupFileBase,
		SimPT_Shell::Ws::WorkspaceFactory::g_project_index_file>;

} // namespace
} // namespace


namespace SimPT_Shell {

class ConverterWindow;

namespace Ws {

	using SimPTProjectBase = SimShell::Ws::Project<StartupFileBase,
		                        SimPT_Shell::Ws::WorkspaceFactory::g_project_index_file> ;

	/**
	 * Abstraction of project info on filesystem, as well as a maintainer
	 * of a work session. SimPT-specific instantiation of
	 * SimShell::Ws::Project template.
	 *
	 * @see SimPT_Shell::Ws::Workspace SimShell::Ws::Project
	 */
	class Project : public SimPTProjectBase
	{
	public:
		Project(const std::string& path,
		             const std::string& prefs_file,
		             const std::shared_ptr<SimShell::Ws::IWorkspace>&);

		virtual ~Project();

		static const ConstructorType Constructor;

		/// @see SimShell::Ws::IProject
		virtual std::vector<QAction*> GetContextMenuActions() const;

		/// @see SimShell::Ws::IProject
		virtual std::vector<WidgetCallback> GetWidgets();

		/// Overrides SimShell::Ws::Project::Refresh
		virtual void Refresh();

	private:
		std::weak_ptr<ConverterWindow> m_converter_window;
	};

} // namespace
} // namespace

#endif  // end_of_include_guard
