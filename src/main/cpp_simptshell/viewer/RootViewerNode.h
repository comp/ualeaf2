#ifndef VIEWER_ROOT_VIEWER_NODE_H_INCLUDED
#define VIEWER_ROOT_VIEWER_NODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for RootViewerNode.
 */

#include "gui/controller/AppController.h"
#include "sim/CoupledSim.h"
#include "sim/Sim.h"
#include "viewer/SubjectNode.h"

class QWidget;

namespace SimShell {
	namespace Viewer {
		extern template class SubjectNode<SimPT_Sim::Sim>;
	}
}

namespace SimPT_Shell {
namespace Viewer {

/**
 * Viewer node that acts as the root of the tree of viewers.
 * Doesn't manage a viewer instance.
 * Informs its children viewers of changes to its subject (which it co-owns).
 */
template <typename SubjectType>
class RootViewerNode : public SimShell::Viewer::SubjectNode<SubjectType>
{
public:
        RootViewerNode(std::shared_ptr<SimShell::Ws::MergedPreferences>,
                std::shared_ptr<SubjectType> subject,
                SimShell::Gui::Controller::AppController* a = nullptr);
};

} // namespace
} // namespace

#endif
