#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

# - try to find cloc tool
#
# Cache Variables:
#  SLOCCOUNT_EXECUTABLE
#
# Non-cache variables you might use in your CMakeLists.txt:
#  SLOCCOUNT_FOUND
#
# Requires these CMake modules:
#  FindPackageHandleStandardArgs 
#
#############################################################################

file( TO_CMAKE_PATH   "${CLOC_ROOT_DIR}"   CLOC_ROOT_DIR )
set( CLOC_ROOT_DIR   "${CLOC_ROOT_DIR}"   CACHE   PATH   "Path to search for cloc" )

# If we have a custom path, look there first.
if( CLOC_ROOT_DIR )
	find_program( CLOC_EXECUTABLE   NAME cloc   PATHS "${CLOC_ROOT_DIR}" )
endif()

find_program( CLOC_EXECUTABLE    NAME cloc    PATHS   /usr/local/bin /op/local/bin /opt/cloc/bin /usr/local/cloc/bin )

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( cloc   DEFAULT_MSG   CLOC_EXECUTABLE )

if( CLOC_FOUND  OR  CLOC_MARK_AS_ADVANCED )
	mark_as_advanced( CLOC_ROOT_DIR)
endif()

mark_as_advanced( CLOC_EXECUTABLE )

#############################################################################