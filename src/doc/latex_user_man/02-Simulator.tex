%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################

\chapter{SimPT operation}
\label{chap:simulator}


The SimPT simulator can be used with two different modes of operation:
\begin{itemize}
	\item via the command line for long running simulations; in this case all simulation parameters defined in the input data are fixed during the program run.
	\item in an interactive fashion, using a graphical user interface; in this case parameters are initially read from the input data file but can be modified by the user at any time during the simulation through the user interface.
\end{itemize}
These modes of operation can be mixed as convenient. The simulator also has restart capability, i.e. simulations can be extended starting where the previous simulation run ended. In what follows, we describe the main elements required for setting up the basic work flow.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The work shell}
The core SimPT simulator (and to a large extent also the parameter explorer and the editor) comes wrapped in a work shell to facilitate organization of the simulation work. The operation  of that shell refers to a number of concepts:
\begin{compactitem}
	\item workspaces
	\item projects
	\item sessions
	\item data files
	\item viewers
	\item post-processors
\end{compactitem}
These concepts are explained briefly here and in the following sections of this chapter.
%
\begin{description}
%
	\item[Workspace] \ \newline
	A workspace is a directory on disk that holds all the resources that you interact with via the simulator. They are:
		\begin{compactitem}
			\item The subdirectories, which each define a project.
			\item The defaults for graphical (\texttt{.simPT-gui-preferences.xml}) and command line (\texttt{.simPT-cli-preferences.xml}) preferences.
			\item A descriptor (\texttt{.simPT-workspace.xml}) of the workspace state (list of projects) and application state at the time of closing the application. It is used to restore that state when opening the application again.
		\end{compactitem}
These resources are all managed via the application.
%
	\item[Project] \ \newline
	A project is a directory whose contents represent a single simulation history, 
	that may have been built by multiple runs of the simulator. It contains all 
	resources associated with that history:
		\begin{compactitem}
			\item It has states of the simulated system at various times in a number  of files (e.g., with the \texttt{xml.gz} output format), one for each time point, or in a single file (e.g., with the \texttt{hdf5} output format).
			\item It may contain various post processing files, i.e., files that do not serve as input for a future simulator run, e.g., \texttt{png} images of the system.
			\item Project graphical (\texttt{.simPT-gui-preferences.xml}) and command 
			line (\texttt{.simPT-cli-preferences.xml}) preferences that override the 
			workspace default preferences. Such preferences customize some simulator 
			actions (e.g., the visualization of results, file format when saving results, 
			at what time intervals to produce output, etc).
			\item A descriptor (\texttt{.simPT-project.xml}) of the project specific 
			application state at the time of closing the application. It is used to restore that state when opening the application again.
		\end{compactitem}
	A project is either open or closed. The open project is the focus of the simulator actions. Only one project can be open in a workspace at any given time.
%
	\item[Session] \ \newline
	A session represents the activation of the simulator core within an open project.  Within a session, the simulator can do a single time step, or run, pause and run some more. The project is the exclusive owner of the session and closing the project closes its session. The session only has in-memory structures, an important one being a descriptor of the state of the viewers that observe the simulator and produce output whenever it has taken a time step.
%
	\item[SimPT data file] \ \newline
	A simPT data file contains a full description of the simulation state. If one is using \texttt{xml} or \texttt{xml.gz} format, the this state information refers to a single point in time and the simulation history is a sequence of such file, one for each time. If one is using \texttt{HDF5} format, all states are stored in a single binary file. More information on the formats can be found later on in this annual. The state information is complete: it contains all relevant data to start or restart a simulation run. 
%
	\item[Viewers] \ \newline
	We have implemented the Model-View-Controller (MVC) design pattern, a well known design in computer science. This means that code for generating simulator state output to file, output to screen, output to logging files etc. is node built into the simulator. This makes code of the simulator more transparent and adding or changing output feature more flexible and extensible. Instead the simulator connects via signals (e.g indicating a time step has been completed) to (multiple) viewers. Whenever a viewer receives a signal it processes the signal according to its own logic. A file log viewer will post a message to a log file. A graphical viewer will update the on-screen image of the plant system being simulated. 
%
	\item[Post-processors] \ \newline
	As the name indicates the post processor are to be used after the simulation has been run. The produce post-processing output, e.g. a sequence of images of the simulated system in \texttt{png} format. The reason for making this a post-processing activity is twofold. Firstly, it simplifies programming logic. Secondly, when generating images of a growing organ, a scale must be set and often one knows the appropriate scale only at the last simulated time step. 
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Running the simulator in commandline mode
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running in command line mode}
The operation of the simulator from the command line is fairly self-documented. When one starts 
the executable with  'simPT\_sim -h' or 'simPT\_sim -\thinspace-help', one obtains the following explanation:
%
\begin{Verbatim}[fontsize=\footnotesize]
USAGE: 
   ./simPT_sim  [-h] [-l] [-m <cli|gui>] [--] <> ...

e.g. 
./simPT_sim --mode cli -w /path/to/workspace -p project_name

Where: 

   -h,  --help
     Displays usage information and exits.

   -l,  --list-modes
     List the available modes

   -m <cli|gui>,  --mode <cli|gui>
     The application mode

   --,  --ignore_rest
     Ignores the rest of the labeled arguments following this flag.
     
   <>  (accepted multiple times)
     application arguments
\end{Verbatim}

It indicates you should use a 'simPT\_sim -m cli' or 'simPT\_sim -\thinspace-mode cli' to activate the command 
line mode of the simulator. If one subsequently executes with the help option, namely 'simPT\_sim -m cli -h', 
one obtains the following information:
\begin{Verbatim}[fontsize=\footnotesize]
USAGE: 
   ./simPT_sim -m cli  [-z <HDF5|XML|XML.GZ>] [-o <BMP|CSV|CSV.GZ|JPEG|PLY
                       |PDF|PNG>] [-c <HDF5|XML|XML.GZ>] [-t <>] [-w
                       <WORKSPACE PATH>] [-p <PROJECT NAME>] [-f <TISSUE
                       FILE>] [-s <NUMBER OF STEPS>] [-q] [-r] [--]
                       [--version] [-h]

Where: 

   -z <HDF5|XML|XML.GZ>,  --input-format-filter <HDF5|XML|XML.GZ>
     Only use a specific input format

   -o <BMP|CSV|CSV.GZ|JPEG|PLY|PDF|PNG>,  --postprocess <BMP|CSV|CSV.GZ
      |JPEG|PLY|PDF|PNG>
     Postprocess mode (no simulation): Postprocess existing files in
     workspace.

   -c <HDF5|XML|XML.GZ>,  --convert <HDF5|XML|XML.GZ>
     Convert mode (no simulation): Convert existing files in workspace.

   -t <>,  --timestep-filter <>
     Filter timesteps to convert, (list of) ranges are accepted, e.g.
     "200-300,600".

   -w <WORKSPACE PATH>,  --workspace <WORKSPACE PATH>
     Path to workspace

   -p <PROJECT NAME>,  --project <PROJECT NAME>
     Name of project

   -f <TISSUE FILE>,  --file <TISSUE FILE>
     Tissue file in project

   -s <NUMBER OF STEPS>,  --stepcount <NUMBER OF STEPS>
     number of steps

   -q,  --quiet
     Quit mode (no output)

   -r,  --revision
     Revision identification

   --,  --ignore_rest
     Ignores the rest of the labeled arguments following this flag.

   --version
     Displays version information and exits.

   -h,  --help
     Displays usage information and exits.   
\end{Verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.70]{images/ScreenShotSimPTStart.png}}
	\end{center}
	\caption{Screen shot of the simulator started with the simPT\_Default\_workspace, that contains nine projects (left pane). Project SmithPhyllotaxis has been opened and the editor panes for workspace and project preferences and for simulation parameters have been opened. The pane at the bottom logs the important events.}
	\label{fig:workspace}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Running the simulator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running the simulator interactively}
Figure \ref{fig:workspace} shows a screen shot of SimPT open at a workspace 
containing 21 projects. The interface presents a number of panels. The panel on the 
left shows the projects in the workspace and their simulation data files. The top right panel is used to access the workspace preferences determining features such as which i/o viewers need to be enabled, what color scheme must be used and so on. This panel must be opened explicitly via the \texttt{Edit} pull down menu. The panel titled "Parameters" allows you to view and edit all configuration parameters of the simulation. The changes you make take effect in the time step following the edit. The panel ``Project Preferences'' allows you to overrule workspace preferences for a particular project. The bottom panel, which appears only when a project is currently open, provides a running log.

To start a simulation in interactive mode, using the graphical user interface, one has to double-click the simulator icon (or execute './simPT\_sim' at the command prompt). Following this one can select a workspace, or if one had in a previous session accepted a particular workspace as default, opens that workspace. Then simply open a project in your current workspace and select  an input file among those listed. Double-click the file to initiate a simulation session with this file as its starting point. Click 'run' in the \emph{Simulation} dropdown menu or press the 'R' key, to let the simulator start the time propagation. To stop the time propagation one can again press the 'R' key or select stop from the drop down menu. It is also possible to execute just a single step of the simulation by clicking 'single step'  in the same drop down menu or by pressing the 'S' key. A last option is to run the simulation for a set amount of steps. This can be done by clicking the 'multiple steps' option or pressing the 'M' key. A window will open which allows you to set the amount of steps to run. After entering the desired number of steps click 'OK' which will start the simulator.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dynamic parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic parameters}
All simulation parameters can be changed dynamically, i.e. every change takes effect 
at the start of time step following the change. This includes the selection of the 
model and of the time evolution algorithm. This change can be effected interactively 
at the discretion of the user or it can be a computed change, i.e., effected by the 
simulator when certain conditions are met such as number of cells exceeds a threshold 
or simulated time reaches a value. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.70]{images/ScreenShotSimPTStart2.png}}
	\end{center}
	\caption{Screen shot of the simulator started with the simPT\_Default\_workspace. Project SmithPhyllotaxis has been opened and the editor pane for workspace preferences is open. The viewers item and most of its sub items have been expanded.}
	\label{fig:workspace2}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Viewers}

\subsection{Multiple viewers}
A viewer is a component that produces output at every simulation step, while the 
simulation is running (as opposed to post-processors, that produce 
output \textit{after} the simulation run.
It is possible to attach multiple viewers to a running simulation and allow 
them to be individually enabled or disabled and to have individual preferences 
for stride and so on. Enabling and disabling viewers is dynamical, i.e., can be 
done during simulation. This is not only convenient but also relevant to performance, 
as image generation and output to file are computationally intensive activities. 


\subsection{Viewers}
The viewers allow one to present a view of the simulation log, the tissue 
images and the simulation data files. Viewers can be activated and deactivated via the \texttt{Viewers} pull down menu. 
The following Viewers are currently available for selection:
\begin{compactitem}
			\item XML File viewer: writes simulation output to separate xml simulation data file for each relevant time step.
			\item HDF5 File viewer: writes simulation output for all relevant time steps to the same HDF5 file.
			\item QT window: allows the user to observe the tissue image in a 
			separate QT Viewer window, as shown in Figure \ref{fig:qtviewer}, in this case with a color coding that reflects the relative size of the cell.
			\item Log dock window: lets user observe a running log in the bottom 'Log' panel, which appears only when a project is currently opened. It shows the path of the running project, running steps, time and the current number of cells.
			\item Log console: logs extensively to the console from which the application was started.
\end{compactitem}

\subsection{Preferences for viewers}
Preferences that customize the action of a viewer can be edited in the 
panel ``Workspace Preferences'' (the edits will apply to preferences of all projects)  or in the panel ``Project Preferences'' (the edits will apply to the open project only). Project preferences may have a special value \texttt{\$WORKSPACE\$} that indicates the corresponding workspace preference is to be used.

An example is shown in Figure \ref{fig:workspace2} for workspace preferences. It indicates for instance that in this case the output file will be compressed (''\texttt{gzip}'' is \texttt{true}) and XML output is written to file at every hundred time steps (''\texttt{stride}'' is \texttt{100}).

Every viewer and the set of viewers as awhole has an ''\texttt{enabled\_at\_startup}'' attribute that does what the name suggests: activating the viewer at start up if set to \texttt{true} or not if set to \texttt{false}. Activating or deactivating a viewer during a simulation run can be done via the '\texttt{Viewers}'' pull down menu. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.62]{images/ScreenShotQtViewer.png}}
	\end{center}
	\caption{Screen shot of Qt screen viewer for the Geometric project in the workspace of \ref{fig:workspace}. The color scheme is user defined and reflects the relative size of the cell.}
	\label{fig:qtviewer}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Post-processors}
\label{section:Postprocessors}

\subsection{Post-processing}
Files can be postprocessed after simulation by right-clicking 
on a (closed) project folder and choosing the ``Postprocess...'' option, 
after which a dialog will pop up. You can choose different steps included 
in different files by marking them in the list. For convenience, you can 
filter steps as well, by specifying a series of comma-separated ranges of 
the format `start-stop:step' in the regex search field. The files in which 
to search can be specified as well in the file search field.
Once the desired steps have been marked, you can select the export 
format (BMP, CSV, JPEG, PDF, PLY or PNG), choose a path where the exported 
files should be stored, and specify a prefix for the files that will be generated. 
The filenames of the exported files will have the format \verb|<prefix>_<timestep>.<extension>|. 


\subsection{Preferences for viewers}
Preferences that customize the action of a post-processor can be edited in the 
panel ``Workspace Preferences'' (the edits will apply to preferences of all projects)  or in the panel  ``Project Preferences'' (the edits will apply to the open project only). Project preferences may have a special value \texttt{\$WORKSPACE\$} that indicates the corresponding workspace preference is to be used.

For graphical export (BMP, JPEG, PDF and PNG), the preferences subtree \verb|graphics| will be used. Additionally, the preferences sub trees \verb|viewer.bitmap_graphics| 
and \verb|viewers.vector_graphics| are used for bitmap graphics export (BMP, JPEG and PNG) 
and vector graphics export (PDF), respectively. See Appendix \ref{chap:PreferencesDictionary} for an overview of those preferences. 

It is advisable to match the width and height of the 
exported graphics to the dimensions of the simulated tissue at the last timestep 
(assuming that the tissue has it's largest size at that time). The width and height for the images in the graphical post-processing can then be specified in the qt preferences 
subtree. If you leave these parameters unspecified, the images that are generated 
in the post-processing will modify the scale as the simulated tissue grows to make 
the tissue fit into the standard output width and height.
To start the conversion, just click the convert button. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Export
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exporters}
\label{section:Export}
During interactive running of the project some interesting states 
or dynamic processes in the tissue can be exported/saved for 
further use or analysis. Use the ``Export'' option in the ``File'' menu 
to export a single file of the current state of the tissue to the desirable format. 
A dialog will pop up to ask the user a path and file name for the exported file. 
The exporters support the file formats for viewers (XML, XML.gz and HDF5), as well as 
those for post-processors (BMP, CSV, JPEG, PLY, PDF and PNG). The XML or HDF5 files 
visible in the project panel also can be exported by double clicking them at first, 
then using the ``Export'' option.
