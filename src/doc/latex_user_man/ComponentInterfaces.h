#ifndef MODEL_COMPONENT_INTERFACES_H_INCLUDED
#define MODEL_COMPONENT_INTERFACES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of Model Components.
 */


namespace SimPT_Sim {
class Cell;
class NeighborNodes;
class Node;
class Wall;
}

#include "sim/SimTimingTraits.h"
#include "sim/SimPhase.h"
#include <array>
#include <functional>
#include <tuple>


namespace SimPT_Sim {

//BEGIN_SNIPPET{FullSource}

using CellChemistryComponent
= std::function<void (Cell*, double*)>;

using CellColorComponent
= std::function<std::array<double, 3> (Cell*)>;

using CellDaughtersComponent
= std::function<void (Cell*, Cell*)>;

using CellHousekeepComponent
= std::function<void (Cell*)>;

using CellSplitComponent
= std::function<std::tuple<bool, bool, 
                           std::array<double, 3>> (Cell*)>;

using CellToCellTransportComponent
= std::function<void (Wall*, double*, double*)>;

using CellToCellTransportBoundaryComponent
= std::function<void (Wall* w, 
                      double* dchem_c1, double* dchem_c2)>;

using DeltaHamiltonianComponent
= std::function<double (const NeighborNodes&, 
                        Node*, std::array<double, 3>)>;

using HamiltonianComponent
= std::function<double (Cell*)>;

using  MoveGeneratorComponent
= std::function<std::array<double,3>()>;

using TimeEvolverComponent
= std::function<std::tuple<SimTimingTraits::CumulativeTimings, 
                           bool>(double, SimPhase)>;

using WallChemistryComponent
= std::function<void (Wall*, double*, double*)>;



//END_SNIPPET{FullSource}
} // namespace





#endif // end_of_include_guard
