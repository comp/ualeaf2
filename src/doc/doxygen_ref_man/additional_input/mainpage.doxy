/*!
 \file
 \brief Placeholder for main page of doxygen documentation.

 \mainpage SimPT
 
 \htmlonly
	 <table >
	 <td>
		 <table align="left" cellpadding="15" >
		 <tr>
			 <td>
				 <img src="SimPT_Large_Logo.png" alt="simPT_logo" vpsace="100" align="left" />
			 </td>
			 <td>
				 Simulator of Plant Tissue a.k.a SimPT or simPT is a cell-based computer 
				 modeling framework for plant tissue morphogenesis. It defines a 
				 set of biologically intuitive C++ objects that provide useful abstractions 
				 for building biological simulations of developmental processes. These include
				 entties such as cells and cell walls, but also algorithmic comonents describing
				 diffusion and active transport of chemicals, rules for cell splitting etc.  
				 The models provide a means for plant researchers to analyze the function of developmental
				 genes in the context of the biophysics of growth and patterning.
			 </td>
			 <td>
			 	 &nbsp;
			 </td>
		 </tr>
		 </table>
	</td>
	<td>
		&nbsp;
	</td>
	</table>
 \endhtmlonly

 \b Documentation
	 \li \b Installation information can be found in the text files README.txt, <br>
	 INSTALL.txt, PLATFORMS.txt, DEPENDENCIES.txt and KNOWN_ISSUES.txt<br>
	 in the toplevel directory of the software distribution.
	 \li \b User manual generated from latex sources is available in pdf format<br>
	 in directory src/doc/latex_user_man of the software distribution.
	 \li \b Reference documentation on the code base consists of these web pages.
 
 \b License \n
	
	Copyright 2011-2016 Universiteit Antwerpen
	
	Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
	the European Commission - subsequent versions of the EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the Licence is distributed on an "AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the Licence for the specific language governing
	permissions and limitations under the Licence.
 */
