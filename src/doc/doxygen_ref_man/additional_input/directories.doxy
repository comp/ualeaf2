/**
*	\file
* 	\brief Documentation on directory structure. Everything used to generate project artefacts is placed in directory src.
*	 	  	 	 	 	 	 	 	 	 	
*	- src/doc : Everything used to generate documentation.
*
*	- src/main : Code related files (sources, third party libraries and headers, ...).
*		+ cpp_execs : Main programs to build executables.
*		+ cpp_parex : Parameter exploration tool.
*			* parex_client : Client side of the parex tool.
*			* parex_node : Compute node for the parex tool.
*			* parex_protocol : Client-server protocol used with parex.
*			* parex_server : Server for parex.
*		+ cpp_sim : Core simulator: biological concepts, algorithms, time evolution.
*			* algo : Algorithms for expansion, division, node insertion, ...
*			* bio : Biological concepts: mesh, cell, wall, edge, node, ...
*			* coupler : Coupled simulations code.
*			* fileformats
*			* math : Miscellaneous mathematical constructs.
*			* model : Interface for models and extension points for core algorithms.
*			* sim : Simulator proper i.e. the simulation driver.
*		        * util : Miscellaneous utilities( conatiner, timekeeper, ...).
*		+ cpp_simptshell : SimPT specific classes for the work shell (workspace, project, session, ...)
*			* cli : Command line interface.
*			* exporters : file export to various formats
*			* gui : graphical user interface
*			* mesh_drawer
*			* session
*			* viewer
*			* viewers : The viewers that are available. 
*			* workspace : Deals with workspace construction and management.
*		+ cpp_simshell: generic classes for the work shell (workspace, project, session, ...)
*			* common
*			* gui
*			* ptree
*			* session
*			* viewer
*			* workspace : Deals with workspace construction and management.
*		+ cpp_tissue_edit : gui tissue editor.
*			* editor : Core editor components.
*			* generator : Generator for meshes (regular, Voronoi).
*			* slicer : Component with slicing capability.
*               + models: model dependent extension points for the algorithms
*                       * <model name> : items for the named group of models
*                                - components: component factory to produce component component instances
*                                       - cell_chemistry: model components for cell chemistry
*                                       - cell_daughters: model components for cell daughters
*                                       - cell_split: model components for cell splitting
*                                       - cell2cell_transport: model components for raction and active transport
*				        - delta_hamiltonian
*				        - hamiltonian : Hamiltonians for the Metropolis algorithm
*				        - move_generator: genrate displacements for Monte Carlo algorithm
*				        - time_evolver : Time evolution schemes.                                      
*                                       - wall_chemistry: model components for wall chemistry
*                                - resources: mainly the workspace template with its files
*                                - tests that can be run for this model group
*		+ resources : Resources i.e. non source code artefacts used in build.
*			* cmake : CMake modules used in the build
*			* data : third party icons.
*			* icons : Icons for desktop use.
*			* lib : External software included at source level in build.
*			* make : Makefile template.
*			* Paraview: the VLeafReader
*			* txt
*		+ swig_sim : Java and Python wrapper sources.
*	
*	- src/test 
*
*/
