#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# api_doc
#============================================================================
find_package( Doxygen )

if( DOXYGEN_FOUND )	

	#=== dot tool ===
	if( DOXYGEN_DOT_FOUND )
		set( DOXY_HAVE_DOT               YES  )
		set( DOXY_DOT_IMAGE_FORMAT       png  )
		set( DOXY_INTERACTIVE_SVG        NO  )
#		set( DOXY_DOT_IMAGE_FORMAT       svg  )
#		set( DOXY_INTERACTIVE_SVG        YES  )
	else()
		set( DOXY_HAVE_DOT               NO    )
		set( DOXY_DOT_IMAGE_FORMAT       gif   )
		set( DOXY_INTERACTIVE_SVG        NO   )
	endif()
	
	#=== setup ===			
	set( DOXY_HTML_DIR 	"reference_doc_html" )
	configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.cmake.in 
					${CMAKE_CURRENT_BINARY_DIR}/Doxyfile )	
	file( COPY images/SimPT_Small_Logo_50x50_transp.png DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

	#=== target ===
	add_custom_target( api_doc ALL	${DOXYGEN_EXECUTABLE} Doxyfile )
	add_custom_target( images ALL
		COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/images
			 ${CMAKE_CURRENT_BINARY_DIR}/${DOXY_HTML_DIR} )
			 
	#=== install ===
	install(DIRECTORY		${CMAKE_CURRENT_BINARY_DIR}/${DOXY_HTML_DIR}
	       DESTINATION		${DOC_INSTALL_LOCATION} )
	install(FILES      		ReferenceManual.html
	       DESTINATION		${DOC_INSTALL_LOCATION} )
		
	#=== unset ===
	unset( DOXY_HAVE_DOT )
	unset( DOXY_HTML_DIR )
	
endif( DOXYGEN_FOUND )

#############################################################################