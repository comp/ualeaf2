#!/bin/bash
#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

set -x

#----------------------------------------------------------------------------
# Defaults
#----------------------------------------------------------------------------
GTEST_DEFAULT_TOTAL_SHARDS=32

#----------------------------------------------------------------------------
# Script must be executed in the tests root dir i.e TESTS_DIR.
#----------------------------------------------------------------------------
TESTS_DIR=$PWD
MONKIT_FILE=$TESTS_DIR/monkit.xml
#
EXEC_DIR=$TESTS_DIR/bin
GTESTER=$EXEC_DIR/gtester
MONKIT_MERGER=$EXEC_DIR/monkit_merger

#----------------------------------------------------------------------------
# Use GTest sharding. Arg provides number of shards, otherwise default
#----------------------------------------------------------------------------
if [ $# -eq 0 ]; then
	GTEST_TOTAL_SHARDS=$GTEST_DEFAULT_TOTAL_SHARDS
else
	GTEST_TOTAL_SHARDS=$1
fi
export GTEST_TOTAL_SHARDS

#----------------------------------------------------------------------------
# Command to be executed in each shard subdir
#----------------------------------------------------------------------------
COMMAND="$GTESTER --gtest_shuffle --gtest_filter=*Perf*.*:-*PerfViewers* --gtest_output=xml:PerfTest.xml"

#----------------------------------------------------------------------------
# Execute the test with sharding.
#----------------------------------------------------------------------------
INDEX=0
while [ $INDEX -lt $GTEST_TOTAL_SHARDS ]
do
	DIR=$TESTS_DIR/PerfTest_shard_$INDEX
	mkdir $DIR
	(export GTEST_SHARD_INDEX=$INDEX; cd $DIR && $COMMAND) &
	INDEX=$(( $INDEX + 1 ))
done

#----------------------------------------------------------------------------
# Wait for tests to finish
#----------------------------------------------------------------------------
wait

#----------------------------------------------------------------------------
# Merge monkit shards into $MONKIT_FILE.
#----------------------------------------------------------------------------
cat << 'INTRO' > $MONKIT_FILE
<?xml version="1.0" encoding="utf-8"?>
<categories>
INTRO

INDEX=0
while [ $INDEX -lt $GTEST_TOTAL_SHARDS ]
do
	DIR=$TESTS_DIR/PerfTest_shard_$INDEX
	for SHARD_FILE in $DIR/monkit-shard*.xml
	do
		sed '1,2d' $SHARD_FILE | sed '$d' >> $MONKIT_FILE
	done
	INDEX=$(( $INDEX + 1 ))
done

cat << 'OUTRO' >> $MONKIT_FILE
</categories>
OUTRO

#----------------------------------------------------------------------------
# Merge observation items in $MONKIT_FILE.
#----------------------------------------------------------------------------
$MONKIT_MERGER -f $MONKIT_FILE

#----------------------------------------------------------------------------
# Done
#----------------------------------------------------------------------------
exit

#############################################################################
