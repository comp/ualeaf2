/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package be.ac.ua.simPT.test;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import be.ac.ua.simPT.*;

public class UnitSimState {
	private SimState state;
	private final double tol = 1.0e-10;

	@Before
	public void setUp() {
		
		// Adding the libsimPT_sim library is only required on windows
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			System.loadLibrary("libsimPTlib_sim");
	
		System.loadLibrary("simPT_sim_jni");
		state = new SimState();
	}

	@Test
	public void testMeshState() {
		state.SetMeshState(new MeshState());
		assertThat(state.GetMeshState(), instanceOf(MeshState.class));
	}

	@Test
	public void testProjectName() {
		state.SetProjectName("test");
		assertEquals("test", state.GetProjectName());
	}

	@Test
	public void testTime() {
		state.SetTime(60.0);
		assertEquals(60.0, state.GetTime(), tol);
	}

	@Test
	public void testTimeStep() {
		state.SetTimeStep(10);
		assertEquals(10, state.GetTimeStep());
	}
}
