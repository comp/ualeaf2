#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Test simulator interface for Python:
#============================================================================

#----------------------------- build ----------------------------------------
install( FILES  UnitMeshState.py  UnitSimState.py  UnitSimWrapper.py  
         DESTINATION  ${TESTS_INSTALL_LOCATION}/bin  
)

#------------------------------ install -----------------------------
install( FILES UnitMeshState.py  UnitSimState.py  UnitSimWrapper.py
         DESTINATION ${TESTS_INSTALL_LOCATION}/bin 
)

#------------------------------ run -----------------------------------------

if ("${PYTHONPATH}" STREQUAL "")
	set ( TESTPYPATH "${CMAKE_INSTALL_PREFIX}/${BIN_INSTALL_LOCATION}")
else()
	set ( TESTPYPATH "${PYTHONPATH}${PYTHON_PATH_SEPARATOR}${CMAKE_INSTALL_PREFIX}/${BIN_INSTALL_LOCATION}")
endif()

string(REPLACE ";" "\\;" TESTPYPATH_ESCAPED "${TESTPYPATH}") 

foreach( TEST      UnitSimState UnitMeshState UnitSimWrapper )
	add_test( NAME ${TEST}_py
		WORKING_DIRECTORY ${CMAKE_INSTALL_PREFIX}/${TESTS_INSTALL_LOCATION}/bin
		COMMAND ${PYTHON_EXECUTABLE} -m unittest -v ${TEST} )
	
	set_tests_properties(${TEST}_py PROPERTIES	ENVIRONMENT "PYTHONPATH=${TESTPYPATH_ESCAPED}" )
		
endforeach( TEST )

#-------------------------- unset variables ---------------------------------
unset( TESTPYPATH_ESCAPED)
unset( TESTPYPATH)

#############################################################################
