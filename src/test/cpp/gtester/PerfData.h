#ifndef TESTS_PERFDATA_H_INCLUDED
#define TESTS_PERFDATA_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header file for performance tests data.
 */

#include <boost/property_tree/ptree.hpp>
#include <mutex>
#include <string>
#include <thread>

namespace SimPT_Shell {
namespace Tests {

class PerfData
{
public:
	///
	static boost::property_tree::ptree BuildMonkitCategory(
		const std::string& category_label, const std::string& observation_label,
		unsigned int value)
	{
		boost::property_tree::ptree obs;
		obs.put("<xmlattr>.name", observation_label);
		obs.put_value(value);

		boost::property_tree::ptree observations;
		observations.add_child("observation", obs);

		boost::property_tree::ptree category;
		category.put("<xmlattr>.name", category_label);
		category.put("<xmlattr>.scale", "seconds");
		category.add_child("observations", observations);

		return category;
	}

	///
	boost::property_tree::ptree& Get()
	{
		return m_ptree;
	}

	///
	void Update(const boost::property_tree::ptree& category_1)
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_ptree.add_child("category", category_1);
	}

	///
	void Update(const boost::property_tree::ptree& category_1,
		const boost::property_tree::ptree& category_2)
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_ptree.add_child("category", category_1);
		m_ptree.add_child("category", category_2);
	}

//	///
//	void Update(const boost::property_tree::ptree& category_t,
//		const boost::property_tree::ptree& category_m,
//		const boost::property_tree::ptree& category_e)
//	{
//		std::lock_guard<std::mutex> guard(m_mutex);
//		m_ptree.add_child("category", category_t);
//		m_ptree.add_child("category", category_m);
//		m_ptree.add_child("category", category_e);
//	}

private:
	std::mutex                     m_mutex;
	boost::property_tree::ptree    m_ptree;
};

} // namespace
} // namespace

#endif // end_of_include_guard
