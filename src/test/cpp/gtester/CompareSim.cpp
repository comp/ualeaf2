/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Compare simulation data (hdf5, ptree and xml format).
 */

#include "CompareSim.h"

#include "fileformats/Hdf5File.h"
#include "ptree/PTreeComparison.h"
#include "sim/Sim.h"
#include "workspace/StartupFilePtree.h"

#include <stdexcept>
#include <boost/property_tree/xml_parser.hpp>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using SimPT_Sim::Sim;

namespace SimPT_Shell {

bool CompareSim::Hdf5Files(const string& f1, const string& f2, double accept_diff)
{
	Hdf5File f1_hdf5(f1);
	Hdf5File f2_hdf5(f2);

	if (!f1_hdf5.IsOpen() || !f2_hdf5.IsOpen())
		return false;

	const size_t numSteps = f1_hdf5.TimeSteps().size();

	if (numSteps != f2_hdf5.TimeSteps().size()) {
		return false;
	}

	for (size_t i = 0; i < numSteps; ++i) {
		try {
			Sim f1_sim;
			f1_sim.Initialize(f1_hdf5.Read(i));
			Sim f2_sim;
			f2_sim.Initialize(f2_hdf5.Read(i));

			if (!PTrees(f1_sim.ToPtree(), f2_sim.ToPtree(), accept_diff))
				return false;
		}
		catch (runtime_error& e) {
			return false;
		}
	}

	return true;
}


bool CompareSim::PTrees(const ptree& sim1, const ptree& sim2, double acceptable_diff)
{
	try {
		string const path_prefix = "vleaf2.";
		vector<string> subtree_list = { "sim_time", "parameters", "mesh" };

		for (string const & subtree : subtree_list) {
			if (!PTreeComparison::CompareNonArray(sim1.get_child(path_prefix + subtree),
					sim2.get_child(path_prefix + subtree),
			                acceptable_diff)) {
				return false;
			}
		}
	} catch (ptree_bad_path& e) {
		return false;
	}
	return true;
}

bool CompareSim::XmlFiles(const std::string& f1, const std::string& f2, double accept_diff)
{
	auto file1 = static_pointer_cast<Ws::StartupFilePtree>(
		Ws::StartupFileBase::Constructor(f1, f1));
	auto f1_pt = file1->ToPtree();


	auto file2 = static_pointer_cast<Ws::StartupFilePtree>(
		Ws::StartupFileBase::Constructor(f2, f2));
	auto f2_pt = file2->ToPtree();

	return PTrees(f1_pt, f2_pt, accept_diff);
}

}
