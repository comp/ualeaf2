#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build & install gtester
#============================================================================
set( TESTS_DIR  ${CMAKE_INSTALL_PREFIX}/${TESTS_INSTALL_LOCATION} )
set( EXEC_DIR   ${TESTS_DIR}/bin )
set( EXEC       gtester )

set( SRC
		main.cpp
		CompareSim.cpp
		PerfDemos.cpp
		ScenarioBatch.cpp
        ScenarioMesh.cpp
		UnitArray3.cpp
		UnitAttributeStore.cpp
		UnitBoundaryType.cpp
		UnitCBMBuilder.cpp
		UnitMeshHDF5.cpp
        UnitSegmentedVector.cpp
		UnitPBMBuilder.cpp
		UnitSimPtree.cpp
		UnitWallType.cpp
		
		parex/parex_client/MockClientServer.cpp
		parex/parex_client/UnitParexClient.cpp
		parex/parex_client/UnitParexExploration.cpp
		parex/parex_client/UnitParexServerInfo.cpp
		parex/parex_node/MockServer.cpp
		parex/parex_node/MockSimulator.cpp
		parex/parex_node/UnitParexNode.cpp
)
set( ADDITIONAL_OBJECTS  $<TARGET_OBJECTS:simPTlib_parex>  )

# Only test conceurrency if no OpenMP
if( NOT OPENMP_FOUND)
    set( SRC ${SRC} ScenarioConcurrency.cpp )
endif()

if( APPLE AND (SIMPT_QT_VERSION EQUAL 5) )
# DO NOT TEST THE EDITOR
else() 
    # TEST THE EDITOR
    set( SRC ${SRC}
		editor/UnitControlLogic.cpp
		editor/UnitEditableCell.cpp
		editor/UnitEditableEdge.cpp
		editor/UnitEditableNode.cpp
		editor/UnitPolygonUtils.cpp
    )
    set( ADDITIONAL_OBJECTS  ${ADDITIONAL_OBJECTS} $<TARGET_OBJECTS:simPTlib_tissue_edit>  )
endif()
set( MOC_HEADERS
		ProjectMessageHandler.h
		parex/parex_client/MockClientServer.h
		parex/parex_node/MockServer.h
		parex/parex_node/MockSimulator.h
)
set( MOC_OUTFILES )

if ( HDF5_FOUND )
	set( SRC ${SRC} UnitMeshHDF5.cpp )
endif()

#-----------------
if ( ${SIMPT_QT_VERSION} EQUAL 4 )
	qt4_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
elseif( ${SIMPT_QT_VERSION} EQUAL 5 )
	qt5_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
endif()
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
add_executable( ${EXEC}  
                    ${SRC} 
                    ${MOC_OUTFILES} 
                    ${ADDITIONAL_OBJECTS} )
target_link_libraries( ${EXEC}  simPTlib_shell simPTlib_sim pthread gtest)  
set_target_properties( ${EXEC}  PROPERTIES RPATH  "${CMAKE_INSTALL_PREFIX}/bin" )

#-----------------  
install( TARGETS ${EXEC} DESTINATION  ${EXEC_DIR} )
add_test( NAME  ${EXEC}_unit  
		WORKING_DIRECTORY  ${TESTS_DIR} 
		COMMAND   ${EXEC_DIR}/${EXEC} --gtest_filter=*Unit*:-*Parex* 
)
# These tests use tcp ports and cannot be used in concurrent testjobs
add_test( NAME  ${EXEC}_parex 
		WORKING_DIRECTORY  ${TESTS_DIR}
		COMMAND   ${EXEC_DIR}/${EXEC} --gtest_filter=*Parex* 
)
add_test( NAME  ${EXEC}_scenario  
		WORKING_DIRECTORY  ${TESTS_DIR}
		COMMAND   ${EXEC_DIR}/${EXEC} --gtest_filter=*Scenario* 
)

#-----------------
unset( TESTS_DIR )  
unset( EXEC      )
unset( SRC       )
unset( MOC_HEADERS  )
unset( MOC_OUTFILES )
unset( ADDITIONAL_OBJECTS )

#############################################################################

