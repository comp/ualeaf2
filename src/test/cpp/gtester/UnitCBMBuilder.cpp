/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitCBMBuilder.
 */

#include "common/InstallDirs.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "bio/MeshCheck.h"
#include "bio/Node.h"
#include "bio/Wall.h"
#include "bio/CBMBuilder.h"
#include "bio/PBMBuilder.h"
#include "ptree/PTreeComparison.h"
#include "workspace/CliWorkspace.h"
#include "workspace/StartupFilePtree.h"

#include <QDir>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <gtest/gtest.h>

#include <memory>
#include <string>

namespace SimPT_Sim {
namespace Tests {

using namespace std;
using namespace SimShell;
using namespace SimPT_Shell;
using namespace SimPT_Shell::Ws;
using namespace boost::property_tree;

class UnitCBMBuilder: public ::testing::TestWithParam<std::string>
{
protected:
	virtual ~UnitCBMBuilder() {}

	virtual void SetUp()
	{
	     g_mesh_xml_node = "vleaf2.mesh";
	}

	virtual void TearDown() {}

	static string       g_mesh_xml_node;
};

string UnitCBMBuilder::g_mesh_xml_node;

namespace {

void CompareMeshes(Mesh& mesh1, Mesh& mesh2)
{
	//Lambdas to check components.
	auto compare_cells = [](const Cell* c1, Cell* c2, const std::string error_message) {
		c2->SetDead(!c2->IsDead());
		ASSERT_TRUE(c1->IsDead() != c2->IsDead()) << error_message;
		c2->SetDead(!c2->IsDead());
	};

	auto compare_nodes = [](const Node* n1, Node* n2, const std::string error_message) {
		n2->SetFixed(!n2->IsFixed());
		ASSERT_TRUE(n1->IsFixed() != n2->IsFixed()) << error_message;
		n2->SetFixed(!n2->IsFixed());
	};

	auto compare_walls = [&mesh1](const Wall* w1, Wall* w2, const std::string error_message) {
		Cell* c1 = w2->GetC1();
		w2->SetC1(nullptr);
		ASSERT_TRUE(w1->GetC1() != nullptr) << error_message;
		w2->SetC1(c1);
	};

	//Check if a node is copied properly.
	compare_nodes(mesh1.GetNodes()[0], mesh2.GetNodes()[0], "Node 0 wasn't copied properly.");

	//Check if a wall is copied properly.
	if (mesh1.GetWalls().size() > 0) {
		Wall* w1 = mesh1.GetWalls().front();
		Wall* w2 = mesh2.GetWalls().front();
		compare_walls(w1, w2, "Wall 0 wasn't copied properly.");
		compare_nodes(w1->GetN1(), w2->GetN1(), "N1 in wall 0 wasn't copied properly.");
		compare_nodes(w1->GetN2(), w2->GetN2(), "N2 in wall 0 wasn't copied properly.");
		compare_cells(w1->GetC1(), w2->GetC1(), "C1 in wall 0 wasn't copied properly.");
		compare_cells(w1->GetC2(), w2->GetC2(), "C2 in wall 0 wasn't copied properly.");
	}

	//Check if a cell is copied properly.
	Cell* c1 = mesh1.GetCells()[0];
	Cell* c2 = mesh2.GetCells()[0];
	compare_cells(c1, c2, "Cell 0 wasn't copied properly.");
	compare_nodes(c1->GetNodes().front(),
		c2->GetNodes().front(), "The first node in cell 0 wasn't copied properly.");
	if (c1->GetWalls().size() > 0) {
		compare_walls(c1->GetWalls().front(),
			c2->GetWalls().front(), "The first wall in cell 0 wasn't copied properly.");
	}

	//Check if the boundary polygon is copied properly.
	compare_cells(mesh1.GetBoundaryPolygon(),
		mesh2.GetBoundaryPolygon(), "The boundary polygon wasn't copied properly.");
}

}

TEST_P(UnitCBMBuilder, PTreeComparison)
{
	//Assert that work space exists and define its workspace_model.
	QDir dir(QString::fromStdString(InstallDirs::GetDataDir()));
	ASSERT_TRUE(dir.cd("simPT_Default_workspace"));
	SimPT_Shell::Ws::CliWorkspace ws_model(dir.absolutePath().toStdString());

	//Read the project file into a Mesh.
	std::string project = GetParam();
	auto file_ptr = ws_model.Get(project)->Back();
	ASSERT_TRUE(dir.exists(QString::fromStdString(file_ptr->GetPath())));
	auto pt = std::static_pointer_cast<StartupFilePtree>(file_ptr)->ToPtree();

	// Build a mesh and copy it.
	auto mesh = PBMBuilder().Build(pt.get_child(g_mesh_xml_node));
	auto mesh_copy = CBMBuilder().Build(*mesh);

	// Define expected_pt.
	ptree mesh_pt;
	mesh_pt.put_child("mesh", pt.get_child(g_mesh_xml_node));

	// Define actual_pt.
	ptree mesh_copy_pt;
	mesh_copy_pt.put_child("mesh", mesh_copy->ToPtree());

	// Check equality.
	EXPECT_EQ(true, PTreeComparison::CompareNonArray(mesh_pt, mesh_copy_pt, 1.0e-5));

	//Check if mesh's yield ptrees that are completely identical (you can do that
	// in memory; not when i/o is involved because of rounding/truncation effects).
	ASSERT_TRUE(mesh->ToPtree() == mesh_copy->ToPtree());
}

TEST_P(UnitCBMBuilder, InMemoryComparison)
{
	//Assert that work space exists and define its workspace_model.
	QDir dir(QString::fromStdString(InstallDirs::GetDataDir()));
	ASSERT_TRUE(dir.cd("simPT_Default_workspace"));
	SimPT_Shell::Ws::CliWorkspace ws_model(dir.absolutePath().toStdString());

	//Read the project file into a Mesh.
	const std::string project = GetParam();
	const auto file_ptr = ws_model.Get(project)->Back();
	ASSERT_TRUE(dir.exists(QString::fromStdString(file_ptr->GetPath())));
	auto pt = std::static_pointer_cast<StartupFilePtree>(file_ptr)->ToPtree();

	// Build a mesh and copy it.
	auto mesh        = PBMBuilder().Build(pt.get_child(g_mesh_xml_node));
	auto mesh_copy   = CBMBuilder().Build(*mesh); //Copy.

	//Catch bugs in nodes, walls and cells.
	CompareMeshes(*mesh, *mesh_copy);

	//Catch bugs in mesh structures made of raw pointers.
	mesh.reset();

	//This will fail when trying to access invalid shared_ptrs.
	ASSERT_TRUE(MeshCheck(*mesh_copy).CheckAll());
}


namespace {

std::string scenarios[] {
	"AuxinGrowth",
	"Geometric",
	"Meinhardt",
	"NoGrowth",
	"TipGrowth",
	"Wortel"
	};

}

INSTANTIATE_TEST_CASE_P(UnitCBMBuilder_a, UnitCBMBuilder, ::testing::ValuesIn(scenarios));

} // namespace
} // namespace

