/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of performance tests.
 */

#include "PerfData.h"

#include "common/InstallDirs.h"
#include "cli/CliController.h"
#include "cli/CliWorkspaceManager.h"
#include "util/clock_man/ClockTraits.h"
#include "util/misc/Exception.h"
#include "util/misc/XmlWriterSettings.h"

#include <gtest/gtest.h>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>

using namespace std;
using namespace std::chrono;
using namespace ::testing;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim::ClockMan;
using namespace SimShell;
using namespace SimShell::Gui;
using boost::optional;

namespace SimPT_Shell {
namespace Tests {

using ElapsedStopclock = ClockTraits<system_clock, seconds>::Stopclock;

class PerfDemos: public TestWithParam<tuple<tuple<string, unsigned int>, unsigned int>>
{
public:
	/// TestCase set up.
	static void SetUpTestCase()
	{
		g_monkit_xml      = "monkit-shard-demos.xml";
		g_input_data_file = "leaf_000000.xml";
	}

	/// Tearing down TestCase
	static void TearDownTestCase()
	{
		// Save the relevant performance data in monkit format.
		ptree monkit;
		monkit.put_child("categories", g_categories.Get());
		write_xml(g_monkit_xml, monkit, std::locale(), XmlWriterSettings::GetTab());
	}

protected:
	/// Destructor has to be virtual.
	virtual ~PerfDemos() {}

	/// Set up for the test fixture
	virtual void SetUp() {}

	/// Tearing down the test fixture
	virtual void TearDown(){}

	// Data members of the test
	unsigned int        m_num_step;
	ptree		    m_monkit;

	// Data member for test case
	static string       g_monkit_xml;
	static PerfData     g_categories;
	static string       g_input_data_file;
};

string       PerfDemos::g_monkit_xml;
PerfData     PerfDemos::g_categories;
string       PerfDemos::g_input_data_file;

TEST_P( PerfDemos, testTimingSteps )
{
	tuple<tuple<string, unsigned int>, unsigned int>   t(GetParam());
	tuple<string, unsigned int>                        scenario(get<0>(t));
	const unsigned int                                 run_label(get<1>(t));
	const string                                       name(get<0>(scenario));
	const unsigned int                                 num_steps = get<1>(scenario);

	// ------------------------------------------------------------------------
	// Create the workspace (local to shard due to races on workspace index).
	// ------------------------------------------------------------------------
	const string ws_name = "perfdemos_workspace";
	auto ws_ptr = CliWorkspaceManager::OpenWorkspace("", ws_name);
	ASSERT_NE( nullptr, ws_ptr );

	// ------------------------------------------------------------------------
	// Adjust workspace prefs.
	// ------------------------------------------------------------------------
	ptree pref_pt = ws_ptr->GetPreferences();
	pref_pt.put("viewers.root.enabled_at_startup", true);
	pref_pt.put("viewers.root.viewers.xml.enabled_at_startup", false);
	ws_ptr->SetPreferences(pref_pt);

	// ------------------------------------------------------------------------
	// Create the project.
	// ------------------------------------------------------------------------
	const string pr_name = name + "_run_" + to_string(run_label);
	auto pr_ptr = CliWorkspaceManager::OpenProject(ws_ptr, pr_name)->second.Project();
	ASSERT_NE( nullptr,  pr_ptr );

	// ------------------------------------------------------------------------
	// Create sim data input file (first try compressed, then uncompressed).
	// ------------------------------------------------------------------------
	bool success { false };
	string file_name { g_input_data_file + ".gz" };

	while (! success) {
	        try {
	                CliWorkspaceManager::OpenLeaf(pr_ptr, file_name, true,
	                        InstallDirs::GetDataDir() + "/simPT_Default_workspace", name, file_name);
	                success = true;
	        } catch(Exception& e) {
	                if (file_name == g_input_data_file) {
	                        throw;
	                }
	                success = false;
	                file_name = g_input_data_file;
	        }
	}

	// ------------------------------------------------------------------------
	// Run the simulator task.
	// ------------------------------------------------------------------------
	const unsigned int   num_repeats = 1;
	IndividualRecords<seconds>     timings;

	for (unsigned int i = 0; i< num_repeats; i++) {
	        auto      controller = CliController::Create(ws_name, true);
	        CliSimulationTask   task(pr_name, true, file_name, true, num_steps);

	        ElapsedStopclock chrono_elapsed("elapsed", true);
	        const int status = controller->Execute(task);
	        ASSERT_EQ( EXIT_SUCCESS, status ) << "CliController::Execute failed for: " << task;
	        timings.Record("elapsed", chrono_elapsed.Get());
	}

	// ------------------------------------------------------------------------
	// Obtain perf data.
	// ------------------------------------------------------------------------
	const unsigned int    elapsed  = timings.GetMinimum("elapsed").count();

	// ------------------------------------------------------------------------
	// Process perf data for MonKit format.
	// ------------------------------------------------------------------------
	const string observation_label = "run_" + to_string(run_label);
	const string category_label_e  = "Plain_" + name + "_elapsed_" + to_string(num_steps);

	ptree category_e = PerfData::BuildMonkitCategory(category_label_e, observation_label, elapsed);

	g_categories.Update(category_e);
}

namespace {

tuple<string, unsigned int>   scenarios[] {
	make_tuple("AuxinGrowth", 1500U  ),
	make_tuple("Geometric",    750U  ),
	make_tuple("Meinhardt",   2700U  ),
        make_tuple("NoGrowth",    1400U  ),
	make_tuple("TipGrowth",   2700U  ),
	make_tuple("Wortel",        10U  )
};

unsigned int run_labels[] { 0, 1, 2};

}

INSTANTIATE_TEST_CASE_P(testTimings_a, PerfDemos, Combine(ValuesIn(scenarios), ValuesIn(run_labels)));

} // namespace
} // namespace
