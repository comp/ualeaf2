/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Main program for gtest runs.
 */

#include "common/InstallDirs.h"
#include "util/misc/Exception.h"
#include <gtest/gtest.h>
#include <QApplication>
#include <QMetaType>
#include <cerrno>
#include <exception>
#include <iostream>

using namespace std;
using namespace SimPT_Sim::Util;
using namespace SimShell;

int main(int argc, char **argv)
{
	int exit_status = EXIT_SUCCESS;
	try {
		::testing::InitGoogleTest(&argc, argv);
		QCoreApplication app(argc, argv, false);
		qRegisterMetaType<std::string>("std::string");

		cout << InstallDirs::GetRootDir() << endl;
		return RUN_ALL_TESTS();
	}
	catch (Exception& e) {
		cerr << "Exception caught: " << e.what() << endl << ">>>>> Aborting ..." << endl;
		exit_status = EXIT_FAILURE;
	}
	catch (exception& e) {
                cerr << "std::exception caught: " << e.what() << endl << ">>>>> Aborting ..." << endl;
                exit_status = EXIT_FAILURE;
	}
	return exit_status;
}
