/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for EditableNodeItem.
 */

#include "bio/Mesh.h"
#include "bio/Node.h"
#include "editor/EditableNodeItem.h"

#include <gtest/gtest.h>

namespace SimPT_Editor {
namespace Tests {

class UnitEditableNodeItem : public ::testing::Test
{
protected:
	UnitEditableNodeItem() :
		::testing::Test(), m_mesh(0), m_node1(nullptr), m_node2(nullptr),
		 m_editable_node_item1(nullptr), m_editable_node_item2(nullptr)
	{
	}

	virtual void SetUp()
	{
		m_node1 = m_mesh.BuildNode(0, {{0, 0, 0}}, true);
		m_node2 = m_mesh.BuildNode(1, {{1, 1, 0}}, false);
		m_editable_node_item1 = new EditableNodeItem(m_node1, 1);
		m_editable_node_item2 = new EditableNodeItem(m_node2, 1);
	}

	virtual void TearDown()
	{
		delete m_editable_node_item1;
		delete m_editable_node_item2;
	}

	// Data members of the test fixture
	SimPT_Sim::Mesh     m_mesh;
	SimPT_Sim::Node*    m_node1;
	SimPT_Sim::Node*    m_node2;
	EditableNodeItem*    m_editable_node_item1;
	EditableNodeItem*    m_editable_node_item2;
};

TEST_F(UnitEditableNodeItem, TestIsAtBoundary)
{
	EXPECT_TRUE(m_editable_node_item1->IsAtBoundary()) << "Node should be at boundary";
	EXPECT_FALSE(m_editable_node_item2->IsAtBoundary()) << "Node shouldn't be at boundary";
}

TEST_F(UnitEditableNodeItem, TestContains)
{
	EXPECT_TRUE(m_editable_node_item1->Contains(m_node1)) << "Editable node should contain logical node";
	EXPECT_TRUE(m_editable_node_item2->Contains(m_node2)) << "Editable node should contain logical node";
	EXPECT_FALSE(m_editable_node_item1->Contains(m_node2)) << "Editable node shouldn't contain logical node";
	EXPECT_FALSE(m_editable_node_item2->Contains(m_node1)) << "Editable node shouldn't contain logical node";
}

TEST_F(UnitEditableNodeItem, TestNode)
{
	SimPT_Sim::Node* node = m_editable_node_item1->Node();
	EXPECT_EQ(m_node1, node) << "Editable node should contain logical node";
	node = m_editable_node_item2->Node();
	EXPECT_EQ(m_node2, node) << "Editable node should contain logical node";
	node = m_editable_node_item1->Node();
	EXPECT_NE(m_node2, node) << "Editable node shouldn't contain logical node";
	node = m_editable_node_item2->Node();
	EXPECT_NE(m_node1, node) << "Editable node shouldn't contain logical node";
}

} // namespace
} // namespace
