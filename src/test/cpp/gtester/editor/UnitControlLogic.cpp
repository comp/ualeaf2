/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for LeafControlLogic.
 */

#include "common/InstallDirs.h"
#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "editor/EditableEdgeItem.h"
#include "editor/EditableNodeItem.h"
#include "editor/EditControlLogic.h"

#include <boost/property_tree/xml_parser.hpp>
#include <gtest/gtest.h>
#include <QDir>
#include <QObject>
#include <QPointF>
#include <QPolygonF>

#include <list>
#include <string>
#include <vector>

namespace SimPT_Editor {
namespace Tests {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimShell;

class UnitLeafControlLogic : public ::testing::Test
{
protected:
	static void SetUpTestCase()
	{
		g_path = InstallDirs::GetDataDir() + "/res/UnitControlLogic/";
		g_xml_root_node = "vleaf2";
	}

	static string     g_path;
	static string     g_xml_root_node;
};

string UnitLeafControlLogic::g_path;
string UnitLeafControlLogic::g_xml_root_node;

TEST_F(UnitLeafControlLogic, TestCanSplitCell)
{
	//Use squared cell, so the nodes that are on the diagonals can be used to split a cell
	//Setup
	ptree tree;
	read_xml(g_path + "TestCanSplitCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	EditableMesh* mesh = logic.GetMesh();
	vector<Cell*> cells = logic.GetCells();
	vector<Node*> nodes = logic.GetNodes();

	//Test
	EXPECT_TRUE(mesh->CanSplitCell(nodes[0],nodes[2])) << "Split on these two nodes should be possible";
	EXPECT_TRUE(mesh->CanSplitCell(nodes[1],nodes[3])) << "Split on these two nodes should be possible";
	EXPECT_FALSE(mesh->CanSplitCell(nodes[0],nodes[1])) << "Split on these two nodes shouldn't be possible";
	EXPECT_FALSE(mesh->CanSplitCell(nodes[0],nodes[3])) << "Split on these two nodes shouldn't be possible";
	EXPECT_FALSE(mesh->CanSplitCell(nodes[1],nodes[2])) << "Split on these two nodes shouldn't be possible";
	EXPECT_FALSE(mesh->CanSplitCell(nodes[2],nodes[3])) << "Split on these two nodes shouldn't be possible";
}

TEST_F(UnitLeafControlLogic, TestDeleteNode)
{
	//Test delete of a node of a triangled cell, delete shouldn't be possible
	unique_ptr<SimPT_Sim::Mesh> mesh(new SimPT_Sim::Mesh(0));
	mesh->BuildNode(0, {{0, 0, 0}}, true);
	mesh->BuildNode(1, {{1, -1, 0}}, true);
	mesh->BuildNode(2, {{-1, -1, 0}}, true);
	mesh->BuildCell(0, {0, 1, 2});
	mesh->BuildBoundaryPolygon({0, 1, 2});
	ptree pt_tissue;
	pt_tissue.put_child("mesh", mesh->ToPtree());
	EditControlLogic logic(pt_tissue);
	vector<Node*> nodes = logic.GetNodes();

	EXPECT_FALSE(logic.DeleteNode(nodes[0])) << "Delete node shouldn't be possible";
	EXPECT_FALSE(logic.DeleteNode(nodes[1])) << "Delete node shouldn't be possible";
	EXPECT_FALSE(logic.DeleteNode(nodes[2])) << "Delete node shouldn't be possible";

	//Test delete of a cell made with 5 nodes, should be possible to delete 2 of the 5 nodes
	mesh.reset(new SimPT_Sim::Mesh(0));
	mesh->BuildNode(0, {{-1, 1, 0}}, true);
	mesh->BuildNode(1, {{0, 2, 0}}, true);
	mesh->BuildNode(2, {{1, 1, 0}}, true);
	mesh->BuildNode(3, {{1, -1, 0}}, true);
	mesh->BuildNode(4, {{-1, -1, 0}}, true);
	mesh->BuildCell(0, {0, 1, 2, 3, 4});
	mesh->BuildBoundaryPolygon({0, 1, 2, 3, 4});
	ptree pt_tissue2;
	pt_tissue2.put_child("mesh", mesh->ToPtree());
	EditControlLogic logic2(pt_tissue2);
	nodes = logic2.GetNodes();

	EXPECT_TRUE(logic2.DeleteNode(nodes[0])) << "Delete node should be possible";
	EXPECT_TRUE(logic2.DeleteNode(nodes[1])) << "Delete node should be possible";
	EXPECT_FALSE(logic2.DeleteNode(nodes[2])) << "Delete node shouldn't be possible";
	EXPECT_FALSE(logic2.DeleteNode(nodes[3])) << "Delete node shouldn't be possible";
	EXPECT_FALSE(logic2.DeleteNode(nodes[4])) << "Delete node shouldn't be possible";

	//Test delete of nodes with more then two edges
	mesh.reset(new SimPT_Sim::Mesh(0));
	mesh->BuildNode(0, {{-1, 1, 0}}, true);
	mesh->BuildNode(1, {{0, 1, 0}}, true);
	mesh->BuildNode(2, {{1, 1, 0}}, true);
	mesh->BuildNode(3, {{1, -1, 0}}, true);
	mesh->BuildNode(4, {{0, -1, 0}}, true);
	mesh->BuildNode(5, {{-1, -1, 0}}, true);
	mesh->BuildCell(0, {0, 1, 4, 5});
	mesh->BuildCell(1, {1, 2, 3, 4});
	mesh->BuildBoundaryPolygon({0, 1, 2, 3, 4, 5});
	ptree pt_tissue3;
	pt_tissue3.put_child("mesh", mesh->ToPtree());
	EditControlLogic logic3(pt_tissue3);
	nodes = logic3.GetNodes();

	EXPECT_FALSE(logic3.DeleteNode(nodes[1])) << "Delete node shouldn't be possible, more then two edges";
	EXPECT_FALSE(logic3.DeleteNode(nodes[4])) << "Delete node shouldn't be possible, more then two edges";


	//Test delete of a node that is shared by more then one cell
	mesh.reset(new Mesh(0));
	mesh->BuildNode(0, {{-1, -1, 0}}, true);
	mesh->BuildNode(1, {{-1, 0, 0}}, true);
	mesh->BuildNode(2, {{0, 1, 0}}, true);
	mesh->BuildNode(3, {{0, 0, 0}}, false);
	mesh->BuildNode(4, {{0, -1, 0}}, true);
	mesh->BuildNode(5, {{1, 0, 0}}, true);
	mesh->BuildNode(6, {{1, -1, 0}}, true);
	mesh->BuildCell(0, {0, 1, 2, 3, 4});
	mesh->BuildCell(1, {2, 5, 6, 4, 3});
	mesh->BuildBoundaryPolygon({0, 1, 2, 5, 6, 4});
	ptree pt_tissue4;
	pt_tissue4.put_child("mesh", mesh->ToPtree());
	EditControlLogic logic4(pt_tissue4);
	nodes = logic4.GetNodes();

	EXPECT_TRUE(logic4.DeleteNode(nodes[3])) << "Delete node should be possible";
	EXPECT_FALSE(logic4.DeleteNode(nodes[2])) << "Delete node shouldn't be possible, more then two edges";
	EXPECT_FALSE(logic4.DeleteNode(nodes[4])) << "Delete node shouldn't be possible, more then two edges";
}

TEST_F(UnitLeafControlLogic, TestDeleteCell)
{
	//Test delete of a cell, when there are only two cells
	//Setup
	ptree tree;
	read_xml(g_path + "TestDeleteCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	auto logicmesh = logic.GetMesh()->GetMesh();
	vector<Cell*> cells = logic.GetCells();

	//Test
	int count = logicmesh->GetCells().size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(count,2) << "Cell count should be two";
	EXPECT_TRUE(logic.DeleteCell(cells[0])) << "Delete cell should be possible";
	count = logicmesh->GetCells().size();
	EXPECT_EQ(count,1) << "Cell count should be one";

	//Test delete of 3 cells, when there are at the start four cells
	//Setup
	read_xml(g_path + "TestDeleteCell2.xml",tree);
	EditControlLogic logic2(tree.get_child(g_xml_root_node));
	auto logicmesh2 = logic2.GetMesh()->GetMesh();
	cells = logic2.GetCells();

	//Test
	count = logicmesh2->GetCells().size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(count,4) << "Cell count should be four";
	EXPECT_TRUE(logic2.DeleteCell(cells[0])) << "Delete cell should be possible";
	count = logicmesh2->GetCells().size();
	EXPECT_EQ(count,3) << "Cell count should be three";
	cells = logic2.GetCells();
	EXPECT_TRUE(logic2.DeleteCell(cells[0])) << "Delete cell should be possible";
	count = logicmesh2->GetCells().size();
	EXPECT_EQ(count,2) << "Cell count should be two";
	cells = logic2.GetCells();
	EXPECT_TRUE(logic2.DeleteCell(cells[0])) << "Delete cell should be possible";
	count = logicmesh2->GetCells().size();
	EXPECT_EQ(count,1) << "Cell count should be one";
}

TEST_F(UnitLeafControlLogic, TestDeleteCells)
{
	//Test delete of 1 cell, when there are at the start two cells
	//Setup
	ptree tree;
	read_xml(g_path + "TestDeleteCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	auto logicmesh = logic.GetMesh()->GetMesh();
	vector<Cell*> cells = logic.GetCells();
	list<Cell*> cellList;
	cellList.push_back(cells[0]);

	//Test
	int count = logicmesh->GetCells().size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(count,2) << "Cell count should be two";
	logic.DeleteCells(cellList);
	count = logicmesh->GetCells().size();
	EXPECT_EQ(count,1) << "Cell count should be one";

	//Test delete of 3 cells, when there are at the start four cells
	//Setup
	read_xml(g_path + "TestDeleteCell2.xml",tree);
	EditControlLogic logic2(tree.get_child(g_xml_root_node));
	auto logicmesh2 = logic2.GetMesh()->GetMesh();
	cells = logic2.GetCells();
	cellList.clear();
	cellList.push_back(cells[0]);
	cellList.push_back(cells[1]);
	cellList.push_back(cells[3]);

	//Test
	count = logicmesh2->GetCells().size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(count,4) << "Cell count should be four";
	logic2.DeleteCells(cellList);
	count = logicmesh2->GetCells().size();
	EXPECT_EQ(count,1) << "Cell count should be one";
}

TEST_F(UnitLeafControlLogic, TestCreateCell)
{
	//Setup1
	ptree tree;
	read_xml(g_path + "TestCreateCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	auto mesh = logic.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();

	//Test1: with one cell, one cell has no walls
	int oldCount = mesh->GetCells().size();
	SimPT_Sim::Cell* cell = logic.CreateCell(nodes[0],nodes[1],
		QPoint(((*nodes[0])[0] * 2),((*nodes[0])[1] * 2)));
	int newCount = mesh->GetCells().size();
	EXPECT_EQ(oldCount+1, newCount) << "Cell count isn't correct";
	int nodeCount = cell->GetNodes().size();
	EXPECT_EQ(nodeCount, 3) << "Node count should be three";
	int wallCount = cell->GetWalls().size();
	EXPECT_EQ(wallCount, 2) << "Wall count should be two";

	//Setup2
	ptree tree2;
	read_xml(g_path + "TestCreateCell2.xml",tree2);
	EditControlLogic logic2(tree2.get_child(g_xml_root_node));
	auto mesh2 = logic2.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes2 = logic2.GetNodes();

	//Test2: with two cells, have walls already
	int oldCount2 = mesh2->GetCells().size();
	SimPT_Sim::Cell* cell2 = logic2.CreateCell(nodes2[0],nodes2[1],
		QPoint(((*nodes2[0])[0] * 2),((*nodes2[0])[1] * 2)));
	int newCount2 = mesh2->GetCells().size();
	EXPECT_EQ(oldCount2+1,newCount2) << "Cell count isn't correct";
	int nodeCount2 = cell2->GetNodes().size();
	EXPECT_EQ(nodeCount2, 3) << "Node count should be three";
	int wallCount2 = cell2->GetWalls().size();
	EXPECT_EQ(wallCount2, 2) << "Wall count should be two";
}

TEST_F(UnitLeafControlLogic, TestReplaceCell)
{
	//Setup1
	ptree tree;
	read_xml(g_path + "TestReplaceCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	auto mesh = logic.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();

	QPolygonF polygon1;
	polygon1.append(QPointF((*nodes[0])[0], (*nodes[0])[1]));
	polygon1.append(QPointF((*nodes[1])[0], (*nodes[1])[1]));
	polygon1.append(QPointF((*nodes[2])[0], (*nodes[2])[1]));
	polygon1.append(QPointF((*nodes[3])[0], (*nodes[3])[1]));

	list<QPolygonF> newCells;
	newCells.push_back(polygon1);
	vector<Cell*> cells = logic.GetCells();

	//Test1: originally there is one cell, we replace that by one new one (replace by itself)
	int oldCount = mesh->GetCells().size();
	logic.ReplaceCell(cells[0], newCells);
	int newCount = mesh->GetCells().size();
	EXPECT_EQ(oldCount, newCount) << "Cell count isn't correct";
	int nodeCount = mesh->GetNodes().size();
	EXPECT_EQ(nodeCount, 4) << "Node count should be four";
	int wallCount = mesh->GetWalls().size();
	EXPECT_EQ(wallCount, 0) << "Wall count should be zero";

	//Setup2
	ptree tree2;
	read_xml(g_path + "TestReplaceCell.xml",tree);
	EditControlLogic logic2(tree.get_child(g_xml_root_node));
	auto mesh2 = logic2.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes2 = logic2.GetNodes();

	QPolygonF polygon2;
	polygon2.append(QPointF((*nodes2[0])[0], (*nodes2[0])[1]));
	polygon2.append(QPointF((*nodes2[1])[0], (*nodes2[1])[1]));
	polygon2.append(QPointF((*nodes2[2])[0], (*nodes2[2])[1]));

	QPolygonF polygon3;
	polygon3.append(QPointF((*nodes2[0])[0], (*nodes2[0])[1]));
	polygon3.append(QPointF((*nodes2[2])[0], (*nodes2[2])[1]));
	polygon3.append(QPointF((*nodes2[3])[0], (*nodes2[3])[1]));

	list<QPolygonF> newCells2;
	newCells2.push_back(polygon2);
	newCells2.push_back(polygon3);
	vector<Cell*> cells2 = logic2.GetCells();

	//Test2: originally there is one cell, we replace that by two new ones
	int oldCount2 = mesh2->GetCells().size();
	logic2.ReplaceCell(cells2[0], newCells2);
	int newCount2 = mesh2->GetCells().size();
	EXPECT_EQ(oldCount2 + 1, newCount2) << "Cell count isn't correct";
	int nodeCount2 = mesh2->GetNodes().size();
	EXPECT_EQ(nodeCount2, 4) << "Node count should be four";
	int wallCount2 = mesh2->GetWalls().size();
	EXPECT_EQ(wallCount2, 3) << "Wall count should be three";

	//Setup3
	ptree tree3;
	read_xml(g_path + "TestReplaceCell2.xml",tree3);
	EditControlLogic logic3(tree3.get_child(g_xml_root_node));
	auto mesh3 = logic3.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes3 = logic3.GetNodes();

	QPolygonF polygon4;
	polygon4.append(QPointF((*nodes3[0])[0], (*nodes3[0])[1]));
	polygon4.append(QPointF((*nodes3[1])[0], (*nodes3[1])[1]));
	polygon4.append(QPointF((*nodes3[2])[0], (*nodes3[2])[1]));

	list<QPolygonF> newCells3;
	newCells3.push_back(polygon4);
	vector<Cell*> cells3 = logic3.GetCells();

	//Test3: originally there are two cells, we replace one of them by one new one (replace by itself)
	int oldCount3 = mesh3->GetCells().size();
	logic3.ReplaceCell(cells3[0], newCells3);
	int newCount3 = mesh3->GetCells().size();
	EXPECT_EQ(oldCount3, newCount3) << "Cell count isn't correct";
	int nodeCount3 = mesh3->GetNodes().size();
	EXPECT_EQ(nodeCount3, 4) << "Node count should be four";
	int wallCount3 = mesh3->GetWalls().size();
	EXPECT_EQ(wallCount3, 3) << "Wall count should be three";

	//Setup4
	ptree tree4;
	read_xml(g_path + "TestReplaceCell3.xml",tree4);
	EditControlLogic logic4(tree4.get_child(g_xml_root_node));
	auto mesh4 = logic4.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes4 = logic4.GetNodes();

	QPolygonF polygon5;
	polygon5.append(QPointF((*nodes4[2])[0], (*nodes4[2])[1]));
	polygon5.append(QPointF((*nodes4[0])[0], (*nodes4[0])[1]));
	polygon5.append(QPointF((*nodes4[4])[0], (*nodes4[4])[1]));

	QPolygonF polygon6;
	polygon6.append(QPointF((*nodes4[2])[0], (*nodes4[2])[1]));
	polygon6.append(QPointF((*nodes4[4])[0], (*nodes4[4])[1]));
	polygon6.append(QPointF((*nodes4[3])[0], (*nodes4[3])[1]));

	list<QPolygonF> newCells4;
	newCells4.push_back(polygon5);
	newCells4.push_back(polygon6);
	vector<Cell*> cells4 = logic4.GetCells();

	//Test4: originally there are two cells, we replace one of them by two new ones
	int oldCount4 = mesh4->GetCells().size();
	logic4.ReplaceCell(cells4[0], newCells4);
	int newCount4 = mesh4->GetCells().size();
	EXPECT_EQ(oldCount4 + 1, newCount4) << "Cell count isn't correct";
	int nodeCount4 = mesh4->GetNodes().size();
	EXPECT_EQ(nodeCount4, 5) << "Node count should be five";
	int wallCount4 = mesh4->GetWalls().size();
	EXPECT_EQ(wallCount4, 6) << "Wall count should be six";
}

TEST_F(UnitLeafControlLogic, TestFindWall)
{
	//Use one triangled cell
	//Setup
	ptree tree;
	read_xml(g_path + "TestFindWall.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	auto mesh = logic.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();
	vector<SimPT_Sim::Edge> edges;
	edges.push_back(SimPT_Sim::Edge(nodes[0],nodes[1]));
	edges.push_back(SimPT_Sim::Edge(nodes[1],nodes[2]));
	edges.push_back(SimPT_Sim::Edge(nodes[2],nodes[0]));

	//Test1: should return three times a nullptr
	Wall* wall1 = mesh->FindWall(edges[0]);
	Wall* wall2 = mesh->FindWall(edges[0]);
	Wall* wall3 = mesh->FindWall(edges[0]);
	int wallcount = mesh->GetWalls().size();
	EXPECT_EQ(wallcount, 0) << "Wrong wall count";
	EXPECT_EQ(wall1, nullptr) << "Wall shouldn't exist";
	EXPECT_EQ(wall2, nullptr) << "Wall shouldn't exist";
	EXPECT_EQ(wall3, nullptr) << "Wall shouldn't exist";

	//Use two cells
	//Setup2
	ptree tree2;
	read_xml(g_path + "TestFindWall2.xml",tree2);
	EditControlLogic logic2(tree2.get_child(g_xml_root_node));
	auto mesh2 = logic2.GetMesh()->GetMesh();
	vector<SimPT_Sim::Node*> nodes2 = logic2.GetNodes();
	vector<SimPT_Sim::Edge> edges2;
	edges2.push_back(SimPT_Sim::Edge(nodes2[0],nodes2[1]));
	edges2.push_back(SimPT_Sim::Edge(nodes2[1],nodes2[3]));
	edges2.push_back(SimPT_Sim::Edge(nodes2[3],nodes2[0]));
	edges2.push_back(SimPT_Sim::Edge(nodes2[2],nodes2[3]));
	edges2.push_back(SimPT_Sim::Edge(nodes2[0],nodes2[2]));
	vector<SimPT_Sim::Wall*> wallvec(logic2.GetWalls());

	//Test2: should give the correct walls
	Wall* wall4 = mesh2->FindWall(edges2[0]);
	Wall* wall5 = mesh2->FindWall(edges2[1]);
	Wall* wall6 = mesh2->FindWall(edges2[2]);
	Wall* wall7 = mesh2->FindWall(edges2[3]);
	Wall* wall8 = mesh2->FindWall(edges2[4]);
	int wallcount2 = mesh2->GetWalls().size();
	EXPECT_EQ(wallcount2, 3) << "Wrong wall count";
	EXPECT_TRUE(find_if(wallvec.begin(), wallvec.end(), [wall4](Wall* w)
				{return wall4 == w;}) != wallvec.end()) << "Wall should be found";
	EXPECT_TRUE(find_if(wallvec.begin(), wallvec.end(), [wall5](Wall* w)
				{return wall5 == w;}) != wallvec.end()) << "Wall should be found";
	EXPECT_TRUE(find_if(wallvec.begin(), wallvec.end(), [wall6](Wall* w)
				{return wall6 == w;}) != wallvec.end()) << "Wall should be found";
	EXPECT_TRUE(find_if(wallvec.begin(), wallvec.end(), [wall7](Wall* w)
				{return wall7 == w;}) != wallvec.end()) << "Wall should be found";
	EXPECT_TRUE(find_if(wallvec.begin(), wallvec.end(), [wall8](Wall* w)
				{return wall8 == w;}) != wallvec.end()) << "Wall should be found";
}

TEST_F(UnitLeafControlLogic, TestSplitEdge)
{
	//Use one squared cell
	//Setup
	ptree tree;
	read_xml(g_path + "TestSplitEdge.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();
	vector<SimPT_Sim::Edge> edges;
	edges.push_back(SimPT_Sim::Edge(nodes[0],nodes[1]));
	edges.push_back(SimPT_Sim::Edge(nodes[1],nodes[2]));
	edges.push_back(SimPT_Sim::Edge(nodes[2],nodes[3]));
	edges.push_back(SimPT_Sim::Edge(nodes[3],nodes[0]));

	//Test: we expect that the new node is in the middle of the given edge
	//Edge1
	Node* node = logic.SplitEdge(edges[0]);
	EXPECT_EQ((*node)[0], (((*edges[0].GetFirst())[0] + (*edges[0].GetSecond())[0])/2))
							<< "Created node is at wrong position";
	EXPECT_EQ((*node)[1], (((*edges[0].GetFirst())[1] + (*edges[0].GetSecond())[1])/2))
							<< "Created node is at wrong position";

	//Edge2
	node = logic.SplitEdge(edges[1]);
	EXPECT_EQ((*node)[0], (((*edges[1].GetFirst())[0] + (*edges[1].GetSecond())[0])/2))
							<< "Created node is at wrong position";
	EXPECT_EQ((*node)[1], (((*edges[1].GetFirst())[1] + (*edges[1].GetSecond())[1])/2))
							<< "Created node is at wrong position";

	//Edge3
	node = logic.SplitEdge(edges[2]);
	EXPECT_EQ((*node)[0], (((*edges[2].GetFirst())[0] + (*edges[2].GetSecond())[0])/2))
							<< "Created node is at wrong position";
	EXPECT_EQ((*node)[1], (((*edges[2].GetFirst())[1] + (*edges[2].GetSecond())[1])/2))
							<< "Created node is at wrong position";

	//Edge4
	node = logic.SplitEdge(edges[3]);
	EXPECT_EQ((*node)[0], (((*edges[3].GetFirst())[0] + (*edges[3].GetSecond())[0])/2))
							<< "Created node is at wrong position";
	EXPECT_EQ((*node)[1], (((*edges[3].GetFirst())[1] + (*edges[3].GetSecond())[1])/2))
							<< "Created node is at wrong position";
}

TEST_F(UnitLeafControlLogic, TestSplitCell)
{
	//Use one squared cell
	//Setup1
	ptree tree;
	read_xml(g_path + "TestSplitCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();
	vector<SimPT_Sim::Cell*> cells = logic.GetCells();

	//Test were we use nodes that are on a diagonal with each other
	vector<SimPT_Sim::Cell*> newCells = logic.SplitCell(cells[0],nodes[0],nodes[2]);
	int size = newCells.size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(size,2)  << "Cell count should be two";

	//Same setup
	EditControlLogic logic2(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes2 = logic2.GetNodes();
	vector<SimPT_Sim::Cell*> cells2 = logic2.GetCells();

	//Test were we use nodes that are on a diagonal with each other (other diagonal)
	vector<SimPT_Sim::Cell*> newCells2 = logic2.SplitCell(cells2[0],nodes2[1],nodes2[3]);
	int size2 = newCells2.size();
	EXPECT_EQ(size2,2) << "Cell count should be two";

	//Setup2
	EditControlLogic logic3(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes3 = logic3.GetNodes();
	vector<SimPT_Sim::Cell*> cells3 = logic3.GetCells();

	//Test were we use nodes that are neighbors of each other
	vector<SimPT_Sim::Cell*> newCells3 = logic3.SplitCell(cells3[0],nodes3[0],nodes3[1]);
	int size3 = newCells3.size();
	EXPECT_EQ(size3,0) << "Cell count should be zero";

	//Same setup
	EditControlLogic logic4(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes4 = logic4.GetNodes();
	vector<SimPT_Sim::Cell*> cells4 = logic4.GetCells();

	//Test were we use nodes that are neighbors of each other (other ones)
	vector<SimPT_Sim::Cell*> newCells4 = logic4.SplitCell(cells4[0],nodes4[0],nodes4[3]);
	int size4 = newCells4.size();
	EXPECT_EQ(size4,0) << "Cell count should be zero";
}

TEST_F(UnitLeafControlLogic, TestSplitCellWithoutGivenCell)
{
	//Use one squared cell
	//Setup1
	ptree tree;
	read_xml(g_path + "TestSplitCell.xml",tree);
	EditControlLogic logic(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes = logic.GetNodes();

	//Test were we use nodes that are on a diagonal with each other
	vector<SimPT_Sim::Cell*> newCells = logic.SplitCell(nodes[0],nodes[2]);
	int size = newCells.size(); //Assign to int, otherwise you compare unsigned vs signed int
	EXPECT_EQ(size,2)  << "Cell count should be two";

	//Same setup
	EditControlLogic logic2(tree.get_child(g_xml_root_node));
	vector<SimPT_Sim::Node*> nodes2 = logic2.GetNodes();

	//Test were we use nodes that are on a diagonal with each other (other diagonal)
	vector<SimPT_Sim::Cell*> newCells2 = logic2.SplitCell(nodes2[1],nodes2[3]);
	int size2 = newCells2.size();
	EXPECT_EQ(size2,2) << "Cell count should be two";
}

} // namespace
} // namespace
