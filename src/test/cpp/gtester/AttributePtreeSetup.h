#ifndef ATTRIBUTE_PTREE_H_INCLUDED
#define ATTRIBUTE_PTREE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface and implementation of AttributePtreeSetup.
 */

#include "util/misc/XmlWriterSettings.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <ostream>
#include <string>

namespace SimPT_Sim {

/**
 * Utility class to deal with attribute property tree (ptree's).
  @see AttributeStore constructor for attribute property tree format.
 */
class AttributePtreeSetup
{
public:
	/// Creates an empty index.
	AttributePtreeSetup() = default;

	/// Adds an attribute of type, name and default value.
	template<typename T>
	void Add(const std::string& name, const T& defaultvalue);

	/// Return ptree.
	boost::property_tree::ptree Get() const;

private:
	boost::property_tree::ptree m_pt;
};

/// Insertion into output stream.
inline std::ostream& operator<<(std::ostream& os, const AttributePtreeSetup& pt);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation.
/////////////////////////////////////////////////////////////////////////////////////////////////////

template<>
inline void AttributePtreeSetup::Add<bool>(const std::string& name, const bool& value)
{
	boost::property_tree::ptree child;
	child.put<std::string>("name", name);
	child.put<std::string>("type", "bool");
	child.put<bool>("default", value);
	m_pt.add_child("attribute", child);
}

template<>
inline void AttributePtreeSetup::Add<int>(const std::string& name, const int& value)
{
	boost::property_tree::ptree child;
	child.put<std::string>("name", name);
	child.put<std::string>("type", "int");
	child.put<int>("default", value);
	m_pt.add_child("attribute", child);
}

template<>
inline void AttributePtreeSetup::Add<double>(const std::string& name, const double& value)
{
	boost::property_tree::ptree child;
	child.put<std::string>("name", name);
	child.put<std::string>("type", "double");
	child.put<double>("default", value);
	m_pt.add_child("attribute", child);
}

template<>
inline void AttributePtreeSetup::Add<std::string>(const std::string& name, const std::string& value)
{
	boost::property_tree::ptree child;
	child.put<std::string>("name", name);
	child.put<std::string>("type", "string");
	child.put<std::string>("default", value);
	m_pt.add_child("attribute", child);
}

inline boost::property_tree::ptree AttributePtreeSetup::Get() const
{
	return m_pt;
}

inline std::ostream& operator<<(std::ostream& os, const AttributePtreeSetup& apt)
{
	boost::property_tree::xml_parser::write_xml(os, apt.Get(), SimPT_Sim::Util::XmlWriterSettings::GetTab());
	return os;
}

} // namespace

#endif // include guard

