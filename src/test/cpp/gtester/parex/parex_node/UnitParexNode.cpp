/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for the worker nodes.
 */

#include "MockServer.h"
#include "MockSimulator.h"
#include "parex_node/WorkerNode.h"

#include <gtest/gtest.h>
#include <QEventLoop>
#include <QThread>
#include <QTimer>

#include <iostream>
#include <memory>

namespace SimPT_Parex {
namespace Tests {

using namespace std;

class UnitParexNode: public ::testing::Test
{
protected:
	/// Set up for the test fixture
	virtual void SetUp()
	{
		m_server = new MockServer(nullptr);
		ASSERT_EQ(true, m_server->SetUp())<< "SetUp failed, we can't continue testing...";
		m_sim = new MockSimulator();
		m_node = new WorkerNode(m_sim, false);
	}

	/// Tearing down the test fixture
	virtual void TearDown()
	{
		delete m_node;
		delete m_server;
	}

	MockServer*                m_server;
	MockSimulator*             m_sim;
	WorkerNode*                m_node;
	QEventLoop                 m_event_loop;
};

TEST_F( UnitParexNode, FindServerBroadcast )
{
	m_server->setCurrentTest(stests::broadcast);
	QTimer::singleShot(2 * 1000, &m_event_loop, SLOT(quit()));
	QTimer::connect(m_server, SIGNAL(Done()), &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_server->TestSucceeded())<< "Connection timed out.";
}

TEST_F( UnitParexNode, AcceptTcp )
{
	m_server->setCurrentTest(stests::acceptTcp);
	QTimer::singleShot(2 * 1000, &m_event_loop, SLOT(quit()));
	QTimer::connect(m_server, SIGNAL(Done()), &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_server->TestSucceeded())<< "Node didn't accept incoming TCP connection";
}

TEST_F( UnitParexNode, StartSimulation)
{
	m_server->setCurrentTest(stests::startSim);
	m_sim->setCurrentTest(stests::startSim);
	QTimer::singleShot(2 * 1000, &m_event_loop, SLOT(quit()));
	QTimer::connect(m_sim, SIGNAL(Done()), &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_sim->TestSucceeded())<< "Node didn't start the simulation";
}

} // namespace
} // namespace
