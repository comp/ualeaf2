#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#  Report config info.
#
#############################################################################

#============================================================================
# Overview report:
#============================================================================
message( STATUS " " )
message( STATUS " " )
message( STATUS "------> CMAKE_SYSTEM                : ${CMAKE_SYSTEM} "           )
message( STATUS "------> CMAKE_SYSTEM_VERSION        : ${CMAKE_SYSTEM_VERSION} "   )
message( STATUS "------> CMAKE_SYSTEM_PROCESSOR      : ${CMAKE_SYSTEM_PROCESSOR} " )
message( STATUS "------> CMAKE_VERSION               : ${CMAKE_VERSION} "          )
message( STATUS "------> CMAKE_PREFIX_PATH           : ${CMAKE_PREFIX_PATH} "      )
message( STATUS "------> ENVIRONMANT PATH            : $ENV{PATH} "                )
message( STATUS "------> SIMPT_PARALLEL_MAKE         : ${SIMPT_PARALLEL_MAKE} "    )
message( STATUS "------> SIMPT_VERBOSE_TESTING       : ${SIMPT_VERBOSE_TESTING} "  )
message( STATUS ""  )
if ( GIT_FOUND )
	message( STATUS "------> Git revision id: ${SIMPT_WC_REVISION_HASH} "       )
	message( STATUS "------> Git commit date: ${SIMPT_WC_LAST_CHANGED_DATE} "   )
endif()
message( STATUS " " )
message( STATUS "------> SIMPT_MAKE_JAVA_WRAPPER     : ${SIMPT_MAKE_JAVA_WRAPPER} "   )
message( STATUS "------> SIMPT_MAKE_PYTHON_WRAPPER   : ${SIMPT_MAKE_PYTHON_WRAPPER} " )
message( STATUS "------> SIMPT_MAKE_DOC              : ${SIMPT_MAKE_DOC} "            )
message( STATUS ""        )
message( STATUS "------> SIMPT_FORCE_NO_HDF5         : ${SIMPT_FORCE_NO_HDF5}"        )
message( STATUS "------> SIMPT_FORCE_NO_OPENMP       : ${SIMPT_FORCE_NO_OPENMP}"      )
message( STATUS "------> SIMPT_FORCE_NO_BOOST_GZIP   : ${SIMPT_FORCE_NO_BOOST_GZIP}"  )
message( STATUS "------> SIMPT_FORCE_NO_ZLIB         : ${SIMPT_FORCE_NO_ZLIB}"        )
#
message( STATUS " " )
message( STATUS "------> CMAKE_BUILD_TYPE            : ${CMAKE_BUILD_TYPE} "          )
message( STATUS "------> CMAKE_C_COMPILER            : ${CMAKE_C_COMPILER}"           )
message( STATUS "------> CMAKE_C_COMPILER_ID         : ${CMAKE_C_COMPILER_ID}"        )
message( STATUS "------> CMAKE_CXX_COMPILER          : ${CMAKE_CXX_COMPILER}"         )
message( STATUS "------> CMAKE_CXX_COMPILER_ID       : ${CMAKE_CXX_COMPILER_ID}"      )
message( STATUS "------> CMAKE_CXX_COMPILER_VERSION  : ${CMAKE_CXX_COMPILER_VERSION}" )
message( STATUS "------> CMAKE_CXX_FLAGS             : ${CMAKE_CXX_FLAGS}"            )
if( CMAKE_BUILD_TYPE MATCHES "^[Rr]elease$" )
	message( STATUS "------> CMAKE_CXX_FLAGS_RELEASE     : ${CMAKE_CXX_FLAGS_RELEASE}")
endif()
if( CMAKE_BUILD_TYPE MATCHES "^[Dd]ebug$" )
	message( STATUS "------> CMAKE_CXX_FLAGS_DEBUG       : ${CMAKE_CXX_FLAGS_DEBUG}"  )
endif()
message( STATUS " " )
#
message( STATUS "------> CMAKE_BINARY_DIR            : ${CMAKE_BINARY_DIR} "          )
message( STATUS "------> CMAKE_INSTALL_PREFIX        : ${CMAKE_INSTALL_PREFIX} "      )
message( STATUS " " )
#
message( STATUS "------> SIMPT_USES_QT_VERSION       : ${SIMPT_QT_VERSION} "         )
message( STATUS "------> QT_INCLUDE_DIR              : ${QT_INCLUDE_DIR} "            )
message( STATUS "------> QT_LIBRARIES                : ${QT_LIBRARIES} "              )
message( STATUS " " )
#
message( STATUS "------> Boost_VERSION               : ${Boost_VERSION} "             )
message( STATUS "------> Boost_INCLUDE_DIRS          : ${Boost_INCLUDE_DIRS} "        )
message( STATUS "------> Boost_Libraries             : ${Boost_Libraries} "           )
message( STATUS " " )
#
if ( SIMPT_FORCE_NO_BOOST_GZIP )
	message( STATUS "------> SIMPT_FORCE_NO_BOOST_GZIP   : ${SIMPT_FORCE_NO_BOOST_GZIP}"  )
else()
	message( STATUS "------> Boost_IOSTREAMS_FOUND       : ${Boost_IOSTREAMS_FOUND}"   )
	message( STATUS "------> Boost_IOSTREAMS_LIBRARY     : ${Boost_IOSTREAMS_LIBRARY}" )
	message( STATUS "------> Boost_WITH_GZIP             : ${Boost_WITH_GZIP} "        )
endif()
#
if( NOT Boost_WITH_GZIP AND SIMPT_FORCE_NO_ZLIB )
	message( STATUS "------> SIMPT_FORCE_NO_ZLIB         : ${SIMPT_FORCE_NO_ZLIB}"        )
endif()
if (NOT Boost_WITH_GZIP AND NOT SIMPT_FORCE_NO_ZLIB )
	message( STATUS "------> ZLIB_FOUND                  : ${ZLIB_FOUND}"              )
	message( STATUS "------> ZLIB_INCLUDE_DIRS           : ${ZLIB_INCLUDE_DIRS} "      )
	message( STATUS "------> ZLIB_LIBRARIES              : ${ZLIB_LIBRARIES}"          )
endif()
message( STATUS " " )
#
if( SIMPT_FORCE_NO_HDF5 )
	message( STATUS "------> SIMPT_FORCE_NO_HDF5         : ${SIMPT_FORCE_NO_HDF5}"        )
else()
	message( STATUS "------> HDF5_FOUND                  : ${HDF5_FOUND} "             )
	if( HDF5_FOUND ) 
		message( STATUS "------> HDF5_INCLUDE_DIRS           : ${HDF5_INCLUDE_DIRS} "  )
		message( STATUS "------> HDF5_LIBRARIES              : ${HDF5_LIBRARIES} "     )
	endif()
endif()
message( STATUS " " )
#
if( SIMPT_FORCE_NO_OPENMP )
	message( STATUS "------> SIMPT_FORCE_NO_OPENMP       : ${SIMPT_FORCE_NO_OPENMP}"      )
else()
	message( STATUS "------> HAVE_CHECKED_OpenMP         : ${HAVE_CHECKED_OpenMP}"     )
	message( STATUS "------> HAVE_FOUND_OpenMP           : ${HAVE_FOUND_OpenMP}"       )
	message( STATUS "------> OpenMP_C_FLAGS              : ${OpenMP_C_FLAGS}"          )
	message( STATUS "------> OpenMP_CXX_FLAGS            : ${OpenMP_CXX_FLAGS} "       )
endif()
message( STATUS " " )
#
if( SIMPT_MAKE_JAVA_WRAPPER OR SIMPT_MAKE_PYTHON_WRAPPER )
	message( STATUS "------> SWIG_EXECUTABLE             : ${SWIG_EXECUTABLE} "        )
	message( STATUS "------> SWIG_VERSION                : ${SWIG_VERSION} "           )
	message( STATUS " " )
endif()
#
if ( SIMPT_MAKE_JAVA_WRAPPER )
	message( STATUS "------> Java_JAVA_EXECUTABLE        : ${Java_JAVA_EXECUTABLE} "   )
	message( STATUS "------> Java_JAVAC_EXECUTABLE       : ${Java_JAVAC_EXECUTABLE} "  )
    message( STATUS "------> Java_VERSION_STRING         : ${Java_VERSION_STRING} "    )
	message( STATUS " " )
endif()
#
if ( SIMPT_MAKE_PYTHON_WRAPPER )
	message( STATUS "------> PYTHON_EXECUTABLE           : ${PYTHON_EXECUTABLE} "      )
	message( STATUS "------> PYTHON_VERSION_STRING       : ${PYTHON_VERSION_STRING} "  )
	message( STATUS "------> PYTHON_PREFIX               : ${PYTHON_PREFIX} "          )
	message( STATUS "------> PYTHON_INCLUDE_DIRS         : ${PYTHON_INCLUDE_DIRS} "    )
	message( STATUS "------> PYTHON_LIBRARIES            : ${PYTHON_LIBRARIES} "       )
	message( STATUS "------> PYTHON_SITE_PACKAGES        : ${PYTHON_SITE_PACKAGES} "   )
	message( STATUS "------> PYTHON_VERSION_STRING       : ${PYTHON_VERSION_STRING} "  )
	message( STATUS " " )
endif()
#
if ( SIMPT_MAKE_DOC )
	message( STATUS "------> DOXYGEN_FOUND               : ${DOXYGEN_FOUND} "              )
	message( STATUS "------> DOXYGEN_DOT_FOUND           : ${DOXYGEN_DOT_FOUND} "          )
	if ( DOXYGEN_FOUND )
		message( STATUS "------> DOXYGEN_EXECUTABLE          : ${DOXYGEN_EXECUTABLE} "     )
		message( STATUS "------> DOXYGEN_VERSION             : ${DOXYGEN_VERSION} "        )
	endif()
	if ( DOXYGEN_DOT_FOUND )
		message( STATUS "------> DOXYGEN_DOT_EXECUTABLE      : ${DOXYGEN_DOT_EXECUTABLE} " )
	endif()
	message( STATUS " " )
	message( STATUS "------> LATEX_FOUND                 : ${LATEX_FOUND} "                )
	if ( LATEX_FOUND )
		message( STATUS "------> LATEX_COMPILER              : ${LATEX_COMPILER} "         )
		message( STATUS "------> PDFLATEX_COMPILER           : ${PDFLATEX_COMPILER} "      )
	endif()
endif()
message( STATUS "" )
message( STATUS "Report complete." )

#############################################################################
