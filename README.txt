#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

"Simulator for Plant Tissue" a.k.a "simPT" is an Open Source framework 
for cell-based modeling of plant tissue growth and development. 

Information on directory layout, installation, dependencies of the project, 
platforms that are supported, and so on, can be found in the files 
INSTALL.txt, DEPENDENCIES.txt and PLATFORMS.txt respectively.
For license info and a list of authors, see LICENSE.txt and AUTHORS.txt.

#############################################################################
